#------------------------------------------------------------------------------#
# Script to compile Elvira with Infiniband
#-------------------------- ELVIRA make include file---------------------------#
#------------------------------------------------------------------------------#

# Default output mode
COMP=intel
MPI=infiniband

SELF_DIR := $(dir $(realpath $(lastword $(MAKEFILE_LIST))))

ELVIRA = $(SELF_DIR)
ELVIRALIBS = $(ELVIRA)../elvira_libs
##
## Name of the executable
## If you want to change the defalut executable name comment the following
## line and substitute accordingly
##
EXECNAME=mainelv_$(MPI)_$(COMP)
##
## System Libraries
##
SISLIB=/usr/lib
##
##
## Mathematic Libraries Directory (BLAS, METIS, PSBLAS)
## If you have a partiuclar directory, comment the following line and
## and substitute correspondingly
##
LIBM  =$(ELVIRALIBS)/libs_$(MPI)_$(COMP)
##
## PSBLAS LIBRARIES
##
## Compilation directives
LIBPSB=-I$(LIBM)
## Linking directives
PSBLIB= -L$(LIBM) -lpsb_util -lpsb_krylov -lpsb_prec -lpsb_base
##
# BLAS LIBRARY
##
BLAS  =  $(LIBM)/blas_LINUX.a
## If your system has installed an optimized version of BLAS libraries
## you will to update this variable accordingly. We gave some examples
##
## OSx system with developers tools installed
#BLAS= -lblas -llapack
## Blue gene system with IBM compiler (FERMI supercomputer)
#BLAS=/opt/ibmmath/essl/5.1/lib64/libesslbg.a /opt/ibmmath/essl/5.1/lib64/libesslsmpbg.a
##
## METIS LIBRARY
LMETIS=  -L$(LIBM) -lmetis 
##
##
## ELECTRIC LIBRARIES
##
HPREC =$(ELVIRA)/prcsn
LIB   =$(ELVIRA)/lib
H_MEL =$(ELVIRA)/em_src
H_SRC =$(ELVIRA)/src
HCRR  =$(H_MEL)/carro
HCOURT=$(H_MEL)/courtemanche
HMC   =$(H_MEL)/FIBmaccannell
HFK   =$(H_MEL)/FK
HGR   =$(H_MEL)/grandi
HGRHF =$(H_MEL)/grandiHF
HIZ   =$(H_MEL)/IZ
HLR2K =$(H_MEL)/lr2k
HMLK  =$(H_MEL)/maleckar
HNYGR =$(H_MEL)/nygren
HOHR  =$(H_MEL)/ohara
HPKFST=$(H_MEL)/PKFstewart
HRYMC =$(H_MEL)/rmcl
HTENT =$(H_MEL)/tentusscher
##
LIBME = -L$(LIB) -llr2k -lnygren -lrmc -ltentusscher -lFK -lIZ \
    -lpkfstewart -lgrandi -lcarro -lgrandiHF -lmaleckar -lohara \
    -lMacCannell -lcourtemanche
##
## Compilers
##
#------------ Intel ------------------------------------------------------------
ifeq ($(COMP),intel)
 F95   = mpif90
 MODU= -module $(ELVIRA)/mod
 LDLIBS= $(PSBLIB) $(LIBME) $(BLAS) $(LMETIS) -shared-intel
 COM = -O3 -align all -inline-factor=2  -heap-arrays
#COM = -g -O0 -warn all -check bounds -heap-arrays -p -traceback 
endif

ifeq ($(COMP),gcc)
 F95   = mpif90
 MODU= -J$(ELVIRA)/mod
 LDLIBS= $(PSBLIB) $(LIBME) $(BLAS) $(LMETIS)
 COM = -O3 -ffree-line-length-none -fconserve-stack
endif

ifeq ($(COMP),gccOsx)
 F95   = mpif90
 MODU= -J$(ELVIRA)/mod
 LDLIBS= $(PSBLIB) $(LIBME) $(BLAS) $(LMETIS)
 COM = -O3 -ffree-line-length-none -fconserve-stack
endif

ifeq ($(COMP),ibm)
 MODU= -I$(ELVIRA)/mod -qmoddir=$(ELVIRA)/mod
 LDLIBS=  $(PSBLIB) $(LIBME) $(BLAS) $(LMETIS) -qnostaticlink
 F95   = mpixlf90_r
 COM = -O3  -qsmallstack=dynlenonheap
endif
	
#
# This option is useful for debug
#
#COM = -g -O0 -warn all -check bounds -heap-arrays -p -traceback 
#------------- gcc compiler ----------------------------------------------------
#F95   = mpif90 
#MODU= -J$(ELVIRA)/mod
#LDLIBS= $(PSBLIB) $(LIBME) $(BLAS) $(LMETIS)
#COM = -O3 -ffree-line-length-none -fconserve-stack
#COM = -O3 -ffree-line-length-none -fconserve-stack -march=native
#------------- IBM compiler ----------------------------------------------------
#MODU= -I$(ELVIRA)/mod -qmoddir=$(ELVIRA)/mod
#DLIBS=  $(PSBLIB) $(LIBME) $(BLAS) $(LMETIS) -qnostaticlink
#F95   = mpixlf90_r
#COM = -O3  -qsmallstack=dynlenonheap

