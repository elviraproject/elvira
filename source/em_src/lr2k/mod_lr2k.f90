!     Last change:  EAH  27 Jan 2007    3:29 pm
!     Last change:  JFR  07 Feb 2009    -----
!     30-11-2009: LR2K_A_P01 returns currents vector for output (EAH)
! ------------------------------------------------------------------------------
module mod_LR2K
! ------------------------------------------------------------------------------
  use mod_precision
  implicit none
  include 'lr2k_parameters.inc'
! ------------------------------------------------------------------------------
 
  type, private :: t_curr  
    real(rp) :: ina      ! Fast Na Current (uA/uF)                      [Fast Sodium Current (time dependant)]
    real(rp) :: ilca     ! Ca current through L-type Ca channel (uA/uF) [Current through L-type Ca Channel]
    real(rp) :: ilcana   ! Na current through L-type Ca channel (uA/uF) [Current through L-type Ca Channel]
    real(rp) :: ilcak    ! K current through L-type Ca channel (uA/uF)  [Current through L-type Ca Channel]
    real(rp) :: icat     ! Ca current through T-type Ca channel (uA/uF) [Current through T-type Ca Channel]
    real(rp) :: ikr      ! Rapidly Activating K Current (uA/uF)         [Rapidly Activating Potassium Current]
    real(rp) :: iks      ! Slowly Activating K Current (uA/uF)          [Slowly Activating Potassium Current]
    real(rp) :: iki      ! Time-independant K current (uA/uF)           [Potassium Current (time-independant)]
    real(rp) :: ikp      ! Plateau K current (uA/uF)                    [Plateau Potassium Current]
    real(rp) :: ikna     ! Na activated K channel                       [Na-Activated K Channel]
    real(rp) :: ikatp    ! ATP-sensitive K current (uA/uF)              [ATP-Sensitive K Channel]
    real(rp) :: ito      ! Transient outward current                    [Ito Transient Outward Current]
    real(rp) :: inaca    ! NaCa exchanger current (uA/uF)               [Sodium-Calcium Exchanger V-S ]
    real(rp) :: inak     ! NaK pump current (uA/uF)                     [Sodium-Potassium Pump]
    real(rp) :: insna    ! Non-specific Na current (uA/uF)              [Nonspecific Ca-activated Current]
    real(rp) :: insk     ! Non-specific K current (uA/uF)               [Nonspecific Ca-activated Current]
    real(rp) :: ipca     ! Sarcolemmal Ca pump current (uA/uF)          [Sarcolemmal Ca Pump]
    real(rp) :: icab     ! Ca background current (uA/uF)                [Ca Background Current]
    real(rp) :: inab     ! Na background current (uA/uF)                [Na Background Current]
    real(rp) :: naiont   ! Total Na Ion Flow (uA/uF)                    [Myoplasmic Na Ion Concentration Changes]
    real(rp) :: kiont    ! Total K Ion Flow (uA/uF)                     [Myoplasmic K Ion Concentration Changes]
    real(rp) :: caiont   ! Total Ca Ion Flow (uA/uF)                    [Myoplasmic Ca Ion Concentration Changes]
    real(rp) :: dcaiontnew ! New rate of change of Ca entry             [JSR Ca Ion Concentration Changes]
    real(rp) :: it       ! Total Current
 end type t_curr

! ------------------------------------------------------------------------------
! 
! ------------------------------------------------------------------------------
  type t_gate
    real(rp) :: m     !.Na activation   (Fast Sodium Current)
    real(rp) :: h     !.Na inactivation (Fast Sodium Current)
    real(rp) :: j     !.Na inactivation (Fast Sodium Current)
    real(rp) :: d     !.Voltage dependant activation gate   (L-type Ca Channel)
    real(rp) :: f     !.Voltage dependant inactivation gate (L-type Ca Channel)
    real(rp) :: xs1   !.Slowly Activating K time-dependant activation (Slowly Activating Potassium)
    real(rp) :: xs2   !.Slowly Activating K time-dependant activation (Slowly Activating Potassium)
    real(rp) :: xr    !.Rapidly Activating K time-dependant activation (Rapidly Activating Potassium)
    real(rp) :: b     !.Voltage dependant activation gate   (T-type Ca Channel)
    real(rp) :: g     !.Voltage dependant inactivation gate (T-type Ca Channel)
    real(rp) :: zdv   !.Ito activation  (Ito Transient Outward Current)
    real(rp) :: ydv   !.Ito inactivation (Ito Transient Outward Current)
  end type t_gate
! ------------------------------------------------------------------------------
  type t_conc
    real(rp) ::nai    !.Intracellular Na Concentration (mM)  [Ion Concentrations]
    real(rp) ::nabm   !.Bulk Medium Na Concentration (mM)    [Ion Concentrations]
    real(rp) ::ki     !.Intracellular K Concentration (mM)   [Ion Concentrations]
    real(rp) ::kbm    !.Bulk Medium K Concentration (mM)     [Ion Concentrations]
    real(rp) ::cai    !.Intracellular Ca Concentration (mM)  [Ion Concentrations]
    real(rp) ::cabm   !.Bulk Medium Ca Concentration (mM)    [Ion Concentrations]
    real(rp) ::jsr    !.NSR Ca Concentration (mM)            [Ion Concentrations]
    real(rp) ::nsr    !.JSR Ca Concentration (mM)            [Ion Concentrations]
!    real(rp) ::nao    !.Extracellular Na Concentration (mM)  [Ion Concentrations]
!    real(rp) ::ko     !.Extracellular Na Concentration (mM)  [Ion Concentrations]
!    real(rp) ::cao    !.Extracellular Na Concentration (mM)  [Ion Concentrations]
  end type t_conc
! ------------------------------------------------------------------------------
  type t_time
    real(rp) :: tjsrol   !.t=0 at time of JSR overload (ms)
    real(rp) :: tcicr    !.t=0 at time of CICR (ms)
  end type t_time
! ------------------------------------------------------------------------------
  type t_var
    real(rp)    :: dcaiont      !.Rate of change of Ca entry                      [JSR Ca Ion Concentration Changes]
    real(rp)    :: caiontold    !.Old rate of change of Ca entry                  [JSR Ca Ion Concentration Changes]
    real(rp)    :: irelcicr     !.Ca release from JSR to myo. due to CICR (mM/ms) [JSR Ca Ion Concentration Changes]
    real(rp)    :: grelbarjsrol !.Rate constant of Ca release from JSR due to overload (ms^-1)
    integer(ip) :: flag         !. Flag condition to test for dvdtmax
  end type t_var
! ------------------------------------------------------------------------------
  type t_lr
    type(t_gate) :: tgate
    type(t_conc) :: tconc
    type(t_time) :: ttime
    type(t_var)  :: tvari
  end type t_lr
! ------------------------------------------------------------------------------
! Parameters that can be modified through a Mask in the data file
! ------------------------------------------------------------------------------
  type, public:: t_prm
    private
    real(rp) :: A_gna        ! 1  Max. Conductance of the Na Channel (mS/uF)
    real(rp) :: A_PCaL       ! 2  ICaL channel conduction reduction factor
    real(rp) :: A_gcat       ! 3  Max. Conductance of T Ca Channel (nS/uF)
    real(rp) :: A_gkrm       ! 4 Max. Conductance of IKr (nS/uF)
    real(rp) :: A_gksm       ! 5  Max. Conductance of IKs (nS/uF)
    real(rp) :: A_gkim       ! 6  Max. Conductance of IK1 (nS/uF)
    real(rp) :: A_gkp        ! 7  Max. Conductance of IKp (nS/uF)
    real(rp) :: A_gkna       ! 8  Maximum conductance of IKNa (mS/uF)
    real(rp) :: A_IKatp      ! 9  Max. conductance of IKATP
    real(rp) :: A_gitodv     ! 10 Max conductance of Ito
    real(rp) :: A_c1         ! 11 Scaling factor for inaca (uA/uF)
    real(rp) :: A_ibarnak    ! 12 Max. current through Na-K pump (uA/uF)
    real(rp) :: A_ibarpca    ! 13 Max. Ca current through sarcolemmal Ca pump (uA/uF)
    real(rp) :: A_gcab       ! 14 Max conductance of Ca background current
    real(rp) :: A_gnab       ! 15 Max conductance of Na background current (In the paper is 0.00141)
    real(rp) :: A_iupbar     ! 16 Max. current through iup channel (mM/ms)
    real(rp) :: p_nao        ! 17 Extracellular Na Concentration (mM)
    real(rp) :: p_ko         ! 18 Extracellular K Concentration (mM)
    real(rp) :: p_cao        ! 19 Extracellular Ca Concentration (mM)
    real(rp) :: p_ATPi       ! 20 Intracellular ATP concentration, IKATP
    real(rp) :: p_ADPi       ! 21 Intracellular ADP concentration, IKATP
    real(rp) :: p_gksm       ! 22 Max. Conductance of IKs (nS/uF)
    real(rp) :: p_gitodv     ! 23 Max conductance of Ito
  end type t_prm
 
! ------------------------------------------------------------------------------
  public   :: get_parameter_LR2K , ic_lr2k, LR2K_A_P01
  private  :: put_param, rev_pot, comp_ina, comp_ical, comp_icat, comp_ikr, comp_iks
  private  :: comp_ito, comp_insca, comp_it, conc_jsr, fun_irelcicr, fun_ireljsrol 
  private  :: conc_nsr, conc_cai 
!  private  :: conc_cleft
  private  :: dm_dt, dh_dt, dj_dt, dd_dt, df_dt, db_dt, dg_dt, comp_iki, comp_ikp
  private  :: comp_ikna, comp_ikatp, comp_ikatp_chema, comp_inaca, comp_inak, comp_ipca 
  private  :: comp_icab, comp_inab,  conc_nai,  conc_ki, calc_itr, csqn_jsr, i_leak_up
  private  :: integral, ib_cakna

contains

!------------------------------------------------------------------------------!
subroutine write_state_LR(lu_st)
implicit none
integer(ip), intent(in)   :: lu_st

    write(lu_st,'(A)',advance='no') '% '
    write(lu_st,'(A)', advance='no') 'm '
    write(lu_st,'(A)',advance='no') 'h '
    write(lu_st,'(A)',advance='no') 'j '
    write(lu_st,'(A)',advance='no') 'd '
    write(lu_st,'(A)',advance='no') 'f '
    write(lu_st,'(A)',advance='no') 'xs1 '
    write(lu_st,'(A)',advance='no') 'xs2 '
    write(lu_st,'(A)',advance='no') 'xr '
    write(lu_st,'(A)',advance='no') 'b '
    write(lu_st,'(A)',advance='no') 'g '
    write(lu_st,'(A)',advance='no') 'zdv '
    write(lu_st,'(A)',advance='no') 'ydv '
    write(lu_st,'(A)',advance='no') 'Nai '
    write(lu_st,'(A)',advance='no') 'Nabm '
    write(lu_st,'(A)',advance='no') 'Ki '
    write(lu_st,'(A)',advance='no') 'Kbm '
    write(lu_st,'(A)',advance='no') 'Cai '
    write(lu_st,'(A)',advance='no') 'Cabm '
    write(lu_st,'(A)',advance='no') 'JSR '
    write(lu_st,'(A)',advance='no') 'HSR '
    write(lu_st,'(A)',advance='no') 'jsrol '
    write(lu_st,'(A)',advance='no') 'cicr '
    write(lu_st,'(A)',advance='no') 'dcaiont '
    write(lu_st,'(A)',advance='no') 'caiontold '
    write(lu_st,'(A)',advance='no') 'irelcicr '
    write(lu_st,'(A)',advance='no') 'grelbarjsrol '
    write(lu_st,'(A)',advance='no') 'flag '
end subroutine write_state_LR
!------------------------------------------------------------------------------!
subroutine write_current_LR(lu_cr)
implicit none
integer(ip), intent(in)   :: lu_cr

    write(lu_cr,'(A)',advance='no') '% '
    write(lu_cr,'(A)', advance='no') 'INa '   !1
    write(lu_cr,'(A)',advance='no') 'INab '   !2
    write(lu_cr,'(A)',advance='no') 'IlCaNa ' !3
    write(lu_cr,'(A)',advance='no') 'InsNa '  !4
    write(lu_cr,'(A)',advance='no') 'INaK '   !5
    write(lu_cr,'(A)',advance='no') 'INaCa '  !6
    write(lu_cr,'(A)',advance='no') 'IKr '    !7
    write(lu_cr,'(A)',advance='no') 'IKs '    !8
    write(lu_cr,'(A)',advance='no') 'IKi '    !9
    write(lu_cr,'(A)',advance='no') 'IKp '    !10
    write(lu_cr,'(A)',advance='no') 'IlCaK '  !11
    write(lu_cr,'(A)',advance='no') 'InsK '   !12
    write(lu_cr,'(A)',advance='no') 'Ito '    !13
    write(lu_cr,'(A)',advance='no') 'Ikna '   !14
    write(lu_cr,'(A)',advance='no') 'Ikatp '  !15
    write(lu_cr,'(A)',advance='no') 'Ilca '   !16
    write(lu_cr,'(A)',advance='no') 'Icab '   !17
    write(lu_cr,'(A)',advance='no') 'Ipca '   !18
    write(lu_cr,'(A)',advance='no') 'Icat '   !19
    write(lu_cr,'(A)',advance='no') 'Itot '   !20
end subroutine write_current_LR
!------------------------------------------------------------------------------!
function get_parameter_LR2K (tcell) result (v_prm)
!------------------------------------------------------------------------------!
  implicit none
  integer(ip), intent(in) :: tcell
  real(rp)                :: v_prm(np_lr)

  select case (tcell)
  case (LR_ENDO_mod)
    v_prm = (/ 1.0_rp, 1.0_rp, 1.0_rp, 1.0_rp, 1.0_rp, 1.0_rp,   &
               1.0_rp, 1.0_rp, 1.0_rp, 1.0_rp, 1.0_rp, 1.0_rp,   &
               1.0_rp, 1.0_rp, 1.0_rp, 1.0_rp, p_nao, p_ko   ,   &
               p_cao, p_ATPi, p_ADPi, p_gksm_endo, p_gitodv_endo /)
  case (LR_MID_mod)
    v_prm = (/ 1.0_rp, 1.0_rp, 1.0_rp, 1.0_rp, 1.0_rp, 1.0_rp,   &
               1.0_rp, 1.0_rp, 1.0_rp, 1.0_rp, 1.0_rp, 1.0_rp,   &
               1.0_rp, 1.0_rp, 1.0_rp, 1.0_rp, p_nao, p_ko,      &
               p_cao, p_ATPi, p_ADPi, p_gksm_mid, p_gitodv_mid /)
  case (LR_EPI_mod)
    v_prm = (/ 1.0_rp, 1.0_rp, 1.0_rp, 1.0_rp, 1.0_rp, 1.0_rp,   &
               1.0_rp, 1.0_rp, 1.0_rp, 1.0_rp, 1.0_rp, 1.0_rp,   &
               1.0_rp, 1.0_rp, 1.0_rp, 1.0_rp, p_nao, p_ko,      &
               p_cao, p_ATPi, p_ADPi, p_gksm_epi, p_gitodv_epi /)
  case default
    write (*,10) tcell; stop
  end select
  !.
  return
10 format ('###.Specified Cell type is not defined:',I3)
end function get_parameter_LR2K
!------------------------------------------------------------------------------
subroutine put_param(v_prm,param)
!------------------------------------------------------------------------------
  implicit none
  real(rp), intent(in)    :: v_prm(:)
  type (t_prm)            :: param

  param%A_gna        = v_prm(1)  ! 1  Max. Conductance of the Na Channel (mS/uF)
  param%A_PCaL       = v_prm(2)  ! 2  ICaL channel conduction reduction factor
  param%A_gcat       = v_prm(3)  ! 3  Max. Conductance of T Ca Channel (nS/uF)
  param%A_gkrm       = v_prm(4)  ! 4  Max. Conductance of IKs (nS/uF)
  param%A_gksm       = v_prm(5)  ! 5  Max. Conductance of IKs (nS/uF)
  param%A_gkim       = v_prm(6)  ! 6  Max. Conductance of IKs (nS/uF)
  param%A_gkp        = v_prm(7)  ! 7  Max. Conductance of IKp (nS/uF)
  param%A_gkna       = v_prm(8)  ! 8  Maximum conductance of IKNa (mS/uF)
  param%A_IKatp      = v_prm(9)  ! 9  Channel density, channels/cm^2, IKATP
  param%A_gitodv     = v_prm(10) ! 11 Max conductance of Ito
  param%A_c1         = v_prm(11) ! 12 Scaling factor for inaca (uA/uF)
  param%A_ibarnak    = v_prm(12) ! 13 Max. current through Na-K pump (uA/uF)
  param%A_ibarpca    = v_prm(13) ! 14 Max. Ca current through sarcolemmal Ca pump (uA/uF)
  param%A_gcab       = v_prm(14) ! 15 Max conductance of Ca background current
  param%A_gnab       = v_prm(15) ! 16 Max conductance of Na background current (In the paper is 0.00141)
  param%A_iupbar     = v_prm(16) ! 17 Max. current through iup channel (mM/ms)
  param%p_nao        = v_prm(17) ! 18 Extracellular Na Concentration (mM)
  param%p_ko         = v_prm(18) ! 19 Extracellular K Concentration (mM)
  param%p_cao        = v_prm(19) ! 20 Extracellular Ca Concentration (mM)
  param%p_ATPi       = v_prm(20) ! 21 Intracellular ATP concentration, IKATP
  param%p_ADPi       = v_prm(21) ! 22 Intracellular ADP concentration, IKATP
  param%p_gksm       = v_prm(22) ! 23 Max. Conductance of IKs (nS/uF)
  param%p_gitodv     = v_prm(23) ! 24 Max conductance of Ito

  return
end subroutine put_param
!------------------------------------------------------------------------------

! ------------------------------------------------------------------------------
function ic_lr2k(tcell) result(lr2kic)
! ------------------------------------------------------------------------------
! ------------------------- Initial Conditions ---------------------------------
! ------------------------------------------------------------------------------
  implicit none
! ------------------------------------------------------------------------------
  integer, intent(in)   :: tcell
  real(rp)              :: lr2kic(nvar_lr)
  real(rp)              :: r_flg

  select case (tcell)
  case(LR_ENDO_mod)  
    r_flg=float(end_flag) 
    lr2kic= (/ end_m,end_h,end_j,end_d,end_f,end_xs1,end_xs2,end_xr,end_b,  &
               end_g,end_zdv,end_ydv,                                       &
               end_nai,end_nabm,end_ki,end_kbm,end_cai,end_cabm,end_jsr,    &
               end_nsr,                                                     &
               end_tjsrol,end_tcicr,                                        &
               end_dcaiont,end_caiontold,end_irelcicr,end_grelbarjsrol,     &
               r_flg /)
  case(LR_MID_mod)  
    r_flg=float(mid_flag)
    lr2kic= (/ mid_m,mid_h,mid_j,mid_d,mid_f,mid_xs1,mid_xs2,mid_xr,mid_b,  &
               mid_g,mid_zdv,mid_ydv,                                       &
               mid_nai,mid_nabm,mid_ki,mid_kbm,mid_cai,mid_cabm,mid_jsr,    &
               mid_nsr,                                                     &
               mid_tjsrol,mid_tcicr,                                        &
               mid_dcaiont,mid_caiontold,mid_irelcicr,mid_grelbarjsrol,     &
               r_flg /)
  case(LR_EPI_mod)  
    r_flg= float(epi_flag)
    lr2kic= (/ epi_m,epi_h,epi_j,epi_d,epi_f,epi_xs1,epi_xs2,epi_xr,epi_b,  &
               epi_g,epi_zdv,epi_ydv,                                       &
               epi_nai,epi_nabm,epi_ki,epi_kbm,epi_cai,epi_cabm,epi_jsr,    &
               epi_nsr,                                                     &
               epi_tjsrol,epi_tcicr,                                        &
               epi_dcaiont,epi_caiontold,epi_irelcicr,epi_grelbarjsrol,     &
               r_flg /)
  case default 
    r_flg=float(def_flag)
    lr2kic= (/ def_m,def_h,def_j,def_d,def_f,def_xs1,def_xs2,def_xr,def_b,  &
               def_g,def_zdv,def_ydv,                                       &
               def_nai,def_nabm,def_ki,def_kbm,def_cai,def_cabm,def_jsr,    &
               def_nsr,                                                     &
               def_tjsrol,def_tcicr,                                        &
               def_dcaiont,def_caiontold,def_irelcicr,def_grelbarjsrol,     &
               r_flg/)
  end select          
  return
end function ic_lr2k
! ------------------------------------------------------------------------------
subroutine get_me_struct(str_me, v_me)
! ------------------------------------------------------------------------------
  implicit none
  type (t_lr), intent(in) :: str_me
  real(rp), intent(out)   :: v_me(nvar_lr)

  v_me( 1) =  str_me%tgate%m
  v_me( 2) =  str_me%tgate%h
  v_me( 3) =  str_me%tgate%j
  v_me( 4) =  str_me%tgate%d
  v_me( 5) =  str_me%tgate%f
  v_me( 6) =  str_me%tgate%xs1
  v_me( 7) =  str_me%tgate%xs2
  v_me( 8) =  str_me%tgate%xr
  v_me( 9) =  str_me%tgate%b
  v_me(10) =  str_me%tgate%g
  v_me(11) =  str_me%tgate%zdv
  v_me(12) =  str_me%tgate%ydv
  v_me(13) =  str_me%tconc%nai
  v_me(14) =  str_me%tconc%nabm
  v_me(15) =  str_me%tconc%ki  
  v_me(16) =  str_me%tconc%kbm
  v_me(17) =  str_me%tconc%cai
  v_me(18) =  str_me%tconc%cabm
  v_me(19) =  str_me%tconc%jsr 
  v_me(20) =  str_me%tconc%nsr 
  v_me(21) =  str_me%ttime%tjsrol
  v_me(22) =  str_me%ttime%tcicr
  v_me(23) =  str_me%tvari%dcaiont
  v_me(24) =  str_me%tvari%caiontold
  v_me(25) =  str_me%tvari%irelcicr
  v_me(26) =  str_me%tvari%grelbarjsrol
  v_me(27) =  float(str_me%tvari%flag)  
  return
end subroutine get_me_struct
! ------------------------------------------------------------------------------
subroutine put_me_struct(str_me, v_me)
! ------------------------------------------------------------------------------
  implicit none
  real(rp), intent(in)      :: v_me(:)
  type (t_lr), intent(out) :: str_me

  str_me%tgate%m            = v_me( 1)
  str_me%tgate%h            = v_me( 2)
  str_me%tgate%j            = v_me( 3)
  str_me%tgate%d            = v_me( 4)
  str_me%tgate%f            = v_me( 5)
  str_me%tgate%xs1          = v_me( 6)
  str_me%tgate%xs2          = v_me( 7)
  str_me%tgate%xr           = v_me( 8)
  str_me%tgate%b            = v_me( 9)
  str_me%tgate%g            = v_me(10)
  str_me%tgate%zdv          = v_me(11)
  str_me%tgate%ydv          = v_me(12)
  str_me%tconc%nai          = v_me(13)
  str_me%tconc%nabm         = v_me(14)
  str_me%tconc%ki           = v_me(15)
  str_me%tconc%kbm          = v_me(16)
  str_me%tconc%cai          = v_me(17)
  str_me%tconc%cabm         = v_me(18)
  str_me%tconc%jsr          = v_me(19)
  str_me%tconc%nsr          = v_me(20)
  str_me%ttime%tjsrol       = v_me(21)
  str_me%ttime%tcicr        = v_me(22)
  str_me%tvari%dcaiont      = v_me(23)
  str_me%tvari%caiontold    = v_me(24)
  str_me%tvari%irelcicr     = v_me(25)
  str_me%tvari%grelbarjsrol = v_me(26)
  str_me%tvari%flag         = int(aint(v_me(27)))
  return
end subroutine put_me_struct
! ------------------------------------------------------------------------------
! &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
! ------------------------------------------------------------------------------
subroutine LR2K_A_P01 (ict,dt,dvdt,V,Iion,v_prm,v_lr2k,v_cr)
! ------------------------------------------------------------------------------
  implicit none
  integer(ip), intent(in)   :: ict
  real(rp), intent(in)      :: V,dt,dvdt
  real(rp), intent(out)     :: Iion
  real (rp),intent(in)      :: v_prm(np_lr)
  real(rp),intent(inout)    :: v_lr2k(nvar_lr)
  real(rp), intent(out)     :: v_cr(ncur_lr)
! ------------------------------------------------------------------------------
  type (t_prm)              :: prm
  type(t_lr)                :: lr2k
  type(t_curr)              :: curr
  real(rp)                  :: eca,eka,ena,itr,irel,i_upleak


  call put_me_struct(lr2k, v_lr2k)
  call put_param(v_prm,prm)
  curr = t_curr(0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,           & 
                0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,           &
                0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 )
  !
  if ((dvdt > 10.0) .and. (lr2k%ttime%tcicr > 10.0) .and. (lr2k%tvari%flag == 1)) lr2k%tvari%flag = 0;

  call rev_pot  (lr2k%tconc,prm,eca,eka,ena)
  call comp_ina (lr2k%tgate,ena,V,dt,curr%ina,prm)              !-Calculates Fast Na Current
  call comp_ical(lr2k%tgate,lr2k%tconc,V,dt,curr,prm)           !-Currents through L-Type Ca Channel
  call comp_icat(lr2k%tgate,eca,V,dt,curr%icat,prm);            !-Currents through T-Type Ca Channel
  call comp_ikr (eka,V,dt,lr2k%tgate,curr%ikr,prm)              !-Rapidly Activating K Current
  call comp_iks (lr2k%tconc,V,dt,lr2k%tgate,curr%iks,prm)       !-Slowly Activating K Current
  curr%iki = comp_iki (eka,V,prm)                               !-Time-Independant K Current
  curr%ikp=comp_ikp (eka,V,prm)                                 !-Plateau K Current
!
!  curr%ikna=comp_ikna(eka,lr2k%tconc%nai,V,prm)         !.Na-activated K Current
!  curr%ikatp= comp_ikatp (eka,prm%p_ko,V)      !.ATP-Sensitive K Current
  curr%ikatp= comp_ikatp_chema(eka,lr2k%tconc%nai,V,prm)
!  curr%ikatp = 0.0
!  call comp_ito(eka,V,dt,lr2k%tgate,curr%ito,prm)       !.Transient Outward Current
!
  curr%inaca=comp_inaca(lr2k%tconc,V,prm)                !.Na-Ca Exchanger Current
  curr%inak= comp_inak(lr2k%tconc,V,prm)                 !.Na-K Pump Current
! 
!  call comp_insca (lr2k%tconc,V,curr%insk,curr%insna,prm)!.Non-Specific ca-Activated Current
!
  curr%ipca=comp_ipca (lr2k%tconc%cai,prm) !.Sarcolemmal Ca Pump Current
  curr%icab=comp_icab(eca,V,prm)           !.Ca Background Current
  curr%inab=comp_inab(ena,V,prm)           !.Na Background Current
  call comp_it(curr) !.Total Current
!---------------------|CONCENTRACIONES|--------------------------------
  lr2k%tconc%nai = conc_nai(curr%naiont,dt,lr2k%tconc%nai) !.new myoplasmic Na ion concentration
  lr2k%tconc%ki  = conc_ki(lr2k%tconc%ki,curr%kiont,dt) !.new myoplasmic K ion concentration
  itr=calc_itr(lr2k%tconc%nsr,lr2k%tconc%jsr)    !.Translocation of Ca from NSR to JSR
  call conc_jsr (V,dt,itr,lr2k%ttime,curr,lr2k%tconc,lr2k%tvari,irel)   !.new JSR Ca ion concentration
  call conc_nsr (dt,itr,lr2k%tconc,i_upleak,prm)       !.new NSR Ca ion concentration
  call conc_cai (dt,curr,lr2k%tconc,i_upleak,irel) !.new myoplasmic Ca ion concentration
! 
!  call conc_cleft(dt,curr,lr2k%tconc,prm)   !.new cleft ion concentrations
!
  Iion                = curr%it
  lr2k%tvari%caiontold= curr%caiont;
  lr2k%tvari%dcaiont  = curr%dcaiontnew;
!  
  call get_me_struct(lr2k,v_lr2k)
  
!.
  v_cr(1:ncur_lr) = (/ curr%ina, curr%inab, curr%ilcana, curr%insna, curr%inak, &
                  curr%inaca, curr%ikr, curr%iks, curr%iki, curr%ikp,           &
                  curr%ilcak, curr%insk, curr%ito, curr%ikna, curr%ikatp,       &
                  curr%ilca, curr%icab, curr%ipca, curr%icat, Iion /)
!.
  return
end subroutine LR2K_A_P01
! ------------------------------------------------------------------------------
! &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
! ------------------------------------------------------------------------------
subroutine rev_pot (conc,prm,eca,eka,ena)
! ------------------------------------------------------------------------------
! ------------------------------ EQUATION 0 ------------------------------------
! ------------------------- Reversal Potential ---------------------------------   
! ----- eca=(r*frdy/(2*temp))*log(cao/cai)
! ----- eka=(r*frdy/temp)*log(ko/ki)
! ----- ena=(r*frdy/temp)*log(nao/nai)
! ------------------------------------------------------------------------------
  implicit none
  type(t_conc),intent (in) :: conc
  type(t_prm), intent(in)  :: prm
  real (rp),  intent (out) :: eca,eka,ena

  eca = 0.5*p_rt_f*log(prm%p_cao/conc%cai);
  eka = p_rt_f*log(prm%p_ko/conc%ki);
  ena = p_rt_f*log(prm%p_nao/conc%nai);
  return
end subroutine rev_pot
! ------------------------------------------------------------------------------
! >>>>>>>>>>>>>>>>>>>>>>>>>> START EQUATION 1 <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
! ------------------------------------------------------------------------------
subroutine comp_ina(gate,ena,V,dt,ina,prm)
! ------------------------------------------------------------------------------
! ---------------- Fast sodium current [((?A)/(?F))] ---------------------------
! ------------------------ EQUATION 1 ------------------------------------------
! m_t,h_t,j_t: Gate Conditions at time t
! nai: Intracellular Na Concentration (mM)
! nao: Extracellular Na Concentration (mM)
! V  : Membrane voltage (mV)
! fna: factor de bloque de la corriente  de sodio
! ------------------------------------------------------------------------------
  implicit none
  real (rp),  intent (in)    :: ena,V,dt
  type(t_prm), intent (in)   :: prm
  real (rp),  intent (out)   :: ina
  type(t_gate),intent (inout):: gate

  gate%m = dm_dt(gate%m,V,dt);              !-EQUATION 1-A
  gate%h = dh_dt(gate%h,V,dt);              !-EQUATION 1-B
  gate%j = dj_dt(gate%j,V,dt);              !-EQUATION 1-C
!
  ina= prm%A_gna*p_gna*gate%m**3.0*gate%h*gate%j*(V-ena); !.Modelo original
!
  return
end subroutine comp_ina
! ------------------------------------------------------------------------------
function dm_dt (mt,V,dt) result(m)
! ------------------------------------------------------------------------------
! ------------------------ EQUATION 1-A ----------------------------------------
! Resolucion de la ecuacion diferencial Nro. 1, pagina 1
! dm/dt=-(am+bm)*m + am;  a = am+bm;    b= am
! ------------------------------------------------------------------------------
  implicit none
  real(rp),intent(in) :: mt,V,dt
  real(rp)            :: m
  real(rp)            :: a_1,am,bm

  a_1 = V+47.13
  am  = 0.32*a_1/(1.0-exp(-0.1*a_1))
  bm  = 0.08*exp(-V/11.0)
!
  m   = integral(mt,am+bm,am,dt);
!
  return
end function dm_dt
! ------------------------------------------------------------------------------
function dh_dt(ht,V,dt) result(h)
! ------------------------------------------------------------------------------
! ---------------------------- EQUATION 1-B ------------------------------------
!          Resolucion de la ecuacion diferencial Nro. 2, pagina 1
! dh/dt=-(ah+bh)*h + ah;  a = ah+bh;    b= ah
! ------------------------------------------------------------------------------
  implicit none
  real(rp),intent(in) :: ht,V,dt
  real(rp)            :: h,ah,bh

  if (V < -40.0) then
    ah = 0.135*exp(-(80.0+V)/6.8);
    bh = 3.56*exp(0.079*V)+310000.0*exp(0.35*V);
  else
    ah = 0.0;
    bh = 1.0/(0.13*(1.0+exp(-(V+10.66)/11.1)));
  endif
!
  h  = integral(ht,ah+bh,ah,dt);
!
return
end function dh_dt
! ------------------------------------------------------------------------------
function dj_dt(jt,V,dt) result(j)
! ------------------------------------------------------------------------------
! --------------------------- EQUATION 1-C -------------------------------------
! Resolucion de la ecuacion diferencial Nro. 3, pagina 2
! dj/dt=-(aj+bj)*j + aj;  a = aj+bj;    b= aj
! ------------------------------------------------------------------------------
  implicit none
  real(rp),intent(in) :: jt,V,dt
  real(rp)            :: j,num,den,aj,bj

  if (V < -40.0)  then
    num =(-1.2714e5*exp(0.2444*V)-3.474e-5*exp(-0.04391*V))*(V+37.78);
    den =(1.0+exp(0.311*(V+79.23)));
    aj = num/den;
    bj = (0.1212*exp(-0.01052*V))/(1.0+exp(-0.1378*(V+40.14)));
  else
    aj = 0.0;
    bj = (0.3*exp(-2.535e-7*V))/(1.0+exp(-0.1*(V+32.0)));
  endif
!
  j  = integral(jt,aj+bj,aj,dt);
!
  return
end function dj_dt
! ------------------------------------------------------------------------------
! >>>>>>>>>>>>>>>>>>>>>>>>>> END   EQUATION 1 <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
! ------------------------------------------------------------------------------
! $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
! ------------------------------------------------------------------------------
! >>>>>>>>>>>>>>>>>>>>>>>>>> START EQUATION 2 <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
! ------------------------------------------------------------------------------
! ------------------------------------------------------------------------------
subroutine comp_ical(gate,conc,V,dt,curr,prm)
! ------------------------------------------------------------------------------
! ---------------------------- EQUATION 2 --------------------------------------
! Total Ionic current through the L-type Ca2  channel [((?A)/(?F))]
! ------------------------------------------------------------------------------
  implicit none
  type(t_conc), intent(in)    :: conc           ! of Ca channel (mM)
  type(t_prm), intent(in)     :: prm
  type(t_gate), intent(inout) :: gate
  type(t_curr), intent(inout) :: curr
  real(rp),intent(in)         :: V,dt
  real(rp)                    :: fca,A,B,ibarca,ibark,ibarna,a_1

! ---------------------- EQUATION 2-A ------------------------------------------
  A = V*p_cazfrt; B = V*p_capzfrt;
  ibarca= ib_cakna(A,B,conc%cai,prm%p_cao,p_gacai,p_gacao);
! ---------------------- EQUATION 2-B ------------------------------------------
  A = V*p_kzfrt; B = V*p_kpzfrt;
  ibark=ib_cakna(A,B,conc%ki,prm%p_ko,p_gaki,p_gako);
! ---------------------- EQUATION 2-C    ---------------------------------------
  A = V*p_nazfrt; B = V*p_napzfrt;
  ibarna=ib_cakna(A,B,conc%nai,prm%p_nao,p_ganai,p_ganao);
! ------------------------------------------------------------------------------
  gate%d = dd_dt(gate%d,V,dt);       !-EQUATION 2-D
  gate%f = df_dt(gate%f,V,dt);       !-EQUATION 2-E
  fca    = 1.0/(1.0+conc%cai/p_kmca);  !-EQUATION 2-F
! ------------------------------------------------------------------------------
  a_1= gate%d*gate%f*fca;
  curr%ilca  = a_1*ibarca*prm%A_PCaL;     !-EQUATION 2 , modificacion 30/11
  curr%ilcana= a_1*ibarna;                !-EQUATION 2
  curr%ilcak = a_1*ibark;                 !-EQUATION 2
!
  return
end subroutine comp_ical
! ------------------------------------------------------------------------------
function dd_dt(d_t,V,dt) result(d)
! ------------------------------------------------------------------------------
! ----------------------- EQUATION 2D ------------------------------------------
  implicit none
  real(rp), intent(in) :: d_t,V,dt
  real(rp)             :: d,a_1,a_2,d_i,ad,bd

  a_1= V+10.0;
  a_2= exp(-a_1/6.24);
  d_i= 1.0/(1.0+a_2);
  ad = (0.035*a_1)/(1.0-a_2);
  bd =((1.0-d_i)/d_i)*ad;
!
  d=integral(d_t,ad+bd,ad,dt);
!
  return
end function dd_dt
! ------------------------------------------------------------------------------
function df_dt(f_t,V,dt) result(f)
! ------------------------------------------------------------------------------
! ----------------------------- EQUATION 2E ------------------------------------
  implicit none
  real(rp), intent(in) :: f_t,V,dt
  real(rp)             :: f,a_1,a_2,a_3,f_i,tii,af

  a_1=1.0+exp((V+32.0)/8.0);
  a_2=1.0+exp((50.0-V)/20.0);
  a_3=0.0337*(V+10.0);
  f_i= 1.0/a_1 + 0.6/a_2;
  tii= 0.0197*exp(-a_3*a_3)+0.02;
  af = tii*f_i;
!
  f=integral(f_t,tii,af,dt);
!
return
end function df_dt
! ------------------------------------------------------------------------------
! >>>>>>>>>>>>>>>>>>>>>>>>>> END   EQUATION 2 <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
! ------------------------------------------------------------------------------
! $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
! ------------------------------------------------------------------------------
! >>>>>>>>>>>>>>>>>>>>>>>>>> START EQUATION 3 <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
! ------------------------------------------------------------------------------
! ------------------------------------------------------------------------------
subroutine comp_icat(gate,eca,V,dt,icat,prm);
! ------------------------------------------------------------------------------
! ----------------------------- EQUATION 3 -------------------------------------
  implicit none
  real(rp), intent(in)       :: eca,V,dt
  type(t_prm), intent(in)    :: prm
  type(t_gate),intent(inout) :: gate
  real(rp), intent(out)      :: icat

  gate%b = db_dt(gate%b,V,dt);                  ! EQUATION 3-A
  gate%g = dg_dt(gate%g,V,dt);                  ! EQUATION 3-B
!
  icat= prm%A_gcat*p_gcat*gate%b*gate%b*gate%g*(V-eca);
!
  return
end subroutine comp_icat
! ------------------------------------------------------------------------------
function db_dt(bt,V,dt) result(b)
! ------------------------------------------------------------------------------
! ---------------------------- EQUATION 3-A ------------------------------------
  implicit none
  real(rp), intent(in) :: bt,V,dt
  real(rp)             :: a_1,tii,bti,b

  a_1=exp((V+25.0)/4.5);
  tii=(1.0+a_1)/(9.8+3.7*a_1);
  bti= tii/(1.0 + exp(-(V+14.0)/10.8));
!
  b=integral(bt,tii,bti,dt);
!
  return
end function db_dt
! ------------------------------------------------------------------------------
function dg_dt(gt,V,dt) result(g)
! ------------------------------------------------------------------------------
! ----------------------------- EQUATION 3-B -----------------------------------
  implicit none
  real(rp), parameter :: cte=1.0/12.0
  real(rp), intent(in):: gt,V,dt
  real(rp)            :: tii,gii,g

  if (V <= 0.0 ) then
    tii =1.0/(-0.875*V+12.0);
  else
    tii= cte;
  endif
  gii = tii/(1.0+exp((V+60.0)/5.6));
!
  g=integral(gt,tii,gii,dt);
!
  return
end function dg_dt
! ------------------------------------------------------------------------------
! >>>>>>>>>>>>>>>>>>>>>>>>>> END   EQUATION 3 <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
! ------------------------------------------------------------------------------
! $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
! ------------------------------------------------------------------------------
! >>>>>>>>>>>>>>>>>>>>>>>>>> START EQUATION 4 <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
! ------------------------------------------------------------------------------
subroutine comp_ikr (ekr,V,dt,gate,ikr,prm)
! ------------------------------------------------------------------------------
! -------------------- COMPONENTS Ikr OF Ik ------------------------------------
! ------------- RAPID TIME DEPENDEN POTASSIUM CURRENT: Ikr----------------------
! ------------------------------ EQUATION 4 ------------------------------------
  implicit none
  real(rp), intent(in)       :: ekr,V,dt
  type(t_prm), intent(in)    :: prm
  type(t_gate), intent(inout):: gate
  real(rp), intent(out)      :: ikr
  real(rp)                   :: gkr,a_1,a_2,t_1,t_2,trii,xr_i,r

  gkr = p_gkrm*sqrt(prm%p_ko/5.4);             !-EQUATION 4-A
!
  a_1 = V + 14.2; a_2=V + 38.9;
  t_1 = 0.00138*a_1/(1.0 - exp(-0.123*a_1));
  t_2 = 0.00061*a_2/(exp(0.145*a_2) - 1.0);
  trii= t_1+t_2;
  xr_i=trii/(1.0+exp(-(V+21.5)/7.5));

  gate%xr=integral(gate%xr,trii,xr_i,dt);          !-EQUATION 4-B

  r = 1.0/(1.0+exp((V+9.0)/22.4));                 !-EQUATION 4-C
!
  ikr=prm%A_gkrm*gkr*gate%xr*r*(V-ekr);                       !-EQUATION 4
!
  return
end subroutine comp_ikr
! ------------------------------------------------------------------------------
! >>>>>>>>>>>>>>>>>>>>>>>>>> END   EQUATION 4 <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
! ------------------------------------------------------------------------------
! $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
! ------------------------------------------------------------------------------
! >>>>>>>>>>>>>>>>>>>>>>>>>> START EQUATION 5 <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
! ------------------------------------------------------------------------------
 subroutine comp_iks (conc,V,dt,gate,iks,prm)
! ------------------------------------------------------------------------------
! ------------ SLOW TIME DEPENDEN POTASSIUM CURRENT: Iks -----------------------
! ----------------------- COMPONENTS Iks OF Ik ---------------------------------
! --------------------------- EQUATION 17 --------------------------------------
  implicit none
  type(t_conc), intent(in)    :: conc
  type(t_prm), intent(in)     :: prm
  real(rp)                    :: V,dt
  type(t_gate), intent(inout) :: gate
  real(rp), intent(out)       :: iks
  real(rp)                    :: gks,a_1,eks,xs_i,d_1,d_2,t_i1,t_i2


  gks = prm%p_gksm*(1.0+0.6/(1.0+(3.8e-5/conc%cai)**1.4)); !-EQUATION 5-A
  a_1 =(prm%p_ko+p_prnak*prm%p_nao)/(conc%ki+p_prnak*conc%nai);
  eks = p_rt_f*log(a_1);                                     !-EQUATION 5-B
  xs_i= 1.0/(1.0+exp(-(V-1.5)/16.7));
  a_1 = V+30.0;
  d_1 = 7.19e-5*a_1/(1.0-exp(-0.148*a_1));
  d_2 = 1.31e-4*a_1/(exp(0.0687*a_1)-1.0);
  t_i1= d_1 + d_2;
  t_i2= 0.25*t_i1;
!
  gate%xs1=integral(gate%xs1,t_i1,xs_i*t_i1,dt);           !-EQUATION 5-C
  gate%xs2=integral(gate%xs2,t_i2,xs_i*t_i2,dt);           !-EQUATION 5-D
!
  iks=prm%A_gksm*gks*gate%xs1*gate%xs2*(V-eks);            !-EQUATION 5
!
  return
end subroutine comp_iks
! ------------------------------------------------------------------------------
! >>>>>>>>>>>>>>>>>>>>>>>>>> END   EQUATION 5 <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
! ------------------------------------------------------------------------------
! $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
! ------------------------------------------------------------------------------
! >>>>>>>>>>>>>>>>>>>>>>>>>> START EQUATION 6 <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
! ------------------------------------------------------------------------------
function comp_iki (eki,V,prm) result(iki)
! ------------------------------------------------------------------------------
! -------------- TIME INDEPENDENT POTASSIUM CURRENT: Ik1 -----------------------
! -------------------------- EQUATION 18 ---------------------------------------
! ------------------------------------------------------------------------------
  implicit none
  real(rp), intent(in)     :: eki,V
  type(t_prm), intent(in)  :: prm
  real(rp)                 :: iki,gki,a_1,aki,num,den,bki,kin

  gki= p_gkim*sqrt(prm%p_ko/5.4);                  !-EQUATION 6-A
  a_1= V - eki;
  aki= 1.02/(1+exp(0.2385*(a_1-59.215)));
  num= 0.49124*exp(0.08032*(a_1+5.476))+exp(0.06175*(a_1-594.31));
  den= 1.0+exp(-0.5143*(a_1+4.753));
  bki= num/den;
  kin = aki/(aki+bki);                     !-EQUATION 6-B
!
  iki = prm%A_gkim*gki*kin*a_1;                       !-EQUATION 6
!
  return
end function comp_iki
! ------------------------------------------------------------------------------
! >>>>>>>>>>>>>>>>>>>>>>>>>> END   EQUATION 6 <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
! ------------------------------------------------------------------------------
! $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
! ------------------------------------------------------------------------------
! >>>>>>>>>>>>>>>>>>>>>>>>>> START EQUATION 7 <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
! ------------------------------------------------------------------------------
! ------------------------------------------------------------------------------
function comp_ikp (eka,V,prm) result(ikp)
! ------------------------------------------------------------------------------
! ---------------- PLATEAU POTASSIUM CURRENT: Ikp ------------------------------
!                      (modelo 1994 pag 1090)
! ------------------------------------------------------------------------------
! ikp: Plateau K current (uA/uF)
! gkp: Channel Conductance of Plateau K Current (mS/uF)
! ekp: Reversal Potential of Plateau K Current (mV)
! kp : K plateau factor
! ekp= eki;
! ------------------------------ EQUATION 7 ------------------------------------
  implicit none
  real(rp), intent(in)   :: eka,V
  type(t_prm),intent(in) :: prm
  real(rp)               :: ikp,kp

  kp = 1.0/(1.0+exp((7.488-V)/5.98));   !-EQUATION 7-A
!
  ikp = prm%A_gkp*p_gkp*kp*(V-eka);            !-EQUATION 7
!
return
end function comp_ikp
! ------------------------------------------------------------------------------
! ------------------------------------------------------------------------------
! >>>>>>>>>>>>>>>>>>>>>>>>>> END   EQUATION 7 <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
! ------------------------------------------------------------------------------
! $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
! ------------------------------------------------------------------------------
! >>>>>>>>>>>>>>>>>>>>>>>>>> START EQUATION 8 <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
! ------------------------------------------------------------------------------
! ------------------------------------------------------------------------------
function comp_ikna(eka,nai,V,prm) result(ikna)
! ------------------------------------------------------------------------------
! -------------- SODIUM ACTIVATED POTASSIUM CURRENT: Ikna ----------------------
! --------------------------- EQUATION 8 --------------------------------------
  implicit none
  real(rp), intent(in)  :: eka,nai,V
  type(t_prm),intent(in):: prm
  real(rp)              :: ikna,pona,pov

  pov = 0.8 - 0.65/(1.0 + exp((V+125.0)/15.0));    !-EQUATION 8-A
  pona= 0.85/(1.0 + (p_kdkna/nai)**2.8);             !-EQUATION 8-B
!
  ikna = prm%A_gkna*p_gkna*pona*pov*(V-eka);                    !-EQUATION 8
!
  return
end function comp_ikna
! ------------------------------------------------------------------------------
! ------------------------------------------------------------------------------
! >>>>>>>>>>>>>>>>>>>>>>>>>> END   EQUATION 8 <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
! ------------------------------------------------------------------------------
! $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
! ------------------------------------------------------------------------------
! >>>>>>>>>>>>>>>>>>>>>>>>>> START EQUATION 9 <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
! ------------------------------------------------------------------------------
function comp_ikatp (eka,ko,V) result(ikatp)
! ------------------------------------------------------------------------------
!.Note: If you wish to use this current in your simulations, there
!       are additional changes which must be made to the code as detailed
!       in Cardiovasc Res 1997;35:256-272
! ------------------------------------------------------------------------------
  implicit none
  real(rp), parameter :: natp = 0.24;           !.K dependence of ATP-sensitive K current
  real(rp), parameter :: nicholsarea = 0.00005; !.Nichol's ares (cm^2)
  real(rp), parameter :: gkatp       = 0.000195/nicholsarea;
  real(rp), parameter :: atpi = 3.0;              !.Intracellular ATP concentraion (mM)
  real(rp), parameter :: hatp = 2.0;              !.Hill coefficient
  real(rp), parameter :: katp = 0.250;          !.Half-maximal saturation point of ATP-sensitive K current (mM)
  real(rp), intent(in):: eka,ko,V
  real(rp)            :: ikatp,patp,gkbaratp

  patp     = 1.0/(1.0+(atpi/katp)**hatp);     !-EQUATION 9-B
  gkbaratp = gkatp*patp*((0.25*ko)**natp);    !-EQUATION 9-A
!
  ikatp = gkbaratp*(V-eka);                   !-EQUATION 9
!
  return
end function comp_ikatp
! ------------------------------------------------------------------------------
! >>>>>>>>>>>>>>>>>>>>>>>>>> END   EQUATION 9 <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
! ------------------------------------------------------------------------------
function comp_ikatp_chema(eka,nai,V,prm) result (ikatp)
! ------------------------------------------------------------------------------  
! Circulation Res 1996;79:208-221 (modelo de chema)
! ------------------------------------------------------------------------------  
  implicit none
  real(rp), intent(in):: eka,nai,V
  type(t_prm), intent(in):: prm
  real(rp)            :: QT,patp,Khatp,gkko,fKo,KhMg,pmg,KhNa,prona,a_1,Hatp
  real(rp)            :: ikatp


  Hatp = 1.2997 + 0.7399*exp(-0.09060000003135*prm%p_ADPi)
  QT   = p_Q10atp**(0.1*(p_temp-310.16));        !.factor de coreccion de Temperatura
  gkko = p_gamma0*(prm%p_ko/5.4)**0.24;      !.eq 10  !.conductancia unitaria (mS)
  fKo  = 0.31*sqrt(prm%p_ko+5.0);                !.influencia de [K]o (pmg) 
  a_1  = V*p_f_rt  
  KhMg = p_KmMg*fKo*exp(-2.0*p_deltaMg*a_1);     !.eq 12
  pmg  = 1.0/(1.0+p_con_Mg_i/KhMg);              !.factor de bloqueo del Mg
  
  KhNa = p_KmNaIKatp*exp(-p_deltaNa*a_1);   
  prona= 1.0/(1.0+(nai/KhNa)*(nai/KhNa));        !.factor de bloqueo del Na
  Khatp= p_K1*(35.7849+17.8701*(prm%p_ADPi**0.2563));!.constante de maxima inhibicion
  patp = 1.0/(1.0+(prm%p_ATPi*1000.0/Khatp)**Hatp);  !.prob. de apertura en funcion de ATP
  
  ikatp= prm%A_IKatp*p_atp_block*p_sigmaIKatp*gkko*pmg*prona*QT*p_p0atp*patp*(V-eka);!(uA/uF)
  
  return
end function comp_ikatp_chema
! ------------------------------------------------------------------------------
! $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
! ------------------------------------------------------------------------------
! >>>>>>>>>>>>>>>>>>>>>>>>>> START EQUATION 10 <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
! ------------------------------------------------------------------------------
! ------------------------------------------------------------------------------
subroutine comp_ito(eka,V,dt,gate,ito,prm)
! ------------------------------------------------------------------------------
! -------------- Calculates Transient Outward Current --------------------------
! -------------------------- EQUATION 10 ---------------------------------------
  implicit none
  real(rp), intent(in)        :: eka,V,dt
  type(t_prm),intent(in)      :: prm
  type(t_gate), intent(inout) :: gate
  real(rp)                    :: ito, rvdv,a_1,azdv,bzdv,aydv,bydv

!
  rvdv = exp(V/100.0);                               !-EQUATION 10-A
!
  a_1  = exp((V-40.0)/25.0); azdv = 10.0*a_1/(1.0+a_1);
  a_1  = exp(-(V+90.0)/25.0);  bzdv = 10.0*a_1/(1.0+a_1);
!
  gate%zdv = integral(gate%zdv,azdv+bzdv,azdv,dt);   !-EQUATION 10-B
!
  aydv = 0.015/(1.0+exp((V+60.0)/5.0));
  a_1  = exp((V+25.0)/5.0); bydv = (0.1*a_1)/(1.0+a_1);
!
  gate%ydv = integral(gate%ydv,aydv+bydv,aydv,dt);   !-EQUATION 10-C
!
  ito = prm%A_gitodv*prm%p_gitodv*gate%zdv**3.0*gate%ydv*rvdv*(V-eka);    !-EQUATION 10
!
  return
end subroutine comp_ito
! ------------------------------------------------------------------------------
! >>>>>>>>>>>>>>>>>>>>>>>>>> END   EQUATION 10 <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
! ------------------------------------------------------------------------------
! $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
! ------------------------------------------------------------------------------
! >>>>>>>>>>>>>>>>>>>>>>>>>> START EQUATION 11 <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
! ------------------------------------------------------------------------------
function comp_inaca(conc,V,prm) result(inaca)
! ------------------------------------------------------------------------------
! ---------------- Reemplazar por el modelo de chema ---------------------------
! ------------------------------------------------------------------------------
! ATP dependent potassium current Na Ca exchanger (modelo del 2000 pag 2402)
! inaca: NaCa exchanger current (uA/uF)
! ------------------------------------------------------------------------------
! ---------------------------- EQUATION 11 -------------------------------------
! ------------------------------------------------------------------------------
  implicit none
  type(t_conc), intent(in) :: conc
  type(t_prm), intent(in)  :: prm
  real(rp)                 :: V
  real(rp)                 :: inaca,a_1,a_2,a_3,a_4

  a_1  = V*p_f_rt;
  a_2  = exp((p_gammas-1.0)*a_1);
  a_3  = exp(a_1)*conc%nai**3.0*prm%p_cao;
  a_4  = prm%p_nao**3.0*conc%cai;
!
  inaca= prm%A_c1*p_c1*a_2*(a_3-a_4)/(1.0+p_c2*a_2*(a_3+a_4)); !-EQUATION 11
!
  return
end function comp_inaca
! ------------------------------------------------------------------------------
! >>>>>>>>>>>>>>>>>>>>>>>>>> END   EQUATION 11 <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
! ------------------------------------------------------------------------------
! $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
! ------------------------------------------------------------------------------
! >>>>>>>>>>>>>>>>>>>>>>>>>> START EQUATION 12 <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
! ------------------------------------------------------------------------------
function comp_inak(conc,V,prm) result(inak)
! ------------------------------------------------------------------------------
!         Sodium potassium pump (modelo 1994 pag 1090)
! inak :  NaK pump current (uA/uF)
! fnak :  Voltage-dependance parameter of inak
! sigma:  [Na]o dependance factor of fnak
! ---------------------------- EQUATION 12 -------------------------------------
  implicit none
  type(t_conc), intent(in) :: conc
  type(t_prm), intent(in)  :: prm
  real(rp), intent(in)     :: V
  real(rp)                 :: inak,sigma,a_1,den,iba_f

  sigma= (exp(prm%p_nao/67.3)-1.0)/7.0;                       !-EQUATION 12-B
  a_1  = p_f_rt*V;
  den  = 1.0 + 0.1245*exp(-0.1*a_1) + 0.0365*sigma*exp(-a_1); 
  iba_f= prm%A_ibarnak*p_ibarnak/den;                         !-EQUATION 12-A
!
  den  = 1.0+(p_kmnai/conc%nai)**2.0;
  inak = iba_f*(1.0/den)*(prm%p_ko/(prm%p_ko+p_kmko));           !-EQUATION 12
!
return
end function comp_inak
! ------------------------------------------------------------------------------
! >>>>>>>>>>>>>>>>>>>>>>>>>> END   EQUATION 12 <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
! ------------------------------------------------------------------------------
! $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
! ------------------------------------------------------------------------------
! >>>>>>>>>>>>>>>>>>>>>>>>>> START EQUATION 13 <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
! ------------------------------------------------------------------------------
subroutine comp_insca (conc,V,insk,insna,prm)
! ------------------------------------------------------------------------------
  implicit none
  type(t_conc), intent(in):: conc
  type(t_prm), intent(in) :: prm
  real(rp), intent(in)    :: V
  real(rp), intent(out)   :: insk,insna
  real(rp)                :: a_1,A,B,ibarnsna,ibarnsk

! ------------------------------------------------------------------------------
  a_1=1.0/(1.0+(p_kmnsca/conc%cai)**3.0);
! ------------------------------------------------------------------------------
  A = p_nazfrt*V; B = V*p_b1;
  ibarnsna=ib_cakna(A,B,conc%nai,prm%p_nao,p_ganai,p_ganao); !-EQUATION 13-A-1
! ------------------------------------------------------------------------------
  A = p_kzfrt*V;  B = V*p_b2;
  ibarnsk =ib_cakna(A,B,conc%ki,prm%p_ko,p_gaki,p_gako);     !-EQUATION 13-B-1
! ------------------------------------------------------------------------------
  insna= ibarnsna*a_1;                    !-EQUATION 13-A
  insk = ibarnsk*a_1;                     !-EQUATION 13-B
!
  return
end subroutine comp_insca
! ------------------------------------------------------------------------------
! >>>>>>>>>>>>>>>>>>>>>>>>>> END   EQUATION 13 <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
! ------------------------------------------------------------------------------
! $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
! ------------------------------------------------------------------------------
! >>>>>>>>>>>>>>>>>>>>>>>>>> START EQUATION 14 <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
! ------------------------------------------------------------------------------
function comp_ipca (cai,prm) result(ipca)
! ------------------------------------------------------------------------------
!       Sarcolemmal calcium pump (modelo 1994 pag 1091)
! ------------------------------ EQUATION 21 -----------------------------------
  implicit none
  real(rp), intent(in)  :: cai
  type(t_prm),intent(in):: prm
  real(rp)              :: ipca
!
  ipca = prm%A_ibarpca*(p_ibarpca*cai)/(p_kmpca+cai);   !-EQUATION 14
!
  return
end function comp_ipca
! ------------------------------------------------------------------------------
! >>>>>>>>>>>>>>>>>>>>>>>>>> END   EQUATION 14 <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
! ------------------------------------------------------------------------------
! $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
! ------------------------------------------------------------------------------
! >>>>>>>>>>>>>>>>>>>>>>>>>> START EQUATION 15 <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
! ------------------------------------------------------------------------------
function comp_icab(eca,V,prm) result(icab)
! ------------------------------------------------------------------------------
!      Calcium background current (modelo 1994 pag 1091)
! ecan: Nernst potential for Ca (mV) == eca
! gcab: Max. conductance of Ca background (mS/uF)
! ------------------------ EQUATION   ------------------------------------------
! ------------------------------------------------------------------------------
! eca=ecan = 0.5*cte.rt_f*log(cao/cai);
  implicit none
  real(rp), intent(in)   :: eca,V
  type(t_prm), intent(in):: prm
  real(rp)               :: icab
!
  icab = prm%A_gcab*p_gcab*(V-eca);        !-EQUATION 15
!
  return
end function comp_icab
! ------------------------------------------------------------------------------
! >>>>>>>>>>>>>>>>>>>>>>>>>> END   EQUATION 15 <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
! ------------------------------------------------------------------------------
! $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
! ------------------------------------------------------------------------------
! >>>>>>>>>>>>>>>>>>>>>>>>>> START EQUATION 16 <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
! ------------------------------------------------------------------------------
function comp_inab(ena,V,prm) result(inab)
! ------------------------------------------------------------------------------
!       Sodium background current (modelo 1994 pag 1091) (uA/uF)
! ------------------------------------------------------------------------------
! gnab       % Max. conductance of Na background (mS/uF)
! enan=ena   % Nernst potential for Na (mV)
! --------------------------- EQUATION 28 --------------------------------------
  implicit none
  real(rp),intent(in) :: ena,V
  type(t_prm), intent(in):: prm
  real(rp)            :: inab
!
  inab = prm%A_gnab*p_gnab*(V-ena);          !-EQUATION 16
!
  return
end function comp_inab
! ------------------------------------------------------------------------------
! >>>>>>>>>>>>>>>>>>>>>>>>>> END   EQUATION 16 <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
! ------------------------------------------------------------------------------
! $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
! ------------------------------------------------------------------------------
! >>>>>>>>>>>>>>>>>>>>>>>>>> START EQUATION 17 <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
! ------------------------------------------------------------------------------
subroutine comp_it(curr);
! ------------------------------------------------------------------------------
! Total sum of currents is calculated here
! ------------------------------------------------------------------------------
  implicit none
  type(t_curr), intent(inout) :: curr

  curr%naiont=curr%ina + curr%inab + curr%ilcana + curr%insna +                &
              3.0*(curr%inak + curr%inaca);
  curr%kiont =curr%ikr + curr%iks + curr%iki + curr%ikp + curr%ilcak +         &
              curr%insk - 2.0*curr%inak+curr%ito+curr%ikna+curr%ikatp;
  curr%caiont=curr%ilca + curr%icab + curr%ipca - 2.0*curr%inaca + curr%icat;
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  curr%it = curr%naiont + curr%kiont + curr%caiont;
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  return
end subroutine comp_it
! ------------------------------------------------------------------------------
! >>>>>>>>>>>>>>>>>>>>>>>>>> END   EQUATION 17 <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
! ------------------------------------------------------------------------------
! $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
! >>>>>>>>>>>>>>>>>>>>>>>> Ion Concentrations <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
! $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
! ------------------------------------------------------------------------------
! >>>>>>>>>>>>>>>>>>>>>>>>>> START EQUATION 18 <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
! ------------------------------------------------------------------------------
function conc_nai(naiont,dt,nai) result(rnai)
! ------------------------------------------------------------------------------
! ----------- CALCULATES NEW MYOPLASMIC Na ION CONCENTRATION -------------------
!  The units of dnai is in mM.  Note that naiont should be multiplied
!  by the cell capacitance to get the correct units. Since cell capa-
!  citance = 1 uF/cm^2, it doesn't explicitly appear in the equation
!  below. This holds true for the calculation of dki and dcai.
!  rnai=nai
! ------------------------------------------------------------------------------
!---------------------------- EQUATION 18 --------------------------------------
  implicit none
  real(rp), intent(in) :: naiont,dt,nai
  real(rp)             :: rnai,dnai

  dnai = -dt*(naiont/p_zna)*p_a_vmf;    !-EQUATION 18
  rnai =  dnai + nai;
  return
end function conc_nai
! ------------------------------------------------------------------------------
! >>>>>>>>>>>>>>>>>>>>>>>>>> END   EQUATION 18 <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
! ------------------------------------------------------------------------------
! $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
! ------------------------------------------------------------------------------
! >>>>>>>>>>>>>>>>>>>>>>>>>> START EQUATION 19 <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
! ------------------------------------------------------------------------------
function conc_ki(ki,kiont,dt) result(rki)
! ------------------------------------------------------------------------------
! --------------- CALCULATES NEW MYOPLASMIC K ION CONCENTRATION ----------------
! st:       Constant Stimulus (uA/cm^2)
! stimtime: Time period during which stimulus is applied (ms)
! rki=ki
!--------------------------- EQUATION 19 ---------------------------------------
  implicit none
  real(rp), intent(in)     :: ki,kiont,dt
  real(rp)                 :: rki,dki

  dki= -dt*(kiont/p_zk)*p_a_vmf;
  rki = dki + ki;
  return
end function conc_ki
! ------------------------------------------------------------------------------
! >>>>>>>>>>>>>>>>>>>>>>>>>> END   EQUATION 19 <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
! ------------------------------------------------------------------------------
! $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
! ------------------------------------------------------------------------------
! >>>>>>>>>>>>>>>>>>>>>>>>>> START EQUATION 20 <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
! ------------------------------------------------------------------------------
function calc_itr(nsr,jsr) result (itr)
! ------------------------------------------------------------------------------
! tautr = 180: Time constant of Ca transfer from NSR to JSR (ms)
! itr:  Translocation current of Ca ions from NSR to JSR (mM/ms)
! ------------------------------------------------------------------------------
! ------------------------ EQUATION  20 ----------------------------------------
  implicit none
  real(rp), intent(in) :: nsr,jsr
  real(rp)             :: itr
!
  itr = (nsr-jsr)*p_i_ttr;  !-EQUATION  20
!
  return
end function calc_itr
! ------------------------------------------------------------------------------
! >>>>>>>>>>>>>>>>>>>>>>>>>> END   EQUATION 20 <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
! ------------------------------------------------------------------------------
! $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
! ------------------------------------------------------------------------------
! >>>>>>>>>>>>>>>>>>>>>>>>>> START EQUATION 21 <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
! ------------------------------------------------------------------------------
subroutine conc_jsr (V,dt,itr,time,curr,conc,tvar,irel)
! ------------------------------------------------------------------------------
! --------------- CALCULATES NEW JSR Ca ION CONCENTRATION ----------------------
! ------------------------------------------------------------------------------
!.csqn : Calsequestrin Buffered Ca Concentration (mM)
!.itr  : Translocation current of Ca ions from NSR to JSR (mM/ms)
! ------------------------------------------------------------------------------
! irelcicr:  Ca release from JSR to myo. due to CICR (mM/ms)
! ireljsrol: Ca release from JSR to myo. due to JSR overload (mM/ms)
! ------------------------------- EQUATION 21 ----------------------------------
! orden de calculo
! ==>  itr
! ==> irelcicr =
! ==> ireljsrol=
! ------------------------------------------------------------------------------
  implicit none
  real(rp), intent(in)        :: V,dt,itr
  type(t_time), intent(inout) :: time
  type(t_conc), intent(inout) :: conc
  type(t_curr), intent(inout) :: curr
  type(t_var),  intent(inout) :: tvar
  real(rp), intent(out)       :: irel
  real(rp)                    :: csqn,ireljsrol,djsr,bjsr,cjsr

  call fun_irelcicr (V,dt,conc,tvar,time,curr)            !-EQUATION 21-A
  csqn = csqn_jsr (conc%jsr);                             !-EQUATION 21-B
  call fun_ireljsrol(dt,csqn,conc,time,tvar,ireljsrol)    !-EQUATION 21-C
!
  irel= tvar%irelcicr+ireljsrol;
  djsr = dt*(itr-irel);
  bjsr = p_csqnbar-csqn-djsr-conc%jsr+p_kmcsqn;
  cjsr = p_kmcsqn*(csqn+djsr+conc%jsr);
!
  conc%jsr = 0.5*(sqrt(bjsr*bjsr+4.0*cjsr)-bjsr);    !-EQUATION 21
!
  return
end subroutine conc_jsr
! ------------------------------------------------------------------------------
subroutine fun_irelcicr (V,dt,conc,tvar,time,curr)
! ------------------------------------------------------------------------------
! flag: Flag condition to test for dvdtmax
! dcaiont:  Rate of change of Ca entry
! dcaiontnew: New rate of change of Ca entry
! caiontold : Old rate of change of Ca entry
! ------------------------------- EQUATION 21-A --------------------------------
! gmaxrel = 150;// Max. rate constant of Ca release from JSR due to overload (ms^-1)
! i_too = 1/tauon = 1/tauoff
! ct_1  = 1.0/(1.0+exp (i_too_*4.0))
! tauon  = 0.5; tauoff = 0.5;
! ------------------------------------------------------------------------------
  implicit none
  real(rp), intent(in)        :: V,dt
  type(t_conc), intent(in)    :: conc
  type(t_var),  intent(inout) :: tvar
  type(t_time), intent(inout) :: time
  type(t_curr), intent(inout) :: curr
  real(rp)                    :: on,off,magrel
!
  curr%dcaiontnew = (curr%caiont-tvar%caiontold)/dt;
!
  if ((V > -35.0).and.(curr%dcaiontnew > tvar%dcaiont).and. (tvar%flag == 0)) then
    tvar%flag  = 1;
    time%tcicr = 0.0;
    on         = p_ct_1;
  else
    on   = 1.0/(1.0 +exp(p_i_too*(-time%tcicr+4.0))) ;
  endif
!
  off     = 1.0 - on;
  magrel  = p_gmaxrel/(1.0+exp((curr%caiont+5.0)/0.9));
  tvar%irelcicr= magrel*on*off*(conc%jsr-conc%cai);
  time%tcicr   = time%tcicr + dt;
  return
end subroutine fun_irelcicr
! ------------------------------------------------------------------------------
subroutine fun_ireljsrol(dt,csqn,conc,time,tvar,ireljsrol)
! ------------------------------------------------------------------------------
! grelbarjsrol: Rate constant of Ca release from JSR due to overload (ms^-1)
! tjsro       : t=0 at time of JSR overload (ms)
! greljsrol   : Rate constant of Ca release from JSR due to CICR (ms^-1)
! i_too = 1/tauon = 1/tauoff
! ------------------------------------------------------------------------------
! ----------------------------- EQUATION 21-C ----------------------------------
  implicit none
  real(rp), intent(in)        :: dt,csqn
  type(t_conc), intent(in)    :: conc
  type(t_var),  intent(inout) :: tvar
  type(t_time), intent(inout) :: time
  real(rp), intent(out)       :: ireljsrol
  real(rp)                    :: a_1,greljsrol

  if ((csqn >= p_csqnth) .and. (time%tjsrol > 50.0)) then
    tvar%grelbarjsrol= 4.0;   !.Spontaneous Release occured at time t
    ireljsrol   = 0.0;
    time%tjsrol = 0.0;
  else
    if (tvar%grelbarjsrol == 0.0) then
      ireljsrol = 0.0;
    else
      a_1       = exp(-time%tjsrol*p_i_too);
      greljsrol = tvar%grelbarjsrol*(1.0 - a_1)*a_1;
      ireljsrol = greljsrol*(conc%jsr-conc%cai);
    endif
  endif
  time%tjsrol    = time%tjsrol+dt;
  return
end subroutine fun_ireljsrol
! ------------------------------------------------------------------------------
function csqn_jsr (jsr) result(csqn)
! ------------------------------------------------------------------------------
! ------------------------------ EQUATION 21-B ---------------------------------
  implicit none
  real(rp), intent(in):: jsr
  real(rp)            :: csqn
!
  csqn = p_csqnbar*(jsr/(jsr+p_kmcsqn));
!
  return
end function csqn_jsr
! ------------------------------------------------------------------------------
! >>>>>>>>>>>>>>>>>>>>>>>>>> END   EQUATION 21 <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
! ------------------------------------------------------------------------------
! $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
! ------------------------------------------------------------------------------
! >>>>>>>>>>>>>>>>>>>>>>>>>> START EQUATION 22 <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
! ------------------------------------------------------------------------------
subroutine conc_nsr(dt,itr,conc,i_upleak,prm)
! ------------------------------------------------------------------------------
!                CALCULATES NEW NSR Ca ION CONCENTRATION
! ------------------------------------------------------------------------------
! ------------------------------- EQUATION 22 ----------------------------------
  implicit none
  real(rp), intent(in)        :: dt,itr
  type(t_conc), intent(inout) :: conc
  type(t_prm),intent(in)      :: prm
  real(rp), intent(out)       :: i_upleak
  real(rp)                    :: dnsr
!
  i_upleak=prm%A_iupbar*i_leak_up(conc%nsr,conc%cai,p_iupbar);           !-EQUATION 22-A
!
  dnsr     = dt*(i_upleak-itr*p_vj_vn); !.dnsr = dt*(iup-ileak-itr*vjsr/vnsr);
  conc%nsr = conc%nsr+dnsr;
  return
end subroutine conc_nsr
! ------------------------------------------------------------------------------
function i_leak_up(nsr,cai,iupbar) result(i_upleak)
! ------------------------------------------------------------------------------
! iup   : Ca uptake from myo. to NSR (mM/ms)
! ileak : Ca leakage from NSR to myo. (mM/ms)
! kleak : Rate constant of Ca leakage from NSR (ms^-1)
! ------------------------------- EQUATION 22-A --------------------------------
! ------------------------------------------------------------------------------
  implicit none
  real(rp), intent(in)   :: nsr,cai, iupbar
  real(rp)               :: i_upleak,ileak,iup, kleak
!
  kleak  = iupbar*p_invnsrbar
  ileak = kleak*nsr;
  iup   = iupbar*cai/(cai+p_kmup);
!
  i_upleak =iup-ileak;                 !-EQUATION 22-A
!
  return
end function i_leak_up
! ------------------------------------------------------------------------------
! >>>>>>>>>>>>>>>>>>>>>>>>>> END   EQUATION 22 <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
! ------------------------------------------------------------------------------
! $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
! ------------------------------------------------------------------------------
! >>>>>>>>>>>>>>>>>>>>>>>>>> START EQUATION 23 <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
! ------------------------------------------------------------------------------
subroutine conc_cai (dt,curr,conc,i_upleak,irel)
! ------------------------------------------------------------------------------
! ------------ CALCULATES NEW MYOPLASMIC Ca ION CONCENTRATION  -----------------
! ------------------------------------------------------------------------------
! irel =irelcicr+ireljsrol
! trpn: Troponin Buffered Ca Concentration (mM)
! caiont : Total Ca Ion Flow (uA/uF)
! dcai   : Change in myoplasmic Ca concentration (mM)
! catotal: Total myoplasmic Ca concentration (mM)
! bmyo   :  b Variable for analytical computation of [Ca] in myoplasm (mM)
! cmyo   :  c Variable for analytical computation of [Ca] in myoplasm (mM)
! dmyo   :  d Variable for analytical computation of [Ca] in myoplasm (mM)
! gpig;  :  Tribute to all the guinea pigs killed for the advancement of knowledge
! ------------------------------------------------------------------------------
  implicit none
  real(rp), intent(in)       :: dt,irel,i_upleak
  type(t_curr), intent(in)   :: curr
  type(t_conc), intent(inout):: conc
  real(rp)                   :: a_1,a_2,a_3,a_4,dcai,catotal,bmyo,cmyo,dmyo,  &
                                gpig,a_cos,trpn,cmdn

  trpn   = p_trpnbar*(conc%cai/(conc%cai+p_kmtrpn));        !.EQUATION 23-A
!
  cmdn   = p_cmdnbar*(conc%cai/(conc%cai+p_kmcmdn));        !.EQUATION 23-B
!
  a_1  = p_a_vmf*(curr%caiont/p_zca);
  a_2  = p_vn_vm*i_upleak - p_vj_vm*irel;
  dcai = -(a_1 + a_2)*dt;                               
!
  catotal= trpn + cmdn + dcai + conc%cai;

  a_1    = p_kmtrpn+p_kmcmdn; a_2 = p_kmcmdn*p_kmtrpn;

  bmyo   = p_cmdnbar + p_trpnbar - catotal + a_1;
  cmyo   =  a_2 - catotal*a_1 + (p_trpnbar*p_kmcmdn)+(p_cmdnbar*p_kmtrpn);
  dmyo   = -a_2*catotal;
  a_3    =  bmyo*bmyo;
  a_4    =  a_3 - 3.0*cmyo;
  gpig   = sqrt(a_4);
!
  a_cos  = acos((9.0*bmyo*cmyo-2.0*a_3*bmyo-27.0*dmyo)/(2.0*a_4**1.5));
!
  conc%cai = (2.0*gpig*cos(a_cos/3.0)-bmyo)/3.0;            !.EQUATION 23
!
return
end subroutine conc_cai
! ------------------------------------------------------------------------------
! >>>>>>>>>>>>>>>>>>>>>>>>>> END   EQUATION 23 <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
! ------------------------------------------------------------------------------
! $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
! ------------------------------------------------------------------------------
! >>>>>>>>>>>>>>>>>>>>>>>>>> START EQUATION 24 <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
! ------------------------------------------------------------------------------
!subroutine conc_cleft(dt,curr,conc,prm)
! ------------------------------------------------------------------------------
! taudiff = 1000; Diffusion Constant for Ion Movement from Bulk Medium to Cleft Space
! vcleft   Cleft volume (uL)
! vcleft = vcell*0.12/0.88;
! i_td   = 1/taudiff
! nabm= 140  Initial Bulk Medium Na (mM)
! ------------------------------------------------------------------------------
! --------------- CALCULATES NEW CLEFT ION CONCENTRATIONS ----------------------
! a_vfcl = acap/(vcleft*frdy))
! nabm      : Bulk Medium Na Concentration (mM)
! kbm       : Bulk Medium K Concentration (mM)
! cabm      : Bulk Medium Ca Concentration (mM)
! ------------------------------------------------------------------------------
!  implicit none
!  real(rp), intent(in)       :: dt
!  type(t_prm), intent(in)    :: prm
!  type(t_curr), intent(inout):: curr
!  type(t_conc), intent(inout):: conc
!  real(rp)                   :: dnao,dko,dcao
!
!  dnao     = dt*(p_i_td*(conc%nabm-prm%p_nao) + curr%naiont*p_a_vcf);
!  conc%nao = dnao+conc%nao;
!
!  dko      = dt*(p_i_td*(conc%kbm-prm%p_ko)+ curr%kiont*p_a_vcf);
!  conc%ko  = dko + prm%p_ko;
!
!  dcao     = dt*(p_i_td*(conc%cabm-prm%p_cao) + curr%caiont*0.5*p_a_vcf);
!  conc%cao = dcao + prm%p_cao;
!
!  return
!end subroutine conc_cleft
! ------------------------------------------------------------------------------
! >>>>>>>>>>>>>>>>>>>>>>>>>> END   EQUATION 24 <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
! ------------------------------------------------------------------------------
! ------------------------------------------------------------------------------
! $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
! ------------------------------------------------------------------------------
! >>>>>>>>>>>>>>>>>>>>>>>> START AUXILIAR EQUATIONS <<<<<<<<<<<<<<<<<<<<<<<<<<<<
! ------------------------------------------------------------------------------
! ------------------------------------------------------------------------------
function integral(yo,a,b,dt) result(y)
! ------------------------------------------------------------------------------
! y=integral(yo,a,b,dt)
!    Esta funcion integra diferenciales del tipo: dy/dt +a*y=b en forma exacta
! con condicion inicial yo
! ------------------------------------------------------------------------------
  implicit none
  real(rp), intent(in) :: yo,a,b,dt
  real(rp)             :: y,aux

  aux=b/a;
  y=(yo-aux)*exp(-a*dt)+aux;
  return
end function integral
! ------------------------------------------------------------------------------
function ib_cakna(A,B,ci,co,gi,go) result(ib)
! ------------------------------------------------------------------------------
  implicit none
  real(rp), intent(in) :: A,B,ci,co,gi,go
  real(rp)             :: ib,ea
!
  ea = exp(A);
  ib = B*(gi*ci*ea-go*co)/(ea-1.0);
!
  return
end function ib_cakna
! ------------------------------------------------------------------------------
! ------------------------------------------------------------------------------
! >>>>>>>>>>>>>>>>>>>>>>>> END   AUXILIAR EQUATIONS <<<<<<<<<<<<<<<<<<<<<<<<<<<<
! ------------------------------------------------------------------------------
end module mod_lr2k
! ------------------------------------------------------------------------------

