!------------------------------------------------------------------------------
! This are the parameters for the Luo-Rudy Model
! Constants
!------------------------------------------------------------------------------
real(rp), parameter, private :: p_pi     = 3.1415926535897932384626433832795_rp; !.Pi
real(rp), parameter, private :: p_r      = 8314.0_rp;            !.Universal Gas Constant (J/kmol*K)
real(rp), parameter, private :: p_frdy   = 96485.0_rp;           !.Faraday s Constant (C/mol)
real(rp), parameter, private :: p_temp   = 310.0_rp;               !.Temperature (K)
real(rp), parameter, private :: p_f_rt   = p_frdy/(p_r*p_temp);
real(rp), parameter, private :: p_rt_f   = (p_r*p_temp)/p_frdy;
real(rp), parameter, private :: p_f2_rt  = p_frdy*p_f_rt;
real(rp), parameter, private :: p_zca    = 2.0_rp;                 !.Ca valence
real(rp), parameter, private :: p_pca    = 0.00054_rp;             !.Permiability of membrane to Ca (cm/s)
real(rp), parameter, private :: p_gacai  = 1.0_rp;                 !.Activity coefficient of Ca
real(rp), parameter, private :: p_gacao  = 0.341_rp;               !.Activity coefficient of Ca
real(rp), parameter, private :: p_cazfrt = p_zca*p_f_rt;        !.z*F/(R*T)
real(rp), parameter, private :: p_capzfrt= p_pca*p_zca*p_cazfrt*p_frdy;    !.p*z^2*F^2/(R*T)
real(rp), parameter, private :: p_zna    = 1.0_rp;                 !.Na valence
real(rp), parameter, private :: p_pna    = 0.000000675_rp;         !.Permiability of membrane to Na (cm/s)
real(rp), parameter, private :: p_ganai  = 0.75_rp;                !.Activity coefficient of Na
real(rp), parameter, private :: p_ganao  = 0.75_rp;                !.Activity coefficient of Na
real(rp), parameter, private :: p_nazfrt = p_zna*p_f_rt;        !.z*F/(R*T)
real(rp), parameter, private :: p_napzfrt= p_pna*p_zna*p_nazfrt*p_frdy;    !.p*z^2*F^2/(R*T)
real(rp), parameter, private :: p_zk     = 1.0_rp;                 !.K valence
real(rp), parameter, private :: p_pk     = 0.000000193_rp;         !.Permiability of membrane to K (cm/s)
real(rp), parameter, private :: p_gaki   = 0.75_rp;                !.Activity coefficient of K
real(rp), parameter, private :: p_gako   = 0.75_rp;                !.Activity coefficient of K
real(rp), parameter, private :: p_kzfrt  = p_zk*p_f_rt;         !.z*F/(R*T)
real(rp), parameter, private :: p_kpzfrt = p_pk*p_zk*p_kzfrt*p_frdy;       !.p*z^2*F^2/(R*T)
real(rp), parameter, private :: p_l      = 0.01_rp;                !.Length of the cell (cm)
real(rp), parameter, private :: p_a      = 0.0011_rp;              !.Radius of the cell (cm)
real(rp), parameter, private :: p_vcell  = 1000.0_rp*p_pi*p_a**2.0*p_l;    !.Cell volume (uL)
real(rp), parameter, private :: p_ageo   = 2.0_rp*(p_pi*p_a**2.0+p_pi*p_a*p_l);!.Geometric membrane area (cm^2)
real(rp), parameter, private :: p_acap   = 2.0_rp*p_ageo;          !.Capacitive membrane area (cm^2)
real(rp), parameter, private :: p_vmyo   = p_vcell*0.68_rp;        !.Myoplasm volume (uL)
real(rp), parameter, private :: p_vmito  = p_vcell*0.26_rp;        !.Mitochondria volume (uL)
real(rp), parameter, private :: p_vsr    = p_vcell*0.06_rp;        !.SR volume (uL)
real(rp), parameter, private :: p_vnsr   = p_vcell*0.0552_rp;      !.NSR volume (uL)
real(rp), parameter, private :: p_vjsr   = p_vcell*0.0048_rp;      !.JSR volume (uL)
real(rp), parameter, private :: p_vcleft = p_vcell*0.12_rp/0.88_rp;   !.Cleft volume (uL)
real(rp), parameter, private :: p_vj_vn  = p_vjsr/p_vnsr;
real(rp), parameter, private :: p_vn_vm  = p_vnsr/p_vmyo;
real(rp), parameter, private :: p_vj_vm  = p_vjsr/p_vmyo;
real(rp), parameter, private :: p_a_vmf  = p_acap/(p_vmyo*p_frdy);
real(rp), parameter, private :: p_a_vcf  = p_acap/(p_vcleft*p_frdy);
! IKs
real(rp), parameter, private :: p_prnak = 0.01833_rp;      !.Na/K Permiability Ratio
! IKNa
real(rp), parameter, private :: p_kdkna= 66.0_rp;          !.Dissociation constant for Na dependance(mM)
! ICaL
real(rp), parameter, private :: p_kmca = 0.0006_rp; !.Half-saturation concentration
! IKATP
real(rp), parameter, private :: p_con_Mg_i  = 0.5_rp;	!.Concentracion intracelular de magnesio(mM).
real(rp), parameter, private :: p_atp_block = 1.0_rp;	!.factor de bloqueo
real(rp), parameter, private :: p_KmMg      = 2.1_rp;	!.Constante de semisaturacion para el Mg en (mM)
real(rp), parameter, private :: p_deltaMg   = 0.32_rp;	!.Distancia electrica para el Mg
real(rp), parameter, private :: p_KmNaIKatp = 25.9_rp;	!.Constante de semisaturacion para el Na en (mM)
real(rp), parameter, private :: p_deltaNa   = 0.35_rp;	!.Distancia electrica para el Na
real(rp), parameter, private :: p_Q10atp    = 1.3_rp;	!.Factor Q10 para la corriente KATP
real(rp), parameter, private :: p_p0atp     = 0.91_rp;	!.Probabilidad de apertura en ausencia de ATP
real(rp), parameter, private :: p_K1        = 1.0_rp;	!.Se utilizara para dar una distribucion gaussiana a KhATP
real(rp), parameter, private :: p_sigmaIKatp= 3.7e8_rp;	 !.Channel density, channels/cm^2, IKATP
real(rp), parameter, private :: p_gamma0    = 35.375e-9_rp; !.Nominal Conductance, IKATP
!  real(rp), parameter, private :: p_Hatp   = 1.48979330707073_rp; !.1.2997+0.7399*exp(-con_ADP_i/11.03752759) coeficiente de Hill
! INaCa
real(rp), parameter, private :: p_c2        = 0.0001_rp;   !.Half-saturation concentration of NaCa exhanger (mM)
real(rp), parameter, private :: p_gammas    = 0.15_rp;     !.Position of energy barrier controlling voltage dependance of inaca
! INaK
real(rp), parameter, private :: p_kmnai     = 10.0_rp;     !.Half-saturation concentration of NaK pump (mM)
real(rp), parameter, private :: p_kmko      = 1.5_rp;      !.Half-saturation concentration of NaK pump (mM)
! INsCa
real(rp), parameter, private :: p_pnsca  = 0.000000175_rp; !.Permiability of channel to Na and K (cm/s)
real(rp), parameter, private :: p_kmnsca = 0.0012_rp;      !.Half-saturation concentration of NSCa channel (mM)
real(rp), parameter, private :: p_b1     = p_f2_rt*p_pnsca*p_zna*p_zna; 
real(rp), parameter, private :: p_b2     = p_f2_rt*p_pnsca*p_zk*p_zk; 
! IpCa
real(rp), parameter, private :: p_kmpca  = 0.0005_rp;      !.Half-saturation concentration of sarcolemmal Ca pump (mM)
! Itr
real(rp), parameter, private :: p_i_ttr = 1.0/180.0_rp;
! JSR
real(rp), parameter, private :: p_kmcsqn =  0.8_rp; !.Equilibrium constant of buffering for CSQN (mM)
real(rp), parameter, private :: p_csqnbar= 10.0_rp; !.Max. [Ca] buffered in CSQN (mM)
! IrelCicr
real(rp), parameter, private :: p_gmaxrel= 150.0_rp;
real(rp), parameter, private :: p_tauon  = 0.5_rp;
real(rp), parameter, private :: p_i_too = 1.0_rp/p_tauon;
real(rp), parameter, private :: p_ct_1 = 3.353501304664781e-4_rp  != 1.0/(1.0+exp (i_too*4.0));
! Ireljsrol
real(rp), parameter, private :: p_csqnth = 8.75_rp; !.Threshold for release of Ca from CSQN due to JSR overload (mM)
! I_leak_up
real(rp), parameter, private :: p_kmup   = 0.00092_rp;!.Half-saturation concentration of iup (mM)
real(rp), parameter, private :: p_nsrbar = 15.0_rp;     !.Max. [Ca] in NSR (mM)
real(rp), parameter, private :: p_invnsrbar = 1.0_rp/p_nsrbar;
! Cai
real(rp), parameter, private :: p_trpnbar = 0.070_rp;  !.Max. [Ca] buffered in TRPN (mM)
real(rp), parameter, private :: p_kmtrpn  = 0.0005_rp; !.Equalibrium constant of buffering for TRPN (mM)
real(rp), parameter, private :: p_kmcmdn  = 0.00238_rp;!.Equalibrium constant of buffering for CMDN (mM)
real(rp), parameter, private :: p_cmdnbar = 0.050_rp;  !.Max. [Ca] buffered in CMDN (mM)
! Cleft
real(rp), parameter, private :: p_taudiff = 1000.0_rp
real(rp), parameter, private :: p_i_td    = 1.0_rp/p_taudiff;

! ------------------------------------------------------------------------------
! -- Parameters to be modified by the Mask
! ------------------------------------------------------------------------------
real(rp), parameter, private :: p_gna = 16.0_rp;            !.Max. Conductance of the Na Channel (mS/uF)
real(rp), parameter, private :: p_PCaL=  1.0_rp;            ! ICaL channel conduction reduction factor
real(rp), parameter, private :: p_gcat = 0.05_rp;           ! Max. Conductance of T Ca Channel (nS/uF)
real(rp), parameter, private :: p_gkrm = 0.02614_rp;        ! Max. Conductance of IKr channel (nS/uF)
real(rp), parameter, private :: p_gksm_epi = 0.433_rp;      ! Max. Conductance of IKs, EPI (nS/uF) (0.433)(0.587 C)
real(rp), parameter, private :: p_gksm_mid = 0.433_rp;      ! Max. Conductance of IKs, MID (nS/uF) (0.433)(0.166 C)
real(rp), parameter, private :: p_gksm_endo= 0.433_rp;      ! Max. Conductance of IKs, ENDO (nS/uF) (0.433)(0.357 C)
real(rp), parameter, private :: p_gkim = 0.75_rp;           ! Max. Conductance of IK1 (nS/uF)
real(rp), parameter, private :: p_gkp = 0.00552_rp;         ! Max. Conductance of IKp (nS/uF)
real(rp), parameter, private :: p_gkna = 0.12848_rp;        !.Maximum conductance of IKNa (mS/uF)
real(rp), parameter, private :: p_ATPi= 6.8_rp;	         !.Intracellular ATP concentration, IKATP
real(rp), parameter, private :: p_ADPi= 15.0_rp;	         !.Intracellular ADP concentration, IKATP
real(rp), parameter, private :: p_gitodv_epi = 0.5_rp;      ! Max conductance of Ito, EPI (0.2064 C)
real(rp), parameter, private :: p_gitodv_mid = 0.5_rp;      ! Max conductance of Ito, MID (0.1806)
real(rp), parameter, private :: p_gitodv_endo = 0.5_rp;     ! Max conductance of Ito,ENDO (0.0671)
real(rp), parameter, private :: p_c1     = 0.00025_rp;      !.Scaling factor for inaca (uA/uF)
real(rp), parameter, private :: p_ibarnak= 2.25_rp;         !.Max. current through Na-K pump (uA/uF)
real(rp), parameter, private :: p_ibarpca= 1.15_rp;         !.Max. Ca current through sarcolemmal Ca pump (uA/uF)
real(rp), parameter, private :: p_gcab = 0.003016_rp;       ! Max conductance of Ca background current
real(rp), parameter, private :: p_gnab = 0.004_rp;          !.Max conductance of Na background current (In the paper is 0.00141)
real(rp), parameter, private :: p_iupbar = 0.00875_rp;      !.Max. current through iup channel (mM/ms)
real(rp), parameter, private :: p_nao    = 140.0_rp;        !.Extracellular Na Concentration (mM)
real(rp), parameter, private :: p_ko     = 4.5_rp;          !.Extracellular K Concentration (mM)
real(rp), parameter, private :: p_cao    = 1.8_rp;          !.Extracellular Ca Concentration (mM)
! -----------------------------------------------------------------------------
!-- INITIAL CONDITIONS  20 min stimulation ------------------------------------
!------------------------------------------------------------------------------
real(rp), parameter :: Vi_LR = -8.7905698449e+01_rp
real(rp), parameter :: end_m =  9.5055375351e-04_rp, mid_m =  9.5055375351e-04_rp,&
                       epi_m =  9.5055375351e-04_rp, def_m =  9.5055375351e-04_rp;
real(rp), parameter :: end_h =  9.9210007056e-01_rp, mid_h =  9.9210007056e-01_rp,&
                       epi_h =  9.9210007056e-01_rp, def_h =  9.9210007056e-01_rp;
real(rp), parameter :: end_j =  9.9470185326e-01_rp, mid_j =  9.9470185326e-01_rp,&
                       epi_j =  9.9470185326e-01_rp, def_j =  9.9470185326e-01_rp;
real(rp), parameter :: end_d =  3.7839334180e-06_rp, mid_d =  3.7839334180e-06_rp,&
                       epi_d =  3.7839334180e-06_rp, def_d =  3.7839334180e-06_rp;
real(rp), parameter :: end_f =  9.8877143590e-01_rp, mid_f =  9.8877143590e-01_rp,&
                       epi_f =  9.8877143590e-01_rp, def_f =  9.8877143590e-01_rp;
real(rp), parameter :: end_xs1 =  4.3763849076e-02_rp, mid_xs1 =  4.3763849076e-02_rp,&
                       epi_xs1 =  4.3763849076e-02_rp, def_xs1 =  4.3763849076e-02_rp;
real(rp), parameter :: end_xs2 =  8.6841013627e-02_rp, mid_xs2 =  8.6841013627e-02_rp,&
                       epi_xs2 =  8.6841013627e-02_rp, def_xs2 =  8.6841013627e-02_rp;
real(rp), parameter :: end_xr =  2.1666506085e-03_rp, mid_xr =  2.1666506085e-03_rp,&
                       epi_xr =  2.1666506085e-03_rp, def_xr =  2.1666506085e-03_rp;
real(rp), parameter :: end_b =  1.0697627347e-03_rp, mid_b =  1.0697627347e-03_rp,&
                       epi_b =  1.0697627347e-03_rp, def_b =  1.0697627347e-03_rp;
real(rp), parameter :: end_g =  8.9908037486e-01_rp, mid_g =  8.9908037486e-01_rp,&
                       epi_g =  8.9908037486e-01_rp, def_g =  8.9908037486e-01_rp;
real(rp), parameter :: end_zdv =  1.2089200000e-02_rp, mid_zdv =  1.2089200000e-02_rp,&
                       epi_zdv =  1.2089200000e-02_rp, def_zdv =  1.2089200000e-02_rp;
real(rp), parameter :: end_ydv =  9.9997800000e-01_rp, mid_ydv =  9.9997800000e-01_rp,&
                       epi_ydv =  9.9997800000e-01_rp, def_ydv =  9.9997800000e-01_rp;
real(rp), parameter :: end_nai =  1.7935503228e+01_rp, mid_nai =  1.7935503228e+01_rp,&
                       epi_nai =  1.7935503228e+01_rp, def_nai =  1.7935503228e+01_rp;
real(rp), parameter :: end_nabm =  1.4000000000e+02_rp, mid_nabm =  1.4000000000e+02_rp,&
                       epi_nabm =  1.4000000000e+02_rp, def_nabm =  1.4000000000e+02_rp;
real(rp), parameter :: end_ki =  1.3136780621e+02_rp, mid_ki =  1.3136780621e+02_rp,&
                       epi_ki =  1.3136780621e+02_rp, def_ki =  1.3136780621e+02_rp;
real(rp), parameter :: end_kbm =  4.5000000000e+00_rp, mid_kbm =  4.5000000000e+00_rp,&
                       epi_kbm =  4.5000000000e+00_rp, def_kbm =  4.5000000000e+00_rp;
real(rp), parameter :: end_cai =  3.2880664181e-04_rp, mid_cai =  3.2880664181e-04_rp,&
                       epi_cai =  3.2880664181e-04_rp, def_cai =  3.2880664181e-04_rp;
real(rp), parameter :: end_cabm =  1.8000000000e+00_rp, mid_cabm =  1.8000000000e+00_rp,&
                       epi_cabm =  1.8000000000e+00_rp, def_cabm =  1.8000000000e+00_rp;
real(rp), parameter :: end_jsr =  7.3279575468e-01_rp, mid_jsr =  7.3279575468e-01_rp,&
                       epi_jsr =  7.3279575468e-01_rp, def_jsr =  7.3279575468e-01_rp;
real(rp), parameter :: end_nsr =  3.5859537250e+00_rp, mid_nsr =  3.5859537250e+00_rp,&
                       epi_nsr =  3.5859537250e+00_rp, def_nsr =  3.5859537250e+00_rp;
real(rp), parameter :: end_tjsrol =  0.0000000000e+00_rp, mid_tjsrol =  0.0000000000e+00_rp,&
                       epi_tjsrol =  0.0000000000e+00_rp, def_tjsrol =  0.0000000000e+00_rp;
real(rp), parameter :: end_tcicr =  0.0000000000e+00_rp, mid_tcicr =  0.0000000000e+00_rp,&
                       epi_tcicr =  0.0000000000e+00_rp, def_tcicr =  0.0000000000e+00_rp;
integer(ip), parameter :: end_flag = 0, mid_flag = 0,&
                          epi_flag = 0, def_flag = 0;
real(rp), parameter :: end_grelbarjsrol =  0.0000000000e+00_rp, mid_grelbarjsrol =  0.0000000000e+00_rp,&
                       epi_grelbarjsrol =  0.0000000000e+00_rp, def_grelbarjsrol =  0.0000000000e+00_rp;
real(rp), parameter :: end_dcaiont = 0.0000000000e+00_rp, mid_dcaiont = 0.0000000000e+00_rp,&
                       epi_dcaiont = 0.0000000000e+00_rp, def_dcaiont = 0.0000000000e+00_rp;
real(rp), parameter :: end_caiontold =  0.0000000000e+00_rp, mid_caiontold =  0.0000000000e+00_rp,&
                       epi_caiontold =  0.0000000000e+00_rp, def_caiontold =  0.0000000000e+00_rp;
real(rp), parameter :: end_irelcicr =  0.0000000000e+00_rp, mid_irelcicr =  0.0000000000e+00_rp,&
                       epi_irelcicr =  0.0000000000e+00_rp, def_irelcicr =  0.0000000000e+00_rp;
!------------------------------------------------------------------------------
!-- STRUCTURE PARAMETERS ------------------------------------------------------
!------------------------------------------------------------------------------
  integer(ip), parameter :: LR_ENDO_mod =  4;  ! Luo Rudy ENDO model      (VGP)
  integer(ip), parameter :: LR_MID_mod  =  5;  ! Luo Rudy MID model       (VGP)
  integer(ip), parameter :: LR_EPI_mod  =  6;  ! Luo Rudy EPI model       (VGP)
  integer(ip),parameter  :: nvar_lr     = 27;  ! Number of state variables
  integer(ip),parameter  :: ncur_lr     = 20;  ! Number of currents
  integer(ip),parameter  :: np_lr       = 23;  ! Number of modifiable parameters
!------------------------------------------------------------------------------
!-- ADAPTIVE TIME STEPING -----------------------------------------------------
!------------------------------------------------------------------------------
!.
  integer(ip), parameter    :: m_stpLR   =    3
  integer(ip), parameter    :: m_iteLR   =    5
  real(rp),    parameter    :: m_dvdtLR  = 0.15
  real(rp),    parameter    :: m_IrelLR  = 0.0075

! ------------------------------------------------------------------------------
!-- INITIAL CONDITIONS  OLD ---------------------------------------------------
!------------------------------------------------------------------------------
!  real(rp), parameter :: Vi_LR  = -88.654973_rp                        ! Resting potential (mV)
!  real(rp), parameter :: end_m  = 0.000838, mid_m  = 0.000838, &
!                         epi_m  = 0.000838, def_m  = 0.000838;  !.Na activation   (Fast Sodium Current)
!  real(rp), parameter :: end_h  = 0.993336, mid_h  = 0.993336, &
!                         epi_h  = 0.993336, def_h  = 0.993336;  !.Na inactivation (Fast Sodium Current)
!  real(rp), parameter :: end_j  = 0.995484, mid_j  = 0.995484, &
!                         epi_j  = 0.995484, def_j  = 0.995484;  !.Na inactivation (Fast Sodium Current)
!  real(rp), parameter :: end_d  = 0.000003, mid_d  = 0.000003, &
!                         epi_d  = 0.000003, def_d  = 0.000003;  !.Voltage dependant activation gate   (L-type Ca Channel)
!  real(rp), parameter :: end_f  = 0.999745, mid_f  = 0.999745, & 
!                         epi_f  = 0.999745, def_f  = 0.999745;  !.Voltage dependant inactivation gate (L-type Ca Channel)
!  real(rp), parameter :: end_xs1= 0.004503, mid_xs1= 0.004503, &
!                         epi_xs1= 0.004503, def_xs1= 0.004503;  !.Slowly Activating K time-dependant activation (Slowly Activating Potassium)
!  real(rp), parameter :: end_xs2= 0.004503, mid_xs2= 0.004503, &
!                         epi_xs2= 0.004503, def_xs2= 0.004503;  !.Slowly Activating K time-dependant activation (Slowly Activating Potassium)
!  real(rp), parameter :: end_xr = 0.000129, mid_xr = 0.000129, &
!                         epi_xr = 0.000129, def_xr = 0.000129;  !.Rapidly Activating K time-dependant activation (Rapidly Activating Potassium)
!  real(rp), parameter :: end_b  = 0.000994, mid_b  = 0.000994, &
!                         epi_b  = 0.000994, def_b  = 0.000994;  !.Voltage dependant activation gate   (T-type Ca Channel)
!  real(rp), parameter :: end_g  = 0.994041, mid_g  = 0.994041, &
!                         epi_g  = 0.994041, def_g  = 0.994041;  !.Voltage dependant inactivation gate (T-type Ca Channel)
!  real(rp), parameter :: end_zdv= 0.0120892, mid_zdv= 0.0120892,&
!                         epi_zdv= 0.0120892, def_zdv= 0.0120892; !.Ito activation  (Ito Transient Outward Current)
!  real(rp), parameter :: end_ydv= 0.999978, mid_ydv= 0.999978,  &
!                         epi_ydv= 0.999978, def_ydv= 0.999978;  !.Ito inactivation (Ito Transient Outward Current)
!  real(rp), parameter :: end_nai = 12.236437, mid_nai = 12.236437, &
!                         epi_nai = 12.236437, def_nai = 12.236437;  !.Initial Intracellular Na (mM)
!  real(rp), parameter :: end_nabm= 140.0, mid_nabm= 140.0,         &
!                         epi_nabm= 140.0, def_nabm= 140.0;      !.Initial Bulk Medium Na (mM)
!  real(rp), parameter :: end_ki  = 136.89149, mid_ki  = 136.89149, &
!                         epi_ki  = 136.89149, def_ki  = 136.89149;  !.Initial Intracellular K (mM)
!  real(rp), parameter :: end_kbm = 4.5, mid_kbm = 4.5,             &
!                         epi_kbm = 4.5, def_kbm = 4.5;         !.Initial Bulk Medium K (mM)      
!!.Modificacion del 30/11/06
!  real(rp), parameter :: end_cai = 0.000079, mid_cai = 0.000079,   &
!                         epi_cai = 0.000079, def_cai = 0.000079;   !.Initial Intracellular Ca (mM)
!  real(rp), parameter :: end_cabm= 1.8, mid_cabm= 1.8,             &
!                         epi_cabm= 1.8, def_cabm= 1.8;        !.Initial Bulk Medium Ca (mM)
!  real(rp), parameter :: end_jsr = 1.179991, mid_jsr = 1.179991,   &
!                         epi_jsr = 1.179991, def_jsr = 1.179991;   !.JSR Ca Concentration (mM)
!  real(rp), parameter :: end_nsr = 1.179991, mid_nsr = 1.179991,   &
!                         epi_nsr = 1.179991, def_nsr = 1.179991;   !.NSR Ca Concentration (mM)
!! ------------------------------------------------------------------------------
!  real(rp), parameter :: end_tjsrol  = 1000.0, mid_tjsrol  = 1000.0,&
!                         epi_tjsrol  = 1000.0, def_tjsrol  = 1000.0;  !.
!  real(rp), parameter :: end_tcicr   = 1000.0, mid_tcicr   = 1000.0,&
!                         epi_tcicr   = 1000.0, def_tcicr   = 1000.0;  !.
!  integer(ip), parameter :: end_flag = 0, mid_flag = 0,             &
!                            epi_flag = 0, def_flag = 0;                    !. Flag condition to test for dvdtmax
!! ------------------------------------------------------------------------------
!  real(rp), parameter :: end_grelbarjsrol=0.0, mid_grelbarjsrol=0.0,&
!                         epi_grelbarjsrol=0.0, def_grelbarjsrol=0.0;       !.Rate constant of Ca release from JSR due to overload (ms^-1)
!  real(rp), parameter :: end_dcaiont  =  0.0, mid_dcaiont  =  0.0,  &
!                         epi_dcaiont  =  0.0, def_dcaiont  =  0.0;       !.Rate of change of Ca entry
!  real(rp), parameter :: end_caiontold =  0.0, mid_caiontold =  0.0,&
!                         epi_caiontold =  0.0, def_caiontold =  0.0;       !.Old rate of change of Ca entry
!  real(rp), parameter :: end_irelcicr  =  0.0, mid_irelcicr  =  0.0,&
!                         epi_irelcicr  =  0.0, def_irelcicr  =  0.0;       !.Ca release from JSR to myo. due to CICR (mM/ms)

