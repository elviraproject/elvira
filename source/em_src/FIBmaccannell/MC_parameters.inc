!------------------------------------------------------------------------------
! This are the parameters for the MacCannell Model 2007 

! Constants
!
! Parameters that can be modified through the input file
!
  !.--K current ---------------------------------------------------------------
  real(rp), parameter, private :: p_gkv = 0.25;             ! nS/pF
  real(rp), parameter, private :: p_gk1 = 0.4822;  	!nS/pF
  !.--Na transport -------------------------------------------------------------
  real(rp), parameter, private :: p_gbna =0.0095;              ! nS/pF
  !.--Concentrations ----------------------------------------------------------
   real(rp), parameter, private :: p_Ko = 5.4;	! [mM]  valor de Grandi et al .2010     5.35->   Maleckar 2009
   real(rp), parameter, private :: p_Nao = 140;	! [mM]  valor de Grandi et al .2010   130.0110->   Maleckar 2009
   real(rp), parameter, private :: p_Nai =9.05; ! 8.2704125; [mM]  valor de Grandi et al .2010  8.5547->   Maleckar 2009  
  real(rp), parameter, private :: p_Ki =145;! 129.4; [mM] ->   Maleckar 2009
!------------------------------------------------------------------------------
!-- FIXED PARAMETERS ----------------------------------------------------------
!------------------------------------------------------------------------------
  real(rp), parameter, private :: p_R     = 8.314;			!. JK^-1kmol^-1  
  real(rp), parameter, private :: p_T     = 308.0;          !. K
  real(rp), parameter, private :: p_F     = 96.485;          !. C/mol
  real(rp), parameter, private :: p_RTF   = p_R*p_T/p_F;    !. 1/mV
  real(rp), parameter, private :: p_iRTF  = p_F/p_R/p_T;    !. mV
  !.--Capacitance --------------------------------------------------------------
 ! real(rp), parameter, private :: p_Cap   = 6.3e-12;	! [F]
 !.--Na Transport --------------------------------------------------------------
  real(rp), parameter, private :: p_INaKmax =2.002;	!. [pA/pF]
  real(rp), parameter, private :: p_KmKf =1.0;            !. [mmol/L]
  real(rp), parameter, private :: p_Kmnaf =11.0;          !. [mmol/L]
  real(rp), parameter, private :: p_Bf =-200.0;           !. [mV]
  real(rp), parameter, private :: p_Vrev =-150.0;         !. [mV]
!------------------------------------------------------------------------------
!-- INITIAL VALUES ------------------------------------------------------------
!------------------------------------------------------------------------------
  real(rp), parameter :: rf_ini =0.063508346419;      !0.0; en el aritculo, pero asi esta estabilizado
  real(rp), parameter :: sf_ini =0.976791239;        ! 1.0; en el articulo, pero asi esta estabilizado  
  !real(rp), parameter :: Vi_Fib = -49.6_rp      !
  real(rp), parameter :: Vi_Fib = -49.6      !
!------------------------------------------------------------------------------
!-- STRUCTURE PARAMETERS ------------------------------------------------------
!------------------------------------------------------------------------------
  integer(ip), parameter :: FIBRO_mod = 18;  ! Fibroblast model          (FBH)
  integer(ip),parameter :: nvar_Mac   = 2;   ! Number of state variables
  integer(ip),parameter :: ncur_Mac   = 5;   ! Number of currents
  integer(ip),parameter :: np_Mac     = 4;   ! Number of modifiable parameters
!------------------------------------------------------------------------------
!-- ADAPTIVE TIME STEPING -----------------------------------------------------
!------------------------------------------------------------------------------
!.  
  integer(ip), parameter    :: m_stpMC   =    5
  real(rp),    parameter    :: m_dvdtMC  =  1.0  
