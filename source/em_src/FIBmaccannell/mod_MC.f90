! This module implements the MacCannell model of 2007
!------------------------------------------------------------------------------
module mod_MacCannell
!------------------------------------------------------------------------------
  use mod_precision
  implicit none
  include 'MC_parameters.inc'
  !.
  type t_Fibm
    sequence
    real(rp) :: rf, sf
  end type t_Fibm
  !.
  type, private :: t_cur
    sequence
    real(rp) :: INaK, IbNa, IK1, IKv, Itot
  end type t_cur


  public  :: ic_MacCannell, MacCannell_A_P01  
 ! public  :: get_parameter_MacCannell, ic_MacCannell, MacCannell_A_P01
  private :: concentrations, currents
  !private :: gates

  type, public:: t_prm
    private
    real(rp) :: p_INaKmax;   !.1
    real(rp) :: p_gbna;   !.2
    real(rp) :: p_gK1;    !.3
    real(rp) :: p_gKv;    !.4
   
  end type t_prm
  
  
!------------------------------------------------------------------------------!
!  &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&  !
!------------------------------------------------------------------------------!
contains
!------------------------------------------------------------------------------!
subroutine write_state_FIB(lu_st)
implicit none
integer(ip), intent(in)   :: lu_st

    write(lu_st,'(A)', advance='no') 'rf '
    write(lu_st,'(A)',advance='no') 'sf '
end subroutine write_state_FIB
!------------------------------------------------------------------------------!
subroutine write_current_FIB(lu_cr)
implicit none
integer(ip), intent(in)   :: lu_cr

    write(lu_cr,'(A)', advance='no') 'INaK '
    write(lu_cr,'(A)',advance='no') 'IK1 '
    write(lu_cr,'(A)',advance='no') 'IKv '
    write(lu_cr,'(A)',advance='no') 'IbNa '
    write(lu_cr,'(A)',advance='no') 'Itot '
end subroutine write_current_FIB
!------------------------------------------------------------------------------!
function get_parameter_MacCannell (tcell) result (v_prm)
!------------------------------------------------------------------------------!
  implicit none
  integer(ip), intent(in) :: tcell
  real (rp)               :: v_prm(np_Mac)


  select case (tcell) !20 MacCannell
    case (FIBRO_mod)
      v_prm = (/ 1.0, 1.0, 1.0, 1.0 /)
    case default
      write (*,10) tcell; stop
  end select
  return
10 format ('###.Specified Cell type is not defined:',I3)
end function get_parameter_MacCannell
!------------------------------------------------------------------------------
subroutine put_param(v_prm,param)
!------------------------------------------------------------------------------
  implicit none
  real(rp), intent(in)     :: v_prm(:)
  type (t_prm), intent(out):: param
  
  param%p_INaKmax   = v_prm( 1)*p_INaKmax     !.  1
  param%p_gbna   = v_prm( 2)*p_gbna     !.  2
  param%p_gk1    = v_prm( 3)*p_gk1      !.  3
  param%p_gkv    = v_prm( 4)*p_gkv      !.  4

  return
end subroutine put_param
!------------------------------------------------------------------------------
function ic_MacCannell(ict) result(Fib_ic)
!------------------------------------------------------------------------------
! This function sets the initial conditions for the MacCannell model
! tt_m is the main structure which contains the membrane potential, ioninc 
! concentrations, and gate variables
!------------------------------------------------------------------------------
  implicit none
!
! Original MacCannell 2007
!
!  real(rp), parameter :: rf = 0.0; 
!  real(rp), parameter :: sf = 1.0;

  integer (ip), intent(in)  :: ict
  real(rp)                  :: Fib_ic(nvar_Mac)
  
  select case (ict)
  case(FIBRO_mod) !.----MacCannell
    Fib_ic = (/rf_ini, sf_ini /)
  end select
  !.

  return
end function ic_MacCannell
!------------------------------------------------------------------------------
subroutine get_me_struct(str_me,v_me)
!------------------------------------------------------------------------------
  implicit none
  type (t_Fibm), intent(in) :: str_me
  real(rp), intent(out)    :: v_me(nvar_Mac)
  v_me( 1) = str_me%rf
  v_me( 2) = str_me%sf
 
  return
end subroutine get_me_struct
!------------------------------------------------------------------------------
subroutine put_me_struct(v_me,str_me)
!------------------------------------------------------------------------------
  implicit none
  real(rp), intent(in)      :: v_me(:)
  type (t_Fibm), intent(out) :: str_me
  
  str_me%rf    =  v_me( 1);
  str_me%sf    =  v_me( 2);
   return
end subroutine put_me_struct
!-------------------------------------------------------------------------------
subroutine concentrations (dt, cur, prm, Fibm)
!-------------------------------------------------------------------------------
! Function to update Ion concentrations
!-------------------------------------------------------------------------------
  implicit none
  real(rp),     intent(in)      :: dt
  type (t_cur), intent(in)      :: cur
  type (t_prm), intent(in)      :: prm
  type (t_Fibm),   intent(inout) :: Fibm
  
  real(rp):: dKi, dNai

   ! Sodium Concentrations 
   dNai =0;

   ! Potassium Concentrations
   dKi =0; 
  
!   grm%Nai = grm%Nai + dNai * dt;
 !  grm%Ki = grm%Ki + dKi * dt;
  
   
  return
end subroutine concentrations
!------------------------------------------------------------------------------
subroutine gates(dt, U, Fibm)
!------------------------------------------------------------------------------
! This function updates the gating variables
!
!------------------------------------------------------------------------------
  implicit none
  real(rp),    intent(in)    :: dt, U
  type(t_Fibm), intent(inout) :: Fibm
  real (rp):: rfinf, sfinf,taurf,tausf    !
   
  ! IKv: Time and voltage dependent K Currenti
 
  rfinf= 1.0/(1 + exp(-(U + 20.0)/11.0));
  taurf= 20.3 + 138 * exp(-((( U + 20 ) / 25.9)**2));

  sfinf= 1.0 /(1 + exp ((U + 23.0)/7.0));
  tausf= 1574 + 5268 * exp(-(((U + 23 ) /22.7)**2));


 ! Update gates

  Fibm%rf = rfinf - (rfinf-Fibm%rf)*exp(-dt/taurf);
  Fibm%sf = sfinf - (sfinf-Fibm%sf)*exp(-dt/tausf);
    
  return
end subroutine gates
!------------------------------------------------------------------------------
subroutine currents ( U,  Fibm, Iion, prm, cur) 
!------------------------------------------------------------------------------
! This function computes currents and concentrations for the MacCannell model
!
!
! XXX:AA-BB, 2008
!------------------------------------------------------------------------------
  implicit none
  real(rp),    intent(in)   :: U
  type(t_Fibm), intent(in)   :: Fibm
  real(rp),    intent(out)  :: Iion
  type(t_prm), intent(in)   :: prm
  type(t_cur), intent(out)  :: cur
  !.
  real(rp):: ENa, EK, aK1, bK1


  !. Nerst Potentials
  ENa = p_RTF * log(p_Nao /p_Nai);  !32.8 con GPB 2010
  EK = -87.325488706291623;
   
 
  ! I_Nak: Na/K Pump current
 
  cur%INaK=( p_INaKmax *p_Ko /(p_Ko +p_KmKf))*((p_Nai**1.5)/((p_Nai**1.5)+ (p_Kmnaf**1.5)))*((U - p_Vrev ) / ( U - p_Bf));   ! (pA/pF)

! IKv: Time and voltage dependent K Current
 
 cur%IKv = p_gkv * Fibm%rf * Fibm%sf *( U - EK); ! pA/pF

  ! I_bNa: Background Na current
  
  cur%IbNa = p_gbna *( U - ENa); ! pA/pF
 
  ! I_K1: Inward rectifying K current

   aK1 = 0.1 / ( 1 + exp (0.06 * ( U - EK -200.0)));
   bK1= ( 3 * exp (0.0002 * ( U - EK +100)) + exp( 0.1 * (U - EK - 10))) / (1 + exp(-0.5 * (U - EK)));    
   cur%IK1=( p_gk1 * aK1 * (U -EK))/(aK1 + bK1);   ! pA/pF


 
  ! Membrane Potential
   cur%Itot = cur%INaK + cur%IbNa + cur%IK1 + cur%IKv;
   
  !------------------------------------
  Iion = cur%Itot;

  return
  
end subroutine currents
!-------------------------------------------------------------------------------
subroutine MacCannell_A_P01 (ict,dt,U,Iion,v_prm,v_gr,v_cr)
!-------------------------------------------------------------------------------
  implicit none
  integer(ip), intent(in)    :: ict   !.Cell Model 
  real(rp),    intent(in)    :: dt, U
  real(rp),    intent(out)   :: Iion
  real(rp),    intent(in)    :: v_prm(:)
  real(rp), intent(inout)    :: v_gr(nvar_Mac)
  real(rp),    intent(out)   :: v_cr(:)
  !.
  type(t_Fibm)                :: Fibm
  type (t_prm)               :: param
  type(t_cur)                :: crr
  !
  call put_me_struct(v_gr,Fibm)
  call put_param(v_prm,param)
  !
  call currents ( U, Fibm, Iion, param, crr)
 ! call concentrations ( dt, crr, param, Fibm)
  call gates( dt, U, Fibm)
  !  
  call get_me_struct(Fibm,v_gr)
  !.
  v_cr(1:ncur_Mac) =(/ crr%INaK, crr%IK1, crr%IKv, crr%IbNa, crr%Itot /)
  
  return
end subroutine MacCannell_A_P01
!-------------------------------------------------------------------------------
end module mod_MacCannell
!-------------------------------------------------------------------------------
