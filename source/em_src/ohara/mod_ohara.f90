!------------------------------------------------------------------------------
! This is the implementation for the O'Hara-Rudy Model 
! Thomas O'Hara, L�szl� Vir�g, Adr�s Varr�, Yoram Rudy
!   Simulation of the Undiseased Human Cardiac Ventricula Action Potential: Model
!   Formulation and Experimental Validation. PLoS Computational Biology (2011).
!   doi:10.1371/journal.pcbi.1002061
!
!------------------------------------------------------------------------------
module mod_ohara
!------------------------------------------------------------------------------
  use mod_precision
  implicit none
  include 'ohara_parameters.inc'
  !.
  type t_ohrm
    sequence
    real(rp) :: nai, nass, ki, kss, cai, cass, cansr, cajsr, m, hf, hs, j, &
                hsp, jp, mL, hL, hLp, a, i_F, iS, aP, iFp, iSp, d, ff, fs, &
                fcaf, fcas, jca, nca, ffp, fcafp, xrf, xrs, xs1, xs2, xk1, &
                Jrelnp, Jrelp, CaMKt       !

  end type t_ohrm
  !.
  type, private :: t_cur
    sequence
    real(rp) :: INa, INaL, Ito, ICaL, ICaNa, ICaK, IKr, IKs, IK1, INaCa_i, &
                INaCa_ss, INaCa, INaK, IKb, INab, IpCa, ICab
                
  end type t_cur
  
  
  public  :: ic_OHara, OHara_A_P01
  private :: model

  type, public:: t_prm
    private
    real(rp) :: GNaL;           !. 1
    real(rp) :: delta_factor;   !. 2
    real(rp) :: Gto;            !. 3
    real(rp) :: Jrel_0;         !. 4
    real(rp) :: Jrelp_0;        !. 5
    real(rp) :: Jupnp_0;        !. 6
    real(rp) :: Jupp_0;         !. 7
    real(rp) :: Bcai_factor;    !. 8
    real(rp) :: PCa;            !. 9
    real(rp) :: GKr;            !.10
    real(rp) :: GKs;            !.11
    real(rp) :: GK1;            !.12
    real(rp) :: Gncx;           !.13
    real(rp) :: Pnak;           !.14
    
    
  end type t_prm
  
  
!------------------------------------------------------------------------------!
!  &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&  !
!------------------------------------------------------------------------------!
contains
!------------------------------------------------------------------------------!
subroutine write_state_OH(lu_st)
implicit none
integer(ip), intent(in)   :: lu_st

    write(lu_st,'(A)',advance='no') '% '
    write(lu_st,'(A)', advance='no') 'Nai '
    write(lu_st,'(A)', advance='no') 'Nass '
    write(lu_st,'(A)', advance='no') 'Ki '
    write(lu_st,'(A)', advance='no') 'Kss '
    write(lu_st,'(A)', advance='no') 'Cai '
    write(lu_st,'(A)',advance='no') 'CaSS '
    write(lu_st,'(A)',advance='no') 'CanSR '
    write(lu_st,'(A)',advance='no') 'CajSR '
    write(lu_st,'(A)',advance='no') 'm '
    write(lu_st,'(A)',advance='no') 'hf '
    write(lu_st,'(A)',advance='no') 'hs '
    write(lu_st,'(A)',advance='no') 'j '
    write(lu_st,'(A)',advance='no') 'hsp '
    write(lu_st,'(A)',advance='no') 'jp '
    write(lu_st,'(A)',advance='no') 'mL '
    write(lu_st,'(A)',advance='no') 'hL '
    write(lu_st,'(A)',advance='no') 'hLp '
    write(lu_st,'(A)',advance='no') 'a '
    write(lu_st,'(A)',advance='no') 'iF '
    write(lu_st,'(A)',advance='no') 'iS '
    write(lu_st,'(A)',advance='no') 'ap '
    write(lu_st,'(A)',advance='no') 'iFp '
    write(lu_st,'(A)',advance='no') 'iSp '
    write(lu_st,'(A)',advance='no') 'd '
    write(lu_st,'(A)',advance='no') 'ff '
    write(lu_st,'(A)',advance='no') 'fs '
    write(lu_st,'(A)',advance='no') 'fcaf '
    write(lu_st,'(A)',advance='no') 'fcas '
    write(lu_st,'(A)',advance='no') 'jca '
    write(lu_st,'(A)',advance='no') 'nca '
    write(lu_st,'(A)',advance='no') 'ffp '
    write(lu_st,'(A)',advance='no') 'fcafp '
    write(lu_st,'(A)',advance='no') 'xrf '
    write(lu_st,'(A)',advance='no') 'xrs '
    write(lu_st,'(A)',advance='no') 'xs1 '
    write(lu_st,'(A)',advance='no') 'xs2 '
    write(lu_st,'(A)',advance='no') 'xk1 '
    write(lu_st,'(A)',advance='no') 'Jrelnp '
    write(lu_st,'(A)',advance='no') 'Jrelp '
    write(lu_st,'(A)',advance='no') 'CaMKt '
end subroutine write_state_OH
!------------------------------------------------------------------------------!
subroutine write_current_OH(lu_cr)
implicit none
integer(ip), intent(in)   :: lu_cr

    write(lu_cr,'(A)',advance='no') '% '
    write(lu_cr,'(A)',advance='no') 'INa '
    write(lu_cr,'(A)',advance='no') 'INaL '
    write(lu_cr,'(A)',advance='no') 'Ito '
    write(lu_cr,'(A)',advance='no') 'ICaL '
    write(lu_cr,'(A)',advance='no') 'ICaNa '
    write(lu_cr,'(A)',advance='no') 'ICaK '
    write(lu_cr,'(A)',advance='no') 'IKr '
    write(lu_cr,'(A)',advance='no') 'IKs '
    write(lu_cr,'(A)',advance='no') 'IK1 '
    write(lu_cr,'(A)',advance='no') 'INaCa_i '
    write(lu_cr,'(A)',advance='no') 'INaCa_ss '
    write(lu_cr,'(A)',advance='no') 'INaCa '
    write(lu_cr,'(A)',advance='no') 'INaK '
    write(lu_cr,'(A)',advance='no') 'IKb '
    write(lu_cr,'(A)',advance='no') 'INab '
    write(lu_cr,'(A)',advance='no') 'IpCa '
    write(lu_cr,'(A)',advance='no') 'ICab '
    write(lu_cr,'(A)',advance='no') 'Itot '
end subroutine write_current_OH
!------------------------------------------------------------------------------!
function pow(base,ex) result (pow_res)
  implicit none
  real(rp), intent(in) :: base,ex
  real(rp)             :: pow_res
  
  pow_res = base**ex;
  
end function pow
!------------------------------------------------------------------------------!
function get_parameter_OHR (tcell) result (v_prm)
!------------------------------------------------------------------------------!
  implicit none
  integer(ip), intent(in) :: tcell
  real (rp)               :: v_prm(np_ohr)

  select case (tcell) !18->ENDO, 19->MID, 20->EPI
  case (OH_ENDO_mod)
  v_prm = (/ GNaL_end, delta_factor_end, Gto_end, Jrel_0_end, Jrelp_0_end,  &
            Jupnp_0_end, Jupp_0_end, Bcai_factor_end, PCa_end, GKr_end,     &
            GKs_end, GK1_end, Gncx_end, Pnak_end/)
    
  case (OH_MID_mod)
  v_prm = (/ GNaL_mid, delta_factor_mid, Gto_mid, Jrel_0_mid, Jrelp_0_mid,  &
            Jupnp_0_mid, Jupp_0_mid, Bcai_factor_mid, PCa_mid, GKr_mid,      &
            GKs_mid, GK1_mid, Gncx_mid, Pnak_mid/)
  case(OH_EPI_mod)
  v_prm = (/ GNaL_epi, delta_factor_epi, Gto_epi, Jrel_0_epi, Jrelp_0_epi,  &
            Jupnp_0_epi, Jupp_0_epi, Bcai_factor_epi, PCa_epi, GKr_epi,      &
            GKs_epi, GK1_epi, Gncx_epi, Pnak_epi/)
            
  case default
    write (*,10) tcell; stop
  end select
  return
10 format ('###.Specified Cell type is not defined:',I3)
end function get_parameter_ohr
!------------------------------------------------------------------------------
subroutine put_param(v_prm,param)
!------------------------------------------------------------------------------
  implicit none
  real(rp), intent(in)     :: v_prm(:)
  type (t_prm), intent(out):: param
  
  param%GNaL         = v_prm( 1)     !. 1
  param%delta_factor = v_prm( 2)     !. 2
  param%Gto          = v_prm( 3)     !. 3
  param%Jrel_0       = v_prm( 4)     !. 4
  param%Jrelp_0      = v_prm( 5)     !. 5
  param%Jupnp_0      = v_prm( 6)     !. 6
  param%Jupp_0       = v_prm( 7)     !. 7
  param%Bcai_factor  = v_prm( 8)     !. 8
  param%PCa          = v_prm( 9)     !. 9
  param%GKr          = v_prm(10)     !.10
  param%GKs          = v_prm(11)     !.11  
  param%GK1          = v_prm(12)     !.12
  param%Gncx         = v_prm(13)     !.13
  param%Pnak         = v_prm(14)     !.14
  return
end subroutine put_param
!------------------------------------------------------------------------------
function ic_OHara(ict) result(ohr_ic)
!------------------------------------------------------------------------------
! This function sets the initial conditions for the O'Hara-Rudy Dynamic Model
! ohr_ic is the main structure which contains the ioninc concentrations, and 
! gate variables
!------------------------------------------------------------------------------
  implicit none
  integer (ip), intent(in)  :: ict
  real(rp)                  :: ohr_ic(nvar_ohr)
  
  select case (ict)
  case(OH_ENDO_mod) !.----ENDO
    ohr_ic = (/ end_nai, end_nass, end_ki, end_kss, end_cai, end_cass,       &
             end_cansr, end_cajsr, end_m, end_hf, end_hs, end_j, end_hsp,    &
             end_jp, end_mL, end_hL, end_hLp, end_a, end_iF, end_iS, end_ap, &
             end_iFp, end_iSp, end_d, end_ff, end_fs, end_fcaf, end_fcas,    &
             end_jca, end_nca, end_ffp, end_fcafp, end_xrf, end_xrs, end_xs1,&
             end_xs2, end_xk1, end_Jrelnp, end_Jrelp, end_CaMKt /)
  case(OH_MID_mod) !.----MID
    ohr_ic = (/ mid_nai, mid_nass, mid_ki, mid_kss, mid_cai, mid_cass,       &
             mid_cansr, mid_cajsr, mid_m, mid_hf, mid_hs, mid_j, mid_hsp,    &
             mid_jp, mid_mL, mid_hL, mid_hLp, mid_a, mid_iF, mid_iS, mid_ap, &
             mid_iFp, mid_iSp, mid_d, mid_ff, mid_fs, mid_fcaf, mid_fcas,    &
             mid_jca, mid_nca, mid_ffp, mid_fcafp, mid_xrf, mid_xrs, mid_xs1,&
             mid_xs2, mid_xk1, mid_Jrelnp, mid_Jrelp, mid_CaMKt /)
  case(OH_EPI_mod) !.----EPI
    ohr_ic = (/ epi_nai, epi_nass, epi_ki, epi_kss, epi_cai, epi_cass,       &
             epi_cansr, epi_cajsr, epi_m, epi_hf, epi_hs, epi_j, epi_hsp,    &
             epi_jp, epi_mL, epi_hL, epi_hLp, epi_a, epi_iF, epi_iS, epi_ap, &
             epi_iFp, epi_iSp, epi_d, epi_ff, epi_fs, epi_fcaf, epi_fcas,    &
             epi_jca, epi_nca, epi_ffp, epi_fcafp, epi_xrf, epi_xrs, epi_xs1,&
             epi_xs2, epi_xk1, epi_Jrelnp, epi_Jrelp, epi_CaMKt /)
  case default
    ohr_ic = (/ def_nai, def_nass, def_ki, def_kss, def_cai, def_cass,       &
             def_cansr, def_cajsr, def_m, def_hf, def_hs, def_j, def_hsp,    &
             def_jp, def_mL, def_hL, def_hLp, def_a, def_iF, def_iS, def_ap, &
             def_iFp, def_iSp, def_d, def_ff, def_fs, def_fcaf, def_fcas,    &
             def_jca, def_nca, def_ffp, def_fcafp, def_xrf, def_xrs, def_xs1,&
             def_xs2, def_xk1, def_Jrelnp, def_Jrelp, def_CaMKt /)
  end select
  !.
  return
end function ic_OHara
!------------------------------------------------------------------------------
subroutine get_me_struct(str_me,v_me)
!------------------------------------------------------------------------------
  implicit none
  type (t_ohrm), intent(in) :: str_me
  real(rp), intent(out)    :: v_me(nvar_ohr)
  
  v_me( 1) = str_me%nai
  v_me( 2) = str_me%nass
  v_me( 3) = str_me%ki
  v_me( 4) = str_me%kss
  v_me( 5) = str_me%cai
  v_me( 6) = str_me%cass
  v_me( 7) = str_me%cansr
  v_me( 8) = str_me%cajsr
  v_me( 9) = str_me%m
  v_me(10) = str_me%hf
  v_me(11) = str_me%hs
  v_me(12) = str_me%j
  v_me(13) = str_me%hsp
  v_me(14) = str_me%jp
  v_me(15) = str_me%mL
  v_me(16) = str_me%hL
  v_me(17) = str_me%hLp
  v_me(18) = str_me%a
  v_me(19) = str_me%i_F
  v_me(20) = str_me%iS
  v_me(21) = str_me%aP
  v_me(22) = str_me%iFp
  v_me(23) = str_me%iSp
  v_me(24) = str_me%d
  v_me(25) = str_me%ff
  v_me(26) = str_me%fs
  v_me(27) = str_me%fcaf
  v_me(28) = str_me%fcas
  v_me(29) = str_me%jca
  v_me(30) = str_me%nca
  v_me(31) = str_me%ffp
  v_me(32) = str_me%fcafp
  v_me(33) = str_me%xrf
  v_me(34) = str_me%xrs
  v_me(35) = str_me%xs1
  v_me(36) = str_me%xs2
  v_me(37) = str_me%xk1
  v_me(38) = str_me%Jrelnp
  v_me(39) = str_me%Jrelp
  v_me(40) = str_me%CaMKt
  
  return
end subroutine get_me_struct
!------------------------------------------------------------------------------
subroutine put_me_struct(v_me,str_me)
!------------------------------------------------------------------------------
  implicit none
  real(rp), intent(in)      :: v_me(:)
  type (t_ohrm), intent(out) :: str_me
  
  str_me%nai    =  v_me( 1);
  str_me%nass   =  v_me( 2);
  str_me%ki     =  v_me( 3);
  str_me%kss    =  v_me( 4);
  str_me%cai    =  v_me( 5);
  str_me%cass   =  v_me( 6);
  str_me%cansr  =  v_me( 7);
  str_me%cajsr  =  v_me( 8);
  str_me%m      =  v_me( 9);
  str_me%hf     =  v_me(10);
  str_me%hs     =  v_me(11);
  str_me%j      =  v_me(12);
  str_me%hsp    =  v_me(13);
  str_me%jp     =  v_me(14);
  str_me%mL     =  v_me(15);
  str_me%hL     =  v_me(16);
  str_me%hLp    =  v_me(17);
  str_me%a      =  v_me(18);
  str_me%i_F    =  v_me(19);
  str_me%iS     =  v_me(20);
  str_me%aP     =  v_me(21);
  str_me%iFp    =  v_me(22);
  str_me%iSp    =  v_me(23);
  str_me%d      =  v_me(24);
  str_me%ff     =  v_me(25);
  str_me%fs     =  v_me(26);
  str_me%fcaf   =  v_me(27);
  str_me%fcas   =  v_me(28);
  str_me%jca    =  v_me(29);
  str_me%nca    =  v_me(30);
  str_me%ffp    =  v_me(31);
  str_me%fcafp  =  v_me(32);
  str_me%xrf    =  v_me(33);
  str_me%xrs    =  v_me(34);
  str_me%xs1    =  v_me(35);
  str_me%xs2    =  v_me(36);
  str_me%xk1    =  v_me(37);
  str_me%Jrelnp =  v_me(38);
  str_me%Jrelp  =  v_me(39);
  str_me%CaMKt  =  v_me(40);
  
  return
end subroutine put_me_struct
!-------------------------------------------------------------------------------
subroutine model (U, dt, ohrm, prm, cur, Ist, Iion)
!-------------------------------------------------------------------------------
! subroutine to calculate the model
!-------------------------------------------------------------------------------
  implicit none
  real(rp),    intent(in)    :: dt, U
  real(rp),    intent(in)    :: Ist
  real(rp),    intent(out)   :: Iion
  type(t_ohrm),intent(inout) :: ohrm
  type(t_prm), intent(in)    :: prm
  type(t_cur), intent(out)   :: cur
  
  real(rp):: ENa, EK, EKs, Jrel, Jup, Jtr, Jdiff, JdiffNa, JdiffK, Jleak,      &
             CaMKa, CaMKb, vffrt, vfrt, mss, tm, hss, thf, ths, Ahf, Ahs, h,   &
             jss, tj, hssp, thsp, hp, tjp, GNa, fINap, mLss, tmL, hLss, thL,   &
             hLssp, thLp, fINaLp, ass, ta, iss, delta_epi, tiF, tiS, AiF, AiS, &
             i, assp, dti_develop, dti_recover, tiFp, tiSp, ip, fItop, dss, td,&
             fss, tff, tfs, Aff, Afs, f, fcass, tfcaf, tfcas, Afcaf, Afcas,    &
             fca, tjca, tffp, fp, tfcafp, fcap, Kmn, k2n, km2n, anca, PhiCaL,  &
             PhiCaNa, PhiCaK, zca, PCap, PCaNa, PCaK, PCaNap, PCaKp, fICaLp,   &
             xrss, txrf, txrs, Axrf, Axrs, xr, rkr, xs1ss, txs1, xs2ss, txs2,  &
             KsCa, xk1ss, txk1, rk1, kna1, kna2, kna3, kasymm, wna, wca, wnaca,&
             kcaon, kcaoff, qna, qca, hca, hna, h1, h2, h3, h4, h5, h6, h7, h8,&
             h9, h10, h11, h12, k1, k2, k3p, k3pp, k3, k4p, k4pp, k4, k5, k6,  &
             k7, k8, x1, x2, x3, x4, E1, E2, E3, E4, KmCaAct, allo, zna,       &
             JncxNa, JncxCa, k1p, k1m, k2p, k2m, k3m, k4m, Knai0, Knao0, delta,&
             Knai, Knao, Kki, Kko, MgADP, MgATP, Kmgatp, HH, eP, Khp, Knap,    &
             Kxkur, P, a1, b1, a2, b2, a3, b3, a4, b4, zk, JnakNa, JnakK, xkb, &
             GKb, PNab, PCab, GpCa, bt, a_rel, Jrel_inf, tau_rel, btp, a_relp, &
             Jrel_infp, tau_relp, fJrelp, Jupnp, Jupp, fJupp, Bcai, Bcass,     &
             Bcajsr
 
  CaMKb=CaMKo*(1.0-ohrm%CaMKt)/(1.0+KmCaM/ohrm%cass);
  CaMKa=CaMKb+ohrm%CaMKt;
 
  ENa=(R*T/Frdy)*log(nao/ohrm%nai);
  EK=(R*T/Frdy)*log(ko/ohrm%ki);
  EKs=(R*T/Frdy)*log((ko+0.01833*nao)/(ohrm%ki+0.01833*ohrm%nai));

  vffrt=U*Frdy*Frdy/(R*T);
  vfrt=U*Frdy/(R*T);

  mss=1.0/(1.0+exp((-(U+39.57))/9.871));
  tm=1.0/(6.765*exp((U+11.64)/34.77)+8.552*exp(-(U+77.42)/5.955));
  ohrm%m=mss-(mss-ohrm%m)*exp(-dt/tm);

  hss=1.0/(1+exp((U+82.90)/6.086));
  thf=1.0/(1.432e-5*exp(-(U+1.196)/6.285)+6.149*exp((U+0.5096)/20.27));
  ths=1.0/(0.009794*exp(-(U+17.95)/28.05)+0.3343*exp((U+5.730)/56.66));
  Ahf=0.99;
  Ahs=1.0-Ahf;
  ohrm%hf=hss-(hss-ohrm%hf)*exp(-dt/thf);
  ohrm%hs=hss-(hss-ohrm%hs)*exp(-dt/ths);
  h=Ahf*ohrm%hf+Ahs*ohrm%hs;

  jss=hss;
  tj=2.038+1.0/(0.02136*exp(-(U+100.6)/8.281)+0.3052*exp((U+0.9941)/38.45));
  ohrm%j=jss-(jss-ohrm%j)*exp(-dt/tj);
  
  hssp=1.0/(1+exp((U+89.1)/6.086));
  thsp=3.0*ths;
  ohrm%hsp=hssp-(hssp-ohrm%hsp)*exp(-dt/thsp);
  
  hp=Ahf*ohrm%hf+Ahs*ohrm%hsp;
  tjp=1.46*tj;
  ohrm%jp=jss-(jss-ohrm%jp)*exp(-dt/tjp);

  GNa=75.0;
  fINap=(1.0/(1.0+KmCaMK/CaMKa));
  cur%INa=GNa*(U-ENa)*ohrm%m*ohrm%m*ohrm%m*((1.0-fINap)*h*ohrm%j+fINap*hp*ohrm%jp);
  
  mLss=1.0/(1.0+exp((-(U+42.85))/5.264));
  tmL=tm;
  ohrm%mL=mLss-(mLss-ohrm%mL)*exp(-dt/tmL);

  hLss=1.0/(1.0+exp((U+87.61)/7.488));
  thL=200.0;
  ohrm%hL=hLss-(hLss-ohrm%hL)*exp(-dt/thL);
  
  hLssp=1.0/(1.0+exp((U+93.81)/7.488));
  thLp=3.0*thL;
  ohrm%hLp=hLssp-(hLssp-ohrm%hLp)*exp(-dt/thLp);

  fINaLp=(1.0/(1.0+KmCaMK/CaMKa));
  cur%INaL=prm%GNaL*(U-ENa)*ohrm%mL*((1.0-fINaLp)*ohrm%hL+fINaLp*ohrm%hLp);

  ass=1.0/(1.0+exp((-(U-14.34))/14.82));
  ta=1.0515/(1.0/(1.2089*(1.0+exp(-(U-18.4099)/29.3814)))+3.5/(1.0+exp((U+100.0)/29.3814)));
  ohrm%a=ass-(ass-ohrm%a)*exp(-dt/ta);

  iss=1.0/(1.0+exp((U+43.94)/5.711));
  delta_epi=1.0-prm%delta_factor*(0.95/(1.0+exp((U+70.0)/5.0)));
  tiF=4.562+1/(0.3933*exp((-(U+100.0))/100.0)+0.08004*exp((U+50.0)/16.59));
  tiS=23.62+1/(0.001416*exp((-(U+96.52))/59.05)+1.780e-8*exp((U+114.1)/8.079));
  tiF=tiF*delta_epi;
  tiS=tiS*delta_epi;
  AiF=1.0/(1.0+exp((U-213.6)/151.2));
  AiS=1.0-AiF;
  ohrm%i_F=iss-(iss-ohrm%i_F)*exp(-dt/tiF);
  ohrm%iS=iss-(iss-ohrm%iS)*exp(-dt/tiS);
  i=AiF*ohrm%i_F+AiS*ohrm%iS;

  assp=1.0/(1.0+exp((-(U-24.34))/14.82));
  ohrm%ap=assp-(assp-ohrm%ap)*exp(-dt/ta);
  
  dti_develop=1.354+1.0e-4/(exp((U-167.4)/15.89)+exp(-(U-12.23)/0.2154));
  dti_recover=1.0-0.5/(1.0+exp((U+70.0)/20.0));
  tiFp=dti_develop*dti_recover*tiF;
  tiSp=dti_develop*dti_recover*tiS;
  ohrm%iFp=iss-(iss-ohrm%iFp)*exp(-dt/tiFp);
  ohrm%iSp=iss-(iss-ohrm%iSp)*exp(-dt/tiSp);
  ip=AiF*ohrm%iFp+AiS*ohrm%iSp;

  fItop=(1.0/(1.0+KmCaMK/CaMKa));
  cur%Ito=prm%Gto*(U-EK)*((1.0-fItop)*ohrm%a*i+fItop*ohrm%ap*ip);

  dss=1.0/(1.0+exp((-(U+3.940))/4.230));
  td=0.6+1.0/(exp(-0.05*(U+6.0))+exp(0.09*(U+14.0)));
  ohrm%d=dss-(dss-ohrm%d)*exp(-dt/td);
  
  fss=1.0/(1.0+exp((U+19.58)/3.696));
  tff=7.0+1.0/(0.0045*exp(-(U+20.0)/10.0)+0.0045*exp((U+20.0)/10.0));
  tfs=1000.0+1.0/(0.000035*exp(-(U+5.0)/4.0)+0.000035*exp((U+5.0)/6.0));
  Aff=0.6;
  Afs=1.0-Aff;
  ohrm%ff=fss-(fss-ohrm%ff)*exp(-dt/tff);
  ohrm%fs=fss-(fss-ohrm%fs)*exp(-dt/tfs);
  f=Aff*ohrm%ff+Afs*ohrm%fs;

  fcass=fss;
  tfcaf=7.0+1.0/(0.04*exp(-(U-4.0)/7.0)+0.04*exp((U-4.0)/7.0));
  tfcas=100.0+1.0/(0.00012*exp(-U/3.0)+0.00012*exp(U/7.0));
  Afcaf=0.3+0.6/(1.0+exp((U-10.0)/10.0));
  Afcas=1.0-Afcaf;
  ohrm%fcaf=fcass-(fcass-ohrm%fcaf)*exp(-dt/tfcaf);
  ohrm%fcas=fcass-(fcass-ohrm%fcas)*exp(-dt/tfcas);
  fca=Afcaf*ohrm%fcaf+Afcas*ohrm%fcas;
  
  tjca=75.0;
  ohrm%jca=fcass-(fcass-ohrm%jca)*exp(-dt/tjca);

  tffp=2.5*tff;
  ohrm%ffp=fss-(fss-ohrm%ffp)*exp(-dt/tffp);
  
  fp=Aff*ohrm%ffp+Afs*ohrm%fs;
  tfcafp=2.5*tfcaf;
  ohrm%fcafp=fcass-(fcass-ohrm%fcafp)*exp(-dt/tfcafp);
  fcap=Afcaf*ohrm%fcafp+Afcas*ohrm%fcas;

  Kmn=0.002;
  k2n=1000.0;
  km2n=ohrm%jca*1.0;
  anca=1.0/(k2n/km2n+pow(1.0+Kmn/ohrm%cass,4.0_rp));
  ohrm%nca=anca*k2n/km2n-(anca*k2n/km2n-ohrm%nca)*exp(-km2n*dt);
  
  PhiCaL=4.0*vffrt*(ohrm%cass*exp(2.0*vfrt)-0.341*cao)/(exp(2.0*vfrt)-1.0);
  PhiCaNa=1.0*vffrt*(0.75*ohrm%nass*exp(1.0*vfrt)-0.75*nao)/(exp(1.0*vfrt)-1.0);
  PhiCaK=1.0*vffrt*(0.75*ohrm%kss*exp(1.0*vfrt)-0.75*ko)/(exp(1.0*vfrt)-1.0);
  zca=2.0;

  PCap=1.1*prm%PCa;
  PCaNa=0.00125*prm%PCa;
  PCaK=3.574e-4*prm%PCa;
  PCaNap=0.00125*PCap;
  PCaKp=3.574e-4*PCap;
  fICaLp=(1.0/(1.0+KmCaMK/CaMKa));
  cur%ICaL=(1.0-fICaLp)*prm%PCa*PhiCaL*ohrm%d*(f*(1.0-ohrm%nca)+ohrm%jca*fca*ohrm%nca)+fICaLp*PCap*PhiCaL*ohrm%d*(fp*(1.0-ohrm%nca)+ohrm%jca*fcap*ohrm%nca);
  cur%ICaNa=(1.0-fICaLp)*PCaNa*PhiCaNa*ohrm%d*(f*(1.0-ohrm%nca)+ohrm%jca*fca*ohrm%nca)+fICaLp*PCaNap*PhiCaNa*ohrm%d*(fp*(1.0-ohrm%nca)+ohrm%jca*fcap*ohrm%nca);
  cur%ICaK=(1.0-fICaLp)*PCaK*PhiCaK*ohrm%d*(f*(1.0-ohrm%nca)+ohrm%jca*fca*ohrm%nca)+fICaLp*PCaKp*PhiCaK*ohrm%d*(fp*(1.0-ohrm%nca)+ohrm%jca*fcap*ohrm%nca);

  xrss=1.0/(1.0+exp((-(U+8.337))/6.789));
  txrf=12.98+1.0/(0.3652*exp((U-31.66)/3.869)+4.123e-5*exp((-(U-47.78))/20.38));
  txrs=1.865+1.0/(0.06629*exp((U-34.70)/7.355)+1.128e-5*exp((-(U-29.74))/25.94));
  Axrf=1.0/(1.0+exp((U+54.81)/38.21));
  Axrs=1.0-Axrf;
  ohrm%xrf=xrss-(xrss-ohrm%xrf)*exp(-dt/txrf);
  ohrm%xrs=xrss-(xrss-ohrm%xrs)*exp(-dt/txrs);
  xr=Axrf*ohrm%xrf+Axrs*ohrm%xrs;

  rkr=1.0/(1.0+exp((U+55.0)/75.0))*1.0/(1.0+exp((U-10.0)/30.0));
  cur%IKr=prm%GKr*sqrt(ko/5.4)*xr*rkr*(U-EK);

  xs1ss=1.0/(1.0+exp((-(U+11.60))/8.932));
  txs1=817.3+1.0/(2.326e-4*exp((U+48.28)/17.80)+0.001292*exp((-(U+210.0))/230.0));
  ohrm%xs1=xs1ss-(xs1ss-ohrm%xs1)*exp(-dt/txs1);
  xs2ss=xs1ss;
  txs2=1.0/(0.01*exp((U-50.0)/20.0)+0.0193*exp((-(U+66.54))/31.0));
  ohrm%xs2=xs2ss-(xs2ss-ohrm%xs2)*exp(-dt/txs2);
 
  KsCa=1.0+0.6/(1.0+pow(3.8e-5/ohrm%cai,1.4_rp));
  cur%IKs=prm%GKs*KsCa*ohrm%xs1*ohrm%xs2*(U-EKs);

  xk1ss=1.0/(1.0+exp(-(U+2.5538*ko+144.59)/(1.5692*ko+3.8115)));
  txk1=122.2/(exp((-(U+127.2))/20.36)+exp((U+236.8)/69.33));
  ohrm%xk1=xk1ss-(xk1ss-ohrm%xk1)*exp(-dt/txk1);
  rk1=1.0/(1.0+exp((U+105.8-2.6*ko)/9.493));
  cur%IK1=prm%GK1*sqrt(ko)*rk1*ohrm%xk1*(U-EK);

  kna1=15.0;
  kna2=5.0;
  kna3=88.12;
  kasymm=12.5;
  wna=6.0e4;
  wca=6.0e4;
  wnaca=5.0e3;
  kcaon=1.5e6;
  kcaoff=5.0e3;
  qna=0.5224;
  qca=0.1670;
  hca=exp((qca*U*Frdy)/(R*T));
  hna=exp((qna*U*Frdy)/(R*T));
  h1=1+ohrm%nai/kna3*(1+hna);
  h2=(ohrm%nai*hna)/(kna3*h1);
  h3=1.0/h1;
  h4=1.0+ohrm%nai/kna1*(1+ohrm%nai/kna2);
  h5=ohrm%nai*ohrm%nai/(h4*kna1*kna2);
  h6=1.0/h4;
  h7=1.0+nao/kna3*(1.0+1.0/hna);
  h8=nao/(kna3*hna*h7);
  h9=1.0/h7;
  h10=kasymm+1.0+nao/kna1*(1.0+nao/kna2);
  h11=nao*nao/(h10*kna1*kna2);
  h12=1.0/h10;
  k1=h12*cao*kcaon;
  k2=kcaoff;
  k3p=h9*wca;
  k3pp=h8*wnaca;
  k3=k3p+k3pp;
  k4p=h3*wca/hca;
  k4pp=h2*wnaca;
  k4=k4p+k4pp;
  k5=kcaoff;
  k6=h6*ohrm%cai*kcaon;
  k7=h5*h2*wna;
  k8=h8*h11*wna;
  x1=k2*k4*(k7+k6)+k5*k7*(k2+k3);
  x2=k1*k7*(k4+k5)+k4*k6*(k1+k8);
  x3=k1*k3*(k7+k6)+k8*k6*(k2+k3);
  x4=k2*k8*(k4+k5)+k3*k5*(k1+k8);
  E1=x1/(x1+x2+x3+x4);
  E2=x2/(x1+x2+x3+x4);
  E3=x3/(x1+x2+x3+x4);
  E4=x4/(x1+x2+x3+x4);
  KmCaAct=150.0e-6;
  allo=1.0/(1.0+pow(KmCaAct/ohrm%cai,2.0_rp));
  zna=1.0;
  JncxNa=3.0*(E4*k7-E1*k8)+E3*k4pp-E2*k3pp;
  JncxCa=E2*k2-E1*k1;

  cur%INaCa_i=0.8*prm%Gncx*allo*(zna*JncxNa+zca*JncxCa);

  h1=1+ohrm%nass/kna3*(1+hna);
  h2=(ohrm%nass*hna)/(kna3*h1);
  h3=1.0/h1;
  h4=1.0+ohrm%nass/kna1*(1+ohrm%nass/kna2);
  h5=ohrm%nass*ohrm%nass/(h4*kna1*kna2);
  h6=1.0/h4;
  h7=1.0+nao/kna3*(1.0+1.0/hna);
  h8=nao/(kna3*hna*h7);
  h9=1.0/h7;
  h10=kasymm+1.0+nao/kna1*(1+nao/kna2);
  h11=nao*nao/(h10*kna1*kna2);
  h12=1.0/h10;
  k1=h12*cao*kcaon;
  k2=kcaoff;
  k3p=h9*wca;
  k3pp=h8*wnaca;
  k3=k3p+k3pp;
  k4p=h3*wca/hca;
  k4pp=h2*wnaca;
  k4=k4p+k4pp;
  k5=kcaoff;
  k6=h6*ohrm%cass*kcaon;
  k7=h5*h2*wna;
  k8=h8*h11*wna;
  x1=k2*k4*(k7+k6)+k5*k7*(k2+k3);
  x2=k1*k7*(k4+k5)+k4*k6*(k1+k8);
  x3=k1*k3*(k7+k6)+k8*k6*(k2+k3);
  x4=k2*k8*(k4+k5)+k3*k5*(k1+k8);
  E1=x1/(x1+x2+x3+x4);
  E2=x2/(x1+x2+x3+x4);
  E3=x3/(x1+x2+x3+x4);
  E4=x4/(x1+x2+x3+x4);
  KmCaAct=150.0e-6;
  allo=1.0/(1.0+pow(KmCaAct/ohrm%cass,2.0_rp));
  JncxNa=3.0*(E4*k7-E1*k8)+E3*k4pp-E2*k3pp;
  JncxCa=E2*k2-E1*k1;
  cur%INaCa_ss=0.2*prm%Gncx*allo*(zna*JncxNa+zca*JncxCa);

  cur%INaCa=cur%INaCa_i+cur%INaCa_ss;

  k1p=949.5;
  k1m=182.4;
  k2p=687.2;
  k2m=39.4;
  k3p=1899.0;
  k3m=79300.0;
  k4p=639.0;
  k4m=40.0;
  Knai0=9.073;
  Knao0=27.78;
  delta=-0.1550;
  Knai=Knai0*exp((delta*U*Frdy)/(3.0*R*T));
  Knao=Knao0*exp(((1.0-delta)*U*Frdy)/(3.0*R*T));
  Kki=0.5;
  Kko=0.3582;
  MgADP=0.05;
  MgATP=9.8;
  Kmgatp=1.698e-7;
  HH=1.0e-7;
  eP=4.2;
  Khp=1.698e-7;
  Knap=224.0;
  Kxkur=292.0;
  P=eP/(1.0+HH/Khp+ohrm%nai/Knap+ohrm%ki/Kxkur);
  a1=(k1p*pow(ohrm%nai/Knai,3.0_rp))/(pow(1.0+ohrm%nai/Knai,3.0_rp)+pow(1.0+ohrm%ki/Kki,2.0_rp)-1.0);
  b1=k1m*MgADP;
  a2=k2p;
  b2=(k2m*pow(nao/Knao,3.0_rp))/(pow(1.0+nao/Knao,3.0_rp)+pow(1.0+ko/Kko,2.0_rp)-1.0);
  a3=(k3p*pow(ko/Kko,2.0_rp))/(pow(1.0+nao/Knao,3.0_rp)+pow(1.0+ko/Kko,2.0_rp)-1.0);
  b3=(k3m*P*HH)/(1.0+MgATP/Kmgatp);
  a4=(k4p*MgATP/Kmgatp)/(1.0+MgATP/Kmgatp);
  b4=(k4m*pow(ohrm%ki/Kki,2.0_rp))/(pow(1.0+ohrm%nai/Knai,3.0_rp)+pow(1.0+ohrm%ki/Kki,2.0_rp)-1.0);
  x1=a4*a1*a2+b2*b4*b3+a2*b4*b3+b3*a1*a2;
  x2=b2*b1*b4+a1*a2*a3+a3*b1*b4+a2*a3*b4;
  x3=a2*a3*a4+b3*b2*b1+b2*b1*a4+a3*a4*b1;
  x4=b4*b3*b2+a3*a4*a1+b2*a4*a1+b3*b2*a1;
  E1=x1/(x1+x2+x3+x4);
  E2=x2/(x1+x2+x3+x4);
  E3=x3/(x1+x2+x3+x4);
  E4=x4/(x1+x2+x3+x4);
  zk=1.0;
  JnakNa=3.0*(E1*a3-E2*b3);
  JnakK=2.0*(E4*b1-E3*a1);

  cur%INaK=prm%Pnak*(zna*JnakNa+zk*JnakK);

  xkb=1.0/(1.0+exp(-(U-14.48)/18.34));
  GKb=0.003;
  cur%IKb=GKb*xkb*(U-EK);

  PNab=3.75e-10;
  cur%INab=PNab*vffrt*(ohrm%nai*exp(vfrt)-nao)/(exp(vfrt)-1.0);

  PCab=2.5e-8;
  cur%ICab=PCab*4.0*vffrt*(ohrm%cai*exp(2.0*vfrt)-0.341*cao)/(exp(2.0*vfrt)-1.0);

  GpCa=0.0005;
  cur%IpCa=GpCa*ohrm%cai/(0.0005+ohrm%cai);

  Iion=cur%INa+cur%INaL+cur%Ito+cur%ICaL+cur%ICaNa+cur%ICaK+cur%IKr+cur%IKs+cur%IK1+cur%INaCa+cur%INaK+cur%INab+cur%IKb+cur%IpCa+cur%ICab;

  ohrm%CaMKt=ohrm%CaMKt+dt*(aCaMK*CaMKb*(CaMKb+ohrm%CaMKt)-bCaMK*ohrm%CaMKt);

  JdiffNa=(ohrm%nass-ohrm%nai)/2.0;
  JdiffK=(ohrm%kss-ohrm%ki)/2.0;
  Jdiff=(ohrm%cass-ohrm%cai)/0.2;

  bt=4.75;
  a_rel=0.5*bt;
  Jrel_inf=prm%Jrel_0*a_rel*(-cur%ICaL)/(1.0+pow(1.5/ohrm%cajsr,8.0_rp));

  tau_rel=max(bt/(1.0+0.0123/ohrm%cajsr),0.005);
  ohrm%Jrelnp=Jrel_inf-(Jrel_inf-ohrm%Jrelnp)*exp(-dt/tau_rel);
  
  btp=1.25*bt;
  a_relp=0.5*btp;
  Jrel_infp=prm%Jrelp_0*a_relp*(-cur%ICaL)/(1.0+pow(1.5/ohrm%cajsr,8.0_rp));
  tau_relp=max(btp/(1.0+0.0123/ohrm%cajsr),0.005);
  ohrm%Jrelp=Jrel_infp-(Jrel_infp-ohrm%Jrelp)*exp(-dt/tau_relp);

  fJrelp=(1.0/(1.0+KmCaMK/CaMKa));
  Jrel=(1.0-fJrelp)*ohrm%Jrelnp+fJrelp*ohrm%Jrelp;

  Jupnp=prm%Jupnp_0*ohrm%cai/(ohrm%cai+0.00092);
  Jupp=prm%Jupp_0*ohrm%cai/(ohrm%cai+0.00092-0.00017);
  fJupp=(1.0/(1.0+KmCaMK/CaMKa));
  Jleak=0.0039375*ohrm%cansr/15.0;
  Jup=(1.0-fJupp)*Jupnp+fJupp*Jupp-Jleak;

  Jtr=(ohrm%cansr-ohrm%cajsr)/100.0;

  ohrm%nai=ohrm%nai+dt*(-(cur%INa+cur%INaL+3.0*cur%INaCa_i+3.0*cur%INaK+cur%INab)*Acap/(Frdy*vmyo)+JdiffNa*vss/vmyo);
  ohrm%nass=ohrm%nass+dt*(-(cur%ICaNa+3.0*cur%INaCa_ss)*Acap/(Frdy*vss)-JdiffNa);

  ohrm%ki=ohrm%ki+dt*(-(cur%Ito+cur%IKr+cur%IKs+cur%IK1+cur%IKb+Ist-2.0*cur%INaK)*Acap/(Frdy*vmyo)+JdiffK*vss/vmyo);
  ohrm%kss=ohrm%kss+dt*(-(cur%ICaK)*Acap/(Frdy*vss)-JdiffK);

  Bcai=1.0/(1.0+prm%Bcai_factor*cmdnmax*kmcmdn/pow(kmcmdn+ohrm%cai,2.0_rp)+trpnmax*kmtrpn/pow(kmtrpn+ohrm%cai,2.0_rp));
  ohrm%cai=ohrm%cai+dt*(Bcai*(-(cur%IpCa+cur%ICab-2.0*cur%INaCa_i)*Acap/(2.0*Frdy*vmyo)-Jup*vnsr/vmyo+Jdiff*vss/vmyo));

  Bcass=1.0/(1.0+BSRmax*KmBSR/pow(KmBSR+ohrm%cass,2.0_rp)+BSLmax*KmBSL/pow(KmBSL+ohrm%cass,2.0_rp));
  ohrm%cass=ohrm%cass+dt*(Bcass*(-(cur%ICaL-2.0*cur%INaCa_ss)*Acap/(2.0*Frdy*vss)+Jrel*vjsr/vss-Jdiff));

  ohrm%cansr=ohrm%cansr+dt*(Jup-Jtr*vjsr/vnsr);

  Bcajsr=1.0/(1.0+csqnmax*kmcsqn/pow(kmcsqn+ohrm%cajsr,2.0_rp));
  ohrm%cajsr=ohrm%cajsr+dt*(Bcajsr*(Jtr-Jrel));
  
  return
end subroutine model
!-------------------------------------------------------------------------------
subroutine OHara_A_P01 (ict,dt,U,Ist,Iion,v_prm,v_ohr,v_cr)
!-------------------------------------------------------------------------------
  implicit none
  integer(ip), intent(in)    :: ict                 !.Cell Model 
  real(rp),    intent(in)    :: dt, U
  real(rp),    intent(in)    :: Ist
  real(rp),    intent(out)   :: Iion
  real(rp),    intent(in)    :: v_prm(:)
  real(rp),    intent(inout) :: v_ohr(nvar_ohr)
  real(rp),    intent(out)   :: v_cr(ncur_ohr)
  !.
  type(t_ohrm)               :: ohrm
  type(t_prm)                :: prm
  type(t_cur)                :: cur
  !
  call put_me_struct(v_ohr,ohrm)
  call put_param(v_prm,prm)
  !
  call model(U, dt, ohrm, prm, cur, Ist, Iion)
   !  
  call get_me_struct(ohrm,v_ohr)
  !.
  v_cr(1:ncur_ohr) =(/ cur%INa, cur%INaL, cur%Ito, cur%ICaL, cur%ICaNa, cur%ICaK, &
                       cur%IKr, cur%IKs, cur%IK1, cur%INaCa_i, cur%INaCa_ss,      &
                       cur%INaCa, cur%INaK, cur%IKb, cur%INab, cur%IpCa, cur%ICab, &
                       Iion/)
  
  return
end subroutine OHara_A_P01
!-------------------------------------------------------------------------------
end module mod_ohara
!-------------------------------------------------------------------------------
