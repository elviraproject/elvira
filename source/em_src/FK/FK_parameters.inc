!------------------------------------------------------------------------------
! This are the parameters for the Fenton Karma Model with 4 variables
! Bueno-Orovio A, Cherry EM, Fenton F
! J Theo Biol 253:544-560, 2008.
!---Scaling factor for potential-----------------------------------------------
  real(rp), parameter, private :: p_Vscl        =  85.7_rp;          !. mV  
  real(rp), parameter, private :: p_Voff        =  -84.0_rp;         !. mV
  !.--Common Parameters -------------------------------------------------------
  real(rp), parameter, private :: p_uo           =  0.0_rp;           ! --
  real(rp), parameter, private :: p_um           =  0.3_rp;           ! --
  real(rp), parameter, private :: p_up           =  0.13_rp;          ! --
  real(rp), parameter, private :: p_invtvp       =  1.0/1.4506_rp;    ! --
  real(rp), parameter, private :: p_ts1          =  2.7342_rp;        ! --
  real(rp), parameter, private :: p_ks           =  2.0994_rp;        ! --
  real(rp), parameter, private :: p_us           =  0.9087_rp;        ! --
!----Parameters for each cell--------------------------------------------------
  real(rp), parameter, private :: p_uu_endo      =  1.56_rp;          ! --
  real(rp), parameter, private :: p_uu_mid       =  1.61_rp;          ! --
  real(rp), parameter, private :: p_uu_epi       =  1.55_rp;          ! --
  !
  real(rp), parameter, private :: p_uq_endo      =  0.2_rp;           ! --
  real(rp), parameter, private :: p_uq_mid       =  0.1_rp;           ! --
  real(rp), parameter, private :: p_uq_epi       =  0.006_rp;         ! --
  !
  real(rp), parameter, private :: p_ur_endo      =  0.006_rp;         ! --
  real(rp), parameter, private :: p_ur_mid       =  0.005_rp;         ! --
  real(rp), parameter, private :: p_ur_epi       =  0.006_rp;         ! --
  !
  real(rp), parameter, private :: p_tv1m_endo    =  75.0_rp;          ! --
  real(rp), parameter, private :: p_tv1m_mid     =  80.0_rp;          ! --
  real(rp), parameter, private :: p_tv1m_epi     =  60.0_rp;          ! --
  !
  real(rp), parameter, private :: p_tv2m_endo    =  10.0_rp;          ! --
  real(rp), parameter, private :: p_tv2m_mid     =  1.4506_rp;        ! --
  real(rp), parameter, private :: p_tv2m_epi     =  1150.0_rp;        ! --
  !
  real(rp), parameter, private :: p_tw1m_endo    =  6.0_rp;           ! --
  real(rp), parameter, private :: p_tw1m_mid     =  70.0_rp;          ! --
  real(rp), parameter, private :: p_tw1m_epi     =  60.0_rp;          ! --
  !
  real(rp), parameter, private :: p_tw2m_endo    =  140.0_rp;         ! --
  real(rp), parameter, private :: p_tw2m_mid     =  8.0_rp;           ! --
  real(rp), parameter, private :: p_tw2m_epi     =  15.0_rp;          ! --
  !
  real(rp), parameter, private :: p_kwm_endo     =  200.0_rp;         ! --
  real(rp), parameter, private :: p_kwm_mid      =  200.0_rp;         ! --
  real(rp), parameter, private :: p_kwm_epi      =  65.0_rp;          ! --
  !
  real(rp), parameter, private :: p_uwm_endo     =  0.016_rp;         ! --
  real(rp), parameter, private :: p_uwm_mid      =  0.016_rp;         ! --
  real(rp), parameter, private :: p_uwm_epi      =  0.03_rp;          ! --
  !
  real(rp), parameter, private :: p_invtwp_endo  =  1.0/280.0_rp;     ! --
  real(rp), parameter, private :: p_invtwp_mid   =  1.0/280.0_rp;     ! --
  real(rp), parameter, private :: p_invtwp_epi   =  1.0/200.0_rp;     ! --
  !
  real(rp), parameter, private :: p_invtfi_endo  =  1.0/0.1_rp;       ! --
  real(rp), parameter, private :: p_invtfi_mid   =  1.0/0.078_rp;     ! --
  real(rp), parameter, private :: p_invtfi_epi   =  1.0/0.11_rp;      ! --
  !
  real(rp), parameter, private :: p_to1_endo     =  470.0_rp;         ! --
  real(rp), parameter, private :: p_to1_mid      =  410.0_rp;         ! --
  real(rp), parameter, private :: p_to1_epi      =  400.0_rp;         ! --
  !
  real(rp), parameter, private :: p_to2_endo     =  6.0_rp;           ! --
  real(rp), parameter, private :: p_to2_mid      =  7.0_rp;           ! --
  real(rp), parameter, private :: p_to2_epi      =  6.0_rp;           ! --
  !
  real(rp), parameter, private :: p_tso1_endo    =  40.0_rp;          ! --
  real(rp), parameter, private :: p_tso1_mid     =  91.0_rp;          ! --
  real(rp), parameter, private :: p_tso1_epi     =  30.0181_rp;       ! --
  !
  real(rp), parameter, private :: p_tso2_endo    =  1.2_rp;           ! --
  real(rp), parameter, private :: p_tso2_mid     =  0.8_rp;           ! --
  real(rp), parameter, private :: p_tso2_epi     =  0.9957_rp;        ! --
  !
  real(rp), parameter, private :: p_kso_endo     =  2.0_rp;           ! --
  real(rp), parameter, private :: p_kso_mid      =  2.1_rp;           ! --
  real(rp), parameter, private :: p_kso_epi      =  2.0458_rp;        ! --
  !
  real(rp), parameter, private :: p_uso_endo     =  0.65_rp;          ! --
  real(rp), parameter, private :: p_uso_mid      =  0.6_rp;           ! --
  real(rp), parameter, private :: p_uso_epi      =  0.65_rp;          ! --
  !
  real(rp), parameter, private :: p_ts2_endo     =  2.0_rp;           ! --
  real(rp), parameter, private :: p_ts2_mid      =  4.0_rp;           ! --
  real(rp), parameter, private :: p_ts2_epi      =  16.0_rp;          ! --
  !
  real(rp), parameter, private :: p_invtsi_endo  =  1.0/2.9013_rp;    ! --
  real(rp), parameter, private :: p_invtsi_mid   =  1.0/3.3849_rp;    ! --
  real(rp), parameter, private :: p_invtsi_epi   =  1.0/1.8875_rp;    ! --
  !
  real(rp), parameter, private :: p_invtwinf_endo=  1.0/0.0273_rp;    ! --
  real(rp), parameter, private :: p_invtwinf_mid =  1.0/0.01_rp;      ! --
  real(rp), parameter, private :: p_invtwinf_epi =  1.0/0.07_rp;      ! --
  !
  real(rp), parameter, private :: p_twinfst_endo =  0.78_rp;          ! --
  real(rp), parameter, private :: p_twinfst_mid  =  0.5_rp;           ! --
  real(rp), parameter, private :: p_twinfst_epi  =  0.94_rp;          ! --
!------------------------------------------------------------------------------
!-- INITIAL CONDITIONS ------------------------------------------------------
!------------------------------------------------------------------------------
  real(rp), parameter :: end_v    =   1.0_rp, mid_v    =   1.0_rp,         &
                         epi_v    =   1.0_rp, fkd_v    =   1.0_rp;        !
  real(rp), parameter :: end_w    =   1.0_rp,  mid_w   =   1.0_rp,         &
                         epi_w    =   1.0_rp,  fkd_w   =   1.0_rp;        !
  real(rp), parameter :: end_s    =   0.0_rp,  mid_s   =   0.0_rp,         &      
                         epi_s    =   0.0_rp,  fkd_s   =   0.0_rp;        ! 
  real(rp), parameter :: Vi_FK    =   p_Voff
!------------------------------------------------------------------------------
!-- STRUCTURE PARAMETERS ------------------------------------------------------
!------------------------------------------------------------------------------
  integer(ip), parameter :: BO_ENDO_mod =  7;  ! Bueno Orovio ENDO model  (PHV)
  integer(ip), parameter :: BO_MID_mod  =  8;  ! Bueno Orovio MID model   (PHV)
  integer(ip), parameter :: BO_EPI_mod  =  9;  ! Bueno Orovio EPI model   (PHV)
  integer(ip),parameter :: nvar_fk      =  3;  ! Number os state parameters
  integer(ip),parameter :: ncur_fk      =  4;  ! Number of currents
  integer(ip),parameter :: np_fk        = 30;  ! Number of modifiable parameters
!------------------------------------------------------------------------------
!-- ADAPTIVE TIME STEPING -----------------------------------------------------
!------------------------------------------------------------------------------
!.
  integer(ip), parameter    :: m_stpFK   =   5
  integer(ip), parameter    :: m_iteFK   =   5
  real(rp),    parameter    :: m_dvdtFK  = 1.0_rp
