! This module implements the Fenton Karma Model of 2008
! 30-11-2009: FK_A_P01 returns currents vector for output (EAH)
!
!------------------------------------------------------------------------------
module mod_FK
!------------------------------------------------------------------------------
  use mod_precision
  implicit none
  include 'FK_parameters.inc'
  !.
  type t_FKm
    sequence
    real(rp)  ::  vg,  wg,  sg
  end type t_FKm
  !.
  type, private :: t_cur
    sequence
    real(rp)  ::  Ifi,  Iso,  Isi
  end type t_cur

  type, public:: t_prm
    private
    real(rp) :: p_Vscl           !. 1
    real(rp) :: p_Voff           !. 2
    real(rp) :: p_uo             !. 3
    real(rp) :: p_um             !. 4
    real(rp) :: p_up             !. 5
    real(rp) :: p_invtvp         !. 6
    real(rp) :: p_ts1            !. 7
    real(rp) :: p_ks             !. 8
    real(rp) :: p_us             !. 9
    real(rp) :: p_uu             !. 10
    real(rp) :: p_uq             !. 11
    real(rp) :: p_ur             !. 12
    real(rp) :: p_tv1m           !. 13
    real(rp) :: p_tv2m           !. 14
    real(rp) :: p_tw1m           !. 15
    real(rp) :: p_tw2m           !. 16
    real(rp) :: p_kwm            !. 17
    real(rp) :: p_uwm            !. 18
    real(rp) :: p_invtwp         !. 19
    real(rp) :: p_invtfi         !. 20
    real(rp) :: p_to1            !. 21
    real(rp) :: p_to2            !. 22
    real(rp) :: p_tso1           !. 23
    real(rp) :: p_tso2           !. 24
    real(rp) :: p_kso            !. 25
    real(rp) :: p_uso            !. 26
    real(rp) :: p_ts2            !. 27
    real(rp) :: p_invtsi         !. 28
    real(rp) :: p_invtwinf       !. 29
    real(rp) :: p_twinfst        !. 30
  end type t_prm
!  
  public  :: get_parameter_FK, ic_FK, FK_A_P01
  private :: put_param, gates_curr
!------------------------------------------------------------------------------!
!  &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&  !
!------------------------------------------------------------------------------!
contains
!------------------------------------------------------------------------------!
subroutine write_state_BO(lu_st)
implicit none
integer(ip), intent(in)   :: lu_st

    write(lu_st,'(A)', advance='no') '%vg '
    write(lu_st,'(A)',advance='no') 'wg '
    write(lu_st,'(A)',advance='no') 'sg '
end subroutine write_state_BO
!------------------------------------------------------------------------------!
subroutine write_current_BO(lu_cr)
implicit none
integer(ip), intent(in)   :: lu_cr

    write(lu_cr,'(A)', advance='no') '%Ifi '
    write(lu_cr,'(A)',advance='no') 'Iso '
    write(lu_cr,'(A)',advance='no') 'Isi '
    write(lu_cr,'(A)',advance='no') 'Itot '
end subroutine write_current_BO
!------------------------------------------------------------------------------!
function get_parameter_FK (tcell) result (v_prm)
!------------------------------------------------------------------------------!
  implicit none
  integer(ip), intent(in) :: tcell
  real(rp)                :: v_prm(np_fk)

  select case(tcell)
  case (BO_ENDO_mod);
  v_prm = (/ p_Vscl, p_Voff, p_uo, p_um, p_up, p_invtvp, p_ts1,           &
             p_ks, p_us, p_uu_endo, p_uq_endo, p_ur_endo, p_tv1m_endo,    &
             p_tv2m_endo, p_tw1m_endo, p_tw2m_endo, p_kwm_endo,           &
             p_uwm_endo, p_invtwp_endo, p_invtfi_endo, p_to1_endo,        &
             p_to2_endo, p_tso1_endo, p_tso2_endo, p_kso_endo, p_uso_endo,&
             p_ts2_endo, p_invtsi_endo, p_invtwinf_endo, p_twinfst_endo  /)
  case (BO_MID_mod);
  v_prm = (/ p_Vscl, p_Voff, p_uo, p_um, p_up, p_invtvp, p_ts1,           &
             p_ks, p_us, p_uu_mid, p_uq_mid, p_ur_mid, p_tv1m_mid,        &
             p_tv2m_mid, p_tw1m_mid, p_tw2m_mid, p_kwm_mid,               &
             p_uwm_mid, p_invtwp_mid, p_invtfi_mid, p_to1_mid,            &
             p_to2_mid, p_tso1_mid, p_tso2_mid, p_kso_mid, p_uso_mid,     &
             p_ts2_mid, p_invtsi_mid, p_invtwinf_mid, p_twinfst_mid /)
  case (BO_EPI_mod);
  v_prm = (/ p_Vscl, p_Voff, p_uo, p_um, p_up, p_invtvp, p_ts1,           &
             p_ks,p_us, p_uu_epi, p_uq_epi, p_ur_epi, p_tv1m_epi,         &
             p_tv2m_epi, p_tw1m_epi, p_tw2m_epi, p_kwm_epi,               &
             p_uwm_epi, p_invtwp_epi, p_invtfi_epi, p_to1_epi,            &
             p_to2_epi, p_tso1_epi, p_tso2_epi, p_kso_epi, p_uso_epi,     &
             p_ts2_epi, p_invtsi_epi, p_invtwinf_epi, p_twinfst_epi /)
  case default;
  write(*,10) tcell; stop
  end select
  !.
  return
  10 format ('###.Specified Cell type is not defined:',I3)
end function get_parameter_FK
!------------------------------------------------------------------------------
subroutine put_param(v_prm,param)
!------------------------------------------------------------------------------
  implicit none
  real(rp), intent(in)    :: v_prm(np_fk)
  type (t_prm)            :: param

  param%p_Vscl    = v_prm(1)
  param%p_Voff    = v_prm(2)
  param%p_uo      = v_prm(3)
  param%p_um      = v_prm(4)
  param%p_up      = v_prm(5)
  param%p_invtvp  = v_prm(6)
  param%p_ts1     = v_prm(7)
  param%p_ks      = v_prm(8)
  param%p_us      = v_prm(9)
  param%p_uu      = v_prm(10)
  param%p_uq      = v_prm(11)
  param%p_ur      = v_prm(12)
  param%p_tv1m    = v_prm(13)
  param%p_tv2m    = v_prm(14)
  param%p_tw1m    = v_prm(15)
  param%p_tw2m    = v_prm(16)
  param%p_kwm     = v_prm(17)
  param%p_uwm     = v_prm(18)
  param%p_invtwp  = v_prm(19)
  param%p_invtfi  = v_prm(20)
  param%p_to1     = v_prm(21)
  param%p_to2     = v_prm(22)
  param%p_tso1    = v_prm(23)
  param%p_tso2    = v_prm(24)
  param%p_kso     = v_prm(25)
  param%p_uso     = v_prm(26)
  param%p_ts2     = v_prm(27)
  param%p_invtsi  = v_prm(28)
  param%p_invtwinf= v_prm(29)
  param%p_twinfst = v_prm(30)
  return
end subroutine put_param
!------------------------------------------------------------------------------
function ic_FK(ict) result(FK_ic)
!------------------------------------------------------------------------------
! This function sets the initial conditions for the 4 var Fenton Karma Model
! tt_m is the main structure which contains the membrane potential, and gate variables
!------------------------------------------------------------------------------
  implicit none
!
! Original Bueno et al., 2008
! 
!.
  integer (ip), intent(in)  :: ict
  real(rp)                  :: FK_ic(nvar_fk)
  
  select case (ict)
  case(BO_ENDO_mod) !.----ENDO
    FK_ic = (/ end_v, end_w, end_s /)
  case(BO_MID_mod) !.----MID
    FK_ic = (/ mid_v, mid_w, mid_s /)
  case(BO_EPI_mod) !.----EPI
    FK_ic = (/ epi_v, epi_w, epi_s /)
  case default
    FK_ic = (/ fkd_v, fkd_w, fkd_s /)
  end select
  !.
  return
end function ic_FK
!------------------------------------------------------------------------------
subroutine get_me_struct(str_me,v_me)
!------------------------------------------------------------------------------
  implicit none
  type (t_FKm), intent(in) :: str_me
  real(rp), intent(out)    :: v_me(nvar_fk)
  
  v_me(1) = str_me%vg
  v_me(2) = str_me%wg
  v_me(3) = str_me%sg
  return
end subroutine get_me_struct
!------------------------------------------------------------------------------
subroutine put_me_struct(v_me,str_me)
!------------------------------------------------------------------------------
  implicit none
  real(rp), intent(in)      :: v_me(nvar_fk)
  type (t_FKm), intent(out) :: str_me
  
  str_me%vg = v_me(1)
  str_me%wg = v_me(2)
  str_me%sg = v_me(3)
  return
end subroutine put_me_struct
!------------------------------------------------------------------------------
subroutine gates_curr(dt, U, prm, FKm, cur, Iion)
!------------------------------------------------------------------------------
! This function updates the gating variables
!
!------------------------------------------------------------------------------
  implicit none
  real(rp),    intent(in)    :: dt, U
  type (t_prm), intent(in)   :: prm
  type(t_FKm), intent(inout) :: FKm
  type(t_cur), intent(out)   :: cur
  real(rp),    intent(out)   :: Iion

  !.
  real (rp)                  :: Hm, Hp, Hq, Hr, uFK
  real (rp)                  :: Dvg, Dwg, Dsg
  real (rp)                  :: TAUVm,TAUWm,TAUso,TAUs,TAUo,Vinf,Winf

  uFK = (U-prm%p_Voff)/prm%p_Vscl
  Hm = 1.0_rp; Hp = 1.0_rp; Hq = 1.0_rp; Hr=1.0_rp
  if (uFK <= prm%p_um) Hm = 0.0_rp;
  if (uFK <= prm%p_up) Hp = 0.0_rp;
  if (uFK <= prm%p_uq) Hq = 0.0_rp;
  if (uFK <= prm%p_ur) Hr = 0.0_rp;
  Vinf = 1.0_rp - Hq
  !.
  ! Calculate time constants
  !
  TAUVm = (1.0_rp-Hq)*prm%p_tv1m + Hq*prm%p_tv2m;
  TAUWm = prm%p_tw1m + 0.5_rp*(prm%p_tw2m - prm%p_tw1m)*(1.0_rp + tanh(prm%p_kwm*(uFK-prm%p_uwm)));
  TAUso = prm%p_tso1 + 0.5_rp*(prm%p_tso2 - prm%p_tso1)*(1.0_rp + tanh(prm%p_kso*(uFK-prm%p_uso)));
  TAUs  = (1.0_rp-Hp)*prm%p_ts1 + Hp*prm%p_ts2;
  TAUo  = (1.0_rp-Hr)*prm%p_to1 + Hr*prm%p_to2;
  !
  !
  ! Gates
  !
  Winf  = (1.0_rp-Hr)*(1.0_rp-uFK*prm%p_invtwinf) + Hr*prm%p_twinfst;
  Dvg   = (1.0_rp-Hm)*(Vinf-FKm%vg)/TAUVm - Hm*FKm%vg*prm%p_invtvp;  ! gate v
  Dwg   = (1.0_rp-Hp)*(Winf-FKm%wg)/TAUWm - Hp*FKm%wg*prm%p_invtwp;  ! gate w
  Dsg   = (0.5_rp*(1.0_rp+tanh(prm%p_ks*(uFK-prm%p_us)))-FKm%sg)/TAUs; ! gate s
  !
  FKm%vg  = FKm%vg + Dvg*dt;
  FKm%wg  = FKm%wg + Dwg*dt;
  FKm%sg  = FKm%sg + Dsg*dt;
  !.
  ! Update currents
  !
  cur%Ifi = -FKm%vg*Hm*(uFK-prm%p_um)*(prm%p_uu-uFK)*prm%p_invtfi;
  cur%Ifi = cur%Ifi*prm%p_Vscl
  !
  cur%Iso = (uFK-prm%p_uo)*(1.0_rp-Hp)/TAUo + Hp/TAUso;
  cur%Iso = cur%Iso*prm%p_Vscl
  !
  cur%Isi = -Hp*FKm%wg*FKm%sg*prm%p_invtsi;
  cur%Isi = cur%Isi*prm%p_Vscl
  !.
  ! Total ionic current
  Iion = cur%Ifi + cur%Iso + cur%Isi
  return
end subroutine gates_curr
!-------------------------------------------------------------------------------
subroutine FK_A_P01 (ict, dt, U, Iion, v_prm, v_FK, v_cr)
!-------------------------------------------------------------------------------
  implicit none
  integer(ip), intent(in)    :: ict
  real(rp),    intent(in)    :: dt, U
  real(rp),    intent(out)   :: Iion
  real (rp),   intent(in)    :: v_prm(np_fk)
  real(rp),    intent(inout) :: v_FK(nvar_fk)
  real(rp),    intent(out)   :: v_cr(ncur_fk)
  !.
  type(t_FKm)                :: FKm
  type (t_prm)               :: param
  type(t_cur)                :: crr
  !
  call put_me_struct(v_FK,FKm)
  call put_param(v_prm,param)
  !
  call gates_curr(dt, U, param, FKm, crr, Iion)
  !
  call get_me_struct(FKm,v_FK)

  !.
  v_cr(1:ncur_fk) = (/ crr%Ifi, crr%Iso,  crr%Isi, Iion /)
  !.
  return
end subroutine FK_A_P01
!-------------------------------------------------------------------------------
end module mod_FK
!-------------------------------------------------------------------------------
