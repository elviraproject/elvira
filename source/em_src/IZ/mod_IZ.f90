! ------------------------------------------------------------------------------
! &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
! ------------------------------------------------------------------------------
module mod_IZ
! ------------------------------------------------------------------------------
  use mod_precision
! ------------------------------------------------------------------------------
  implicit none
  include 'IZ_parameters.inc'

  public  :: IZ_A_P01, get_parameter_IZ 
  private :: put_parameter_IZ

  type, public:: t_prm
          private
          real(rp) :: v1
          real(rp) :: v2
          real(rp) :: G
  end type t_prm 
! ------------------------------------------------------------------------------
contains
! ------------------------------------------------------------------------------
!------------------------------------------------------------------------------!
subroutine write_current_IZ(lu_cr)
implicit none
integer(ip), intent(in)   :: lu_cr

    write(lu_cr,'(A)',advance='no') 'Itot '
end subroutine write_current_IZ
! ------------------------------------------------------------------------------
function get_parameter_IZ (t_cel) result (v_prm)
! ------------------------------------------------------------------------------
  implicit none
  integer(ip)             :: t_cel
  real (rp)               :: v_prm(np_iz)

  v_prm = (/ v1, v2, G/)
end function get_parameter_IZ
! ------------------------------------------------------------------------------
subroutine put_parameter_IZ(v_prm, param)
  implicit none
  real(rp), intent(in)    :: v_prm(:)
  type(t_prm), intent(out):: param

  param%v1 = v_prm(1)
  param%v2 = v_prm(2)
  param%G  = v_prm(3)
end subroutine put_parameter_IZ
! ------------------------------------------------------------------------------
subroutine IZ_A_P01 ( V, dt, Iion, v_prm)
! ------------------------------------------------------------------------------
  implicit none
  real(rp), intent(in)    :: V, dt, v_prm(:)
  real(rp), intent(out)   :: Iion
  type(t_prm)             :: prm

  call put_parameter_IZ(v_prm, prm)
  Iion= prm%G*V*(V-prm%v1)*(V-prm%v2)
  return
end subroutine IZ_A_P01
! ------------------------------------------------------------------------------
end module mod_IZ
