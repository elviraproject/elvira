! This module implements the tenTusscher Model of 2006
!
! 30-11-2009: TenTusscher_A_P01 returns currents vector for output (EAH)
!
!------------------------------------------------------------------------------
module mod_tentusscher
!------------------------------------------------------------------------------
  use mod_precision
  implicit none
  include 'ttm_parameters.inc'
  !.
  type t_ttm
    sequence
    real(rp)  ::  Cai, CaSR, CaSS,  Nai, Ki, m,   h,   j,   xs,  r,  s, d,     &
                  f,   f2,   fcass, rr,  oo, xr1, xr2
  end type t_ttm
  !.
  type, private :: t_cur
    sequence
    real(rp)  ::  IKr,  IKs,  IK1, IpK, IKATP, Ito, INa, IbNa, INaK, INaCa,    &
                  ICaL, IbCa, IpCa
  end type t_cur
  
  public  :: ic_TenTusscher, TenTusscher_A_P01
  private :: concentrations, gates, currents

  type, public:: t_prm
    private
    real(rp) :: A_pKNa        !.  1
    real(rp) :: A_GKr         !.  2
    real(rp) :: A_GK1         !.  3
    real(rp) :: A_GNa         !.  4
    real(rp) :: A_GCaL        !.  5
    real(rp) :: A_GbNa        !.  6
    real(rp) :: A_GbCa        !.  7
    real(rp) :: A_GpCa        !.  8
    real(rp) :: A_GpK         !.  9
    real(rp) :: A_PNaK        !. 10
    real(rp) :: A_kNaCa       !. 11
    real(rp) :: A_Gto         !. 12
    real(rp) :: A_GKs         !. 13
    real(rp) :: A_IKATp       !. 14
    real(rp) :: p_ATPi        !. 15
    real(rp) :: p_ADPi        !. 16
    real(rp) :: p_Ko          !. 17
    real(rp) :: p_Nao         !. 18
    real(rp) :: p_Cao         !. 19
    real(rp) :: p_GKs         !. 20
    real(rp) :: p_Gto         !. 21
    real(rp) :: p_p0ATP       !. 22
    real(rp) :: p_aIKATP      !. 23
    real(rp) :: p_bIKATP      !. 24
    real(rp) :: p_nks         !. 25
  end type t_prm
!------------------------------------------------------------------------------!
!  &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&  !
!------------------------------------------------------------------------------!
contains
!------------------------------------------------------------------------------!
subroutine write_state_TT(lu_st)
implicit none
integer(ip), intent(in)   :: lu_st

    write(lu_st,'(A)',advance='no') '% '
    write(lu_st,'(A)', advance='no') 'Cai '
    write(lu_st,'(A)',advance='no') 'CaSR '
    write(lu_st,'(A)',advance='no') 'CaSS '
    write(lu_st,'(A)',advance='no') 'Nai '
    write(lu_st,'(A)',advance='no') 'Ki '
    write(lu_st,'(A)',advance='no') 'm '
    write(lu_st,'(A)',advance='no') 'h '
    write(lu_st,'(A)',advance='no') 'j '
    write(lu_st,'(A)',advance='no') 'xs '
    write(lu_st,'(A)',advance='no') 'r '
    write(lu_st,'(A)',advance='no') 's '
    write(lu_st,'(A)',advance='no') 'd '
    write(lu_st,'(A)',advance='no') 'f '
    write(lu_st,'(A)',advance='no') 'f2 '
    write(lu_st,'(A)',advance='no') 'fcass '
    write(lu_st,'(A)',advance='no') 'rr '
    write(lu_st,'(A)',advance='no') 'oo '
    write(lu_st,'(A)',advance='no') 'xr1 '
    write(lu_st,'(A)',advance='no') 'xr2 '
end subroutine write_state_TT
!------------------------------------------------------------------------------!
subroutine write_current_TT(lu_cr)
implicit none
integer(ip), intent(in)   :: lu_cr

    write(lu_cr,'(A)',advance='no') '% '
    write(lu_cr,'(A)', advance='no') 'IKr '
    write(lu_cr,'(A)',advance='no') 'IKs '
    write(lu_cr,'(A)',advance='no') 'IK1 '
    write(lu_cr,'(A)',advance='no') 'IpK '
    write(lu_cr,'(A)',advance='no') 'Ito '
    write(lu_cr,'(A)',advance='no') 'INa '
    write(lu_cr,'(A)',advance='no') 'IbNa '
    write(lu_cr,'(A)',advance='no') 'INaK '
    write(lu_cr,'(A)',advance='no') 'INaCa '
    write(lu_cr,'(A)',advance='no') 'ICaL '
    write(lu_cr,'(A)',advance='no') 'IbCa '
    write(lu_cr,'(A)',advance='no') 'IpCa '
    write(lu_cr,'(A)',advance='no') 'IKATP '
    write(lu_cr,'(A)',advance='no') 'Itot '
end subroutine write_current_TT
!------------------------------------------------------------------------------!
function get_parameter_TT (tcell) result (v_prm)
!------------------------------------------------------------------------------!
  implicit none
  integer(ip), intent(in) :: tcell
  real (rp)               :: v_prm(np_tt)

  select case (tcell)
  case (TT_ENDO_mod)
    v_prm = (/ 1.0_rp, 1.0_rp, 1.0_rp, 1.0_rp, 1.0_rp, 1.0_rp, 1.0_rp, 1.0_rp, & 
               1.0_rp, 1.0_rp, 1.0_rp, 1.0_rp, 1.0_rp, 0.0_rp, p_ATPi, p_ADPi, &
               p_Ko, p_Nao, p_Cao, p_GKs_endo, p_Gto_endo, p_p0ATP_endo,       &
               p_aIKATP_endo, p_bIKATP_endo, p_nks/)
  case (TT_MID_mod)
    v_prm = (/ 1.0_rp, 1.0_rp, 1.0_rp, 1.0_rp, 1.0_rp, 1.0_rp, 1.0_rp, 1.0_rp, & 
               1.0_rp, 1.0_rp, 1.0_rp, 1.0_rp, 1.0_rp, 0.0_rp, p_ATPi, p_ADPi, &
               p_Ko, p_Nao, p_Cao, p_GKs_mid, p_Gto_mid, p_p0ATP_mid,          &
               p_aIKATP_mid, p_bIKATP_mid, p_nks/)
  case (TT_EPI_mod)
    v_prm = (/ 1.0_rp, 1.0_rp, 1.0_rp, 1.0_rp, 1.0_rp, 1.0_rp, 1.0_rp, 1.0_rp, & 
               1.0_rp, 1.0_rp, 1.0_rp, 1.0_rp, 1.0_rp, 0.0_rp, p_ATPi, p_ADPi, &
               p_Ko, p_Nao, p_Cao, p_GKs_epi, p_Gto_epi, p_p0ATP_epi,          &
               p_aIKATP_epi, p_bIKATP_epi, p_nks/)
  case default
    write (*,10) tcell; stop
  end select
  return
10 format ('###.Specified Cell type is not defined:',I3)
end function get_parameter_TT
!------------------------------------------------------------------------------
subroutine put_param(v_prm,param)
!------------------------------------------------------------------------------
  implicit none
  real(rp), intent(in)     :: v_prm(:)
  type (t_prm), intent(out):: param

  param%A_pKNa  = v_prm(1)   !.  1
  param%A_GKr   = v_prm(2)   !.  2
  param%A_GK1   = v_prm(3)   !.  3
  param%A_GNa   = v_prm(4)   !.  4
  param%A_GCaL  = v_prm(5)   !.  5
  param%A_GbNa  = v_prm(6)   !.  6
  param%A_GbCa  = v_prm(7)   !.  7
  param%A_GpCa  = v_prm(8)   !.  8
  param%A_GpK   = v_prm(9)   !.  9
  param%A_PNaK  = v_prm(10)  !. 10
  param%A_kNaCa = v_prm(11)  !. 11
  param%A_Gto   = v_prm(12)  !. 12
  param%A_GKs   = v_prm(13)  !. 13
  param%A_IKATP = v_prm(14)  !. 14
  param%p_ATPi  = v_prm(15)  !. 15
  param%p_ADPi  = v_prm(16)  !. 16
  param%p_Ko    = v_prm(17)  !. 17
  param%p_Nao   = v_prm(18)  !. 18
  param%p_Cao   = v_prm(19)  !. 19
  param%p_GKs   = v_prm(20)  !. 20
  param%p_Gto   = v_prm(21)  !. 21
  param%p_p0ATP = v_prm(22)  !. 22
  param%p_aIKATP= v_prm(23)  !. 23
  param%p_bIKATP= v_prm(24)  !. 24
  param%p_nks   = v_prm(25)  !. 25

  return
end subroutine put_param
!------------------------------------------------------------------------------
function ic_TenTusscher(ict) result(tt_ic)
!------------------------------------------------------------------------------
! This function sets the initial conditions for the Ten Tusscher Model
! tt_m is the main structure which contains the membrane potential, ioninc 
! concentrations, and gate variables
!------------------------------------------------------------------------------
  implicit none

  integer (ip), intent(in)  :: ict
  real(rp)                  :: tt_ic(nvar_tt)
  
  select case (ict)
  case( TT_ENDO_mod) !.----ENDO
    tt_ic = (/ end_Cai, end_CaSR, end_CaSS,  end_Nai, end_Ki,  end_m,        &
               end_h,   end_j,    end_xs,    end_r,   end_s,   end_d,        &
               end_f,   end_f2,   end_fcass, end_rr,  end_oo,  end_xr1,      &
               end_xr2 /)
  case( TT_MID_mod) !.----MID
    tt_ic = (/ mid_Cai, mid_CaSR, mid_CaSS,  mid_Nai, mid_Ki,  mid_m,        &
               mid_h,   mid_j,    mid_xs,    mid_r,   mid_s,   mid_d,        &
               mid_f,   mid_f2,   mid_fcass, mid_rr,  mid_oo,  mid_xr1,      &
               mid_xr2 /)
  case( TT_EPI_mod) !.----EPI
    tt_ic = (/ epi_Cai, epi_CaSR, epi_CaSS,  epi_Nai, epi_Ki,  epi_m,        &
               epi_h,   epi_j,    epi_xs,    epi_r,   epi_s,   epi_d,        &
               epi_f,   epi_f2,   epi_fcass, epi_rr,  epi_oo,  epi_xr1,      &
               epi_xr2 /)
  case default
    tt_ic = (/ ttd_Cai, ttd_CaSR, ttd_CaSS,  ttd_Nai, ttd_Ki,  ttd_m,        &
               ttd_h,   ttd_j,    ttd_xs,    ttd_r,   ttd_s,   ttd_d,        &
               ttd_f,   ttd_f2,   ttd_fcass, ttd_rr,  ttd_oo,  ttd_xr1,      &
               ttd_xr2 /)
  end select
  !.

  return
end function ic_TenTusscher
!------------------------------------------------------------------------------
subroutine get_me_struct(str_me,v_me)
!------------------------------------------------------------------------------
  implicit none
  type (t_ttm), intent(in) :: str_me
  real(rp), intent(out)    :: v_me(nvar_tt)
   
  v_me( 1) = str_me%Cai
  v_me( 2) = str_me%CaSR
  v_me( 3) = str_me%CaSS
  v_me( 4) = str_me%Nai
  v_me( 5) = str_me%Ki
  v_me( 6) = str_me%m
  v_me( 7) = str_me%h
  v_me( 8) = str_me%j
  v_me( 9) = str_me%xs
  v_me(10) = str_me%r
  v_me(11) = str_me%s
  v_me(12) = str_me%d
  v_me(13) = str_me%f
  v_me(14) = str_me%f2
  v_me(15) = str_me%fcass
  v_me(16) = str_me%rr
  v_me(17) = str_me%oo
  v_me(18) = str_me%xr1
  v_me(19) = str_me%xr2
  return
end subroutine get_me_struct
!------------------------------------------------------------------------------
subroutine put_me_struct(v_me,str_me)
!------------------------------------------------------------------------------
  implicit none
  real(rp), intent(in)      :: v_me(:)
  type (t_ttm), intent(out) :: str_me
  
  str_me%Cai   = v_me(1)
  str_me%CaSR  = v_me(2)
  str_me%CaSS  = v_me(3)
  str_me%Nai   = v_me(4)
  str_me%Ki    = v_me(5)
  str_me%m     = v_me(6)
  str_me%h     = v_me(7)
  str_me%j     = v_me(8)
  str_me%xs    = v_me(9)
  str_me%r     = v_me(10)
  str_me%s     = v_me(11)
  str_me%d     = v_me(12)
  str_me%f     = v_me(13)
  str_me%f2    = v_me(14)
  str_me%fcass = v_me(15)
  str_me%rr    = v_me(16)
  str_me%oo    = v_me(17)
  str_me%xr1   = v_me(18)
  str_me%xr2   = v_me(19)
  return
end subroutine put_me_struct
!-------------------------------------------------------------------------------
subroutine concentrations (dt, Istm, Iax, cur, prm, ttm)
!-------------------------------------------------------------------------------
! Function to update Ion concentrations
!-------------------------------------------------------------------------------
  implicit none
  real(rp),     intent(in)      :: dt, Istm, Iax
  type (t_cur), intent(in)      :: cur
  type (t_prm), intent(in)      :: prm
  type (t_ttm),   intent(inout) :: ttm
  !.
  real(rp)                      :: kCaSR , k1, k2, VcF_Cap,   &
                                   dRR, Irel, Ileak, Iup, Ixfer, CaCSQN, dCaSR, &
                                   bjsr, cjsr, CaSSBuf, dCaSS, bcss, ccss,      &
                                   CaBuf, dCai, bc, cc, totINa, dNai, totIK, dKi

  VcF_Cap = p_iVcF*p_Cap
  !.
  kCaSR   = p_maxsr-((p_maxsr-p_minsr)/(1.0+(p_EC/ttm%CaSR)**2));
  k1      = p_k1p/kCaSR;
  k2      = p_k2p*kCaSR;
  dRR     = p_k4*(1.0 - ttm%rr) - k2*ttm%CaSS*ttm%rr;
  ttm%rr  = ttm%rr + dt*dRR;
  ttm%oo  = k1*ttm%CaSS*ttm%CaSS*ttm%rr/(p_k3 + k1*ttm%CaSS*ttm%CaSS);
  !.
  Irel    = p_Vrel*ttm%oo*(ttm%CaSR-ttm%CaSS);
  Ileak   = p_Vleak*(ttm%CaSR-ttm%Cai);
  Iup     = p_Vmxu/(1.0+((p_Kup*p_Kup)/ (ttm%Cai*ttm%Cai)));
  Ixfer   = p_Vxfer*(ttm%CaSS - ttm%Cai);
  !.
  CaCSQN  = p_Bufsr*ttm%CaSR/(ttm%CaSR + p_Kbufsr);
  dCaSR   = dt*(Iup- Irel- Ileak);
  bjsr    = p_Bufsr - CaCSQN - dCaSR - ttm%CaSR + p_Kbufsr;
  cjsr    = p_Kbufsr*(CaCSQN + dCaSR + ttm%CaSR);
  ttm%CaSR  = 0.5*(sqrt(bjsr*bjsr+4.0*cjsr) - bjsr);
  !.
  CaSSBuf = p_Bufss*ttm%CaSS/(ttm%CaSS + p_Kbufss);
  dCaSS   = dt*(-Ixfer*(p_Vc/p_Vss) + Irel*(p_Vsr/p_Vss) + (-cur%ICaL*p_iVssF2*p_Cap));
  bcss    = p_Bufss - CaSSBuf-dCaSS-ttm%CaSS+p_Kbufss;
  ccss    = p_Kbufss*(CaSSBuf+dCaSS+ttm%CaSS);
  ttm%CaSS  = 0.5*(sqrt(bcss*bcss+4.0*ccss)-bcss);
  !.
  CaBuf   = p_Bufc*ttm%Cai/(ttm%Cai+p_Kbufc);
  dCai    = dt*((-(cur%IbCa+cur%IpCa-2.0*cur%INaCa)*p_iVcF2*p_Cap)-(Iup-Ileak)*p_Vsr/p_Vc+Ixfer);
  bc      = p_Bufc-CaBuf-dCai-ttm%Cai+p_Kbufc;
  cc      = p_Kbufc*(CaBuf+dCai+ttm%Cai);
  ttm%Cai   = 0.5*(sqrt(bc*bc+4.0*cc)-bc);
  !.
  totINa  =cur%INa+cur%IbNa+3.0*(cur%INaK+cur%INaCa);
  dNai    = -totINa*p_iVcF*p_Cap;
  ttm%Nai = ttm%Nai+dt*dNai;
  !.
  totIK   = Istm + cur%IK1+cur%Ito+cur%IKr+cur%IKs-2.0*cur%INaK+cur%IpK+cur%IKATP;
  dKi     = -totIK*p_iVcF*p_Cap;
  ttm%Ki  =  ttm%Ki+dt*dKi + Iax ;
  !.
  return
end subroutine concentrations
!------------------------------------------------------------------------------
subroutine gates( cellType, dt, U, prm, ttm)
!------------------------------------------------------------------------------
! This function updates the gating variables
!
!------------------------------------------------------------------------------
  implicit none
  integer(ip), intent(in)    :: cellType
  real(rp),    intent(in)    :: dt, U
  type (t_prm), intent(in)   :: prm
  type(t_ttm), intent(inout) :: ttm
  !.
  real (rp)                  :: AM, BM, TAUM, c1, MINF, AH1, BH1, TAUH, HINF,   &
                                AJ1, BJ1, TAUJ, AJ2, BJ2, JINF, &
                                XsINF, Axs, Bxs, TAUXs, RINF, TAUR, SINF,  &
                                TAUS, DINF, Ad, Bd, Cd, TAUD, FINF, Af, Bf, Cf, &
                                TAUF, F2INF, Af2, Bf2, Cf2, TAUF2, denCaSS,     &
                                FCaSSINF, TAUFCaSS, AH2, BH2
  real (rp)                  :: Xr1INF, axr1, bxr1, TAUXr1, Xr2INF, axr2, bxr2,&
                                TAUXr2
  real (rp)                  :: gamVk, ur1, ur2, dW, xs_a, xsk1
  !.
  !.--- Fast Na+ current
  AM    = 1.0/(1.0 + exp(0.2*(-60.0 - U)));
  BM    = 0.1/(1.0 + exp(0.2*( U + 35.0))) + 0.1/(1.0 + exp(0.005*(U - 50.0)));
  TAUM  = AM*BM;
  c1    = 1.0/(1.0 + exp((-56.86 - U)/9.03));
  MINF  = c1*c1;
  if (U >= -40.0) then
     AH1  = 0.0_rp;
     BH1  = 0.77/(0.13*(1.0 + exp(-(U+10.66)/11.1)));
     TAUH = 1.0/(AH1 + BH1);
  else
     AH2   = 0.057*exp(-(U+80.0)/6.8);
     BH2   = 2.7*exp(0.079*U) + 3.1E5*exp(0.3485*U);
     TAUH  = 1.0/(AH2 + BH2);
  endif
  c1   = 1.0/(1.0+exp((U + 71.55)/7.43));
  HINF = c1*c1;
  if (U >= -40.0) then
     AJ1  = 0.0_rp;
     BJ1  = 0.6*exp(0.057*U)/(1.0 + exp(-0.1*(U+32.0)));
     TAUJ = 1.0/(AJ1 + BJ1);
  else
     c1   = -2.5428E4*exp(0.2444*U) - 6.948e-6*exp(-0.04391*U);
     AJ2  = c1*(U + 37.78)/(1. + exp(0.311*(U+79.23)));
     BJ2  = 0.02424*exp(-0.01052*U)/(1.+exp(-0.1378*(U + 40.14)));
     TAUJ = 1.0/(AJ2 + BJ2);
  end if
  JINF  = HINF;
  !
  ! Rapid delay rectifier IKr, Orig TenTusscher
  !
   Xr1INF=1.0/(1.0+exp((-26.0-U)/7.0));
   axr1=450.0/(1.0+exp(0.1*(-45.0-U)));
   bxr1=6.0/(1.0+exp((U-(-30.0))/11.5));
   TAUXr1=axr1*bxr1;
   Xr2INF=1.0/(1.0+exp((U+88.0)/24.0));
   axr2=3.0/(1.0+exp(0.05*(-60.0-U)));
   bxr2=1.12/(1.0+exp(0.05*(U-60.0)));
   TAUXr2=axr2*bxr2;
  !
  ! Slow delay rectifier IKs
  !
  XsINF  = 1.0/(1.0+exp((-5.0-U)/14.0));
  Axs    = (1400.0/(sqrt(1.0+exp((5.0-U)/6.0))));
  Bxs    = (1.0/(1.0+exp((U-35.0)/15.0)));
  TAUXs  = Axs*Bxs+80.0;
  !
  ! Transcient Outward current Ito
  !
  RINF = 1.0/(1.0+exp((20.0-U)/6.0));
  TAUR = 9.5*exp(-(U+40.0)**2.0/1800.0)+0.8;
  !.
  if ( cellType == TT_ENDO_mod ) then
     SINF = 1.0/(1.0+exp(0.2*(U+28.0)));
     TAUS = 1000.0*exp(-0.001*(U+67.0)**2.0)+8.0;
  else
     SINF = 1.0/(1.0+exp(0.2*(U+20.0)));
     TAUS = 85.0*exp(-0.003125*(U+45.0)**2.0)+5.0/(1.0+exp(0.2*(U-20.0)))+3.0;
  end if
  !
  ! L-Type Ca2+ current ICaL
  !
  DINF = 1.0/(1.0+exp((-8.0-U)/7.5));       ! Orig TenTusscher Model
  Ad   = 1.4/(1.0+exp((-35.0-U)/13.0))+0.25;
  Bd   = 1.4/(1.0+exp(0.2*(U+5.0)));
  Cd   = 1.0/(1.0+exp(0.05*(50.0-U)));
  TAUD = Ad*Bd+Cd;
  !.
  FINF = 1.0/(1.0+exp((U+20.0)/7.0));
  Af   = 1102.5*exp(-(U+27.0)**2.0/225.0);
  Bf   = 200.0/(1.0+exp(0.1*(13.0-U)));
  Cf   = 180.0/(1.0+exp(0.1*(U+30.0)))+20.0;
  TAUF=Af+Bf+Cf;                            ! Orig TenTusscher Model
  !.
  F2INF = 0.67/(1.0+exp((U+35.0)/7.0))+0.33; ! Orig TenTusscher Model
  Af2   =600.0*exp(-(U+25.0)**2.0/170.0);    ! Orig TenTusscher Model
  Bf2   = 31.0/(1.0+exp(0.1*(25.0-U)));      ! Orig TenTusscher Model
  Cf2   = 16.0/(1.0+exp(0.1*(U+30.0)));      ! Orig TenTusscher Model
  TAUF2 = Af2+Bf2+Cf2;                       ! Orig TenTusscher Model
  !.
  denCaSS  = 1.0/(1.0+400.0*ttm%CaSS*ttm%CaSS);
  FCaSSINF = 0.6*denCaSS+0.4;                ! Orig TenTusscher Model
  TAUFCaSS = 80.0*denCaSS+2.0;
  !.
  ! Update gates
  ttm%m     = MINF-(MINF-ttm%m)*exp(-dt/TAUM);
  ttm%h     = HINF-(HINF-ttm%h)*exp(-dt/TAUH);
  ttm%j     = JINF-(JINF-ttm%j)*exp(-dt/TAUJ);
  ttm%xr1 = Xr1INF-(Xr1INF-ttm%xr1)*exp(-dt/TAUXr1); ! Orig TenTusscher Model
  ttm%xr2 = Xr2INF-(Xr2INF-ttm%xr2)*exp(-dt/TAUXr2); ! Orig TenTusscher Model
  if (prm%p_nks > 0.0) then
      call random_number(ur1)
      call random_number(ur2)
      dW = sqrt(-2.0*log(ur1))*cos(2*p_pi*ur2);
      xs_a = ttm%xs + (XsINF-ttm%xs)*dt/TAUXs;
      gamVk  = sqrt(dt*(XsINF + (1.0-2.0*XsINF)*ttm%xs)/(TAUXs*prm%p_nks));
      xsk1 = xs_a + gamVk*dW;
      do while((xsk1<0.0).or.(xsk1>1.0))
         call random_number(ur1)
         call random_number(ur2)
         dW = sqrt(-2.0*log(ur1))*cos(2*p_pi*ur2);
         xsk1 = xs_a + gamVk*dW;
      end do
      ttm%xs = xsk1;
!      write(*,*) 'Updating modified', dW, gamVk, xsk1
  else
!      write(*,*) 'Updating original'
      ttm%xs    = XsINF-(XsINF-ttm%xs)*exp(-dt/TAUXs);
  endif
  ttm%s     = SINF-(SINF-ttm%s)*exp(-dt/TAUS);
  ttm%r     = RINF-(RINF-ttm%r)*exp(-dt/TAUR);
  ttm%d     = DINF-(DINF-ttm%d)*exp(-dt/TAUD);
  ttm%f     = FINF-(FINF-ttm%f)*exp(-dt/TAUF);
  ttm%f2    = F2INF-(F2INF-ttm%f2)*exp(-dt/TAUF2);
  ttm%fcass = FCaSSINF-(FCaSSINF-ttm%fcass)*exp(-dt/TAUFCaSS);
  return
end subroutine gates
!------------------------------------------------------------------------------
subroutine currents ( U, ttm, Iion, prm, cur) 
!------------------------------------------------------------------------------
! This function computes currents and concentrations for the ten Tusscher Model
!
! KHWJ ten Tusscher, D Noble, PJ Noble, AV PanfiloV%
!  Am J Phys 286:H1573-H1589, 2004.
! KHWJ ten Tusscher and AV PanfiloV%
!  Am J Phys 291:H1088-H1100, 2006
!------------------------------------------------------------------------------
  implicit none
  real(rp),    intent(in)   :: U
  type(t_ttm), intent(in)   :: ttm
  real(rp),    intent(out)  :: Iion
  type (t_prm), intent(in)  :: prm
  type(t_cur), intent(out)  :: cur
  !.
  real(rp)                  :: EK, ENa, EKs,ECa, atmp, rec_iNaK, rec_ipK, atmp1,&
                               Ak1, denBk1, Bk1, rec_iK1, gam0, KhM, fM, KhN,   &
                               fN, fT, fATP, denICaL, ICaL, atmp2, denINaCa1,   &
                               denINaK,IK,INa,ICa, Km, H

  !.
  EK      = p_RTF*log(prm%p_Ko/ttm%Ki);
  ENa     = p_RTF*log(prm%p_Nao/ttm%Nai);
  EKs     = p_RTF*log((prm%p_Ko+prm%A_pKNa*p_pKNa*prm%p_Nao)/(ttm%Ki+prm%A_pKNa*p_pKNa*ttm%Nai));
  ECa     = 0.5*p_RTF*log(prm%p_Cao/ttm%Cai);
  atmp    = U*p_iRTF;
  rec_iNaK= 1.0/(1.0+0.1245*exp(-0.1*atmp)+0.0353*exp(-atmp));
  rec_ipK = 1.0/(1.0+exp((25.0-U)/5.98));
  atmp1   = U-EK;
  Ak1     = 0.1/(1.0+exp(0.06*(atmp1-200.0)));
  denBk1  = (1.0+exp(-0.5*atmp1));
  Bk1     = (3.0*exp(2.0E-4*(atmp1+100.0))+exp(0.1*(atmp1-10.0)))/denBk1;
  rec_iK1 = Ak1/(Ak1+Bk1);
  !.--- Ito current
  cur%Ito = prm%A_Gto*prm%p_Gto*ttm%r*ttm%s*atmp1;
  !.--- IKr current
  cur%IKr=prm%A_GKr*p_GKr*sqrt(prm%p_Ko/5.4)*ttm%xr1*ttm%xr2*atmp1;  ! Orig TenTusscher Model
  !.--- IK1 current
  cur%IK1=prm%A_GK1*p_GK1*rec_iK1*atmp1;                      ! Orig TenTusscher Model
  !.--- IpK current
  cur%IpK = prm%A_GpK*p_GpK*rec_ipK*atmp1;
  !.--- IKATP current
  gam0      = 35.375e-9*(prm%p_Ko/5.4)**0.24;
  KhM       = 0.651*exp(-2.0*p_iRTF*p_dMgATP*U)*sqrt(5.0+prm%p_Ko);
  fM        = 1.0/(1.0+p_MgiATP/KhM);
  KhN       = p_Kh0NaATP*exp(-p_iRTF*p_dNaATP*U);
  fN        = 1.0/(1.0+(ttm%Nai/KhN)**2.0);
  fT        = 1.3**(p_T-310.16);
  Km        = prm%p_aIKATP*(35.8+17.9*prm%p_ADPi**0.256);
  H         = 1.3+0.74*prm%p_bIKATP*exp(-0.09*prm%p_ADPi);
  fATP      = 1.0/(1.0+(1000.0*prm%p_ATPi/Km)**H);
  cur%IKATP = prm%A_IKATP*p_sigATP*gam0*fM*fN*fT*prm%p_p0ATP*fATP*atmp1;
  !.--- INa current
  atmp1     = U-ENa;
  cur%INa   = prm%A_GNa*p_GNa*(ttm%m*ttm%m*ttm%m)*ttm%h*ttm%j*atmp1;
  !.--- IbNa current
  cur%IbNa  = prm%A_GbNa*p_GbNa*atmp1;
  !.--- ICaL current
  atmp1     = 2.0*(U - 15.0)*p_iRTF;
  denICaL   = exp(atmp1)-1.0;
  ICaL      = 2.0*prm%A_GCaL*p_GCaL*ttm%d*ttm%f*ttm%f2*ttm%fcass*(atmp1*p_F)*(0.25*ttm%CaSS*exp(atmp1)-prm%p_Cao);
  cur%ICaL  = ICaL/denICaL;
  !.--- IKs current
  cur%IKs   = prm%A_GKs*prm%p_GKs*ttm%xs*ttm%xs*(U-EKs);
  !.--- INaCa current
  atmp1     = exp(p_gam*atmp);
  atmp2     = exp((p_gam-1.0)*atmp);
  denINaCa1 = (p_KmNai**3.0+prm%p_Nao**3.0)*(p_KmCa+prm%p_Cao)*(1.0+p_ksat*atmp2);
  cur%INaCa = prm%A_kNaCa*p_kNaCa*(atmp1*ttm%Nai**3.0*prm%p_Cao-atmp2*prm%p_Nao**3.0*ttm%Cai*p_alf)/denINaCa1;
  !.--- INaK current
  denINaK   = (prm%p_Ko+p_KmK)*(ttm%Nai+p_KmNa);
  cur%INaK  = prm%A_PNaK*p_PNaK*prm%p_Ko*ttm%Nai*rec_iNaK/denINaK;
  !.--- IpCa current
  cur%IpCa  = prm%A_GpCa*p_GpCa*ttm%Cai/(p_KpCa+ttm%Cai);
  !.--- IbCa current
  cur%IbCa  = prm%A_GbCa*p_GbCa*(U-ECa);
  !.--- Calculating total current
  IK   = cur%IKr  + cur%IKs  + cur%IK1  + cur%IpK   + cur%IKATP + cur%Ito;
  INa  = cur%INa  + cur%IbNa + cur%INaK + cur%INaCa;
  ICa  = cur%ICaL + cur%IbCa + cur%IpCa;
  !.
  Iion = IK + INa + ICa;
  !.
  return
10 format (15(2X,F12.6))
end subroutine currents
!-------------------------------------------------------------------------------
subroutine TenTusscher_A_P01 (ict,dt,Istm,Iax,U,Iion,v_prm,v_tt,v_cr)
!-------------------------------------------------------------------------------
  implicit none
  integer(ip), intent(in)    :: ict   !.Cell Model 
  real(rp),    intent(in)    :: dt, Istm, Iax, U
  real(rp),    intent(out)   :: Iion
  real(rp),    intent(in)    :: v_prm(:)
  real(rp), intent(inout)    :: v_tt(nvar_tt)
  real(rp), intent(out)      :: v_cr(ncur_tt)
  !.
  type(t_ttm)                :: ttm
  type (t_prm)               :: param
  type(t_cur)                :: crr
  !
  call put_me_struct(v_tt,ttm)
  call put_param(v_prm,param)
  !
  call currents ( U, ttm, Iion, param, crr) 
  call concentrations ( dt, Istm, Iax, crr, param, ttm)
  call gates( ict, dt, U, param, ttm)
  !  
  call get_me_struct(ttm,v_tt)

  !.
  v_cr(1:ncur_tt)=(/ crr%IKr, crr%IKs,  crr%IK1,  crr%IpK,   crr%Ito, crr%INa,     &
                crr%IbNa, crr%INaK, crr%INaCa, crr%ICaL,  crr%IbCa, crr%IpCa, &
                crr%IKATP, Iion+Istm /)
  !.
  return
end subroutine TenTusscher_A_P01
!-------------------------------------------------------------------------------
end module mod_tentusscher
!-------------------------------------------------------------------------------
