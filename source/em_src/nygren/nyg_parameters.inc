!------------------------------------------------------------------------------
! This are the parameters for the Nygren Model
! Constants
!------------------------------------------------------------------------------
! Model Constants
!
 real(rp), parameter, private :: p_R        = 8314.0;      ! J/mol??K 
 real(rp), parameter, private :: p_FA       = 96487.0;     ! ??C/mmol  
 real(rp), parameter, private :: p_Te       = 306.15;      ! K 
!
 real(rp), parameter, private :: p_voli     = 0.005884;    ! nl
 real(rp), parameter, private :: p_volo     = 0.000800224; ! nl
 real(rp), parameter, private :: p_voldo    = 0.00011768;  ! nl
 real(rp), parameter, private :: p_volrel   = 0.0000441;   ! nl
 real(rp), parameter, private :: p_volup    = 0.0003969;   ! nl
! 
 real(rp), parameter, private :: p_nab      = 130.0;       ! mM/l
 real(rp), parameter, private :: p_kb       = 5.4;         ! mM/l
 real(rp), parameter, private :: p_cab      = 1.8;         ! mM/l
 real(rp), parameter, private :: p_mgi      = 2.5;         ! mM/l
 real(rp), parameter, private :: p_kca      = 0.25;        ! mM/l
 real(rp), parameter, private :: p_ecal     = 60.0;        ! mV
!
 real(rp), parameter, private :: p_tauna    = 14.3;	   ! s
 real(rp), parameter, private :: p_tauk     = 10.0;        ! s
 real(rp), parameter, private :: p_tauca    = 24.7;        ! s
 real(rp), parameter, private :: p_taudi    = 0.010;       ! s
 real(rp), parameter, private :: p_imnak    = 70.8253;	   ! pA 
 real(rp), parameter, private :: p_knakk    = 1.0          ! mM/l
 real(rp), parameter, private :: p_knakna   = 11.0;	   ! mM/l
 real(rp), parameter, private :: p_imcap    = 4.0;	   ! pA 
 real(rp), parameter, private :: p_kcap     = 0.0002;	   ! Mm/l
 real(rp), parameter, private :: p_knaca    = 0.0374842    ! pA/(mM/l)**4
 real(rp), parameter, private :: p_lambda   = 0.45;           
 real(rp), parameter, private :: p_dnaca    = 0.0003;	   ! (mM/l)**(-4) 
 real(rp), parameter, private :: p_phinaen  =  -1.68;	   ! pA 
 real(rp), parameter, private :: p_imup     = 2800.0;	   ! pA 
 real(rp), parameter, private :: p_kcyca    = 0.0003; 	   ! mM/l 
 real(rp), parameter, private :: p_ksrca    = 0.5;         ! mM/l 
 real(rp), parameter, private :: p_kxcs     = 0.4; 	   ! mM/l
 real(rp), parameter, private :: p_tautr    = 0.01;	   ! s 
 real(rp), parameter, private :: p_alprel   = 200000.0;    ! pA/mM 
 real(rp), parameter, private :: p_kreli    = 0.0003;	   ! mM/l
 real(rp), parameter, private :: p_kreldo   = 0.003;	   ! nM/l 
 real(rp), parameter, private :: p_krecov   = 0.815;	   ! 1/s
!
! Variable Parameters of the model
!  
 real(rp), parameter, private :: p_pna      = 0.0016;	   ! nl/s	
 real(rp), parameter, private :: p_gcal     = 6.75;	   ! nS  
 real(rp), parameter, private :: p_gt       = 7.5;	   ! ns
 real(rp), parameter, private :: p_gsus     = 2.75;	   ! ns
 real(rp), parameter, private :: p_gkfast   = 0.5;	   ! nS 
 real(rp), parameter, private :: p_gkslow   = 1.0;	   ! nS 
 real(rp), parameter, private :: p_gk1      = 3.0;	   ! nS 
 real(rp), parameter, private :: p_gbna     = 0.060599;	   ! nS
 real(rp), parameter, private :: p_gbca     = 0.078681;	   ! nS 
 real(rp), parameter, private :: p_taufl1   = 1.0;         ! reducing factor
 real(rp), parameter, private :: p_Vhm      = 63.6;        ! mV
 real(rp), parameter, private :: p_Vrm      = 1.0;         ! mV
 real(rp), parameter, private :: p_Ddof     = 0.0;         ! microM
!------------------------------------------------------------------------------
!-- INITIAL CONDITIONS --------------------------------------------------------
!------------------------------------------------------------------------------
!
! Original Nygren
!
  real(rp), parameter :: Vi_NY    = -74.2525;
  real(rp), parameter :: mic      = 0.0032017;
  real(rp), parameter :: h1ic     = 0.8814;
  real(rp), parameter :: h2ic     = 0.8742;
  real(rp), parameter :: dlic     = 0.000013005;
  real(rp), parameter :: fl1ic    = 0.9986;
  real(rp), parameter :: fl2ic    = 0.9986;
  real(rp), parameter :: rric     = 0.0010678;
  real(rp), parameter :: sic      = 0.9490;
  real(rp), parameter :: rsusic   = 0.00015949;
  real(rp), parameter :: ssusic   = 0.9912;
  real(rp), parameter :: paic     = 0.0001;
  real(rp), parameter :: nic      = 0.0048357;
  real(rp), parameter :: naiic    = 8.5547;
  real(rp), parameter :: kiic     = 129.435;
  real(rp), parameter :: caiic    = 0.00006729;
  real(rp), parameter :: caupic   = 0.6646;
  real(rp), parameter :: carelic  = 0.6465;
  real(rp), parameter :: cadoic   = 0.000072495;
  real(rp), parameter :: ocic     = 0.0275;
  real(rp), parameter :: otcic    = 0.0133;
  real(rp), parameter :: otmgcic  = 0.1961;
  real(rp), parameter :: otnmgmgic= 0.7094;
  real(rp), parameter :: ocalseic = 0.4369;
  real(rp), parameter :: f1ic     = 0.4284;
  real(rp), parameter :: f2ic     = 0.0028;
  real(rp), parameter :: naoic    = 130.011;
  real(rp), parameter :: koic     = 5.3581;
  real(rp), parameter :: caoic    = 1.8147;
  real(rp), parameter :: otcd     = 0.0;
  real(rp), parameter :: otmgcd   = 0.0;
  real(rp), parameter :: ocd      = 0.0;
  real(rp), parameter :: ocalsed  = 0.0;
  real(rp), parameter :: caupd    = 0.0;
  real(rp), parameter :: careld   = 0.0;
  real(rp), parameter :: bdofic  =  0.0;
!------------------------------------------------------------------------------
!-- STRUCTURE PARAMETERS ------------------------------------------------------
!------------------------------------------------------------------------------
  integer(ip), parameter :: NYGREN_mod = 10;  ! Nygren model              (AH)
  integer(ip),parameter  :: nvar_ny    = 35;  ! Number of state variables
  integer(ip),parameter  :: ncur_ny    = 19;  ! Number of currents
  integer(ip),parameter  :: np_ny      = 13;  ! Number of modifiable parameters
!------------------------------------------------------------------------------
!-- ADAPTIVE TIME STEPING -----------------------------------------------------
!------------------------------------------------------------------------------
!.
  integer(ip), parameter    :: m_stpNY   =    5
  real(rp),    parameter    :: m_dvdtNY  =  1.0
