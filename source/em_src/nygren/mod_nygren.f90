! This module implements the Nygren Model
!
! 30-11-2009: Nygren_A_P01 returns currents vector for output (EAH)
!
!------------------------------------------------------------------------------
module mod_nygren
!------------------------------------------------------------------------------
  use mod_precision
  implicit none
  include 'nyg_parameters.inc'
  !.
  type t_nyg
    sequence
    real(rp) :: m, h1, h2, dl, fl1, fl2, rr, s, rsus, ssus, pa, n, nai, ki, cai,  &
                caup, carel, cado, oc, otc, otmgc, otnmgmg, ocalse, f1, f2, nao,  &   
                ko, cao, otcd, otmgcd, ocd, ocalsed, caupd, careld, bdof
  end type t_nyg
  !.
  public  :: get_parameter_NYG , ic_Nygren, Nygren_A_P01
  private :: put_param, f_m, f_h_total, f_na, f_ca, f_It, f_Isus, f_IKr, f_IK,  &
             f_IK1, f_basal, f_ICAP, f_INAK, f_INACA, f_SRCa, f_ICab, f_Icon,   &
             f_Cleft
  !.
  type, public:: t_prm
    private
    real(rp)  :: p_pna    ! 1	
    real(rp)  :: p_gcal   ! 2  
    real(rp)  :: p_gt     ! 3
    real(rp)  :: p_gsus   ! 4
    real(rp)  :: p_gkfast ! 5 
    real(rp)  :: p_gkslow ! 6 
    real(rp)  :: p_gk1    ! 7 
    real(rp)  :: p_gbna   ! 8
    real(rp)  :: p_gbca   ! 9 
    real(rp)  :: p_taufl1 ! 10
    real(rp)  :: p_Vhm    ! 11
    real(rp)  :: p_Vrm    ! 12
    real(rp)  :: p_Ddof   ! 13
  end type t_prm
!------------------------------------------------------------------------------!
!  &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&  !
!------------------------------------------------------------------------------!
contains
!------------------------------------------------------------------------------!
subroutine write_state_NYGREN(lu_st)
implicit none
integer(ip), intent(in)   :: lu_st

    write(lu_st,'(A)',advance='no') '% '
    write(lu_st,'(A)', advance='no') 'm '
    write(lu_st,'(A)',advance='no') 'h1 '
    write(lu_st,'(A)',advance='no') 'h2 '
    write(lu_st,'(A)',advance='no') 'dl '
    write(lu_st,'(A)',advance='no') 'fl1 '
    write(lu_st,'(A)',advance='no') 'fl2 '
    write(lu_st,'(A)',advance='no') 'rr '
    write(lu_st,'(A)',advance='no') 's '
    write(lu_st,'(A)',advance='no') 'rsus '
    write(lu_st,'(A)',advance='no') 'ssus '
    write(lu_st,'(A)',advance='no') 'pa '
    write(lu_st,'(A)',advance='no') 'n '
    write(lu_st,'(A)',advance='no') 'Nai '
    write(lu_st,'(A)',advance='no') 'Ki '
    write(lu_st,'(A)',advance='no') 'Cai '
    write(lu_st,'(A)',advance='no') 'Caup '
    write(lu_st,'(A)',advance='no') 'Carel '
    write(lu_st,'(A)',advance='no') 'Cabo '
    write(lu_st,'(A)',advance='no') 'oc '
    write(lu_st,'(A)',advance='no') 'otc '
    write(lu_st,'(A)',advance='no') 'otmgc '
    write(lu_st,'(A)',advance='no') 'otnmgmg '
    write(lu_st,'(A)',advance='no') 'ocalse '
    write(lu_st,'(A)',advance='no') 'f1 '
    write(lu_st,'(A)',advance='no') 'f2 '
    write(lu_st,'(A)',advance='no') 'Nao '
    write(lu_st,'(A)',advance='no') 'Ko '
    write(lu_st,'(A)',advance='no') 'Cao '
    write(lu_st,'(A)',advance='no') 'otcd '
    write(lu_st,'(A)',advance='no') 'otmgcd '
    write(lu_st,'(A)',advance='no') 'ocd '
    write(lu_st,'(A)',advance='no') 'ocalsed '
    write(lu_st,'(A)',advance='no') 'caupd '
    write(lu_st,'(A)',advance='no') 'careld '
    write(lu_st,'(A)',advance='no') 'bdof '
end subroutine write_state_NYGREN
!------------------------------------------------------------------------------!
subroutine write_current_NYGREN(lu_cr)
implicit none
integer(ip), intent(in)   :: lu_cr

    write(lu_cr,'(A)',advance='no') '% '
    write(lu_cr,'(A)', advance='no') 'INa '
    write(lu_cr,'(A)',advance='no') 'ICaL '
    write(lu_cr,'(A)',advance='no') 'ek '
    write(lu_cr,'(A)',advance='no') 'It '
    write(lu_cr,'(A)',advance='no') 'Isus '
    write(lu_cr,'(A)',advance='no') 'IKr '
    write(lu_cr,'(A)',advance='no') 'IKs '
    write(lu_cr,'(A)',advance='no') 'IK '
    write(lu_cr,'(A)',advance='no') 'IK1 '
    write(lu_cr,'(A)',advance='no') 'ena '
    write(lu_cr,'(A)',advance='no') 'IbNa '
    write(lu_cr,'(A)',advance='no') 'IbCa '
    write(lu_cr,'(A)',advance='no') 'Ib '
    write(lu_cr,'(A)',advance='no') 'ICap '
    write(lu_cr,'(A)',advance='no') 'INaK '
    write(lu_cr,'(A)',advance='no') 'INaCa '
    write(lu_cr,'(A)',advance='no') 'Iup '
    write(lu_cr,'(A)',advance='no') 'Irel '
    write(lu_cr,'(A)',advance='no') 'Itot '
end subroutine write_current_NYGREN
!------------------------------------------------------------------------------!
function get_parameter_NYG (tcell) result (v_prm)
!------------------------------------------------------------------------------!
  implicit none
  integer(ip), intent(in) :: tcell
  real (rp)               :: v_prm(np_ny)

  select case (tcell)
  case (NYGREN_mod)
    v_prm = (/ p_pna, p_gcal, p_gt, p_gsus, p_gkfast, p_gkslow, p_gk1, &
               p_gbna, p_gbca, p_taufl1, p_Vhm, p_Vrm, p_Ddof /) 
  case default
    write (*,10) tcell; stop
  end select
  !.
  return
  10 format ('###.Specified Cell type is not defined:',I3)
end function get_parameter_NYG
!------------------------------------------------------------------------------
subroutine put_param(v_prm,param)
!------------------------------------------------------------------------------
  implicit none
  real(rp), intent(in)    :: v_prm(:)
  type (t_prm)            :: param

  param%p_pna    = v_prm(1)  ! 1	
  param%p_gcal   = v_prm(2)  ! 2  
  param%p_gt     = v_prm(3)  ! 3
  param%p_gsus   = v_prm(4)  ! 4
  param%p_gkfast = v_prm(5)  ! 5 
  param%p_gkslow = v_prm(6)  ! 6 
  param%p_gk1    = v_prm(7)  ! 7 
  param%p_gbna   = v_prm(8)  ! 8
  param%p_gbca   = v_prm(9)  ! 9 
  param%p_taufl1 = v_prm(10) ! 10 
  param%p_Vhm    = v_prm(11) ! 11 
  param%p_Vrm    = v_prm(12) ! 12 
  param%p_Ddof   = v_prm(13) ! 13 
  return
end subroutine put_param
!------------------------------------------------------------------------------
function ic_Nygren(ict) result(nyg_ic)
!------------------------------------------------------------------------------
! This function sets the initial conditions for the Ten Tusscher Model
! tt_m is the main structure which contains the membrane potential, ioninc 
! concentrations, and gate variables
!------------------------------------------------------------------------------
  implicit none

  !.
  integer (ip), intent(in)  :: ict
  real(rp)                  :: nyg_ic(nvar_ny)
  
  select case (ict)
  case(NYGREN_mod) !.----ATRIA
    nyg_ic = (/ mic, h1ic, h2ic, dlic, fl1ic, fl2ic, rric, sic, rsusic, ssusic, &
                paic, nic, naiic, kiic, caiic, caupic, carelic, cadoic, ocic,   &
                otcic, otmgcic, otnmgmgic, ocalseic, f1ic, f2ic, naoic, koic,   &
                caoic, otcd, otmgcd, ocd, ocalsed, caupd, careld, bdofic /)
  case default
    nyg_ic = (/ mic, h1ic, h2ic, dlic, fl1ic, fl2ic, rric, sic, rsusic, ssusic, &
                paic, nic, naiic, kiic, caiic, caupic, carelic, cadoic, ocic,   &
                otcic, otmgcic, otnmgmgic, ocalseic, f1ic, f2ic, naoic, koic,   &
                caoic, otcd, otmgcd, ocd, ocalsed, caupd, careld, bdofic /)
  end select
  !.

  return
end function ic_Nygren
!------------------------------------------------------------------------------
subroutine get_me_struct(str_me,v_me)
!------------------------------------------------------------------------------
  implicit none
  type (t_nyg), intent(in) :: str_me
  real(rp), intent(out)    :: v_me(nvar_ny)
  
  v_me(1)  =   str_me%m 
  v_me(2)  =   str_me%h1 
  v_me(3)  =   str_me%h2
  v_me(4)  =   str_me%dl
  v_me(5)  =   str_me%fl1
  v_me(6)  =   str_me%fl2
  v_me(7)  =   str_me%rr
  v_me(8)  =   str_me%s
  v_me(9)  =   str_me%rsus
  v_me(10) =   str_me%ssus
  v_me(11) =   str_me%pa
  v_me(12) =   str_me%n
  v_me(13) =   str_me%nai
  v_me(14) =   str_me%ki
  v_me(15) =   str_me%cai
  v_me(16) =   str_me%caup
  v_me(17) =   str_me%carel
  v_me(18) =   str_me%cado
  v_me(19) =   str_me%oc
  v_me(20) =   str_me%otc
  v_me(21) =   str_me%otmgc
  v_me(22) =   str_me%otnmgmg
  v_me(23) =   str_me%ocalse
  v_me(24) =   str_me%f1
  v_me(25) =   str_me%f2
  v_me(26) =   str_me%nao
  v_me(27) =   str_me%ko
  v_me(28) =   str_me%cao
  v_me(29) =   str_me%otcd
  v_me(30) =   str_me%otmgcd
  v_me(31) =   str_me%ocd
  v_me(32) =   str_me%ocalsed
  v_me(33) =   str_me%caupd
  v_me(34) =   str_me%careld
  v_me(35) =   str_me%bdof
  return
end subroutine get_me_struct
!------------------------------------------------------------------------------
subroutine put_me_struct(v_me,str_me)
!------------------------------------------------------------------------------
  implicit none
  real(rp), intent(in)      :: v_me(:)
  type (t_nyg), intent(out) :: str_me
  str_me%m       = v_me(1)
  str_me%h1      = v_me(2)
  str_me%h2      = v_me(3)
  str_me%dl      = v_me(4)
  str_me%fl1     = v_me(5)
  str_me%fl2     = v_me(6)
  str_me%rr      = v_me(7)
  str_me%s       = v_me(8)
  str_me%rsus    = v_me(9)
  str_me%ssus    = v_me(10)
  str_me%pa      = v_me(11)
  str_me%n       = v_me(12)
  str_me%nai     = v_me(13)
  str_me%ki      = v_me(14)
  str_me%cai     = v_me(15)
  str_me%caup    = v_me(16)
  str_me%carel   = v_me(17)
  str_me%cado    = v_me(18)
  str_me%oc      = v_me(19)
  str_me%otc     = v_me(20)
  str_me%otmgc   = v_me(21)
  str_me%otnmgmg = v_me(22)
  str_me%ocalse  = v_me(23)
  str_me%f1      = v_me(24)
  str_me%f2      = v_me(25)
  str_me%nao     = v_me(26)
  str_me%ko      = v_me(27)
  str_me%cao     = v_me(28)
  str_me%otcd    = v_me(29)
  str_me%otmgcd  = v_me(30)
  str_me%ocd     = v_me(31)
  str_me%ocalsed = v_me(32)
  str_me%caupd   = v_me(33)
  str_me%careld  = v_me(34)
  str_me%bdof    = v_me(35)
  return
end subroutine put_me_struct
! ======================================================================================
! ------------------------------------------------------------------------------------- 
! ===================================================================================== 
subroutine f_m(v,dt,m)
! ===================================================================================== 
! m activation gate  ----Ecuacion 4
! ===================================================================================== 
  implicit none
  real (rp),intent(in)   :: v,dt
  real (rp),intent(inout):: m
  real (rp)              ::taum,mm

  taum = (0.000042*exp(-((v+25.57)/28.8)**2))+0.000024;
  mm = 1.0/(1+exp((v+27.12)/(-8.21)));
  m = m + (dt*((mm-m)/taum)); !<-----
  return
end subroutine f_m
! ------------------------------------------------------------------------------------- 
subroutine f_h_total(v,dt,prm,h1,h2)
! ===================================================================================== 
! h total Sodium Gate ----Ecuacion 5
! ===================================================================================== 
  implicit none
  real(rp),intent(in)    :: v,dt
  type (t_prm)           :: prm
  real(rp),intent(inout) :: h1,h2
  real(rp)               :: tauh1,tauh2,hm

  tauh1 = (0.03_rp/(1.0_rp+exp((v+35.1)/(3.2))))+0.0003;
  tauh2 = (0.12_rp/(1.0_rp+exp((v+35.1)/(3.2))))+0.003; 
  hm = 1.0_rp/(1.0_rp+exp((v+prm%p_Vhm)/(5.3)));
  h1 = h1 + (dt*((hm-h1)/tauh1)); 
  h2 = h2 + (dt*((hm-h2)/tauh2)); 
  return
end subroutine f_h_total
! ------------------------------------------------------------------------------------- 
subroutine f_na(v,m,h1,h2,nao,nai,prm,INA,ena)
! ===================================================================================== 
! Na current  ----Ecuacion 3
! ===================================================================================== 
  implicit none
  real(rp),intent(in)   :: v,m,h1,h2,nao,nai
  type (t_prm)          :: prm
  real(rp),intent(out)  :: INA,ena
  real(rp)              :: aux1, aux2, FRT, invFRT

  FRT = (p_R*p_Te)/p_FA;
  invFRT = 1.0/FRT;
  ena = FRT* log(nao/nai);

  aux2 =  (p_R*p_Te*(exp(v*invFRT)-1.0));
  aux1 = prm%p_pna*((m**3))*(0.9*h1 + 0.1*h2)*nao*v*(p_FA**2);
  INA = aux1*(exp((v-ena)*invFRT)-1.0)/aux2;
  return
end subroutine f_na
! ------------------------------------------------------------------------------------- 
subroutine f_ca(v,dt,cado,prm,dl,fl1,fl2,ICAL)
! ===================================================================================== 
! Ca current    ----- Ecuacion 7
! ===================================================================================== 
! Ca,L current 
! ------------------------------------------------------------------------------------- 
  implicit none
  real(rp),intent(in)    :: v,dt,cado
  type (t_prm)           :: prm
  real(rp),intent(inout) :: dl,fl1,fl2
  real(rp),intent(out)   :: ICAL
  real(rp)               :: taudl,dml,taufl1,taufl2,fml,fca


  taudl = (0.0027*exp(-(((v+35.0)/30.0)**2)))+0.002;
  dml = 1.0 /(1+exp((v+9.0)/(-5.8)));
  taufl1= (0.161*exp(-(((v+40.0)/14.4)**2)))+0.01;
  taufl2= (1.3323*exp(-(((v+40.0)/14.2)**2)))+0.0626;
  fml   = 1.0/(1+exp((v+27.4)/(7.1)));
  dl    = dl + (dt*((dml-dl)/taudl)); 
  fl1   = fl1 + dt*(fml-fl1)/(prm%p_taufl1*taufl1);
  fl2   = fl2 + (dt*((fml-fl2)/taufl2));
  fca   = cado/(cado+p_kca);
  ICAL = prm%p_gcal*dl*((fca*fl1)+((1.0-fca)*fl2))*(v-p_ecal);
  return
end subroutine f_ca
! ------------------------------------------------------------------------------------- 
subroutine f_It(v,dt,ko,ki,prm,rr,s,ek,It)
! ===================================================================================== 
! It current ----- Ecuacion 13
! ===================================================================================== 
  implicit none
  real(rp),intent(in)    :: v,dt,ko,ki
  type (t_prm)           :: prm
  real(rp),intent(inout) :: rr,s
  real(rp),intent(out)   :: ek,It
  real(rp)               :: taus,taur,rm,sm

  taus= (0.4812*exp(-(((v+52.45)/(14.97))**2)))+0.01414;
  taur= (0.0035*exp(-((v/30.0)**2)))+0.0015;
  rm  = 1.0/(1+exp((v-prm%p_Vrm)/(-11.0)));
  sm  = 1.0/(1+exp((v+40.5)/(11.5))); 
  rr  = rr + (dt*((rm -rr)/taur)); 
  s   = s + (dt*((sm-s)/taus));
  ek  = ((p_R*p_Te)/p_FA)* log(ko/ki);
  It  = prm%p_gt*rr*s*(v-ek); !It ajustado
  return
end subroutine f_It
! ------------------------------------------------------------------------------------- 
subroutine f_Isus(v,dt,ek,prm,rsus,ssus,Isus)
! ===================================================================================== 
! Isus CURRENT           ------------Ecuacion 15
! =====================================================================================
  implicit none
  real(rp),intent(in)    :: v,dt,ek
  type (t_prm)           :: prm
  real(rp),intent(inout) :: rsus,ssus
  real(rp),intent(out)   :: Isus
  real(rp)               :: taussus,taursus,rsusm,ssusm

  taussus = (0.047/(1+exp((v+60.0)/(10.0))))+0.3;
  taursus = (0.009/(1+exp((v+5.0)/(12.0))))+0.0005;
  rsusm  = 1.0/(1+exp((v+4.3)/(-8.0)));
  ssusm = (0.4/(1+exp((v+20.0)/(10.0))))+0.6; 
  rsus = rsus + (dt*((rsusm -rsus)/taursus)); 
  ssus = ssus + (dt*((ssusm -ssus)/taussus));
  Isus = prm%p_gsus*rsus*ssus*(v-ek);! Isus ajustado gsus cte
  return
end subroutine f_Isus
! ------------------------------------------------------------------------------------- 
subroutine f_IKr(v,dt,ek,prm,pa,IKr,bdof)
! ===================================================================================== 
! IK delayed rectifier current ------------Ecuacion 19 y 20
! K,fast current
! ===================================================================================== 
  implicit none
  real(rp),intent(in)    :: v,dt,ek
  type (t_prm)           :: prm
  real(rp),intent(inout) :: pa, bdof
  real(rp),intent(out)   :: IKr
  real(rp)               :: taupa,pam,pi,bdofpam1,bdofpam2
  real(rp)               :: papi,unompi

  taupa= 0.03118+(0.21718*exp(-(((v+20.1376)/(22.1996))**2)));
  pam  = 1.0/(1+exp((v+15.0)/(-6.0)));
  pi   = 1.0/(1+exp((v+55.0)/(24.0)));
  pa   = pa + (dt*((pam-pa)/taupa));
  papi = pa*pi; 
  unompi = 1.0-pi;
  bdofpam1  = (0.467*papi+unompi*0.073429)*prm%p_Ddof;
  bdofpam2 = 0.003273*papi+0.000514*unompi;
  bdof = bdof + dt*(bdofpam1*(1.0-bdof)-bdofpam2*bdof);
  IKr  = prm%p_gkfast*pa*pi*(v-ek)*(1.0-bdof);
  return 
end subroutine f_IKr
! ------------------------------------------------------------------------------------- 
subroutine f_IK(v,dt,ek,IKr,prm,n,IK)
! ===================================================================================== 
! K,slow current -----Ecuacion 18
! ===================================================================================== 
  implicit none
  real(rp),intent(in)    :: v,dt,ek,IKr
  type (t_prm)           :: prm
  real(rp),intent(inout) :: n
  real(rp),intent(out)   :: IK
  real(rp)               :: taun,nm,IKs

  taun= 0.7+(0.4*exp(-(((v-20.0)/(20.0))**2)));
  nm  = 1.0/(1+exp((v-19.9)/(-12.7)));
  n   = n + (dt*((nm-n)/taun));                
  IKs = prm%p_gkslow*n*(v-ek); 
! ------------------------------------------------------------------------------------- 
  IK = IKr + IKs;
  return 
end subroutine f_IK
! ------------------------------------------------------------------------------------- 
subroutine f_IK1(v,ek,ko,prm,IK1)
! ===================================================================================== 
! IK1 inward rectifier current -----Ecuacion 21
! ===================================================================================== 
  implicit none
  real(rp),intent(in) :: v,ek,ko
  type (t_prm)        :: prm
  real(rp),intent(out):: IK1
  real(rp)            :: ik1max1

  ik1max1 = 1.0+exp((1.5*(v-ek+3.6)*p_FA)/(p_R*p_Te)); 
  IK1 = ((prm%p_gk1*(ko**(0.4457)))*(v-ek))/ik1max1; 
  return
end subroutine f_IK1
! ------------------------------------------------------------------------------------- 
subroutine f_basal(v,cao,cai,ena,prm,IBNA,IBCA,IB)
! ===================================================================================== 
! Basal currents  -----Ecuacion  22
! ===================================================================================== 
  implicit none
  real(rp),intent(in)  :: v,cao,cai,ena
  type (t_prm)         :: prm
  real(rp),intent(out) :: IBNA,IBCA,IB
  real(rp)             :: eca
  
  eca = ((p_R*p_Te)/(2.0*p_FA))*log(cao/cai);
  IBNA = prm%p_gbna*(v-ena);
  IBCA = prm%p_gbca*(v-eca);
  IB = IBNA+IBCA;
  return
end subroutine f_basal
! ------------------------------------------------------------------------------------- 
subroutine f_ICAP(cai,prm,ICAP)
! ===================================================================================== 
! Ca pump current -----Ecuacion 24
! ===================================================================================== 
  implicit none
  real(rp),intent(in)  :: cai
  type (t_prm)         :: prm
  real(rp),intent(out) :: ICAP

  ICAP = p_imcap*(cai/(cai+p_kcap));
  return 
end subroutine f_ICAP
! ------------------------------------------------------------------------------------- 
subroutine f_INAK(v,nai,ko,prm,INAK)
! ===================================================================================== 
! Na-K pump current -----Ecuacion 23
! =====================================================================================
  implicit none
  real(rp),intent(in) :: v,nai,ko
  type (t_prm)        :: prm
  real(rp),intent(out):: INAK
  real(rp)            :: inakmax

  inakmax = ((nai**1.5)/((nai**1.5)+(p_knakna**1.5))); 
  INAK = p_imnak*(ko/(ko+p_knakk))*inakmax*((v+150.0)/(v+200.0)); 
  return
end subroutine f_INAK
! ------------------------------------------------------------------------------------- 
subroutine f_INACA(v,nao,nai,cao,cai,prm,INACA)
! ===================================================================================== 
! Na-Ca exchanger current -----Ecuacion 25
! ===================================================================================== 
  implicit none
  real(rp),intent(in) :: v,nao,nai,cao,cai
  type (t_prm)        :: prm
  real(rp),intent(out):: INACA
  real(rp)            :: phif,phir,nmr,dnm
  
  phif  = exp((p_lambda*v*p_FA)/(p_R*p_Te));
  phir  = exp(((p_lambda-1.0)*v*p_FA)/(p_R*p_Te));
  nmr   = (((nai**3)*cao)*phif)-(((nao**3)*cai)*phir); 
  dnm   = 1.0+(p_dnaca*(((nao**3)*(cai))+((nai**3)*cao))); 
  INACA = p_knaca*(nmr/dnm);
  return
end subroutine f_INACA
! ------------------------------------------------------------------------------------- 
subroutine f_SRCa(dt,cai,cado,prm,f1,f2,ocalse,caup,carel)
! ===================================================================================== 
! SR Ca handling -----Ecuacion  29
! ===================================================================================== 
  implicit none
  real(rp),intent(in)    :: dt,cai,cado
  type (t_prm)           :: prm
  real(rp),intent(inout) :: f1,f2,ocalse,caup,carel
  real(rp)               :: kinact,kact,itr,num2,den2,IUP,IREL,aux

  kinact = 33.96+(339.6*((cai/(cai+p_kreli))**4));
  kact = 203.8*(((cai/(cai+p_kreli))**4)+((cado/(cado+p_kreldo))**4));

  f1 = f1 + (dt*(p_krecov*(1.0-f1-f2)-(kact*f1))); 
  f2 = f2 + (dt*((kact*f1)-(kinact*f2)));
  ocalse = ocalse + (dt*((480.0*carel*(1.0-ocalse))-(400.0*ocalse))); 

  itr   = (caup-carel)*((2.0*p_FA*p_volrel)/p_tautr);
  num2  = (cai/p_kcyca)-(((p_kxcs**2)*caup)/p_ksrca); 
  den2  = ((cai+p_kcyca)/p_kcyca)+(p_kxcs*((caup+p_ksrca)/p_ksrca)); 
  IUP   = p_imup * (num2/den2);
  IREL  = p_alprel*((f2/(f2+0.25))**2)*(carel-cai);            
  caup  = caup + (dt*((IUP-itr)/(2.0*p_volup*p_FA)));
  aux   = (31.0*((480.0*carel*(1.0-ocalse))-(400.0*ocalse)));
  carel = carel + (dt*(((itr-IREL)/(2.0*p_volrel*p_FA))-aux));
  return
end subroutine f_SRCa
! ------------------------------------------------------------------------------------- 
subroutine f_ICab(dt,cai,prm,oc,otc,otmgc,otmgcd,otnmgmg)
! ===================================================================================== 
! Intracellular Ca buffering  -----Ecuacion 27
! =====================================================================================  
  implicit none
  real(rp),intent(in)    :: dt,cai
  type (t_prm)           :: prm
  Real(rp),intent(inout) :: oc,otc,otmgc,otnmgmg,otmgcd
  Real(rp)               :: ocd,otcd

  ocd     = (200000.0*cai*(1.0-oc))-(476.0*oc);
  otcd    = (78400.0*cai*(1.0-otc))-(392.0*otc);
  otmgcd  = (200000.0*cai*(1.0-otmgc-otnmgmg))-(6.6*otmgc);
  oc      = oc  + (dt*(ocd));
  otc     = otc + (dt*(otcd));
  otmgc   = otmgc   + (dt*(otmgcd));
  otnmgmg = otnmgmg + (dt*((2000.0*p_mgi*(1.0-otmgc-otnmgmg))-(666.0*otnmgmg))); 
  return
end subroutine f_ICab
! ------------------------------------------------------------------------------------- 
subroutine f_Icon(dt,INA,IBNA,INAK,INACA,ICAL,IBCA,ICAP,IUP,IREL,It,Isus,IK1,IKs, &
                  IKr,otcd,otmgcd,ocd,prm,nai,ki,cado,cai)
! ===================================================================================== 
! Intracellular ion concentration  -----Ecuacion 26
! ===================================================================================== 
  implicit none
  real(rp),intent(in)    :: dt,INA,IBNA,INAK,INACA,ICAL,IBCA,ICAP,IUP,IREL,It,     &
                            Isus,IK1,IKs,IKr,otcd,otmgcd,ocd
  type (t_prm)           :: prm
  real(rp),intent(inout) :: nai,ki,cado,cai
  real(rp)               :: phicai,IDI,aux1,aux2

  phicai= (0.08*otcd)+(0.16*otmgcd)+(0.045*ocd); 
  aux1  = (p_FA*p_voli);
  nai   = nai + (dt*(-(INA+IBNA+(3.0*INAK)+(3.0*INACA)+p_phinaen)/aux1));
  ki    = ki +  (dt*(-(It+Isus+IK1+IKs+IKr-(2.0*INAK))/aux1)); 
  IDI   = (cado-cai)*(2.0*p_FA*p_voldo/p_taudi);
  cado  = cado + (dt*(-((ICAL + IDI)/(2.0*p_voldo*p_FA)))); !Error in paper:(iCAL - IDI)
  aux2  = 2.0*aux1-phicai;
  cai   = cai - dt*(((IBCA+ICAP+IUP)-(IDI+IREL+2.0*INACA))/aux2); 
  return
end subroutine f_Icon
! ------------------------------------------------------------------------------------- 
subroutine f_Cleft(dt,INA,IBNA,INAK,INACA,It,Isus,IK1,IKs,IKR,ICAL,IBCA,    &
                   ICAP,prm,nao,ko,cao)	
! =====================================================================================
! Cleft Spase ion concentration -----Ecuacion 28
! =====================================================================================
  implicit none
  real(rp),intent(in)    :: dt,INA,IBNA,INAK,INACA,It,Isus,IK1,IKs,IKR,     &
                            ICAL,IBCA,ICAP
  type (t_prm)           :: prm
  real(rp),intent(inout) :: nao,ko,cao
  real(rp)               :: aux1,aux2

  aux1 = (p_volo*p_FA);
  aux2 = INA+IBNA+(3.0*INAK)+(3.0*INACA);
  nao  = nao + (dt*(((p_nab-nao)/p_tauna)+((aux2+p_phinaen)/aux1)));
  ko   = ko  + (dt*(((p_kb-ko)/p_tauk)+((It+Isus+IK1+IKs+IKr-(2.0*INAK))/aux1)));
  cao  = cao + (dt*(((p_cab-cao)/p_tauca)+((ICAL+IBCA+ICAP-(2.0*INACA))/(2.0*aux1))));
  return
end subroutine f_Cleft
!-------------------------------------------------------------------------------
subroutine Nygren_A_P01 (ict, dt, v, Iion, v_prm, v_nyg, v_cr)
!-------------------------------------------------------------------------------
  implicit none
  integer(ip), intent(in)    :: ict
  real(rp),    intent(in)    :: dt, v
  real(rp),    intent(in)    :: v_prm(:)
  real(rp),    intent(out)   :: Iion
  real(rp),    intent(inout) :: v_nyg(nvar_ny)
  real(rp),    intent(out)   :: v_cr(ncur_ny)
  type(t_nyg)                :: nygren
  type (t_prm)               :: param
  real(rp)                   :: INA,ICAL,ek,It,Isus,IKr,IKs,IK,IK1,ena,    &
                                IBNA,IBCA,IB,ICAP,INAK,INACA,IUP,IREL
  !
  call put_me_struct(v_nyg,nygren)
  call put_param(v_prm,param)
  !
  call f_m(v,dt,nygren%m)  !....
  call f_h_total(v,dt,param,nygren%h1,nygren%h2)
  call f_na(v,nygren%m,nygren%h1,nygren%h2,nygren%nao,nygren%nai,param,INA,ena)
  call f_ca(v,dt,nygren%cado,param,nygren%dl,nygren%fl1,nygren%fl2,ICAL)
  call f_It(v,dt,nygren%ko,nygren%ki,param,nygren%rr,nygren%s,ek,It)
  call f_Isus(v,dt,ek,param,nygren%rsus,nygren%ssus,Isus)
  call f_IKr(v,dt,ek,param,nygren%pa,IKr,nygren%bdof)
  call f_IK(v,dt,ek,IKr,param,nygren%n,IK)
  call f_IK1(v,ek,nygren%ko,param,IK1)
  call f_basal(v,nygren%cao,nygren%cai,ena,param,IBNA,IBCA,IB)
  call f_ICAP(nygren%cai,param,ICAP)
  call f_INAK(v,nygren%nai,nygren%ko,param,INAK)
  call f_INACA(v,nygren%nao,nygren%nai,nygren%cao,nygren%cai,param,INACA)
  call f_SRCa(dt,nygren%cai,nygren%cado,param,nygren%f1,nygren%f2,nygren%ocalse,      &
              nygren%caup,nygren%carel)
  call f_ICab(dt,nygren%cai,param,nygren%oc,nygren%otc,nygren%otmgc,nygren%otmgcd,    &
              nygren%otnmgmg)
  call f_Icon(dt,INA,IBNA,INAK,INACA,ICAL,IBCA,ICAP,IUP,IREL,It,Isus,IK1,IKs,   &
              IKr,nygren%otcd,nygren%otmgcd,nygren%ocd,param,nygren%nai,nygren%ki,    &
              nygren%cado,nygren%cai)
  call f_Cleft(dt,INA,IBNA,INAK,INACA,It,Isus,IK1,IKs,IKR,ICAL,IBCA,    &
               ICAP,param,nygren%nao,nygren%ko,nygren%cao)
  Iion = INA+It+Isus+ICAL+IK+IK1+IB+ICAP+INAK+INACA;
  !
  call get_me_struct(nygren,v_nyg)

  !.
  v_cr(1:ncur_ny) = (/ INA, ICAL, ek, It, Isus, IKr, IKs, IK, IK1, ena,    &
                  IBNA, IBCA, IB, ICAP, INAK, INACA, IUP, IREL, Iion /)
  !.

  return
end subroutine Nygren_A_P01
!-------------------------------------------------------------------------------
end module mod_nygren
!-------------------------------------------------------------------------------
