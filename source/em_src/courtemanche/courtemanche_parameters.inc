!-----------------------------------------------------------------------------------------------------------------------------
! This are the parameters for the Courtemanche Model (M Courtemanche, RJ Ramirez, S Nattel; Am J Physiol 275:H301-H321, 1998)
! Implemented by Laura Martínez Mateu (laumarma@gbio.i3bh.es)  20/01/2014
! Checked  by JFR (jfrodrig@unizar.es)  31/03/2014
!
! p_INakmax must be increased by a 40% in order to stabilize Nai and Ki homeostasis and do not loose
! the spike and dome morphology of the AP
! Constants
!-----------------------------------------------------------------------------------------------------------------------------
! Model Constants
! (Parameter hace que sean constantes simbólicas y que su valor no pueda cambiar a lo largo de la ejecución del programa)

! Cm=100pF

 real(rp), parameter, private :: p_R        = 8.3143;       ! J/mol·K    
 real(rp), parameter, private :: p_F        = 96.4867;      ! C/mmol  
 real(rp), parameter, private :: p_T        = 310.0;	    ! K 
 real(rp), parameter, private :: p_Cm       = 100.0;        ! pF
 
 real(rp), parameter, private :: p_Vcell    = 20100.0;      ! um^3
 real(rp), parameter, private :: p_Vi       = 13668.0;      ! um^3
 real(rp), parameter, private :: p_Vup      = 1109.52;      ! um^3
 real(rp), parameter, private :: p_Vrel     = 96.48;        ! um^3
 
 real(rp), parameter, private :: p_Ko       = 5.4;          ! mM
 real(rp), parameter, private :: p_Nao      = 140.0;        ! mM
 real(rp), parameter, private :: p_Cao      = 1.8;          ! mM

 !-----------------------------------------------------------------------------------------------------------
 real(rp), parameter, private :: p_gNa      = 7.8;	   		! nS/pF 
 real(rp), parameter, private :: p_gK1      = 0.09;	    	! nS/pF  
 real(rp), parameter, private :: p_gto      = 0.1652;	    ! nS/pF 
 real(rp), parameter, private :: p_gKr      = 0.029411765;	! nS/pF
 real(rp), parameter, private :: p_gKs      = 0.12941176;  	! nS/pF 
 real(rp), parameter, private :: p_gCaL     = 0.12375;	    ! nS/pF
 real(rp), parameter, private :: p_gbCa     = 0.00113;	    ! nS/pF
 real(rp), parameter, private :: p_gbNa     = 0.0006744375; ! nS/pF
 real(rp), parameter, private :: p_gKur     = 0.005;        ! nS/pF  (valor para las CI del paper)
 
 ! Región CT-BB
 real(rp), parameter, private :: p_gCaL_CT_BB  = 0.2067;	! nS/pF  (g_CaL_CT_BB/g_CaL=1.67)
 
 ! Región TVR
 real(rp), parameter, private :: p_gCaL_TVR = 0.0829;	    ! nS/pF  (g_CaL_TVR/g_CaL=0.67) 
 real(rp), parameter, private :: p_gKr_TVR  = 0.045;	    ! nS/pF  (g_Kr_TVR/g_Kr=1.53) 

 ! Región RAA
 real(rp), parameter, private :: p_gto_RAA  = 0.1123;	    ! nS/pF  (gto_RAA/gto=0.68)
 real(rp), parameter, private :: p_gKr_RAA  = 0.0382;	    ! nS/pF  (gKr_RAA/gKr=1.3) 
 
 ! Región LA
 real(rp), parameter, private :: p_gKr_LA  = 0.047;	    ! nS/pF  (gKr_LA/gKr=1.6) 
 
 ! Región PV
 real(rp), parameter, private :: p_gKr_PV  = 0.0706;	    ! nS/pF  (gKr_PV/gKr=2.4) 
 
 ! Región LAA
 real(rp), parameter, private :: p_gto_LAA = 0.1123;	    ! nS/pF  (gto_LAA/gto=0.68)
 real(rp), parameter, private :: p_gKr_LAA = 0.0612;	    ! nS/pF  (gKr_LAA/gKr=2.08) 
 
 ! Región MVR
 real(rp), parameter, private :: p_gCaL_MVR = 0.0829;	    ! nS/pF  (gCaL_MVR/gCaL=0.67) 
 real(rp), parameter, private :: p_gKr_MVR  = 0.0717;	    ! nS/pF  (gKr_MVR/gKr=2.44)

 !-------------------------------------------------------------------------------------------------------
 
 real(rp), parameter, private :: p_INaKmax  = 1.0*0.59933874;	! pA/pF
 real(rp), parameter, private :: p_INaCamax = 1600.0;       ! pA/pF
 real(rp), parameter, private :: p_IpCamax  = 0.275;	    ! pA/pF 

 real(rp), parameter, private :: p_Iupmax   = 0.005;	    ! mM/ms

 real(rp), parameter, private :: p_KQ10     = 3.0;	        ! 
 real(rp), parameter, private :: p_gamma    = 0.35;	        ! 
 
 real(rp), parameter, private :: p_KmNai    = 10.0;          ! mM
 real(rp), parameter, private :: p_KmKo     = 1.5;           ! mM
 real(rp), parameter, private :: p_KmNa     = 87.5;          ! mM
 
 real(rp), parameter, private :: p_KmCa     = 1.38;          ! 
 real(rp), parameter, private :: p_ksat     = 0.1;           !
 
 real(rp), parameter, private :: p_krel     = 30.0;          ! 1/ms 
 
 real(rp), parameter, private :: p_Kup      = 0.00092;       ! mM
 real(rp), parameter, private :: p_Caupmax  = 15.0;          ! mM
 real(rp), parameter, private :: p_CMDNmax  = 0.05;          ! mM
 real(rp), parameter, private :: p_TRPNmax  = 0.07;          ! mM
 real(rp), parameter, private :: p_CSQNmax  = 10.0;          ! mM
 real(rp), parameter, private :: p_KmCmdn   = 0.00238;       ! mM
 real(rp), parameter, private :: p_KmTrpn   = 0.0005;        ! mM 
 real(rp), parameter, private :: p_KmCsqn   = 0.8;           ! mM
  
 !real(rp), parameter, private :: p_ACh      = 1.0E-24;	     ! mM	
 
!------------------------------------------------------------------------------
!-- INITIAL CONDITIONS --------------------------------------------------------
!------------------------------------------------------------------------------
!
! Partiendo de las Cond.Ini. del paper
!
  real(rp), parameter :: Vi_courte    = -81.18;
  real(rp), parameter :: Naiic        = 1.117e+01;
  real(rp), parameter :: mic          = 2.908e-03;
  real(rp), parameter :: hic          = 9.649e-01;
  real(rp), parameter :: jic          = 9.775e-01;
  real(rp), parameter :: Kiic         = 1.390e+02;
  real(rp), parameter :: oaic         = 3.043e-02;
  real(rp), parameter :: oiic         = 9.992e-01;
  real(rp), parameter :: uaic         = 4.966e-03;
  real(rp), parameter :: uiic         = 9.986e-01;
  real(rp), parameter :: xric         = 3.296e-05;
  real(rp), parameter :: xsic         = 1.869e-02;
  real(rp), parameter :: Caiic        = 1.013e-04;
  real(rp), parameter :: dic          = 1.367e-04;
  real(rp), parameter :: fic          = 9.996e-01;
  real(rp), parameter :: fCaic        = 7.755e-01;
  real(rp), parameter :: Carelic      = 1.488e+00;
  real(rp), parameter :: uic          = 0.000e+00;
  real(rp), parameter :: vic          = 1.000e+00;
  real(rp), parameter :: wic          = 9.992e-01;
  real(rp), parameter :: Caupic       = 1.488e+00;
  
  
 ! Condicions iniciales para la heterogeneidad en la aurícula, con 10min de estabilización a BCL=1000ms 
 
 ! RA_PM
  real(rp), parameter :: Vi_courte_RA_PM    = -81.47112568;
  real(rp), parameter :: uic_RA_PM          = 0.0;
  real(rp), parameter :: vic_RA_PM          = 0.99999999;
  real(rp), parameter :: wic_RA_PM          = 0.999212096;
  real(rp), parameter :: dic_RA_PM          = 0.00013183;
  real(rp), parameter :: hic_RA_PM          = 0.96703672;
  real(rp), parameter :: xric_RA_PM         = 0.00096624;
  real(rp), parameter :: Naiic_RA_PM        = 13.60984407;
  real(rp), parameter :: Kiic_RA_PM         = 134.677545;
  real(rp), parameter :: Carelic_RA_PM      = 1.09091364;
  real(rp), parameter :: oiic_RA_PM         = 0.99928115;
  real(rp), parameter :: uiic_RA_PM         = 0.99132309;
  real(rp), parameter :: mic_RA_PM          = 0.00277271;
  real(rp), parameter :: jic_RA_PM          = 0.97886876;
  real(rp), parameter :: fic_RA_PM          = 0.94750162;
  real(rp), parameter :: xsic_RA_PM         = 0.01907215;
  real(rp), parameter :: oaic_RA_PM         = 0.02995297;
  real(rp), parameter :: uaic_RA_PM         = 0.00482015;
  real(rp), parameter :: fCaic_RA_PM        = 0.73882150;
  real(rp), parameter :: Caiic_RA_PM        = 0.00012364;
  real(rp), parameter :: Caupic_RA_PM       = 1.56179367;
 
 ! CT_BB
  real(rp), parameter :: Vi_courte_CT_BB    = -81.45969227;
  real(rp), parameter :: uic_CT_BB          = 0.0;
  real(rp), parameter :: vic_CT_BB          = 0.99999999;
  real(rp), parameter :: wic_CT_BB          = 0.99921156;
  real(rp), parameter :: dic_CT_BB          = 0.000132025;
  real(rp), parameter :: hic_CT_BB          = 0.96693984;
  real(rp), parameter :: xric_CT_BB         = 0.00161403;
  real(rp), parameter :: Naiic_CT_BB        = 15.47923447;
  real(rp), parameter :: Kiic_CT_BB         = 132.64543373;
  real(rp), parameter :: Carelic_CT_BB      = 1.68158908;
  real(rp), parameter :: oiic_CT_BB         = 0.99927899;
  real(rp), parameter :: uiic_CT_BB         = 0.99009998;
  real(rp), parameter :: mic_CT_BB          = 0.00277794;
  real(rp), parameter :: jic_CT_BB          = 0.97876873;
  real(rp), parameter :: fic_CT_BB          = 0.93161654;
  real(rp), parameter :: xsic_CT_BB         = 0.01930460;
  real(rp), parameter :: oaic_CT_BB         = 0.02997292;
  real(rp), parameter :: uaic_CT_BB         = 0.00482617;
  real(rp), parameter :: fCaic_CT_BB        = 0.67553857;
  real(rp), parameter :: Caiic_CT_BB        = 0.00016799;
  real(rp), parameter :: Caupic_CT_BB       = 2.13376871;
  
 ! TVR
  real(rp), parameter :: Vi_courte_TVR    = -81.62437466;
  real(rp), parameter :: uic_TVR          = 0.0;
  real(rp), parameter :: vic_TVR          = 0.99999999;
  real(rp), parameter :: wic_TVR          = 0.99921916;
  real(rp), parameter :: dic_TVR          = 0.00012933;
  real(rp), parameter :: hic_TVR          = 0.96813667;
  real(rp), parameter :: xric_TVR         = 0.00049662;
  real(rp), parameter :: Naiic_TVR        = 12.70273706;
  real(rp), parameter :: Kiic_TVR         = 135.68551654;
  real(rp), parameter :: Carelic_TVR      = 0.77988984;
  real(rp), parameter :: oiic_TVR         = 0.99930216;
  real(rp), parameter :: uiic_TVR         = 0.99247229;
  real(rp), parameter :: mic_TVR          = 0.0027036;
  real(rp), parameter :: jic_TVR          = 0.97966186;
  real(rp), parameter :: fic_TVR          = 0.96847189;
  real(rp), parameter :: xsic_TVR         = 0.01872522;
  real(rp), parameter :: oaic_TVR         = 0.02969923;
  real(rp), parameter :: uaic_TVR         = 0.00474390;
  real(rp), parameter :: fCaic_TVR        = 0.77939294;
  real(rp), parameter :: Caiic_TVR        = 9.90036495E-5;
  real(rp), parameter :: Caupic_TVR       = 1.21482367;
 
 ! MVR
  real(rp), parameter :: Vi_courte_MVR    = -81.73889305;
  real(rp), parameter :: uic_MVR          = 0.0;
  real(rp), parameter :: vic_MVR          = 0.99999999;
  real(rp), parameter :: wic_MVR          = 0.99922440;
  real(rp), parameter :: dic_MVR          = 0.00012749;
  real(rp), parameter :: hic_MVR          = 0.96892834;
  real(rp), parameter :: xric_MVR         = 0.00041721;
  real(rp), parameter :: Naiic_MVR        = 12.84212123;
  real(rp), parameter :: Kiic_MVR         = 135.5563812;
  real(rp), parameter :: Carelic_MVR      = 0.74815751;
  real(rp), parameter :: oiic_MVR         = 0.99931717;
  real(rp), parameter :: uiic_MVR         = 0.99264806;
  real(rp), parameter :: mic_MVR          = 0.00265306;
  real(rp), parameter :: jic_MVR          = 0.98021245;
  real(rp), parameter :: fic_MVR          = 0.97336161;
  real(rp), parameter :: xsic_MVR         = 0.01858256;
  real(rp), parameter :: oaic_MVR         = 0.02951149;
  real(rp), parameter :: uaic_MVR         = 0.00468786;
  real(rp), parameter :: fCaic_MVR        = 0.78249133;
  real(rp), parameter :: Caiic_MVR        = 9.72288495E-5;
  real(rp), parameter :: Caupic_MVR       = 1.17928747;
  
 ! RAA
  real(rp), parameter :: Vi_courte_RAA    = -81.60239665;
  real(rp), parameter :: uic_RAA          = 0.0;
  real(rp), parameter :: vic_RAA          = 0.99999999;
  real(rp), parameter :: wic_RAA          = 0.99921815;
  real(rp), parameter :: dic_RAA          = 0.00012969;
  real(rp), parameter :: hic_RAA          = 0.96797184;
  real(rp), parameter :: xric_RAA         = 0.00106636;
  real(rp), parameter :: Naiic_RAA        = 13.99709675;
  real(rp), parameter :: Kiic_RAA         = 134.28257209;
  real(rp), parameter :: Carelic_RAA      = 1.13011572;
  real(rp), parameter :: oiic_RAA         = 0.99929879;
  real(rp), parameter :: uiic_RAA         = 0.99030316;
  real(rp), parameter :: mic_RAA          = 0.00271340;
  real(rp), parameter :: jic_RAA          = 0.97952357;
  real(rp), parameter :: fic_RAA          = 0.95017447;
  real(rp), parameter :: xsic_RAA         = 0.01890953;
  real(rp), parameter :: oaic_RAA         = 0.02973617;
  real(rp), parameter :: uaic_RAA         = 0.00475497;
  real(rp), parameter :: fCaic_RAA        = 0.73344928;
  real(rp), parameter :: Caiic_RAA        = 0.00012711;
  real(rp), parameter :: Caupic_RAA       = 1.59765466;
  
 ! LAA
  real(rp), parameter :: Vi_courte_LAA    = -81.79127112;
  real(rp), parameter :: uic_LAA          = 0.0;
  real(rp), parameter :: vic_LAA          = 0.99999999;
  real(rp), parameter :: wic_LAA          = 0.99922678;
  real(rp), parameter :: dic_LAA          = 0.00012666;
  real(rp), parameter :: hic_LAA          = 0.9692745;
  real(rp), parameter :: xric_LAA         = 0.00083904;
  real(rp), parameter :: Naiic_LAA        = 14.14444903;
  real(rp), parameter :: Kiic_LAA         = 134.15672156;
  real(rp), parameter :: Carelic_LAA      = 1.0565325;
  real(rp), parameter :: oiic_LAA         = 0.99932351;
  real(rp), parameter :: uiic_LAA         = 0.99050318;
  real(rp), parameter :: mic_LAA          = 0.00263025;
  real(rp), parameter :: jic_LAA          = 0.98043143;
  real(rp), parameter :: fic_LAA          = 0.95865682;
  real(rp), parameter :: xsic_LAA         = 0.01866412;
  real(rp), parameter :: oaic_LAA         = 0.02942674;
  real(rp), parameter :: uaic_LAA         = 0.00466267;
  real(rp), parameter :: fCaic_LAA        = 0.73993835;
  real(rp), parameter :: Caiic_LAA        = 0.00012293;
  real(rp), parameter :: Caupic_LAA       = 1.52303703;
  
 ! LA
  real(rp), parameter :: Vi_courte_LA    = -81.64648594;
  real(rp), parameter :: uic_LA          = 0.0;
  real(rp), parameter :: vic_LA          = 0.99999999;
  real(rp), parameter :: wic_LA          = 0.99922018;
  real(rp), parameter :: dic_LA          = 0.00012897;
  real(rp), parameter :: hic_LA          = 0.96828249;
  real(rp), parameter :: xric_LA         = 0.00075908;
  real(rp), parameter :: Naiic_LA       = 13.74058732;
  real(rp), parameter :: Kiic_LA         = 134.56740337;
  real(rp), parameter :: Carelic_LA      = 1.021789;
  real(rp), parameter :: oiic_LA         = 0.99930472;
  real(rp), parameter :: uiic_LA         = 0.99156221;
  real(rp), parameter :: mic_LA          = 0.00269377;
  real(rp), parameter :: jic_LA          = 0.97974509;
  real(rp), parameter :: fic_LA          = 0.95632612;
  real(rp), parameter :: xsic_LA         = 0.01882381;
  real(rp), parameter :: oaic_LA         = 0.02966352;
  real(rp), parameter :: uaic_LA         = 0.00473322;
  real(rp), parameter :: fCaic_LA        = 0.74520968;
  real(rp), parameter :: Caiic_LA        = 0.00011959;
  real(rp), parameter :: Caupic_LA       = 1.49076955;
  
 ! PV
  real(rp), parameter :: Vi_courte_PV    = -81.79856496;
  real(rp), parameter :: uic_PV          = 0.0;
  real(rp), parameter :: vic_PV          = 0.99999999;
  real(rp), parameter :: wic_PV          = 0.99922712;
  real(rp), parameter :: dic_PV          = 0.00012655;
  real(rp), parameter :: hic_PV          = 0.96932559;
  real(rp), parameter :: xric_PV         = 0.0006131;
  real(rp), parameter :: Naiic_PV        = 13.85497081;
  real(rp), parameter :: Kiic_PV         = 134.47146480;
  real(rp), parameter :: Carelic_PV      = 0.96187432;
  real(rp), parameter :: oiic_PV         = 0.99932453;
  real(rp), parameter :: uiic_PV         = 0.99177634;
  real(rp), parameter :: mic_PV          = 0.00262709;
  real(rp), parameter :: jic_PV          = 0.98047077;
  real(rp), parameter :: fic_PV          = 0.96355274;
  real(rp), parameter :: xsic_PV         = 0.01862588;
  real(rp), parameter :: oaic_PV         = 0.02941471;
  real(rp), parameter :: uaic_PV         = 0.0046591;
  real(rp), parameter :: fCaic_PV        = 0.75088626;
  real(rp), parameter :: Caiic_PV        = 0.00011604;
  real(rp), parameter :: Caupic_PV       = 1.42801233; 
  
!------------------------------------------------------------------------------
!-- STRUCTURE PARAMETERS ------------------------------------------------------
!------------------------------------------------------------------------------
  integer(ip), parameter :: Courte_mod  		= 23;  			! Courtemanche model (CRN)  
  integer(ip), parameter :: Courte_mod_RA_PM  	= 231;  		! Courtemanche model (CRN): RA/PM  
  integer(ip), parameter :: Courte_mod_CT_BB  	= 232;  		! Courtemanche model (CRN): CT/BB  
  integer(ip), parameter :: Courte_mod_TVR  	= 233;  		! Courtemanche model (CRN): TVR  
  integer(ip), parameter :: Courte_mod_RAA  	= 234;  		! Courtemanche model (CRN): RAA  
  integer(ip), parameter :: Courte_mod_LA  		= 235;  		! Courtemanche model (CRN): LA  
  integer(ip), parameter :: Courte_mod_LAA  	= 236;  		! Courtemanche model (CRN): LAA  
  integer(ip), parameter :: Courte_mod_PV  		= 237;  		! Courtemanche model (CRN): PV  
  integer(ip), parameter :: Courte_mod_MVR  	= 238;  		! Courtemanche model (CRN): MVR  
  integer(ip),parameter  :: nvar_courte 		= 20;  			! Number of state variables
  integer(ip),parameter  :: ncur_courte 		= 17;  			! Number of currents
  integer(ip),parameter  :: np_courte   		= 9;  			! Number of modifiable parameters  ! De momento pongo las conductancias máximas
!------------------------------------------------------------------------------
!-- ADAPTIVE TIME STEPING -----------------------------------------------------
!------------------------------------------------------------------------------
!.
  integer(ip), parameter    :: m_stpML   =    5
  real(rp),    parameter    :: m_dvdtML  =  1.0
