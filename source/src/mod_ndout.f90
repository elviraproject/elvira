!  -----------------------------------------------------------------------------
module mod_ndout
!  -----------------------------------------------------------------------------
! This module reads a list of nodes on which all model variables (or only the 
! potential) are printed. The frequency at which data will be saved is also 
! read in this module
!  -----------------------------------------------------------------------------
  use mod_precision
  use mod_filename
  use mod_error
!  -----------------------------------------------------------------------------
  implicit none
!  -----------------------------------------------------------------------------

!  -----------------------------------------------------------------------------
!  nn_p  : number of nodes for postprocessing
!. n_int : frequency at wich data is saved
!. flag  : Indicates if all variables will be saved (flag=1) or if only the
!          potential (flag=0)
!. l_nd  : list of nodes
!  -----------------------------------------------------------------------------
  type, public :: t_ndout
    private
    integer(ip)               :: nn_p = 0
    integer(ip)               :: n_int = 0 
    integer(ip)               :: flag = 0
    integer(ip),allocatable   :: l_nd(:),ln_md(:),tc_md(:)
  end type t_ndout
  !.
contains
! ------------------------------------------------------------------------------
subroutine lec_node_out(lu_m, arch_d, strout, nd_out)
! ------------------------------------------------------------------------------
! Esta rutina lee los nodos para postproceso
! ------------------------------------------------------------------------------
  implicit none
  integer(ip), intent(in)      :: lu_m
  character (len=*), intent(in):: arch_d, strout
  type(t_ndout)                :: nd_out
  !.
  integer(ip)                  :: lu_lec, io_err, i_err


  lu_lec = lu_m; call unitnumber (arch_d, strout, lu_lec, io_err)
  if (io_err > 0) call w_error (47,'subroutine lec_node_out',1)
  !.
  read (lu_lec,*,iostat=io_err) nd_out%nn_p, nd_out%n_int, nd_out%flag
  if (io_err > 0) call w_error (3,'nd_out%nn_p, nd_out%n_int, nd_out%flag',1)
  !
  allocate (nd_out%l_nd(nd_out%nn_p), stat=i_err)
  if (i_err > 0 ) call w_error (4,'nd_out%l_nd(nd_out%nn_p)',1)
  !. 
  read (lu_lec,*,iostat=io_err) nd_out%l_nd(1:nd_out%nn_p) 
  if (io_err > 0) call w_error (3,'nd_out%l_nd(1:nd_out%nn_p)',1)
  !.
  if (lu_lec /= lu_m) call close_unit (lu_lec)
  write(*,10) nd_out%nn_p; write(lu_log,10) nd_out%nn_p 
  if(nd_out%flag.eq.1) then
    write (std_o,*) ' writing all state variables' 
    write (lu_log,*) ' writing all state variables' 
  else
    write (std_o,*) ' writing only the action potential' 
    write (lu_log,*) ' writing only the action potential' 
  endif
  write (std_o,20) nd_out%n_int
  write (std_o,30) nd_out%l_nd(1),nd_out%l_nd(nd_out%nn_p)
  !.
  return
10 format ('###.Output nodes, read                            : ',I8)
20 format ('  Freq.: ',I6)
30 format ('  ',I6,' ... ',I6)
end subroutine lec_node_out
! ------------------------------------------------------------------------------
subroutine bin_write_node_out (lu_m,ln_d, nd_out) 
! ------------------------------------------------------------------------------
  implicit none
  integer(ip), intent(in)      :: lu_m, ln_d(:)
  type(t_ndout)                :: nd_out
  !.
  integer(ip)                  :: cn_p, i, inod
  integer(ip)                  :: lst(nd_out%nn_p)

  cn_p=0
  do i=1,nd_out%nn_p
    inod = nd_out%l_nd(i)
    if (ln_d(inod) > 0) then
      cn_p = cn_p+1
      lst(cn_p)=ln_d(inod)
    end if
  end do
  !.
  write(lu_m) '*NODEOUTPUT', cn_p,nd_out%n_int, nd_out%flag
  !.
  if (cn_p > 0) write (lu_m) lst(1:cn_p)
  return
end subroutine bin_write_node_out
! ------------------------------------------------------------------------------
subroutine ascii_write_node_out (lu_m,ln_d, nd_out) 
! ------------------------------------------------------------------------------
  implicit none
  integer(ip), intent(in)      :: lu_m, ln_d(:)
  type(t_ndout)                :: nd_out
  !.
  integer(ip)                  :: cn_p, i, inod
  integer(ip)                  :: lst(nd_out%nn_p)

  cn_p=0
  do i=1,nd_out%nn_p
    inod = nd_out%l_nd(i)
    if (ln_d(inod) > 0) then
      cn_p = cn_p+1
      lst(cn_p)=ln_d(inod)
    end if
  end do
  !.
  write(lu_m,10) '*NODEOUTPUT', cn_p,nd_out%n_int, nd_out%flag
  !.
  if (cn_p > 0) write (lu_m,20) lst(1:cn_p)
  return
10 format(A,3(2X,I6))
20 format(10(:,2X,I8))
end subroutine ascii_write_node_out
! ------------------------------------------------------------------------------
subroutine bin_read_node_out (lu_m, nd_out) 
! ------------------------------------------------------------------------------
  implicit none
  integer(ip), intent(in)      :: lu_m
  type(t_ndout)                :: nd_out
  !.
  character (len=11)           :: str
  integer(ip)                  :: io_err, i_err

  !.
  read(lu_m, iostat=io_err) str, nd_out%nn_p, nd_out%n_int, nd_out%flag
  if (io_err > 0) call w_error (3,'str,nd_out%nn_p, nd_out%n_int, nd_out%flag',1)
  !.
  if (nd_out%nn_p > 0) then
    allocate (nd_out%l_nd(nd_out%nn_p), stat=i_err)
    if (i_err > 0 ) call w_error (4,'nd_out%l_nd(nd_out%nn_p)',1)
    !.
    read (lu_m,iostat=io_err) nd_out%l_nd(1:nd_out%nn_p) 
    if (io_err > 0) call w_error (3,'nd_out%l_nd(1:nd_out%nn_p)',1)
  end if
  return
end subroutine bin_read_node_out
!  -----------------------------------------------------------------------------
function deallocate_node_out (nd_out) result(flg)
!  -----------------------------------------------------------------------------
  implicit none
  type(t_ndout)            :: nd_out
  integer(ip)              :: flg
  !.
  integer(ip)              :: istat
  
  flg = 0
  if (allocated(nd_out%l_nd)) then
    deallocate (nd_out%l_nd, stat=istat)
    nd_out%nn_p=0; nd_out%n_int=0 
    if (istat /= 0) flg = 1
  else
    flg = -1
  end if
  return
end function deallocate_node_out
! ------------------------------------------------------------------------------
function get_ndout_nnp(nd_out) result(nn_p)
! ------------------------------------------------------------------------------
  implicit none
  type(t_ndout)            :: nd_out
  integer(ip)              :: nn_p


  nn_p = nd_out%nn_p
  return
end function get_ndout_nnp
! ------------------------------------------------------------------------------
function get_ndout_nint (nd_out) result(n_int)
! ------------------------------------------------------------------------------
  implicit none
  type(t_ndout)            :: nd_out
  integer(ip)              :: n_int

  n_int = nd_out%n_int
  return
end function get_ndout_nint
! ------------------------------------------------------------------------------
function get_ndout_flag (nd_out) result(flag)
! ------------------------------------------------------------------------------
  implicit none
  type(t_ndout)            :: nd_out
  integer(ip)              :: flag

  flag = nd_out%flag
  return
end function get_ndout_flag
! ------------------------------------------------------------------------------
function get_node_post( nd_out) result(lst_np)
! ------------------------------------------------------------------------------
  implicit none
  type(t_ndout)            :: nd_out
  integer(ip)              :: lst_np(size(nd_out%l_nd))
  integer(ip)              :: n

  n = 0
  if (allocated(nd_out%l_nd)) then
    n = size(nd_out%l_nd)
  end if
  !.
  if (n > 0) then
    lst_np(1:n) = nd_out%l_nd(1:n)
  end if
  return
end function get_node_post
! ------------------------------------------------------------------------------
end module mod_ndout
! ------------------------------------------------------------------------------
