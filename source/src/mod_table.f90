!  -----------------------------------------------------------------------------
module mod_table
!  -----------------------------------------------------------------------------
  use mod_precision
  use mod_error
!  -----------------------------------------------------------------------------
  implicit none
!  ----------------------------------   TABLAS  --------------------------------
!  Elvio Heidenreich => 25/05/2005
!  Definicion de los campos del objeto TABLAS
!  Donde:
!       dato  = Vector de datos de la tabla
!       n_tbl = Numero de usuario de la tabla
!  -----------------------------------------------------------------------------
  type t_table
    private
    integer(ip)          :: n_tbl
    real(rp),allocatable :: data(:)
  end type t_table
!  -----------------------------------------------------------------------------
  type, public :: t_obtab 
    private
    integer(ip)                :: ntab = 0
    type(t_table), allocatable :: tbl(:)
  end type t_obtab
!  -----------------------------------------------------------------------------
contains
! ------------------------------------------------------------------------------
subroutine lec_tabla (lu_m, tabla)
! ------------------------------------------------------------------------------
! Esta rutina lee las historias (funciones) de la unidad lu_m
! ------------------------------------------------------------------------------
  implicit none
  integer(ip)                   :: lu_m
  type (t_obtab)                :: tabla
  !.
  integer(ip)                   :: idum, n_dat, io_err, i_err, i


  read (lu_m,*,iostat = io_err) tabla%ntab 
  if (io_err > 0) call w_error (25,'tabla%ntab',1)
  !.
  allocate (tabla%tbl(tabla%ntab), stat=i_err)
  if (i_err > 0 ) call w_error (26,'tabla%tbl(tabla%ntab)',1)
  !.
  do i=1, tabla%ntab
    read (lu_m,*,iostat = io_err) idum, n_dat
    if (io_err > 0) call w_error (27,'idum, idum',1)
    !.
    allocate (tabla%tbl(i)%data(n_dat), stat=i_err)
    if (i_err > 0 ) call w_error (26,'tabla%tbl(i)%data(idum)',1)
    !.
    backspace(lu_m)
    read (lu_m,*,iostat = io_err) tabla%tbl(i)%n_tbl, idum, tabla%tbl(i)%data
    if (io_err > 0) call w_error (27,'tabla%tbl(i)%()',1)
  end do
!
  write(     *,10)  tabla%ntab
  write(lu_log,10)  tabla%ntab
! 
  return
10 format ('###.Tablas,     leidas                            : ',I8)
end subroutine lec_tabla
! ------------------------------------------------------------------------------
function deallocate_tabla (tabla) result(flg)
! ------------------------------------------------------------------------------
  implicit none
  type (t_obtab)             :: tabla
  integer(ip)                :: flg
  !.
  integer(ip)                :: i, istat, i_err

  if (allocated(tabla%tbl)) then
    i_err = 0
    do i=1,tabla%ntab
      deallocate (tabla%tbl(i)%data, stat=istat)
      if (istat /= 0) i_err = 1
    end do
    deallocate (tabla%tbl, stat=istat)
    if (istat == 0) flg = 0 
    if ((istat /= 0).or.(i_err == 1)) flg = 1
  else
    flg = -1
  end if
  return
end function deallocate_tabla
! ------------------------------------------------------------------------------
subroutine bin_write_table(lu_m, tabla)
! ------------------------------------------------------------------------------
! Esta rutina lee las historias (funciones) de la unidad lu_m
! ------------------------------------------------------------------------------
  implicit none
  integer(ip)                   :: lu_m
  type (t_obtab)                :: tabla
  !.
  integer(ip)                   :: i


  write (lu_m) '#TABLE', tabla%ntab 
  
  if (tabla%ntab > 0)  then
    do i=1, tabla%ntab
      write (lu_m) tabla%tbl(i)%n_tbl, size(tabla%tbl(i)%data) 
      write (lu_m) tabla%tbl(i)%data
    end do
  end if
  return
end subroutine bin_write_table
! ------------------------------------------------------------------------------
subroutine ascii_write_table(lu_m, tabla)
! ------------------------------------------------------------------------------
! Esta rutina lee las historias (funciones) de la unidad lu_m
! ------------------------------------------------------------------------------
  implicit none
  integer(ip)                   :: lu_m
  type (t_obtab)                :: tabla
  !.
  integer(ip)                   :: i


  write (lu_m,10) '#TABLE', tabla%ntab 
  
  if (tabla%ntab > 0)  then
    do i=1, tabla%ntab
      write (lu_m,20) tabla%tbl(i)%n_tbl, size(tabla%tbl(i)%data) 
      write (lu_m,30) tabla%tbl(i)%data
    end do
  end if
  return
10 format (A,2X,I3)
20 format (I3,2X,I3)
30 format (10(:,2X,F12.6))
end subroutine ascii_write_table



! ------------------------------------------------------------------------------
subroutine bin_read_table (lu_m, tabla)
! ------------------------------------------------------------------------------
! Esta rutina lee las historias (funciones) de la unidad lu_m
! ------------------------------------------------------------------------------
  implicit none
  integer(ip)                   :: lu_m
  type (t_obtab)                :: tabla
  !.
  character (len = 6)           :: str
  integer(ip)                   :: n_dat, io_err, i_err, i


  read (lu_m,iostat = io_err) str, tabla%ntab 
  if (io_err > 0) call w_error (25,'tabla%ntab',1)
  !.
  if ( tabla%ntab > 0 ) then 
    allocate (tabla%tbl(tabla%ntab), stat=i_err)
    if (i_err > 0 ) call w_error (26,'tabla%tbl(tabla%ntab)',1)
    !.
    do i=1, tabla%ntab
      read (lu_m,iostat = io_err) tabla%tbl(i)%n_tbl, n_dat
      if (io_err > 0) call w_error (27,'tabla%tbl(i)%n_tbl, n_dat',1)
      !.
      allocate (tabla%tbl(i)%data(n_dat), stat=i_err)
      if (i_err > 0 ) call w_error (26,'tabla%tbl(i)%data(idum)',1)
      !.
      read (lu_m, iostat = io_err) tabla%tbl(i)%data
      if (io_err > 0) call w_error (27,'tabla%tbl(i)%data',1)
    end do
  end if
  return
end subroutine bin_read_table
! ------------------------------------------------------------------------------





! ------------------------------------------------------------------------------
end module mod_table
! ------------------------------------------------------------------------------
