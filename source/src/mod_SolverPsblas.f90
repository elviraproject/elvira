! ------------------------------------------------------------------------------
module mod_SolvePsblas 
! ------------------------------------------------------------------------------
 use mod_precision
 use mpi_mod;
 use psb_base_mod;   use psb_prec_mod
 use psb_krylov_mod; use psb_util_mod

 implicit none
 
 type public:: t_sset
   character(len=8) :: cmethd  !.'CG','CGS','BICG','BICGSTAB','BICGSTABL','RGMRES'
   character(len=4) :: ptype   !.Preconditioner NONE  DIAG  BJAC
   character(len=4) :: afmt    !.Storage format CSR COO JAD 
   integer(ip)      :: istopc  !.Stopping criterion
   integer(ip)      :: itmax   !.MAXIT
   integer(ip)      :: itrace  !.> 0 imprime info de la convergencia c/itrace iter.
   integer(ip)      :: irst    !.Restart for RGMRES  and BiCGSTABL
   real(rp)         :: eps     !.
 end type t_sset

contains
! ------------------------------------------------------------------------------
subroutine matrix_assembly (isol,iopt,kg_crs,Fg,Xg)
! ------------------------------------------------------------------------------
  implicit none


  integer(ip),intent(in)     :: isolve,iopt


  type(t_crst)               :: kg_crs
  real(rp),intent(in)        :: Fg(:)
  real(rp),intent(inout)     :: Xg(:)
  !.psblas setting
  character (len=10)         :: cmethd, ptype
  character (len= 5)         :: afmt
  integer(ip)                :: istopc, itmax, itrace, irst
  integer(ip), allocatable   :: vg(:), v_nnz(:)
  !.
  !.sparse matrix and preconditioner
  type(psb_dspmat_type)      :: a
  type(psb_dprec_type)       :: prec
  ! descriptor
  type(psb_desc_type)        :: desc_a
  ! dense matrices
  real(rp), allocatable      :: b(:), x(:)
  ! blacs parameters
  integer(ip)                :: ictxt, iam, np
  integer(ip)                :: ngdl, nnz, ierr, sr, er
  !.
  integer(ip)                :: irow(54), icol(54)
  real(rp)                   :: val(54), zt(10), eps, err

  integer(ip)                :: i, sci, eci, nri, iter, info, err_act
  integer(ip)                :: ite, iter_c  


  save a, prec, desc_a, cmethd, ptype, afmt, istopc, itmax, itrace, irst, eps
  save ictxt, iam, np, sr, er, nri, ite, x, b, iter_c

  !.
  sol_p = setpsb_SolPar(isol)

  !.
  call psb_erractionsave(err_act)
  call psb_init(ictxt)
  call psb_info(ictxt, iam, np)
    !.
    !print *,iam, 'before sendRecvCRS'
    call SendRecvCRS (0, iam, np, mpi_comm_world, kg_crs)
    !print *,iam,'after senRecvCRS'
    !.
    ngdl = ngdl_g (np)  !.Number of degrees of freedom
    allocate (vg(ngdl), v_nnz(np), stat=ierr)
    !.
    call psbcdall_vg ( np, vg, v_nnz)
    !.
    nnz = v_nnz(iam + 1)
    !.
    call psb_cdall( ictxt, desc_a, info, mg=ngdl, vg=vg)
    call psb_spall( a, desc_a, info, nnz=nnz)
    ! Allocate b and x 
    call psb_geall(  b, desc_a, info)
    call psb_geall(  x, desc_a, info)
    !.
    if(info.ne.0) stop '###.Error in iterative solver'
    call psb_barrier(ictxt)
    !.t1 = psb_wtime()
    call sr_and_er(iam, sr, er)
    !.
    do i= sr, er - 1
      sci         = kg_crs%row_p( i )
      eci         = kg_crs%row_p( i + 1) - 1
      nri         = eci - sci + 1
      irow(1:nri) = i
      icol(1:nri) = kg_crs%col_i(sci:eci)
      val (1:nri) = kg_crs%value(sci:eci)
      !.
      call psb_spins( nri, irow(1:nri), icol(1:nri), val (1:nri), a, desc_a, info)
      !.
      zt(1)=0.0_rp
      call psb_geins( 1,(/ i/), zt(1:1), b, desc_a, info)
      call psb_geins( 1,(/ i/), zt(1:1), x, desc_a, info)
     end do
    !.
    call psb_barrier(ictxt)    
    !.
    call psb_cdasb( desc_a, info)
    call psb_spasb( a, desc_a, info, dupl=psb_dupl_err_,afmt=afmt)
    call psb_barrier(ictxt)
    !.
    call psb_precinit( prec, ptype, info)
    call psb_barrier(ictxt)
    call psb_precbld(a,desc_a,prec,info)
    !call psb_prec_descr(prec)
    call sr_and_er(iam, sr, er)
    nri = er - sr
    ite = 0
    iter_c = 0
!!$  case (2)
!!$
!!$  case (3)
!!$    call psb_geins(er-sr, (/(i,i=sr,er-1)/), Fg(1:nri), b,desc_a,info,psb_dupl_ovwrt_)
!!$    call psb_geins(er-sr, (/(i,i=sr,er-1)/), Xg(1:nri), x,desc_a,info,psb_dupl_ovwrt_)
!!$    !.
!!$    call psb_geasb( b, desc_a, info)
!!$    call psb_geasb( x, desc_a, info)
!!$    !.
!!$    call psb_barrier(ictxt)
!!$   ! t1 = psb_wtime()  
!!$    call psb_krylov (cmethd, a, prec, b, x, eps, desc_a, info, itmax=itmax,     &
!!$                     iter=iter, err=err, itrace=itrace, istop=istopc, irst=irst)
!!$    !print *,'###.Despues de psb_krylov', iam     
!!$    !.
!!$    Xg=x
!!$    call psb_barrier(ictxt)
!!$    !.
!!$    iter_c= iter + iter_c
!!$    if (mod (ite, 200) == 0) then
!!$      if ( iam ==    0  ) write (*,30) iam, ite, iter_c, err
!!$      if ( iam == (np-1)) write (*,30) iam, ite, iter_c, err
!!$      iter_c = 0
!!$    end if
!!$    !.
!!$    ite =ite  + 1
!!$  case (4) !.cleanup storage and exit
!!$    call psb_gefree(b,desc_a,info)
!!$    call psb_gefree(x,desc_a,info)
!!$    call psb_spfree(a,desc_a,info)
!!$    call psb_precfree(prec,info)
!!$    call psb_cdfree(desc_a,info) 
!!$  case default
!!$    write (*,10); stop 
!!$  end select
!!$
!!$
!!$
!!$  return
!!$10 format ('##.Myrank: ',I3,' ##.ngdl: ',I4,' ##.nnz',I4)
!!$20 format ('##.Vg ..........',/,250I5)
!!$30 format ('##.Iam: ', I4,' Step: ',I6,' Iterations: ', I6,' error: ',E10.3)
end subroutine Solver_iterativo
! ------------------------------------------------------------------------------
function setpsb_SolPar(isol) result(sol_p)
! ------------------------------------------------------------------------------
  implicit none
  integer(ip)   :: isol
  type (t_sset) :: sol_p
  
  sol_p = t_sset('BICGSTAB', 'BJAC', 'CSR', 1, 1000, -1, 2, 1.d-8)  
  !.
  select case (isol)
  case (1);    sol_p%cmethd = 'CG'
  case (2);    sol_p%cmethd = 'CGS'
  case (3);    sol_p%cmethd = 'BICG'
  case (4);    sol_p%cmethd = 'BICGSTAB'
  case (5);    sol_p%cmethd = 'BICGSTABL'
  case (6);    sol_p%cmethd = 'RGMRES'
  case default;sol_p%cmethd = 'BICGSTAB'
  end select
  !.
  return
end function setpsb_SolPar
! ------------------------------------------------------------------------------

! ------------------------------------------------------------------------------
end module mod_solveriterativo
