! ------------------------------------------------------------------------------
module mod_param_elm
! ------------------------------------------------------------------------------
  use mod_precision
  use mod_filename
  use mod_error
! ------------------------------------------------------------------------------
  implicit none
! ------------------------------------------------------------------------------
! ------------------------------------------------------------------------------
!  p_elm : puntero al elemento
!. dat_i : vector de indices para la mascara
!. dat_r : vector de datos
! ------------------------------------------------------------------------------
  type, public :: t_prmelm
    private
    integer(ip)             :: p_elm
    integer(ip),allocatable :: dat_i(:)
    real(rp), allocatable   :: dat_r(:)
  end type t_prmelm
  !
  type, public:: t_obprmelm
    private
    integer(ip)                 :: n_prmelm = 0
    type(t_prmelm), allocatable :: prm_elm(:)
  end type t_obprmelm
! ------------------------------------------------------------------------------
contains
! ------------------------------------------------------------------------------
subroutine lec_prmelm (lu_m, arch_d, strout, prmelm)
! ------------------------------------------------------------------------------
! Esta rutina lee los parametros de los nodos
! ------------------------------------------------------------------------------
  implicit none
  integer(ip), parameter        :: max=100
  integer(ip), intent(in)       :: lu_m
  character (len=*), intent(in) :: arch_d, strout
  type(t_obprmelm)              :: prmelm
  !.
  integer(ip)                   :: lu_lec,io_err,i_err
  integer(ip)                   :: i, nro_p,dat_i(max)
  real(rp)                      :: dat_r(max)


  lu_lec = lu_m; call unitnumber (arch_d, strout, lu_lec, io_err)
  if (io_err > 0) call w_error (51,'subroutine lec_prmelm',1)
  !.
  read (lu_lec,*,iostat=io_err) prmelm%n_prmelm  !.Numero de elementos con parametros
  if (io_err > 0) call w_error (3,'prmelm%n_prmelm',1)
  !
  allocate (prmelm%prm_elm(prmelm%n_prmelm), stat=i_err)
  if (i_err > 0 ) call w_error (4,'prmelm%prm_elm(prmelm%n_prmelm)',1)
  !. 
  do i=1,prmelm%n_prmelm
    read (lu_lec,*,iostat=io_err) prmelm%prm_elm(i)%p_elm, nro_p, dat_i(1:nro_p), &
                                  dat_r(1:nro_p) !.nro de nodo y propiedad
    if (io_err > 0) call w_error (52,i,1) 
    !.
    allocate(prmelm%prm_elm(i)%dat_i(nro_p),prmelm%prm_elm(i)%dat_r(nro_p),stat=i_err)
    if (i_err > 0) call w_error(53, i, 1)
    !.
    prmelm%prm_elm(i)%dat_i(1:nro_p)= dat_i(1:nro_p)
    prmelm%prm_elm(i)%dat_r(1:nro_p)= dat_r(1:nro_p)
  end do
  !.
  if (lu_lec /= lu_m) call close_unit (lu_lec)
  write(*,10) prmelm%n_prmelm ; write(lu_log,10) prmelm%n_prmelm 
  !.
  write (std_o,20) prmelm%prm_elm(1)%p_elm
  nro_p = size(prmelm%prm_elm(1)%dat_i)
  write (std_o,30) (prmelm%prm_elm(1)%dat_i(i),i=1,nro_p)
  write (std_o,40) (prmelm%prm_elm(1)%dat_r(i),i=1,nro_p)
  !.
  write (std_o,20) prmelm%prm_elm(prmelm%n_prmelm)%p_elm
  nro_p = size(prmelm%prm_elm(prmelm%n_prmelm)%dat_i)
  write (std_o,30) (prmelm%prm_elm(prmelm%n_prmelm)%dat_i(i),i=1,nro_p)
  write (std_o,40) (prmelm%prm_elm(prmelm%n_prmelm)%dat_r(i),i=1,nro_p)
  !.
  return
10 format ('###.Element mask for Ionic model                  : ',I8)
20 format (I6)
30 format (20(I6,1X))
40 format (20(E12.4,1X))
end subroutine lec_prmelm
! ------------------------------------------------------------------------------
subroutine bin_write_prmelm (lu_m, le_d, prmelm)
! ------------------------------------------------------------------------------
!
! ------------------------------------------------------------------------------
  implicit none
  integer(ip), intent(in)      :: lu_m, le_d(:)
  type(t_obprmelm)             :: prmelm
  !.
  integer(ip)                  :: l_aux(prmelm%n_prmelm)
  integer(ip)                  :: c_pe, iel, i, j, nro_p


  c_pe = 0; l_aux = 0
  do i=1, prmelm%n_prmelm         !.Numero de elementos con parametros
    iel = prmelm%prm_elm(i)%p_elm !.Puntero al elemento
    if (le_d(iel) /= 0) then      !.le_d(:) me indica si el nodo pertenece al elemento   
      c_pe = c_pe + 1
      l_aux(c_pe) = i             !.Puntero al parametro elemental   
    end if
  end do
  !.
  write (lu_m) '*PARAM_ELEM', c_pe
  do i=1,c_pe
    j=l_aux(i)                                  !.Puntero al parametro  
    iel  = prmelm%prm_elm(j)%p_elm              !.Recupero el numero del elemento
    nro_p= size(prmelm%prm_elm(j)%dat_i)        !.Nro de parametros
    write (lu_m) le_d(iel), nro_p               !.Nro del elemento Renumerado, Nro  
    write (lu_m) prmelm%prm_elm(j)%dat_i
    write (lu_m) prmelm%prm_elm(j)%dat_r
  end do
  return
end subroutine bin_write_prmelm
! ------------------------------------------------------------------------------
subroutine ascii_write_prmelm (lu_m, le_d, prmelm)
! ------------------------------------------------------------------------------
!
! ------------------------------------------------------------------------------
  implicit none
  integer(ip), intent(in)      :: lu_m, le_d(:)
  type(t_obprmelm)             :: prmelm
  !.
  integer(ip)                  :: l_aux(prmelm%n_prmelm)
  integer(ip)                  :: c_pe, iel, i, j, nro_p


  c_pe = 0
  do i=1, prmelm%n_prmelm  !.Numero de elementos con parametros
    iel = prmelm%prm_elm(i)%p_elm
    if (le_d(iel) /= 0) then
      c_pe = c_pe + 1
      l_aux(c_pe) = i
    end if
  end do
  !.
  write (lu_m,10) '*PARAM_ELEM', c_pe
  do i=1,c_pe
    j=l_aux(i)
    iel  = prmelm%prm_elm(j)%p_elm
    nro_p= size(prmelm%prm_elm(j)%dat_i)
    write (lu_m,20) le_d(iel), nro_p
    write (lu_m,30) prmelm%prm_elm(j)%dat_i
    write (lu_m,40) prmelm%prm_elm(j)%dat_r
  end do
  return
10 format (A,2X,I8)
20 format (I8,2X,I8)
30 format (10(:,2X,I3))
40 format (10(:,2X,F12.6))
end subroutine ascii_write_prmelm
! ------------------------------------------------------------------------------
subroutine bin_read_prmelm (lu_m, prmelm)
! ------------------------------------------------------------------------------
!
! ------------------------------------------------------------------------------
  implicit none
  integer(ip), intent(in)      :: lu_m
  type(t_obprmelm)             :: prmelm
  !.
  character (len=11)           :: str 
  integer(ip)                  :: i, nro_p, io_err, i_err


  read (lu_m, iostat=io_err) str, prmelm%n_prmelm  !.Numero de elementos con parametros
  if (io_err > 0) call w_error (3,'prmelm%n_prmelm',1)
  !
  allocate (prmelm%prm_elm(prmelm%n_prmelm), stat=i_err)
  if (i_err > 0 ) call w_error (4,'prmelm%prm_elm(prmelm%n_prmelm)',1)
  !. 
  do i=1,prmelm%n_prmelm
    read (lu_m, iostat=io_err) prmelm%prm_elm(i)%p_elm,  nro_p 
    if (io_err > 0) call w_error (52,i,1) 
    !.
    allocate(prmelm%prm_elm(i)%dat_i(nro_p),prmelm%prm_elm(i)%dat_r(nro_p),stat=i_err)
    if (i_err > 0) call w_error(53, i, 1)
    !.
    read (lu_m, iostat=io_err) prmelm%prm_elm(i)%dat_i
    read (lu_m, iostat=io_err) prmelm%prm_elm(i)%dat_r
    if (io_err > 0) call w_error (52,i,1) 
  end do
  return
end subroutine bin_read_prmelm
!  -----------------------------------------------------------------------------
function deallocate_prmelm (prmelm) result (flg)
!  -----------------------------------------------------------------------------
  implicit none
  type(t_obprmelm)             :: prmelm
  integer(ip)                  :: flg
  !.
  integer(ip)                  ::  i, istat

  flg = 0
  if (allocated(prmelm%prm_elm)) then
    do i=1,prmelm%n_prmelm
      deallocate (prmelm%prm_elm(i)%dat_i,prmelm%prm_elm(i)%dat_r, stat=istat)
      if (istat /= 0) flg = 1
    end do
    deallocate (prmelm%prm_elm, stat=istat)
    if (istat /= 0) flg = 1
  else
    flg = -1
  end if
  return
end function deallocate_prmelm
! -------------------------------------------------------------------------------
subroutine get_parampointer_elm (prmelm, n, iaux )
! -------------------------------------------------------------------------------
  implicit none
  type(t_obprmelm), intent(in) :: prmelm
  integer(ip),      intent(out):: n, iaux(:)

  !.
  integer(ip)                 :: i,k
  !.

  n = 0; iaux = 0
  if (allocated(prmelm%prm_elm)) then
    do i=1,size(prmelm%prm_elm)
      k = prmelm%prm_elm(i)%p_elm
      iaux(k) = i 
    end do
    n=i-1
  end if
  return
end subroutine get_parampointer_elm
! ------------------------------------------------------------------------------
subroutine get_mask_value_elm ( p_prm, prmelm, n, dat_i, dat_r)
! ------------------------------------------------------------------------------
  implicit none
  integer(ip), intent(in)      :: p_prm
  type(t_obprmelm), intent(in) :: prmelm
  !.
  integer(ip), intent(out)    :: n, dat_i(:)
  real(rp),    intent(out)    :: dat_r(:)
  !.

  n = size(prmelm%prm_elm(p_prm)%dat_i)
  !.
  dat_i(1:n) = prmelm%prm_elm(p_prm)%dat_i
  dat_r(1:n) = prmelm%prm_elm(p_prm)%dat_r
  return
end subroutine get_mask_value_elm




! -------------------------------------------------------------------------------
end module mod_param_elm
!  -----------------------------------------------------------------------------
!  -----------------------------------------------------------------------------
