!  -----------------------------------------------------------------------------
module mod_mpisndrcv
!  -----------------------------------------------------------------------------
  use mod_precision
  use mod_node
  use mpi_mod
!  -----------------------------------------------------------------------------
  implicit none
  type, public:: t_sr
    private
    integer(ip)              :: np_ds     !.Number of destination process
    integer(ip), allocatable :: i_snd(:)  !.Local inidices of d.o.f to be sent
    integer(ip), allocatable :: i_rcv(:)  !.Local indices of d.o.f to be received
    real(rp),    allocatable :: d_snd(:)  !.Data to send
    real(rp),    allocatable :: d_rcv(:)  !.Data to receive
  end type t_sr
  !.
  type, public :: t_sdrc
    integer(ip)              :: n_sr = 0  !.Number of processes to which send and receive
    type(t_sr),  allocatable :: srd(:)    !.Message data
  end type t_sdrc
  !.
!  -----------------------------------------------------------------------------
  !.
contains
!  -----------------------------------------------------------------------------
subroutine dof_sndrcv ( ndof_g, comm, iam, np, node, sndrcv)
!  -----------------------------------------------------------------------------
  implicit none
  integer(ip),  intent(in)    :: ndof_g, comm, iam, np
  type(t_obnd), intent(in)    :: node
  type(t_sdrc)                :: sndrcv
  !.Vector de correspondencia entre dof global y dof local
  integer(ip)                 :: glb_lcl(ndof_g)  
  !.
  integer(ip)                 :: sr_t = mpi_integer
  integer(ip)                 :: dom(30)
  integer(ip)                 :: n_nod, i, j, k, n, nd_v, jk, jn, nd_g, nd_l, dst 
  integer(ip)                 :: i_sr(np), p_sr(np)
  integer(ip)                 :: i_err, status(MPI_STATUS_SIZE), ierr

  n_nod = get_nnod(node)  
  glb_lcl = 0
  do i = 1, n_nod
    nd_g = get_nd_num_g ( i, node)
    glb_lcl(nd_g) = i
  end do
  !.primero contamos cuantos valores tenemos que enviar y cuantos recibir
  i_sr = 0
  do i=1, n_nod
    call get_node_subdomain(i, nd_v, dom, node)
    if (nd_v > 1) then
      do j = 1, nd_v
        k = dom(j); 
        if (k /= iam + 1) then
          i_sr(k) = i_sr(k) + 1
        end if
      end do
    end if
  end do
  !.
  sndrcv%n_sr = count(i_sr > 0)   !.Numero de proc. a los que enviar/recibir
  !.
  allocate (sndrcv%srd(sndrcv%n_sr), stat=i_err)
  if (i_err > 0 ) call w_error (4,'sndrcv%srd(sndrcv%n_sr)',1)
  !.
  !.Dimensionado de los vectores de indices 
  k = 0; p_sr=0
  do i=1, np
    n = i_sr(i)
    if (n > 0) then
      k = k + 1; p_sr(i) = k
      !.
      sndrcv%srd(k)%np_ds = i - 1
      allocate (sndrcv%srd(k)%i_snd(n), sndrcv%srd(k)%i_rcv(n),                  &
                sndrcv%srd(k)%d_snd(n), sndrcv%srd(k)%d_rcv(n), stat=i_err)
      if (i_err > 0 ) call w_error (4,'sndrcv%srd(k)%i_s_r(n)',1)
      sndrcv%srd(k)%i_snd =   0; sndrcv%srd(k)%d_snd = 0.0 
      sndrcv%srd(k)%i_rcv =   0; sndrcv%srd(k)%d_rcv = 0.0
    end if
  end do
  !.
  i_sr = 0
  do i=1, n_nod
    call get_node_subdomain(i, nd_v, dom, node)
    if (nd_v > 1) then
      nd_g = get_nd_num_g ( i, node) !.Numero de grado de libertad global
      do j = 1, nd_v
        k = dom(j); 
        if (k /= iam + 1) then
          jk = p_sr(k)
          jn = i_sr(k) + 1; i_sr(k)= jn
          sndrcv%srd(jk)%i_snd(jn) = nd_g
        end if
      end do
    end if
  end do
  !.
  do i=1,sndrcv%n_sr
    n  = size(sndrcv%srd(i)%i_snd)
    dst= sndrcv%srd(i)%np_ds
    !.
    call mpi_sendrecv(sndrcv%srd(i)%i_snd(1), n, sr_t, dst, iam+1, &
                      sndrcv%srd(i)%i_rcv(1), n, sr_t, dst, dst+1, comm, status, ierr)
!    call mpi_sendrecv(sndrcv%srd(i)%i_snd(1:n), n, sr_t, dst, iam+1, &
!                      sndrcv%srd(i)%i_rcv(1:n), n, sr_t, dst, dst+1, comm, status, ierr)
    !.
  end do
  !.
!  call write_sndrcv( iam, np, iam + 100, sndrcv)
  !.
  !.Pasaje del indice global al local.
  do i=1,sndrcv%n_sr
    n  = size(sndrcv%srd(i)%i_snd)
    do j=1,n
      nd_g=sndrcv%srd(i)%i_snd(j); nd_l =  glb_lcl(nd_g)
      if (nd_l > 0) then
        sndrcv%srd(i)%i_snd(j) = nd_l
      else
        write (*,10) iam, sndrcv%srd(i)%np_ds, j, nd_g
      end if
      nd_g=sndrcv%srd(i)%i_rcv(j); nd_l =  glb_lcl(nd_g)
      if (nd_l > 0) then
        sndrcv%srd(i)%i_rcv(j) = nd_l
      else
        write (*,20) iam, sndrcv%srd(i)%np_ds, j, nd_g
      end if
    end do
  end do
  !.
!  call write_sndrcv( iam, np, iam + 200, sndrcv)


  return
10 format ('###.Error in dof_sndrcv:', ' ->Iam: ',I8, '  -> destination: ',I8, &
           ' -> local index: ',I8, ' -> global index: ',I8) 
20 format ('###.Error in dof_sndrcv:', ' ->Iam: ',I8, '  -> source     : ',I8, &
           ' -> local index: ',I8, ' -> global index: ',I8) 
end subroutine dof_sndrcv
!  -----------------------------------------------------------------------------
subroutine write_sndrcv(iam, np, lu_o, sndrcv)
!  -----------------------------------------------------------------------------
  implicit none
  integer(ip), intent(in)     :: iam, np, lu_o
  type(t_sdrc)                :: sndrcv
  !.
  integer(ip)                 :: i

  call write_n_str(lu_o, 80,'='); 
  write (lu_o, 10) iam, np
  call write_n_str(lu_o, 80,'.'); 
  !.
  write (lu_o, 20) 
  do i = 1,sndrcv%n_sr 
    call write_n_str(lu_o, 80,'-'); 
    write (lu_o,30) sndrcv%srd(i)%np_ds
    write (lu_o,40) sndrcv%srd(i)%i_snd
    write (lu_o,50) sndrcv%srd(i)%i_rcv
  end do
  !.
  return
10 format('###.RANK: ',I6,' of ',I6) 
20 format('DATA TO SEND/RECEIVE. ......')
30 format('DESTINATION/SOURCE: ', I6)
40 format('INDEX TO SND ',/,10(:,1X,I8))
50 format('INDEX TO RCV ',/,10(:,1X,I8))
end subroutine write_sndrcv
!  -----------------------------------------------------------------------------
subroutine rhs_sndrcv( iam, comm, rhs, sndrcv)
!  -----------------------------------------------------------------------------
  implicit none
  integer(ip), intent(in)     :: iam, comm
  type(t_sdrc)                :: sndrcv
  real(rp),    intent(inout)  :: rhs(:)
  !.
  integer(ip)                 :: i, n, m, dst, status(MPI_STATUS_SIZE), ierr, j, k
  integer(ip)                 :: sr_t = mpi_double_precision


 ! call write_sndrcv( iam, 2, iam + 300, sndrcv)
  do i=1,sndrcv%n_sr
    n  = size(sndrcv%srd(i)%i_snd)
    m  = size(sndrcv%srd(i)%i_rcv)
    dst= sndrcv%srd(i)%np_ds
 !   print *, 'n',n,'m',m,'dst',dst
    do j=1,n
      k = sndrcv%srd(i)%i_snd(j)
  !    print *,'Iam:', iam, i, k
      sndrcv%srd(i)%d_snd(j) = rhs(k)
    end do
    !.
    !.call MPI_Send(sndrcv%srd(i)%d_snd(1:n), n, sr_t, dst, iam+1, comm, ierr)
    !.call MPI_Recv(sndrcv%srd(i)%d_rcv(1:m), m, sr_t, dst, dst+1, comm, status, ierr)
    !.
    call mpi_sendrecv(sndrcv%srd(i)%d_snd(1), n, sr_t, dst, iam+1, &
                      sndrcv%srd(i)%d_rcv(1), m, sr_t, dst, dst+1, comm, status, ierr)
!    call mpi_sendrecv(sndrcv%srd(i)%d_snd(1:n), n, sr_t, dst, iam+1, &
!                      sndrcv%srd(i)%d_rcv(1:m), m, sr_t, dst, dst+1, comm, status, ierr)

  end do
  !.
  do i=1,sndrcv%n_sr
    n  = size(sndrcv%srd(i)%i_rcv)
    do j=1,n
      k = sndrcv%srd(i)%i_rcv(j)
      rhs(k)= rhs(k) + sndrcv%srd(i)%d_rcv(j)
    end do
  end do
  return
end subroutine rhs_sndrcv
!  -----------------------------------------------------------------------------
subroutine mm_completing ( iam, comm,  n_nod, sndrcv, sprs_m)
!  -----------------------------------------------------------------------------
  use mod_estsparse
  implicit none
  integer(ip), intent(in)     :: iam, comm, n_nod
  type(t_sdrc)                :: sndrcv
  type(t_crs),  intent(inout) :: sprs_m
  !.
  real(rp)                    :: m_mtx(n_nod)
  integer(ip)                 :: sr_t = mpi_double_precision
  integer(ip)                 :: i, j, k, n, dst, status(MPI_STATUS_SIZE), ierr
 

  call get_mmatrix(sprs_m, m_mtx)
  !.
  do i=1,sndrcv%n_sr
    n  = size(sndrcv%srd(i)%i_snd)
    dst= sndrcv%srd(i)%np_ds
    do j=1,n
      k                      = sndrcv%srd(i)%i_snd(j)
      sndrcv%srd(i)%d_snd(j) = m_mtx(k)
    end do
    !.
!    write(*,10) iam,dst,n,size(sndrcv%srd(i)%d_snd),size(sndrcv%srd(i)%d_rcv)
    call mpi_sendrecv(sndrcv%srd(i)%d_snd(1), n, sr_t, dst, iam+1, &
                      sndrcv%srd(i)%d_rcv(1), n, sr_t, dst, dst+1, comm, status, ierr)
!    call mpi_sendrecv(sndrcv%srd(i)%d_snd(1:n), n, sr_t, dst, iam+1, &
!                      sndrcv%srd(i)%d_rcv(1:n), n, sr_t, dst, dst+1, comm, status, ierr)

!    write(*,20) iam,dst,n,size(sndrcv%srd(i)%d_snd),size(sndrcv%srd(i)%d_rcv)
  end do
  !.
  do i=1,sndrcv%n_sr
    n  = size(sndrcv%srd(i)%i_rcv)
    do j=1,n
      k        = sndrcv%srd(i)%i_rcv(j)
      m_mtx(k) = m_mtx(k) + sndrcv%srd(i)%d_rcv(j)
    end do
  end do
  !.
  call set_mmatrix(iam, sprs_m, m_mtx)
10 format('*** Soy el rank ',I2,' voy a enviar a ',I2,1X,I2,' datos ', &
          'vector snd ',I4,' vector rcv ',I4)
20 format('*** Soy el rank ',I2,'  he enviado  a ',I2,1X,I2,' datos ', &
          'vector snd ',I4,' vector rcv ',I4)
  !.
  return
end subroutine mm_completing
!  -----------------------------------------------------------------------------
end module mod_mpisndrcv

