!-------------------------------------------------------------------------------
module mod_invcond
!-------------------------------------------------------------------------------
  use mod_precision
  implicit none
!-------------------------------------------------------------------------------  
contains
!-------------------------------------------------------------------------------  
subroutine condense_matrices (n_total, n_el_fre, s, c)
!-------------------------------------------------------------------------------  
! condensation of element matrices to remove internal degrees of freedom
!-------------------------------------------------------------------------------  
! internal degrees of freedom *must* come last
!    :  Saa : Sab  : : da :     : ca :
!    :......:......: :....:  =  :....:
!    :  Sba : Sbb  : : db :     : cb :
! enter full s ; return condensed in saa and ca
! dimension  saa(n_el_fre,n_el_fre), ca(n_el_fre)
! Saa* = (Saa) - (Sab)*(Sbb)^-1*(Sab)T
! ca*  = (ca) -  (Sab)*(Sbb)^-1*(cb)
! n_total  = orig. no. of d.o.f. of element
! n_el_fre = final no. of d.o.f. of element
! nelim    = number of dof to eliminate
! s        = square element matrix
! c        = element column matrix
!-------------------------------------------------------------------------------  
  implicit none
  integer(ip),  intent(in):: n_total, n_el_fre
  real(rp), intent(inout) :: s (n_total, n_total), c (n_total)
  real(rp), parameter     :: zero = 0.0_rp
  integer(ip)             :: i, j, k, l, m, nelim
  real(rp)                :: ck, skk, slkskk                                     

  nelim = n_total - n_el_fre
  do i = 1, nelim
    j = n_total - i
    k = j + 1
    skk = s (k, k)
    ck  = c (k)
    if ( skk /= zero ) then
      c (k) = ck / skk
      do l = 1, j
        slkskk = s (l, k) / skk
        s (l, k) = slkskk
        do m = l, j
          s (l, m) = s (l, m) - s (k, m) * slkskk
          s (m, l) = s (l, m)
        end do
        c (l) = c (l) - ck * slkskk
      end do
    end if
  end do
end subroutine condense_matrices
!--------------------------------------------------------------------------------
subroutine InvCondMat ( n, m, Ke)
!--------------------------------------------------------------------------------
! Condensacion de los elementos de la matriz para remover los grados de libertad
! internos. Los grados de libertad internos deben estar a lo ultimo en la matriz
!-------------------------------------------------------------------------------  
! n  = numero original de grados de libertad del macro elemento
! m  = numero final de grados de libertad del macro elemento. 
!--------------------------------------------------------------------------------
! :  Ke_11 : Ke_12  : : U_1 :     : f_1 :
! :........:........: :.....:  =  :.....:
! :  Ke_21 : Ke_22  : : U_2 :     : f_2 :
!--------------------------------------------------------------------------------
!  Ke_11*U_1 +  Ke_12*U_2 = f_1 
!  Ke_21*U_1 +  Ke_22*U_2 = f_2 => U_2 = Ke_22^-1*(f_2 - Ke_21*U_1)
!  
!  Ke_11*U_1 + Ke_12*Ke_22^-1*(f_2 - Ke_21*U_1) = f_1
!
!  (Ke_11 - Ke_12*Ke_22^-1*Ke_21)*U_1 = f_1 - Ke_12*Ke_22^-1*f_2
!
!  (Ke_11)r = Ke_11 - Ke_12*Ke_22^-1*Ke_21 => matriz a ensamblar.
!    
!  Ademas tenemos que guardar Ke_12 o Ke_21 (como son simetricas una es la 
!  tranpuesta de la otra) y Ke_22^-1. Esto es porque en cada paso de tiempo 
!  tenemos que calcular el termino derecho y recuperar U_2 para calcular el 
!  termino reactivo.
!--------------------------------------------------------------------------------
  implicit none
  integer(ip),intent (in)    :: n, m
  real (rp),  intent (inout) :: Ke(n, n)
  !.
  real (rp)                  :: K12_K22i(m,n-m)
  integer(ip)                :: i_err, l
  !.

  l = m+1
  !.
  call inv_small_mat (n-m, Ke(l:n,l:n),Ke(l:n,l:n), i_err)
  !.
  K12_K22i = matmul(Ke(1:m,l:n),Ke(l:n,l:n))
  !.
  Ke(1:m,1:m) = Ke(1:m,1:m) - matmul(K12_K22i,Ke(l:n,1:m))
  !.
  return
end subroutine InvCondMat
!--------------------------------------------------------------------------------
subroutine inv_small_mat (n, A, A_inv, i_error)
!--------------------------------------------------------------------------------
! Invert a small matrix A(n,n) 
! A     = original matrix
! A_inv = inverse of matrix a
! d     = a diagonal element of a
! n     = size of a
!--------------------------------------------------------------------------------
  implicit none
  integer(ip),intent (in)  :: n
  real (rp),  intent (in)  :: A     (n, n)
  real (rp),  intent (out) :: A_inv (n, n)
  integer,    intent (out) :: i_error  ! fatal result if /= 0
  !.
  integer(ip)              :: i, k
  real (rp)                :: d, det

  i_error = 0 ; A_inv = A       ! initialize
  select case (n) !-->  1-d
  case (1)
    det = A (1,1)
    if ( abs(det) <= spacing(det) ) then
      i_error = 1
    else
      A_inv(1,1) = 1.d0/det
    end if
  case (2) !-->  2-d
    call invert_2by2 (A, a_inv, det, i_error)
  case (3) !-->  3-d
    call invert_3by3 (A, a_inv, det, i_error)
  case default
    do  k = 1,n                      ! loop over diagonals
      d = A_inv (k, k)               ! pivot term
      if ( abs(d) <= spacing(d) ) then          ! fatal condition
        i_error = 1 
        exit ! this do on k  !b
      end if ! fatal error
      A_inv (k, k) = 1.d0
      A_inv (k, :) = A_inv (k, :)/d  ! scale row
      do i = 1, n                    ! loop over row again
        if ( i == k ) cycle ! to next i
        d            = A_inv (i, k)        
        A_inv (i, k) = 0.d0        
        A_inv (i, :) = A_inv (i, :) - d*A_inv (k, :) 
      end do ! over row i   
    end do ! over k-th column
  end select
  if ( i_error > 0 ) then
    print *, 'Error: inv_small_mat failed, identity matrix returned'
    call real_identity (n, A_inv)
  end if ! inversion failed
end subroutine inv_small_mat
!--------------------------------------------------------------------------------
subroutine invert_2by2 (A, Ainv, det, i_error)
!--------------------------------------------------------------------------------
! calculate the determinant and inverse of a(2,2)
! A    = original matrix
! Ainv = inverse of matrix A
! det  = determinant of a
!--------------------------------------------------------------------------------
  implicit none
  real(rp),    intent(in)    :: A (2, 2)
  real(rp),    intent(out)   :: Ainv (2, 2), det
  integer(ip), intent(inout) :: i_error


  det = a (1, 1) * a (2, 2) - a (1, 2) * a (2, 1)
  if ( abs(det) <= spacing(det) ) then
    i_error = 1
  else
    ainv (1, 1) =   a (2, 2) / det
    ainv (1, 2) = - a (1, 2) / det
    ainv (2, 1) = - a (2, 1) / det
    ainv (2, 2) =   a (1, 1) / det
  end if ! singular
end subroutine invert_2by2
!--------------------------------------------------------------------------------
subroutine invert_3by3 (A, Ainv, det, i_error)
!--------------------------------------------------------------------------------
! find inverse and determinant of matrix a(3,3)
! A    = original matrix
! Ainv = inverse of matrix A
! det  = determinant of a
!-------------------------------------------------------------------------------
  implicit none
  real(rp),    intent(in)    :: A(3,3)
  real(rp),    intent(out)   :: Ainv(3,3), det
  integer(ip), intent(inout) :: i_error

                                        
  Ainv(1,1) =  A(2,2)*A(3,3) - A(3,2)*A(2,3)
  Ainv(2,1) = -A(2,1)*A(3,3) + A(3,1)*A(2,3)
  Ainv(3,1) =  A(2,1)*A(3,2) - A(3,1)*A(2,2)
  Ainv(1,2) = -A(1,2)*A(3,3) + A(3,2)*A(1,3)
  Ainv(2,2) =  A(1,1)*A(3,3) - A(3,1)*A(1,3)
  Ainv(3,2) = -A(1,1)*A(3,2) + A(3,1)*A(1,2)
  Ainv(1,3) =  A(1,2)*A(2,3) - A(2,2)*A(1,3)
  Ainv(2,3) = -A(1,1)*A(2,3) + A(2,1)*A(1,3)
  Ainv(3,3) =  A(1,1)*A(2,2) - A(2,1)*A(1,2)
  det = A(1,1)*Ainv(1,1) + A(1,2)*Ainv(2,1) + A(1,3)*Ainv(3,1)
  if ( abs(det) <= spacing(det) ) then
    i_error = 1
  else
    ainv = ainv/det
  end if
end subroutine invert_3by3
!-------------------------------------------------------------------------------
subroutine real_identity (n, eye)
!-------------------------------------------------------------------------------
!          compute a real n by n identity matrix
!-------------------------------------------------------------------------------
  implicit none
  integer,   intent(in)  :: n            ! size of matrix
  real(rp),  intent(out) :: eye (n, n)   ! identity matrix
  !.
  integer :: j
  real(rp), parameter :: i_2 (2, 2)= reshape ((/ 1._rp, 0._rp,                  &
                                                 0._rp, 1._rp /),(/ 2, 2/) )
  real(rp), parameter :: i_3 (3, 3)= reshape ((/ 1._rp, 0._rp, 0._rp,           &
                                                 0._rp, 1._rp, 0._rp,           &
                                                 0._rp, 0._rp, 1._rp /),(/ 3, 3/))
  select case (n)
  case (:0) ; stop 'Invalid size, real_identity'
  case (1)  ; eye = 1._rp
  case (2)  ; eye = i_2
  case (3)  ; eye = i_3
  case default
    eye = 0._rp
    do j = 1, n; 
      eye (j, j) = 1._rp; 
    end do
  end select
  return
end subroutine real_identity
!-------------------------------------------------------------------------------

end module mod_invcond

!-------------------------------------------------------------------------------
