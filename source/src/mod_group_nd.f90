!  -----------------------------------------------------------------------------
module mod_group_nd
!  -----------------------------------------------------------------------------
  use mod_precision
  use mod_filename
  use mod_error
!  -----------------------------------------------------------------------------
  implicit none
!  --------------------------------GRUPO DE NUDOS ------------------------------
!  Definicion de los campos del objeto GNUDO(Grupo de nudos)
!  Donde:
!       name    = Nombre del grupo de nodos
!       grnd    = Vector de punteros a los nodos correspondiente
!       n_grnd  = Numero de grupos de nodos
! ------------------------------------------------------------------------------
  type t_gnod
    private
    character (len= 32)         :: name
    integer(ip), allocatable    :: gr_nod(:)
  end type t_gnod
!  -----------------------------------------------------------------------------
  type, public:: t_obgnd
    private
    integer(ip)                ::n_grp = 0
    type (t_gnod), allocatable ::grpnod(:)
  end type t_obgnd
!  -----------------------------------------------------------------------------
  contains
! ------------------------------------------------------------------------------
subroutine lec_grpnod (lu_m, arch_d, strout, grpnd)
! ------------------------------------------------------------------------------
! Esta rutina lee los grupos de elementos
! ------------------------------------------------------------------------------
  implicit none
  integer(ip), intent(in)      :: lu_m
  character (len=*), intent(in):: arch_d, strout
  type (t_obgnd)               :: grpnd
  !.
  integer(ip)                  :: lu_lec, io_err, i_err, n_dat, i


  lu_lec = lu_m; call unitnumber (arch_d, strout, lu_lec, io_err)
  if (io_err > 0) call w_error (40,strout,1)
  !.
  read (lu_lec,*,iostat=io_err) grpnd%n_grp
  if (io_err > 0) call w_error (41,'grpndm%n_grp',1)
  !.
  allocate (grpnd%grpnod(grpnd%n_grp), stat=i_err)
  if (i_err > 0 ) call w_error (42,'grpnd%grpnod(grpnd%n_grp)',1)
  !.
  do i = 1, grpnd%n_grp
    read (lu_lec, *, iostat = io_err) grpnd%grpnod(i)%name, n_dat
    if (io_err > 0) call w_error (43,'grpnd%grpnod(i)%name, n_dat',1)
    !.
    allocate (grpnd%grpnod(i)%gr_nod(n_dat),stat=i_err) 
    if (i_err > 0 ) call w_error (42,'grpnd%grpnod(i)%gr_nod(n_dat)',1)
    !.
    read (lu_lec, *, iostat = io_err) grpnd%grpnod(i)%gr_nod(1:n_dat)
    if (io_err > 0) call w_error (43,'grpnd%grpnod(i)%gr_nod(1:n_dat)',1)
  end do
  !.
  if (lu_lec /= lu_m) call close_unit (lu_lec)
  !.
  write(     *,10) grpnd%n_grp; write(lu_log,10) grpnd%n_grp
  !.
  return
10 format ('###.Grupos de nodos, leidos                       : ',I8)
end subroutine lec_grpnod
!  -----------------------------------------------------------------------------
subroutine bin_write_grpnod (lu_m, ln_d, grpnd)
! ------------------------------------------------------------------------------
! Esta rutina lee los grupos de elementos
! ------------------------------------------------------------------------------
  implicit none
  integer(ip), intent(in)      :: lu_m, ln_d(:)
  type (t_obgnd)               :: grpnd
  !.
  integer(ip)                  :: l_grp(grpnd%n_grp), ng_d
  integer(ip)                  :: i, j, inod

  ng_d = 0 !.Contador del numero de grupos en el dominio
  l_grp=0
  do i=1,grpnd%n_grp
    !.
    do j=1,size(grpnd%grpnod(i)%gr_nod)
      inod = grpnd%grpnod(i)%gr_nod(j)
      if (ln_d(inod)  /= 0) l_grp(i)=l_grp(i) + 1
    end do
    if (l_grp(i) > 0) ng_d = ng_d + 1
  end do
  !.
  write (lu_m) '#GRPNODE', ng_d
  !.
  if (ng_d > 0) then
    do i=1,grpnd%n_grp
      if (l_grp(i) > 0) then
        write (lu_m)  grpnd%grpnod(i)%name, l_grp(i)
        !.
        do j=1,size(grpnd%grpnod(i)%gr_nod)
          inod = grpnd%grpnod(i)%gr_nod(j)
          if (ln_d(inod)  /= 0) write (lu_m) ln_d(inod)
        end do
      end if
    end do
  end if
  return
end subroutine bin_write_grpnod
!  -----------------------------------------------------------------------------
subroutine ascii_write_grpnod (lu_m, ln_d, grpnd)
! ------------------------------------------------------------------------------
! Esta rutina lee los grupos de elementos
! ------------------------------------------------------------------------------
  implicit none
  integer(ip), intent(in)      :: lu_m, ln_d(:)
  type (t_obgnd)               :: grpnd
  !.
  integer(ip)                  :: l_grp(grpnd%n_grp), ng_d
  integer(ip)                  :: i, j, inod

  ng_d = 0 !.Contador del numero de grupos en el dominio
  l_grp=0
  do i=1,grpnd%n_grp
    !.
    do j=1,size(grpnd%grpnod(i)%gr_nod)
      inod = grpnd%grpnod(i)%gr_nod(j)
      if (ln_d(inod)  /= 0) l_grp(i)=l_grp(i) + 1
    end do
    if (l_grp(i) > 0) ng_d = ng_d + 1
  end do
  !.
  write (lu_m,10) '#GRPNODE', ng_d
  !.
  if (ng_d > 0) then
    do i=1,grpnd%n_grp
      if (l_grp(i) > 0) then
        write (lu_m,10)  grpnd%grpnod(i)%name, l_grp(i)
        !.
        do j=1,size(grpnd%grpnod(i)%gr_nod)
          inod = grpnd%grpnod(i)%gr_nod(j)
          if (ln_d(inod)  /= 0) write (lu_m,advance = 'no',fmt=20) ln_d(inod)
        end do
        write (lu_m,'(/)')
      end if
    end do
  end if
  return
10 format (A,2X,I8)
20 format (10(:,2X,I8))
end subroutine ascii_write_grpnod



!  -----------------------------------------------------------------------------
subroutine bin_read_grpnod (lu_m, grpnd)
! ------------------------------------------------------------------------------
! Esta rutina lee los grupos de elementos
! ------------------------------------------------------------------------------
  implicit none
  integer(ip), intent(in)      :: lu_m
  type (t_obgnd)               :: grpnd
  !.
  character (len=8)            :: str
  integer(ip)                  :: i, j, io_err, i_err, n_dat


  read (lu_m, iostat=io_err) str ,grpnd%n_grp
  !print *,'str',str
  !read (lu_m, iostat=io_err) grpnd%n_grp
  if (io_err > 0) call w_error (41,'grpndm%n_grp',1)
  !.
  allocate (grpnd%grpnod(grpnd%n_grp), stat=i_err)
  if (i_err > 0 ) call w_error (42,'grpnd%grpnod(grpnd%n_grp)',1)
  !.
  do i = 1, grpnd%n_grp
    read (lu_m, *, iostat = io_err) grpnd%grpnod(i)%name, n_dat
    if (io_err > 0) call w_error (43,'grpnd%grpnod(i)%name, n_dat',1)
    !.
    allocate (grpnd%grpnod(i)%gr_nod(n_dat),stat=i_err) 
    if (i_err > 0 ) call w_error (42,'grpnd%grpnod(i)%gr_nod(n_dat)',1)
    do j=1, n_dat
      read (lu_m, iostat = io_err) grpnd%grpnod(i)%gr_nod(j)
      if (io_err > 0) call w_error (43,'grpnd%grpnod(i)%gr_nod(j)',1)
    end do
  end do
  return
end subroutine bin_read_grpnod
! ------------------------------------------------------------------------------
function deallocate_grpnod (grpnd) result(flg)
! ------------------------------------------------------------------------------ 
  implicit none
  type (t_obgnd)               :: grpnd
  integer(ip)                  :: flg
  !.
  integer(ip)                  :: i_err, i, istat

  if (allocated(grpnd%grpnod)) then
    i_err = 0
    do i=1,grpnd%n_grp
      deallocate (grpnd%grpnod(i)%gr_nod, stat=istat)
      if (istat /= 0) i_err = 1
    end do
    deallocate (grpnd%grpnod, stat=istat)
    if (istat == 0) flg = 0 
    if ((istat /= 0).or.(i_err == 1)) flg = 1
  else
    flg = -1
  end if
  return
end function deallocate_grpnod
! ------------------------------------------------------------------------------
end module mod_group_nd
! ------------------------------------------------------------------------------
