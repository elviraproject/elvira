! ------------------------------------------------------------------------------
module savestorCurrent
! ------------------------------------------------------------------------------
! At this module we allocate the storage variables for the currents and write
! the variables.
! ------------------------------------------------------------------------------
  use mod_precision
  use mod_UnitUtil
  use mod_problemsetting
  use mod_Iion
  !.
! ------------------------------------------------------------------------------
!. mx_c ==> 14 Ten Tusscher model
!. mx_c ==> 19 Nygren model
!. mx_c ==> 21 Luo Rudy model
!. mx_c ==> 16 PKFstewart model
!. mx_c ==>  4 Fenton Karma model
!. mx_c ==> 22 Grandi model
!. flg_nc(:) Flag indicating in which node we must store the current.
!. lst_nc(:) Node list
!. str_var(:,:,:)
! ------------------------------------------------------------------------------
  implicit none
  integer(ip), private, parameter          :: mx_t=100  !.maximum number of stored increments.
  integer(ip), private, parameter          :: mx_c=100  !.maximum number of currents
  integer(ip), private, allocatable, save  :: flg_nc(:)
  integer(ip), private, allocatable, save  :: lst_nc(:)
  integer(ip), private, allocatable, save  :: nc_pn(:)
  real(rp),    private, allocatable, save  :: str_cur(:,:,:), aux_cur(:,:)
  real(rp),    private,              save  :: t_save(mx_t)
  integer(ip), private                     :: nn_p, n_int

! ------------------------------------------------------------------------------
contains
!  ------------------------------------------------------------------------------
subroutine setcurrentpost (iam, ite, prblm)
!  ------------------------------------------------------------------------------
!  iam : process number
!  ite : increment number
! prblm: problem setting structure
!  ------------------------------------------------------------------------------
  implicit none
  integer(ip),      intent(in) :: iam, ite
  type(t_prblm),    intent(in) :: prblm
  !.
  integer(ip)                  :: ierr
  integer(ip)                  :: n_nod , i, inod, prop, t_cel


  if (ite == 0) then
    n_nod = get_nnod(prblm%mshdata%node)   !.Number of nodes in the domain
    !.
    nn_p  = get_ndout_nnp (prblm%stpdata%nd_out) !.Number of nodes for postprocessing
    n_int = get_ndout_nint(prblm%stpdata%nd_out) !.Frequency with which data is stored
    !.
    allocate (lst_nc(nn_p),nc_pn(nn_p),flg_nc(n_nod),stat = ierr)         
    !.
    lst_nc = get_node_post (prblm%stpdata%nd_out)  !.Node list for postprocessing
    flg_nc = 0
    !.
    do i=1,nn_p
      inod         = lst_nc(i) !.node number
      flg_nc(inod) = i
      prop         = get_node_prop (inod, prblm%mshdata%node) !.Pointer to nodal property
      t_cel        = get_celltype  (prop, prblm%mshdata%prnd) !.cell type
      nc_pn(i)     = get_numcur(t_cel)                        !.Num. Curr. in the model
    end do  
    allocate (str_cur(mx_c, nn_p, mx_t), aux_cur(mx_c,nn_p), stat = ierr)
    str_cur = 0.0; aux_cur = 0.0
  else
    write (*,10) ite; stop
  end if
  !.
   return
10 format ('###.Error in subroutine setcurrentpost, iteration: ',I6)
end subroutine setcurrentpost
! ------------------------------------------------------------------------------ 
subroutine currents_storage(i, v_cr)
! ------------------------------------------------------------------------------ 
! Temporal storage of currents
! i   : node number
! v_cr: vector with model currents
! ------------------------------------------------------------------------------ 
  implicit none
  integer(ip), intent(in) :: i
  real(rp),    intent(in) :: v_cr(:)
  !.
  integer(ip)             :: p_nd


  if (nn_p > 0 .and. flg_nc(i) > 0 ) then
    p_nd = flg_nc(i)
    aux_cur(:,p_nd) = v_cr(:) !.Current storage
  end if
  return
end subroutine currents_storage
! ------------------------------------------------------------------------------
subroutine currentsavewrite(iam, lu_e, mshdata, ite, t)
! ------------------------------------------------------------------------------
! Save temporal current data into the buffer and Write buffer into output file
! iam     : process number
! lu_e    : output file unit number
! mshdata : Model data (nodal coordinates, properties, conectivities, etc)
! ite     : increment number
! t       : time
! ------------------------------------------------------------------------------
  implicit none
  integer(ip),  intent(in) :: iam, lu_e, ite
  type(t_mesh), intent(in) :: mshdata
  real(rp),     intent(in) :: t
  integer(ip),  save       :: nsav=1

  !--Storage and writing of currents of selected nodes -------
  if ((nn_p > 0) .and. (mod(ite,n_int) == 0) ) then
    if (nsav < mx_t) then
      str_cur(:,:,nsav)=aux_cur(:,:); t_save(nsav) = t
      nsav = nsav + 1
    else
      str_cur(:,:,nsav)=aux_cur(:,:); t_save(nsav) = t
      call write_current (iam, lu_e, mshdata%fn_pst,mshdata%node)
      nsav = 1   
    end if
  end if
  !.
  return
end subroutine currentsavewrite
! ------------------------------------------------------------------------------
subroutine write_current ( iam, lu_e, fn_pos, node )
! ------------------------------------------------------------------------------
! Writes buffer into output file
! lu_e    : output file unit number
! fn_pos  : Output file name identifier
! node    : node structure
! ------------------------------------------------------------------------------
  implicit none
  integer(ip),       intent(in) :: iam, lu_e
  character (len=*), intent(in) :: fn_pos
  type(t_obnd),      intent(in) :: node
  !.
  integer(ip)                   :: i, j, n_nl, nn_g, m, nc
  character (len = 256)         :: fn_var 

  do i=1,nn_p
    n_nl= lst_nc(i)                !.Local node number 
    nn_g= get_nd_num_g (n_nl,node) !.Global degree of freedom
    !.
    m = len_trim(fn_pos); fn_var = fn_pos(1:m)//'prc'
    m = len_trim(fn_var); call join_str_num (fn_var, iam, m)
    m = len_trim(fn_var); fn_var = fn_var(1:m)//'_'
    call f_filename(fn_var,'.cur', nn_g, fn_var, m)
    open (unit=lu_e, file = fn_var(1:m), status='unknown', position= 'append')
    nc =  nc_pn(i) !.Number of cell model currents
    do j = 1,mx_t
      write (lu_e, 10) t_save(j), str_cur(1:nc,i,j)
    end do
    close (lu_e)
  end do
  return
10 format (100(1X,E14.6))
end subroutine write_current
! ------------------------------------------------------------------------------
end module savestorCurrent
