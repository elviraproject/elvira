! ------------------------------------------------------------------------------
module mod_timer
! ------------------------------------------------------------------------------
  use mod_precision
  implicit none
  type, public :: timer
    private
    real (rp)  :: saved_time
  end type timer
  !.
  public :: start_timer, elapsed_time
  !.
contains
! ------------------------------------------------------------------------------
subroutine start_timer (this)
! ------------------------------------------------------------------------------
  implicit none
  type(timer)  :: this
  integer      :: value(8)

  call date_and_time ( VALUES = value)
  this%saved_time = 86400.0*value(3) + 3600.0*value(5) + 60.0*value(6) +       &
                    value(7) + 0.001*value(8)
end subroutine start_timer
! ------------------------------------------------------------------------------
real(rp) function elapsed_time(this)
! ------------------------------------------------------------------------------
  implicit none
  type(timer)  :: this
  integer      :: value(8)
  real(rp)     :: current_time

  call date_and_time(values=value)
  current_time = 86400.0*value(3) + 3600.0*value(5) + 60.0*value(6) +         &
                 value(7) + 0.001*value(8)
  elapsed_time = current_time - this%saved_time
end function elapsed_time
! ------------------------------------------------------------------------------
end module mod_timer
! ------------------------------------------------------------------------------
