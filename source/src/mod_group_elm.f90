!  -----------------------------------------------------------------------------
module mod_group_elm
!  -----------------------------------------------------------------------------
  use mod_precision
  use mod_filename
  use mod_error
!  -----------------------------------------------------------------------------
  implicit none
!  ---------------------------------- GRUPO ELEMENTOS --------------------------
! Grupo de elementos
! Donde:
!    name    = Nombre del Grupo de elementos
!    grelm   = Vector de punteros a los elementos que forman el grupo
!    n_grelm = Numero de grupos de elementos
!  -----------------------------------------------------------------------------
  type t_gelm
    private
    character (len=32)      :: name
    integer(ip), allocatable:: gr_elm(:)
  end type t_gelm;
  !.
  type, public:: t_obgel
    private
    integer(ip)                ::n_grp = 0
    type (t_gelm), allocatable ::grpelm(:)
  end type t_obgel
!  -----------------------------------------------------------------------------
  contains
! ------------------------------------------------------------------------------
subroutine lec_grpelm (lu_m, arch_d, strout, grpel)
! ------------------------------------------------------------------------------
! Esta rutina lee los grupos de elementos
! ------------------------------------------------------------------------------
  implicit none
  integer(ip), intent(in)      :: lu_m
  character (len=*), intent(in):: arch_d, strout
  type (t_obgel)               :: grpel
  !.
  integer(ip)                  :: lu_lec, io_err, i_err, n_dat, i


  lu_lec = lu_m; call unitnumber (arch_d, strout, lu_lec, io_err)
  if (io_err > 0) call w_error (36,strout,1)
  !.
  read (lu_lec,*,iostat=io_err) grpel%n_grp
  if (io_err > 0) call w_error (37,'grpelm%n_grp',1)
  !.
  allocate (grpel%grpelm(grpel%n_grp), stat=i_err)
  if (i_err > 0 ) call w_error (38,'grpel%grpelm(grpel%n_grp)',1)
  !.
  do i = 1, grpel%n_grp
    read (lu_lec, *, iostat = io_err) grpel%grpelm(i)%name,n_dat
    if (io_err > 0) call w_error (39,'name, n_dat',1)
    !.
    allocate (grpel%grpelm(i)%gr_elm(n_dat),stat=i_err) 
    if (i_err > 0 ) call w_error (38,'grpel%grpelm(i)%gr_elm(n_dat)',1)
    !.
    read (lu_lec, *, iostat = io_err)grpel%grpelm(i)%gr_elm(1:n_dat)
    if (io_err > 0) call w_error (39,'grpel%grpelm(i)%()',1)
  end do
  !.
  if (lu_lec /= lu_m) call close_unit (lu_lec)
  !.
  write(     *,10) grpel%n_grp; write(lu_log,10) grpel%n_grp
  !.
  return
10 format ('###.Grupos de elementos, leidos                   : ',I8)
end subroutine lec_grpelm
!  -----------------------------------------------------------------------------
subroutine bin_write_grpel(lu_m, le_d, grpel)
! ------------------------------------------------------------------------------
! Esta rutina escribe en binario los grupos de elementos que pertenecen a un 
! dominio
! ------------------------------------------------------------------------------
  implicit none
  integer(ip), intent(in)      :: lu_m, le_d(:)
  type (t_obgel)               :: grpel
  !.
  integer(ip)                  :: l_grp(grpel%n_grp), ng_d
  integer(ip)                  :: i, j, iel


  ng_d = 0 !.Contador del numero de grupos en el dominio
  l_grp= 0 !.Contador del numero de elementos en cada grupo
  do i=1,grpel%n_grp
    !.
    do j=1,size(grpel%grpelm(i)%gr_elm)
      iel = grpel%grpelm(i)%gr_elm(j)
      if (le_d(iel)  /= 0) l_grp(i)=l_grp(i) + 1
    end do
    if (l_grp(i) > 0) ng_d = ng_d + 1
  end do
  !.
  write (lu_m) '#GRPELEM', ng_d
  !.
  if (ng_d > 0) then
    do i=1,grpel%n_grp
      if (l_grp(i) > 0) then
        write (lu_m)  grpel%grpelm(i)%name, l_grp(i)
        !.
        do j=1,size(grpel%grpelm(i)%gr_elm)
          iel = grpel%grpelm(i)%gr_elm(j)
          if (le_d(iel)  /= 0) write (lu_m) le_d(iel)
        end do
      end if
    end do
  end if
  return
end subroutine bin_write_grpel
!  -----------------------------------------------------------------------------
subroutine ascii_write_grpel(lu_m, le_d, grpel)
! ------------------------------------------------------------------------------
! Esta rutina escribe en binario los grupos de elementos que pertenecen a un 
! dominio
! ------------------------------------------------------------------------------
  implicit none
  integer(ip), intent(in)      :: lu_m, le_d(:)
  type (t_obgel)               :: grpel
  !.
  integer(ip)                  :: l_grp(grpel%n_grp), ng_d
  integer(ip)                  :: i, j, iel


  ng_d = 0 !.Contador del numero de grupos en el dominio
  l_grp= 0 !.Contador del numero de elementos en cada grupo
  do i=1,grpel%n_grp
    !.
    do j=1,size(grpel%grpelm(i)%gr_elm)
      iel = grpel%grpelm(i)%gr_elm(j)
      if (le_d(iel)  /= 0) l_grp(i)=l_grp(i) + 1
    end do
    if (l_grp(i) > 0) ng_d = ng_d + 1
  end do
  !.
  write (lu_m,10) '#GRPELEM', ng_d
  !.
  if (ng_d > 0) then
    do i=1,grpel%n_grp
      if (l_grp(i) > 0) then
        write (lu_m,10)  grpel%grpelm(i)%name, l_grp(i)
        !.
        do j=1,size(grpel%grpelm(i)%gr_elm)
          iel = grpel%grpelm(i)%gr_elm(j)
          if (le_d(iel)  /= 0) write (lu_m,advance='no',fmt=20) le_d(iel)
        end do
        write (lu_m,'(/)')
      end if
    end do
  end if
  return
10 format (A,2X,I8)
20 format (10(:,2X,I8))
end subroutine ascii_write_grpel


! ------------------------------------------------------------------------------
subroutine bin_read_grpel (lu_m, grpel)
! ------------------------------------------------------------------------------
! Esta rutina lee los grupos de elementos
! ------------------------------------------------------------------------------
  implicit none
  integer(ip), intent(in)      :: lu_m
  type (t_obgel)               :: grpel
  !.
  character (len= 8)           :: str      
  integer(ip)                  :: io_err, i_err, n_dat, i, j


  read (lu_m,iostat=io_err)   str, grpel%n_grp
  if (io_err > 0) call w_error (37,'grpelm%n_grp',1)
  !.
  allocate (grpel%grpelm(grpel%n_grp), stat=i_err)
  if (i_err > 0 ) call w_error (38,'grpel%grpelm(grpel%n_grp)',1)
  !.
  do i = 1, grpel%n_grp
    read (lu_m,  iostat = io_err) grpel%grpelm(i)%name, n_dat
    if (io_err > 0) call w_error (39,'grpel%grpelm(i)%name, n_dat',1)
    !.
    allocate (grpel%grpelm(i)%gr_elm(n_dat),stat=i_err) 
    if (i_err > 0 ) call w_error (38,'grpel%grpelm(i)%gr_elm(n_dat)',1)
    !.
    do j=1,n_dat
      read (lu_m, iostat = io_err) grpel%grpelm(i)%gr_elm(j)
      if (io_err > 0) call w_error (39,'grpel%grpelm(i)%gr_elm(j)',1)
    end do
  end do
  !.
  return
end subroutine bin_read_grpel
! ------------------------------------------------------------------------------
function deallocate_grpel (grpel) result(flg)
! ------------------------------------------------------------------------------ 
  implicit none
  type (t_obgel)               :: grpel
  integer(ip)                  :: flg
  !.
  integer(ip)                  :: i, istat

  flg = 0
  if (allocated(grpel%grpelm)) then
    do i=1,grpel%n_grp
      deallocate (grpel%grpelm(i)%gr_elm, stat=istat)
      if (istat /= 0) flg = 1
    end do
    deallocate (grpel%grpelm, stat=istat)
    if (istat /= 0) flg = 1
  else
    flg = -1
  end if
  return
end function deallocate_grpel
! ------------------------------------------------------------------------------
end module mod_group_elm
! ------------------------------------------------------------------------------
