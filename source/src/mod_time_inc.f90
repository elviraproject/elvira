!  -----------------------------------------------------------------------------
module mod_time_inc
!  -----------------------------------------------------------------------------
  use mod_precision
  use mod_filename
  use mod_error 
!  -----------------------------------------------------------------------------
  implicit none
  !.
  type, public :: t_tinc
    private 
    real(rp)   :: dtini
    real(rp)   :: dtmin 
    real(rp)   :: tmax 
    real(rp)   :: dtmax 
    integer(ip):: max_inc
  end type t_tinc

  contains
!  -----------------------------------------------------------------------------
subroutine lec_time_inc(lu_m, tinc)
!  -----------------------------------------------------------------------------
  implicit none
  integer(ip), intent(in)  :: lu_m
  type(t_tinc)             :: tinc
  !.
  integer(ip)              :: io_err


  !.Reading time incrementation parameters
  read (lu_m,*,iostat=io_err) tinc
!  read (lu_m,*,iostat=io_err) tinc%dtini,tinc%dtmin,tinc%tmax,tinc%dtmax,tinc%max_inc
  if (io_err > 0) call w_error (3,'stepdata%timeinc',1)
  return
end subroutine lec_time_inc
!  -----------------------------------------------------------------------------
subroutine get_stpdata_time_inc (timeinc, dtini, dtmin, tmax, dtmax, max_inc)
!  -----------------------------------------------------------------------------
  implicit none
  type(t_tinc)                     :: timeinc
  !.
  real(rp),    optional, intent(out)  :: dtini
  real(rp),    optional, intent(out)  :: dtmin 
  real(rp),    optional, intent(out)  :: tmax 
  real(rp),    optional, intent(out)  :: dtmax 
  integer(ip), optional, intent(out)  :: max_inc

  if (present(dtini  )) dtini   = timeinc%dtini
  if (present(dtmin  )) dtmin   = timeinc%dtmin
  if (present(tmax   )) tmax    = timeinc%tmax
  if (present(dtmax  )) dtmax   = timeinc%dtmax
  if (present(max_inc)) max_inc = timeinc%max_inc
  !.
  return
end subroutine get_stpdata_time_inc
!  -----------------------------------------------------------------------------
subroutine bin_write_timeinc(lu_m, tinc)
!  -----------------------------------------------------------------------------
  implicit none
  integer(ip), intent(in)  :: lu_m
  type(t_tinc)             :: tinc


  write (lu_m) '*TIME_INC'  
  write (lu_m) tinc
!  write (lu_m) tinc%dtini,tinc%dtmin,tinc%tmax,tinc%dtmax,tinc%max_inc
  return
end subroutine bin_write_timeinc

!  -----------------------------------------------------------------------------
subroutine ascii_write_timeinc(lu_m, tinc)
!  -----------------------------------------------------------------------------
  implicit none
  integer(ip), intent(in)  :: lu_m
  type(t_tinc)             :: tinc


  write (lu_m,10) '*TIME_INC'  
  write (lu_m,20) tinc
!  write (lu_m,20) tinc%dtini,tinc%dtmin,tinc%tmax,tinc%dtmax,tinc%max_inc
  return
10 format (A)
20 format (4(:,2X,F12.6),I10)
end subroutine ascii_write_timeinc
!  -----------------------------------------------------------------------------
subroutine bin_read_timeinc(lu_m, tinc)
!  -----------------------------------------------------------------------------
  implicit none
  integer(ip), intent(in)  :: lu_m
  type(t_tinc)             :: tinc
  !.
  character(len=9)         :: str
  integer(ip)              :: io_err


  read (lu_m, iostat = io_err) str  
  !
  read (lu_m, iostat=io_err)  tinc
!  read (lu_m, iostat=io_err)  tinc%dtini,tinc%dtmin,tinc%tmax,tinc%dtmax,tinc%max_inc
  if (io_err > 0) call w_error (3,'stepdata%timeinc',1)
  return
end subroutine bin_read_timeinc
!  -----------------------------------------------------------------------------
end module mod_time_inc
!  -----------------------------------------------------------------------------


