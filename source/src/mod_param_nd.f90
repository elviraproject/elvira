!  -----------------------------------------------------------------------------
module mod_param_nd
!  -----------------------------------------------------------------------------
  use mod_precision
  use mod_filename
  use mod_error
!  -----------------------------------------------------------------------------
  implicit none
!  -----------------------------------------------------------------------------

!  -----------------------------------------------------------------------------
!  p_nod : puntero al nodo
!. dat_i : vector de indices para la mascara
!. dat_r : vector de datos
!  -----------------------------------------------------------------------------
  type, public :: t_prmnd
    private
    integer(ip)             :: p_nod
    integer(ip),allocatable :: dat_i(:)
    real(rp),   allocatable :: dat_r(:)
  end type t_prmnd
  !.
  type t_obprmnd
    integer(ip)                :: n_prmnd = 0
    type(t_prmnd), allocatable :: prm_nd(:)
  end type t_obprmnd

contains
! ------------------------------------------------------------------------------
subroutine lec_prmnd (lu_m, arch_d, strout, prmnd)
! ------------------------------------------------------------------------------
! Esta rutina lee los parametros de los nodos
! ------------------------------------------------------------------------------
  implicit none
  integer(ip), parameter       :: max=100
  integer(ip), intent(in)      :: lu_m
  character (len=*), intent(in):: arch_d, strout
  type(t_obprmnd)              :: prmnd
  !.
  integer(ip)                  :: lu_lec,io_err,i_err
  integer(ip)                  :: i, nro_n, nro_p, dat_i(max)
  real(rp)                     :: dat_r(max)


  lu_lec = lu_m; call unitnumber (arch_d, strout, lu_lec, io_err)
  if (io_err > 0) call w_error (47,'subroutine lec_prmnd',1)
  !.
  read (lu_lec,*,iostat=io_err) prmnd%n_prmnd  !.Numero de nodos con parametros
  if (io_err > 0) call w_error (3,'prmnd%n_prmnd',1)
  !
  allocate (prmnd%prm_nd(prmnd%n_prmnd), stat=i_err)
  if (i_err > 0 ) call w_error (4,'prmnd%prm_nd(prmnd%n_prmnd)',1)
  !. 
  do i=1,prmnd%n_prmnd
    read (lu_lec,*,iostat=io_err) nro_n,nro_p,dat_i(1:nro_p),dat_r(1:nro_p) !.nro de nodo y propiedad
    if (io_err > 0) call w_error (50,i,1) 
    !.
    allocate(prmnd%prm_nd(i)%dat_i(nro_p),prmnd%prm_nd(i)%dat_r(nro_p),stat=i_err)
    if (i_err > 0) call w_error(49, i, 1)
    !.
    prmnd%prm_nd(i)%p_nod          = nro_n
    prmnd%prm_nd(i)%dat_i(1:nro_p) = dat_i(1:nro_p)
    prmnd%prm_nd(i)%dat_r(1:nro_p) = dat_r(1:nro_p)
  end do
  !.
  if (lu_lec /= lu_m) call close_unit (lu_lec)
  write (std_o,10) prmnd%n_prmnd ; write(lu_log,10) prmnd%n_prmnd 
  !.
  write (std_o,20) prmnd%prm_nd(1)%p_nod
  nro_p = size(prmnd%prm_nd(1)%dat_i)
  write (std_o,30) (prmnd%prm_nd(1)%dat_i(i),i=1,nro_p)
  write (std_o,40) (prmnd%prm_nd(1)%dat_r(i),i=1,nro_p)
  !.
  write (std_o,20) prmnd%prm_nd(prmnd%n_prmnd)%p_nod
  nro_p = size(prmnd%prm_nd(prmnd%n_prmnd)%dat_i)
  write (std_o,30) (prmnd%prm_nd(prmnd%n_prmnd)%dat_i(i),i=1,nro_p)
  write (std_o,40) (prmnd%prm_nd(prmnd%n_prmnd)%dat_r(i),i=1,nro_p)
  !.
  return
10 format ('###.Nodal mask for Ionic model                    : ',I8)
20 format (I6)
30 format (20(I6,1X))
40 format (20(E12.4,1X))
end subroutine lec_prmnd
! ------------------------------------------------------------------------------
subroutine get_mask_value_nd (p_prm, prmnd, n, dat_i, dat_r)
! ------------------------------------------------------------------------------
  implicit none
  integer(ip), intent(in)     :: p_prm
  type(t_obprmnd), intent(in) :: prmnd
  !.
  integer(ip), intent(out)    :: n, dat_i(:)
  real(rp),    intent(out)    :: dat_r(:)
  !.

  n = size(prmnd%prm_nd(p_prm)%dat_i)
  !.
  dat_i(1:n) = prmnd%prm_nd(p_prm)%dat_i
  dat_r(1:n) = prmnd%prm_nd(p_prm)%dat_r
  return
end subroutine get_mask_value_nd
! -------------------------------------------------------------------------------
subroutine get_parampointer (prmnd, n, iaux )
! -------------------------------------------------------------------------------
  implicit none
  integer(ip), intent(inout)  :: n, iaux(:)
  type(t_obprmnd), intent(in) :: prmnd
  !.
  integer(ip)                 :: i,k
  !.

  n = 0
  if (allocated(prmnd%prm_nd)) then
    do i=1,size(prmnd%prm_nd)
      k = prmnd%prm_nd(i)%p_nod
      iaux(k) = i 
    end do
    n=i-1
  end if
  return
end subroutine get_parampointer
! ------------------------------------------------------------------------------
subroutine bin_write_prmnd (lu_m, ln_d, prmnd)
! ------------------------------------------------------------------------------
! Esta rutina lee los parametros de los nodos
! ------------------------------------------------------------------------------
  implicit none
  integer(ip), intent(in)      :: lu_m, ln_d(:)
  type(t_obprmnd)              :: prmnd
  !.
  integer(ip)                  :: l_aux(prmnd%n_prmnd)

  integer(ip)                  :: c_pn, inod, i, j, nro_p


  c_pn = 0; l_aux = 0
  do i=1, prmnd%n_prmnd  !.Numero de nodos con parametros
    inod = prmnd%prm_nd(i)%p_nod !.puntero al nodo
    if (ln_d(inod) /= 0) then    !.nodo pertenece al subdominio
      c_pn = c_pn + 1
      l_aux(c_pn) = i
    end if
  end do
  !.
  write (lu_m) '*PARAM_NODE', c_pn
  do i=1,c_pn
    j=l_aux(i)                             !.puntero al parametro
    inod = prmnd%prm_nd(j)%p_nod           !.nodo
    nro_p= size(prmnd%prm_nd(j)%dat_i)
    write (lu_m) ln_d(inod), nro_p
    write (lu_m) prmnd%prm_nd(j)%dat_i
    write (lu_m) prmnd%prm_nd(j)%dat_r
  end do
  return
end subroutine bin_write_prmnd
! ------------------------------------------------------------------------------
subroutine ascii_write_prmnd (lu_m, ln_d, prmnd)
! ------------------------------------------------------------------------------
! Esta rutina lee los parametros de los nodos
! ------------------------------------------------------------------------------
  implicit none
  integer(ip), intent(in)      :: lu_m, ln_d(:)
  type(t_obprmnd)              :: prmnd
  !.
  integer(ip)                  :: l_aux(prmnd%n_prmnd)

  integer(ip)                  :: c_pn, inod, i, j, nro_p

  c_pn = 0
  do i=1, prmnd%n_prmnd  !.Numero de nodos con parametros
    inod = prmnd%prm_nd(i)%p_nod !.puntero al nodo
    if (ln_d(inod) /= 0) then    !.nodo pertenece al subdominio
      c_pn = c_pn + 1
      l_aux(c_pn) = i
    end if
  end do
  !.
  write (lu_m,10) '*PARAM_NODE', c_pn
  do i=1,c_pn
    j=l_aux(i)                             !.puntero al parametro
    inod = prmnd%prm_nd(j)%p_nod           !.nodo
    nro_p= size(prmnd%prm_nd(j)%dat_i)
    write (lu_m,20) ln_d(inod), nro_p
    write (lu_m,30) prmnd%prm_nd(j)%dat_i
    write (lu_m,40) prmnd%prm_nd(j)%dat_r
  end do
  return
10 format (A,2X,I8)
20 format (I8,2X,I8)
30 format (10(:,2X,I3))
40 format (10(:,2X,F12.6))
end subroutine ascii_write_prmnd


! ------------------------------------------------------------------------------
subroutine bin_read_prmnd (lu_m, prmnd)
! ------------------------------------------------------------------------------
! Esta rutina lee los parametros de los nodos
! ------------------------------------------------------------------------------
  implicit none
  integer(ip), intent(in)      :: lu_m
  type(t_obprmnd)              :: prmnd
  !.
  character (len = 11)         :: str
  integer(ip)                  :: i, nro_p, io_err, i_err


  read (lu_m,iostat=io_err) str, prmnd%n_prmnd  !.Numero de nodos con parametros
  if (io_err > 0) call w_error (3,'prmnd%n_prmnd',1)
  !
  allocate (prmnd%prm_nd(prmnd%n_prmnd), stat=i_err)
  if (i_err > 0 ) call w_error (4,'prmnd%prm_nd(prmnd%n_prmnd)',1)
  !. 
  do i=1,prmnd%n_prmnd
    read (lu_m,iostat=io_err) prmnd%prm_nd(i)%p_nod, nro_p !.nro de nodo y propiedad
    if (io_err > 0) call w_error (50,i,1) 
    !.
    allocate(prmnd%prm_nd(i)%dat_i(nro_p),prmnd%prm_nd(i)%dat_r(nro_p),stat=i_err)
    if (i_err > 0) call w_error(49, i, 1)
    !.
    read (lu_m, iostat=io_err) prmnd%prm_nd(i)%dat_i
    read (lu_m, iostat=io_err) prmnd%prm_nd(i)%dat_r
    if (io_err > 0) call w_error (50,i,1) 
  end do
  return
end subroutine bin_read_prmnd
! ------------------------------------------------------------------------------
function deallocate_prmnd (prmnd) result(flg)
! ------------------------------------------------------------------------------ 
  implicit none
  type(t_obprmnd)              :: prmnd
  integer(ip)                  :: flg
  !.
  integer(ip)                  :: i_err, i, istat

  if (allocated(prmnd%prm_nd)) then
    i_err = 0
    do i=1, prmnd%n_prmnd 
      deallocate (prmnd%prm_nd(i)%dat_i,prmnd%prm_nd(i)%dat_r, stat=istat)
      if (istat /= 0) i_err = 1
    end do
    deallocate (prmnd%prm_nd, stat=istat)
    if (istat == 0) flg = 0 
    if ((istat /= 0).or.(i_err == 1)) flg = 1
  else
    flg = -1
  end if
  return
end function deallocate_prmnd
! ------------------------------------------------------------------------------
end module mod_param_nd
! ------------------------------------------------------------------------------
