! ------------------------------------------------------------------------------
module modPostProceso
! ------------------------------------------------------------------------------
  use mod_precision
  use mod_UnitUtil
  use mod_problemsetting
  !.
  use mod_com_EM_nd
! ------------------------------------------------------------------------------
  implicit none
  integer(ip), allocatable, save  :: lst_np(:),nv_pn(:)
  !.
! ------------------------------------------------------------------------------
contains
!  ------------------------------------------------------------------------------
subroutine PostProceso (iam, ite, prblm, Vn, Qn, t)
!  ------------------------------------------------------------------------------
!  lu_p : unidad que se usa para escribir el potencial
!  lu_e : unidad que se usa para escribir las distintas variables electricas
!  ------------------------------------------------------------------------------
  implicit none
  integer(ip), parameter       :: mx_t=100, mx_v=50 ! mx_v deprecated
  integer(ip),      intent(in) :: iam, ite
  type(t_prblm)                :: prblm
  real(rp),    intent(in)      :: Vn(:), Qn(:), t
  !.
  integer(ip),         save    :: lu_p, lu_e
  integer(ip),         save    :: frc, n_nod, m,nn_p,n_int,nsav=1,flg_v
  character (len=256), save    :: fname
  real(rp),   allocatable,save :: str_var(:,:,:)
  integer(ip)                  :: ierr, flag_post
  integer(ip)                  :: i, i_nd, n_v, max_nv!, nproc
  real(rp)                     :: val(100)

!tipo_p = prblm%mshdata%atype
  if (ite == 0) then
    call a_unit(lu_p); call a_unit(lu_e); !.Unidades de escritura
!    call  EnsightMsh (iam, lu_p, prblm%mshdata%fn_pst, prblm%mshdata)
    call get_output_var ( 1, prblm%stpdata%g_out, flg_v, frc)
    if (frc == 0) frc = huge(frc)
!    call  get_outputset (prblm%stpdata%g_out,frec = frc)
    !.
    n_nod = get_nnod(prblm%mshdata%node)   !n_elm = get_nelm(prblm%mshdata%elem)
    m = len_trim(prblm%mshdata%fn_pst)
    fname = prblm%mshdata%fn_pst(1:m)//'prc_'
    call file_name (fname,'_', iam, fname, m)
    m = len_trim(fname)
    !.
    nn_p  = get_ndout_nnp (prblm%stpdata%nd_out) !.Number of nodes for postprocessing
    n_int = get_ndout_nint(prblm%stpdata%nd_out) !.Frequency for storage
    !.
    allocate (lst_np(nn_p),nv_pn(nn_p),stat = ierr)         

    lst_np = get_node_post (prblm%stpdata%nd_out)   !.List of nodes for postprocessing
    flag_post = get_ndout_flag (prblm%stpdata%nd_out)!.Postprocessing variables flag
    !.
    do i=1,nn_p
      i_nd = lst_np(i)
      nv_pn(i)= 3 !.t + Vi + Jion
      if(flag_post.eq.1) then
        call get_EM_nd(i_nd, n_v, val)
        nv_pn(i)= nv_pn(i) + n_v !.number of model variables + t + Vi + Qi
      endif
    end do
    !call mpi_comm_size (mpi_comm_world, nproc, ierr)
    !call mpi_barrier   (mpi_comm_world, ierr)
    max_nv = max_num_state_var(prblm%mshdata)
    write(*,*) '##MAX NUMBER OF STATE VAR IN MODEL: ', max_nv
    if(flag_post.eq.1) then
      allocate (str_var(max_nv+3, nn_p, mx_t), stat=ierr)
    else
      allocate (str_var(3, nn_p, mx_t), stat=ierr)
    end if
    str_var = 0.0
  end if
  !.
  !--Storage and saving of nodal variables on selected nodes -------
  if ((nn_p > 0) .and. (mod(ite,n_int) == 0) ) then
    flag_post = get_ndout_flag (prblm%stpdata%nd_out)!.Postprocessing variables flag
    if (nsav < mx_t) then
      call storage_elecvar(str_var(:,:,nsav), nn_p,t,Vn,Qn,flag_post)
      nsav = nsav + 1
    else
      call storage_elecvar(str_var(:,:,nsav), nn_p,t,Vn,Qn,flag_post)
      call write_elecvar   (iam, lu_e,prblm%mshdata%fn_pst,prblm%mshdata%node,    &
                            str_var, mx_t, nn_p, flag_post)
      nsav = 1
    end if
  end if
  !.
  return
10 format ('###.Iteration: ',I8,'  >>>.CPU time [sec] : ',F12.2)
end subroutine PostProceso
! ------------------------------------------------------------------------------
!                 POSTROCESSING NODAL VARIABLES
!-------------------------------------------------------------------------------
subroutine storage_elecvar (str_var, n, t,V,Q,flag_post)
! ------------------------------------------------------------------------------
! This subroutine stores temporarily the value of state variables for the
! models. 
! str_var  : multidimensional array with state variables
! n        : Number of nodes to output results
! t        : Current time
! V        : Action potential
! Q        : Total ionic current
! flag_post: Output flag, flag=1 outputs all state variables, flag=0 outputs
!            only t, V and Q.
! ------------------------------------------------------------------------------
  implicit none
  integer(ip), parameter       :: max=100
  real(rp),    intent(inout)   :: str_var(:,:)
  integer(ip), intent(in)      :: n, flag_post
  real(rp),    intent(in)      :: t, V(:), Q(:)
!
  real(rp)                     :: val(max)
  integer(ip)                  :: i, i_nd, n_v


  do i=1,n
    i_nd = lst_np(i)
    !.
    str_var(1:3,i)=(/t, V(i_nd), Q(i_nd)/)
    if (flag_post.eq.1) then
      call get_EM_nd(i_nd, n_v, val)
      str_var(4:n_v+3,i)=val(1:n_v)
    end if
  end do
  return
end subroutine storage_elecvar
! ------------------------------------------------------------------------------
subroutine write_elecvar ( iam, lu_v, fn_pos, node, str_var, mx_t, nn_p, flag_post)
! ------------------------------------------------------------------------------
! This subroutine writes to disk the state variables that have been stored in the
! array str_var
! iam      : process number
! lu_v     : unit number (used for writing file)
! fn_post  : root to identify output file
! node     : node structure 
! str_var  : multidimensional array with state variables
! max_t    : buffer size for state varaibles storage
! nn_p     : Number of nodes to output results
! flag_post: Output flag, flag=1 outputs all state variables, flag=0 outputs
!            only t, V and Q.
! ------------------------------------------------------------------------------
  implicit none
  character (len=*), intent(in) :: fn_pos
  type(t_obnd),      intent(in) :: node
  real(rp),          intent(in) :: str_var(:,:,:)
  integer(ip),       intent(in) :: iam, lu_v, mx_t, nn_p, flag_post
  !.
  integer(ip)                   :: i, j, n_nl, nn_g, m, nv
  character (len = 256)         :: fn_var 

  do i=1,nn_p
    n_nl= lst_np(i)                !.Local node number
    nn_g= get_nd_num_g (n_nl,node) !.Global degree of freedom (node number in electric)
    !.
    m = len_trim(fn_pos); fn_var = fn_pos(1:m)//'prc'
    m = len_trim(fn_var); call join_str_num (fn_var, iam, m)
    m = len_trim(fn_var); fn_var = fn_var(1:m)//'_'
    call f_filename(fn_var,'.var', nn_g, fn_var, m)
    open (unit=lu_v, file = fn_var(1:m), status='unknown', position= 'append')
    if(flag_post.eq.1) then
       nv =  nv_pn(i) !.Number of state variables + t + Vi + Qi
    else
       nv=3
    end if
    do j = 1,mx_t
      write (lu_v, 10) str_var(1:nv,i,j)
    end do
    close (lu_v)
  end do
  return
10 format (F15.4,100(1X,E17.9))
end subroutine write_elecvar
! ------------------------------------------------------------------------------
! ------------------------------------------------------------------------------
! ------------ SUBROUTINES FOR GENERATING OUTPUT FOR ENSIGHT ----------------
! ------------------------------------------------------------------------------
subroutine EnsightMsh (iam, lu_p, fn_pos, mshdata)
! ------------------------------------------------------------------------------
!  lu_s : unidad logica de la malla
!  i_nom  : entero que se concatena con la raiz del nombre del archivo
!  a_nom  : raiz del nombre del archivo
! ------------------------------------------------------------------------------
  implicit none
  integer(ip), parameter        :: max=30
  integer(ip),       intent(in) :: iam, lu_p
  character (len=*), intent(in) :: fn_pos
  type(t_mesh),      intent(in) :: mshdata
  !.
  integer(ip)                   :: num(max), tip(max), conc(max)
  real(rp)                      :: coord(max)
  integer(ip)                   :: m, n_nod, i, j, n_elm, idim, tipo, n_tp, tipo1, nro_e, n_pe
  character (len=256)           :: fn_msh

  character (len=80)            :: type_CEI

  call file_name (fn_pos, '.geo', iam, fn_msh, m)
  open (unit=lu_p,file=fn_msh(1:m),status='unknown')
!
!  if (iam == 0) then
  write(lu_p,10)
  write(lu_p,20) mshdata%title(1:len_trim(mshdata%title))
  write(lu_p,30)
  write(lu_p,40)
!  end if
  !.
  write(lu_p,50)
  write(lu_p,60) iam+1
  write(lu_p,70)
  write(lu_p,80)
  !.
  n_nod = get_nnod (mshdata%node)  !.numero de nodos 
  !.
  write(lu_p,60) n_nod
  !.
  write(lu_p,60) (i,i=1,n_nod)
  !.
  !do i =1, n_nod
  !  write(lu_p,60) get_nd_num_g (i, mshdata%node)
  !end do
  !.
  do i =1, n_nod 
    call get_nodecoord (i,idim,coord,mshdata%node)
    write(lu_p,90) coord(1)
  end do
  !.
  do i =1, n_nod 
    call get_nodecoord (i,idim,coord,mshdata%node)
    if (idim >= 2) then
      write(lu_p,90) coord(2)
    else
      write(lu_p,90) 0.0
    end if
  end do
  !.
  do i =1, n_nod 
    call get_nodecoord (i,idim,coord,mshdata%node)
    if (idim >= 3) then
      write(lu_p,90) coord(3)
    else
      write(lu_p,90) 0.0
    end if
  end do
  !.
  n_elm = get_nelm (mshdata%elem)  !.numero de elementos
  !.
  call get_numberofelmtype (mshdata%elem, n_tp, tip, num)
  !.n_tp: numero de tipos de elementos
  !.tip : vector de tipos de elementos 
  !.num : vector de numero de elementos de cada tipo
  do i=1,n_tp
    tipo  = tip(i)
    tipo1 = elvira_id_elm(tipo)
    call id_elem_CEI (tipo1, type_CEI, n_pe)
    write(lu_p,20) type_CEI
    !.
    write(lu_p,60) num(i)
    !.
    do j = 1,n_elm
      call get_elmconc_tipo (j, tipo, nro_e, n_pe, conc, mshdata%elem)
      if (nro_e > 0) write(lu_p,60) nro_e
    end do
    !.
    do j = 1,n_elm
      call get_elmconc_tipo (j, tipo, nro_e, n_pe, conc, mshdata%elem)
      if (nro_e > 0) write(lu_p,100) conc(1:n_pe)
    end do
  enddo
  close(lu_p)
  return
 10 format('Ensight Model Geometry File')
 20 format(A80)
 30 format('node id given')
 40 format('element id given')
 50 format('part')
 60 format(I10)
 70 format('Model, Geometry')
 80 format('coordinates')
 90 format(E12.5)
100 format(100I10)
end subroutine EnsightMsh
! ------------------------------------------------------------------------------
function  elvira_id_elm(tipo) result (tip)
! ------------------------------------------------------------------------------
  implicit none
  integer(ip), intent(in)       :: tipo
  integer(ip)                   :: tip

  select case (tipo)
  case(1,15,20:22,61:66); tip =  2 !.Linea
  case(3,16,23,24); tip =  4 !.Triangulo
  case(5,17,25:33); tip =  6 !.Cuadrilatero
  case(34);         tip =  7 !.Cuadrilatero de 8 nodos
  case(7,18,35,36); tip =  8 !.Tetraedro 
  case(9,19,37,38); tip = 14 !.Hexaedro
  case default
    write (*,10)'Posproceso, elvira_id_elm',tipo; stop
  end select
  return
10 format ('###.Error in: ',A,2X,I6)
end function elvira_id_elm
! ------------------------------------------------------------------------------
subroutine id_elem_CEI(itip,name,np)
! ------------------------------------------------------------------------------
  implicit none
  character (len= 9),parameter:: elemName(1:17)=(/'point    ','bar2     ',      &
       'bar3     ','tria3    ','tria6    ','quad4    ','quad8    ','tetra4   ', &
       'tetra10  ','pyramid5 ','pyramid13','penta6   ','penta15  ','hexa8    ', &
       'hexa20   ','nsided   ','nfaced   '/)
  character(len=11),parameter :: g_elemName(1:17)=(/'g_point    ','g_bar2     ',&
       'g_bar3     ','g_tria3    ','g_tria6    ','g_quad4    ','g_quad8    ',   &
       'g_tetra4   ','g_tetra10  ','g_pyramid5 ','g_pyramid13','g_penta6   ',   &
       'g_penta15  ','g_hexa8    ','g_hexa20   ','g_nsided   ','g_nfaced   '/)
  integer(ip), intent(in) :: itip
  integer(ip), intent(out):: np
  character(len=*)        :: name

  select case (itip)
  case( 1); name=elemName( 1); np = 1
  case( 2); name=elemName( 2); np = 2
  case( 3); name=elemName( 3); np = 3
  case( 4); name=elemName( 4); np = 3
  case( 5); name=elemName( 5); np = 6
  case( 6); name=elemName( 6); np = 4
  case( 7); name=elemName( 7); np = 8
  case( 8); name=elemName( 8); np = 4
  case( 9); name=elemName( 9); np =10
  case(10); name=elemName(10); np = 5
  case(11); name=elemName(11); np =13
  case(12); name=elemName(12); np = 6
  case(13); name=elemName(13); np =15
  case(14); name=elemName(14); np = 8
  case(15); name=elemName(14); np =20
  !case(16); name=elemName(16); np = 0 !.completar cuando exista
  !case(17); name=elemName(17); np = 0 !.completar cuando exista
  case default; 
    write (*,10) 'id_elem_CEI',itip; stop
  end select
  return
10 format ('###.Error in: ',A,2X,I6)
end subroutine id_elem_CEI
! ------------------------------------------------------------------------------
subroutine ensight_escalar (lu_p, fname, iam, ite, n, Vn)
! ------------------------------------------------------------------------------
  implicit none
  integer(ip),intent(in)        :: lu_p, iam, ite, n
  character (len=*), intent(in) :: fname
  real(rp), intent(in)          :: Vn(:)
  !.
  character (len=256)           :: fn_out
  integer(ip)                   :: m,i

  call f_filename (fname,'.ens', ite, fn_out, m)
  open (unit = lu_p, file= fn_out(1:m),status='unknown')
  !.
  write (lu_p, 10)
  write (lu_p, 20)
  write (lu_p, 30) iam+1
  write (lu_p, 40)
  write (lu_p, 50) (Vn(i), i=1, n)
  close(lu_p)
  return
10 format('Ensight Model Post Process ')
20 format('part')
30 format(I10)
40 format('coordinates')
50 format(E12.5)
end subroutine ensight_escalar
! ------------------------------------------------------------------------------
subroutine ensight_vector (lu_p, fname, iam, ite, V) 
! ------------------------------------------------------------------------------
! Esta subrutina escribe un archivo de postproceso para ensight de una variable
! vectorial en nodos.
! ------------------------------------------------------------------------------
  implicit none
  integer(ip),intent(in)        :: lu_p, iam, ite
  character (len=*), intent(in) :: fname
  real(rp), intent(in)          :: V(:,:)
  !.
  character (len=256)           :: fn_out
  integer(ip)                   :: m, i, j, nnod, ncom

  call f_filename (fname,'.ens', ite, fn_out, m)
  open (unit = lu_p, file= fn_out(1:m),status='unknown')
  !.
  nnod=size(V(1,:))
  ncom=size(V(:,1))
  write (lu_p,10)'Ensight Model Post Process '
  write (lu_p,20)'part'
  write (lu_p,30) iam + 1
  write (lu_p,40)'coordinates'
  do j=1,ncom
    do i=1,nnod
      write(lu_p,50) V(j,i)
    end do
  end do
  close(lu_p)
  return
10 format('Ensight Model Post Process ')
20 format('part')
30 format(I10)
40 format('coordinates')
50 format(E12.5)
end subroutine ensight_vector
end module modPostProceso
