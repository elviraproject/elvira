!  -----------------------------------------------------------------------------
module mod_sndrcv_xg
!  -----------------------------------------------------------------------------
  use mod_precision
  use mod_node
  use mpi_mod
!  -----------------------------------------------------------------------------
  implicit none
!  -----------------------------------------------------------------------------
  type, public:: t_snd
    private
    integer(ip)              :: dst              !.Number of destiantion (process)
    integer(ip), allocatable :: i_snd(:)         !.Local index of d.o.f to send
    real(rp),    allocatable :: d_snd(:)         !.Data to send
!    integer(ip), pointer :: i_snd(:)=>NULL() !.Local index of d.o.f to send
!    real(rp),    pointer :: d_snd(:)=>NULL() !.Data to send
  end type t_snd
  !.
  type, public:: t_rcv
    private
    integer(ip)              :: src      !.Number of source (process)
    integer(ip), allocatable :: i_rcv(:) !.Local index of d.o.f to receive 
    real(rp),    allocatable :: d_rcv(:) !.Data to receive
!    integer(ip), pointer :: i_rcv(:)=>NULL() !.Indice local de los  grados de libertad a recibir
!    real(rp),    pointer :: d_rcv(:)=>NULL() !.Datos a recibir
  end type t_rcv
  !.
  type, public :: t_sndrcv
    integer(ip)              :: n_snd =0 !.number of processes to which data must be sent
    integer(ip)              :: n_rcv =0 !.number of processes from which data is received
    type(t_snd), allocatable :: snd(:)   !.Data in sent menssage 
    type(t_rcv), allocatable :: rcv(:)   !.Data in menssage received  
!    type(t_snd), pointer :: snd(:)=>NULL() !.Datos de los mensajes de envio
!    type(t_rcv), pointer :: rcv(:)=>NULL() !.Data in menssage received
  end type t_sndrcv

contains
! ------------------------------------------------------------------------------
subroutine set_sndrcv_xg (ndof_l, iam, np, node, sr_xg)
! ------------------------------------------------------------------------------
  implicit none
  integer(ip),  intent(in)   :: ndof_l, iam, np
  type(t_obnd), intent(in)   :: node
  type(t_sndrcv)             :: sr_xg
  !.
  integer(ip)                :: i_snd(np),i_rcv(np),dom(np),p_snd(np),p_rcv(np)
  integer(ip)                :: i, j, k, id, js, jr, n, i_err, nd_v, ks, kr

  i_snd = 0; i_rcv = 0
  do i= 1, ndof_l
    call get_node_subdomain(i, nd_v, dom, node)
    if  (nd_v > 1) then
      id = dom(1)
      if (id == iam + 1) then  !.Enviar
        do j = 2, nd_v
          k = dom(j); i_snd(k) = i_snd(k) + 1
        end do
      else                   !.Recibir
        i_rcv(id) = i_rcv(id)+1
      end if
    end if
  end do
  !.
  sr_xg%n_snd = count(i_snd > 0)   !.Numero de proc. a los que enviar
  sr_xg%n_rcv = count(i_rcv > 0)   !.Numero de proc. de los que recibir 
  !.
  allocate ( sr_xg%snd(sr_xg%n_snd), sr_xg%rcv(sr_xg%n_rcv), stat=i_err)
  if (i_err > 0 ) call w_error (4,'sr_xg%snd(:), sr_xg%rcv(:)',1)
  !.
  k = 0
  do i = 1, np
    if (i_snd(i) > 0) then
      n = i_snd(i); k=k+1; sr_xg%snd(k)%dst=i-1; p_snd(i) = k
      allocate (sr_xg%snd(k)%i_snd(n), sr_xg%snd(k)%d_snd(n), stat=i_err) 
      if (i_err > 0 ) call w_error (4,'sr_xg%snd(k)%i_snd(:);%d_snd(:)',1)
      sr_xg%snd(k)%i_snd = 0; sr_xg%snd(k)%d_snd = 0.0
    end if
  end do
  !.
  k=0
  do i = 1, np
    if (i_rcv(i) > 0) then
      n = i_rcv(i); k=k+1; sr_xg%rcv(k)%src=i-1; p_rcv(i) = k
      allocate (sr_xg%rcv(k)%i_rcv(n), sr_xg%rcv(k)%d_rcv(n), stat=i_err) 
      if (i_err > 0 ) call w_error (4,'sr_xg%rcv(k)%i_rcv(:);%d_rcv(:)',1)
      sr_xg%rcv(k)%i_rcv = 0; sr_xg%rcv(k)%d_rcv =0.0
    end if
  end do
  !.-----------------------------------------------------------------------------
  i_snd = 0; i_rcv = 0
  do i= 1, ndof_l
    call get_node_subdomain(i, nd_v, dom, node)
    if  (nd_v > 1) then
      id = dom(1)
      if (id == iam + 1) then  !.Enviar
        do j = 2, nd_v
          k = dom(j); js = i_snd(k) + 1; i_snd(k) = js 
          ks = p_snd(k)
          sr_xg%snd(ks)%i_snd(js) = i
        end do
      else                     !.Recibir
        k = id; jr = i_rcv(k) + 1; i_rcv(k) = jr
        kr = p_rcv(k)
        sr_xg%rcv(kr)%i_rcv(jr) = i
      end if
    end if
  end do
  !.
  return
end subroutine set_sndrcv_xg
! ------------------------------------------------------------------------------
subroutine write_sndrcv_xg (iam, np, lu_o, sr_xg)
!  -----------------------------------------------------------------------------
  implicit none
  integer(ip), intent(in)     :: iam, np, lu_o
  type(t_sndrcv)              :: sr_xg
  !.
  integer(ip)                 :: i, n

  call write_n_str(lu_o, 80,'='); 
  write (lu_o, 10) iam, np
  call write_n_str(lu_o, 80,'.'); 
  !.
  call write_n_str(lu_o, 80,'S'); 
  write (lu_o, 20) 
  do i = 1,sr_xg%n_snd 
    call write_n_str(lu_o, 80,'-');
    n=size(sr_xg%snd(i)%i_snd)
    write (lu_o,30) sr_xg%snd(i)%dst,n
    write (lu_o,40) sr_xg%snd(i)%i_snd(1:n)
  end do
  !.
  call write_n_str(lu_o, 80,'R'); 
  write (lu_o, 50) 
  do i = 1,sr_xg%n_rcv 
    call write_n_str(lu_o, 80,'-'); 
    n=size(sr_xg%rcv(i)%i_rcv)
    write (lu_o,60) sr_xg%rcv(i)%src, n
    write (lu_o,40) sr_xg%rcv(i)%i_rcv(1:n)
  end do
  !.
  return
10 format('###.RANK: ',I6,' of ',I6) 
20 format('DATA TO SND. ......')
30 format('DESTINATION: ', I6,' n:',I6)
40 format('DOF   : ',/,10(:,1X,I8))
50 format('DATA TO RCV. ......')
60 format('SOURCE: ', I6,' n:',I6)
end subroutine write_sndrcv_xg
! ------------------------------------------------------------------------------
subroutine sndrcv_xg (comm, iam, sr_xg, Xg)
!  -----------------------------------------------------------------------------
  implicit none
  integer(ip), intent(in)     :: comm, iam
  type(t_sndrcv)              :: sr_xg
  real(rp)                    :: Xg(:)
  !.
  integer(ip)                 :: sr_t = mpi_double_precision
  integer(ip)                 :: i, j, k, dst, src, n_s, n_r 
  integer(ip)                 :: ierr, status(MPI_STATUS_SIZE), tag
  

 ! call  write_sndrcv_xg (iam, 4, 123+iam, sr_xg)
  !.
  write (iam +500,'(10(:1X,F9.3))') Xg
  tag = 1
  do i = 1,sr_xg%n_snd 
    dst = sr_xg%snd(i)%dst 
    n_s = size(sr_xg%snd(i)%i_snd)
    do j=1,n_s
      k = sr_xg%snd(i)%i_snd(j)
      sr_xg%snd(i)%d_snd(j) = Xg(k)
    end do
    !.
  !  print *,'send iam:',iam, sr_xg%snd(i)%d_snd(1:n_s)
    call mpi_send(sr_xg%snd(i)%d_snd(1), n_s, sr_t, dst, iam+1, comm, ierr)
!    call mpi_send(sr_xg%snd(i)%d_snd(1:n_s), n_s, sr_t, dst, iam+1, comm, ierr)
    !.
  end do
  !.
  !------------------------------------------------------------------------------
  !.
 ! print *,'iam, sr_xg%n_rcv', iam,  sr_xg%n_rcv
  do i = 1,sr_xg%n_rcv 
    src = sr_xg%rcv(i)%src
    n_r = size(sr_xg%rcv(i)%i_rcv)
    !.
  !  print *,'Antes n_r: ', n_r
    call mpi_recv(sr_xg%rcv(i)%d_rcv(1), n_r, sr_t, src, src+1, comm, status, ierr)
!    call mpi_recv(sr_xg%rcv(i)%d_rcv(1:n_r), n_r, sr_t, src, src+1, comm, status, ierr)
  !  print *,'Despues n_r: ', n_r
 !   print *,'reciv iam:',iam,sr_xg%rcv(i)%d_rcv
    !.
    do j=1,size(sr_xg%rcv(i)%d_rcv)
!      print *,'iam, i,j,n_r',iam, i,j,n_r
      k = sr_xg%rcv(i)%i_rcv(j)
      Xg(k)=sr_xg%rcv(i)%d_rcv(j)
    end do
  end do
  !.
  write (iam +600,'(10(:1X,F9.3))') Xg
  return
10 format ('###.Fuente : ', I6, ' Nro. datos: ',I6)
20 format ('###.Indices: ',/,10(:,1X,I6))
30 format ('###.Valores: ',/,10(:,1X,F12.6))
end subroutine sndrcv_xg
!  -----------------------------------------------------------------------------


end module mod_sndrcv_xg

