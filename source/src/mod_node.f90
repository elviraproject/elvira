!  -----------------------------------------------------------------------------
module mod_node
!  -----------------------------------------------------------------------------
  use mod_precision
  use mod_filename
  use mod_error
  use mod_busqueda
!  -----------------------------------------------------------------------------
  implicit none
!.
!  -------------------------- NODOS --------------------------------------------
!  Definicion de los campos del objeto NODO.
!  Donde:
!       n_nd  = Numero del nodo
!       tipo  = Tipo de nodo
!       p_pnd = Punteros a las propiedades del nodo
!       cxyz  = Coordenadas de los nodos
!  -----------------------------------------------------------------------------
  type, public :: t_nod
    integer(ip)           :: n_nd
!   integer(ip)           :: tipo
    integer(ip)           :: p_pnd
    real(rp), allocatable :: cxyz(:)
!    real(rp), pointer :: cxyz(:) => NULL()
  end type t_nod
!  -------------------------- Elementos a nodos --------------------------------
!  Definicion de los campos del objeto de todos los elementos que concurren a 
!  un nodo:
!  je_en = Vector de punteros a los elementos que concurren al nodo
  type, public :: t_ean 
    private
    integer(ip), allocatable :: je_en(:)
!    integer(ip), pointer :: je_en(:) => NULL()
  end type t_ean
!  -----------------------------------------------------------------------------
!  -------------------------- Caras a nodos ------------------------------------
!  Definicion de los campos del objeto de todos los segmentos que concurren a 
!  un nodo:
!  je_sn = Vector de punteros a las caras (faces) que concurren al nodo
  type, public ::  t_fan
    private
    integer(ip), allocatable  :: je_fn(:)
!    integer(ip), pointer  :: je_fn(:) => NULL()
  end type t_fan
!  -----------------------------------------------------------------------------
!  -------------------------- Segmentos a nodos --------------------------------
!  Definicion de los campos del objeto de todos los segmentos que concurren a 
!  un nodo:
!  je_sn = Vector de punteros a los segmentos que concurren al nodo
  type, public :: t_san
    private
    integer(ip), allocatable  :: je_sn(:)
!    integer(ip), pointer  :: je_sn(:) => NULL()
  end type t_san
!  -----------------------------------------------------------------------------
!  -------------------------- Nodos a nodos ------------------------------------
!  Definicion de los campos del objeto de todos los nodos que concurren a un 
!  nodo 
!  je_nn = Vector de punteros a los nodos que concurren al nodo segun FEM
!  id_nn = Vector de punteros a nodos que concurren a un nodo que estan en la 
!          frontera entre dos o mas dominios
  type, public :: t_nan
    private
    integer(ip), allocatable  :: je_nn(:)
!    integer(ip), pointer  :: je_nn(:) => NULL()
  !  integer(ip), pointer  :: id_nn(:)
  end type t_nan
!  -----------------------------------------------------------------------------
!  -------------------------- Nodos a subdominio -------------------------------
!  Definicion de los campos del objeto dominios a que pertenece un nodo 
  type, public :: t_nd_d
    private
    integer(ip), allocatable  :: nd_d(:)
!    integer(ip), pointer  :: nd_d(:) => NULL()
  end type t_nd_d
!  -----------------------------------------------------------------------------
  type, public :: t_obnd
    private
    integer(ip)           :: n_nod = 0
    type(t_nod),  allocatable :: ar_nd(:) !.Numero coordenadas y propiedades
    type(t_ean),  allocatable :: ar_en(:) !.Elementos que concurren al nodo 
    type(t_fan),  allocatable :: ar_fn(:) !.Caras (faces) que concurren al nodo 
    type(t_san),  allocatable :: ar_sn(:) !.Segmentos  que concurren al nodo 
    type(t_nan),  allocatable :: ar_nn(:) !.Nodos que concurren al nodo 
    type(t_nd_d), allocatable :: nd_dm(:) !.Dominos a los que pertenece el nodo 
!    type(t_nod),  pointer :: ar_nd(:)=>NULL() !.Numero coordenadas y propiedades
!    type(t_ean),  pointer :: ar_en(:)=>NULL() !.Elementos que concurren al nodo 
!    type(t_fan),  pointer :: ar_fn(:)=>NULL() !.Caras (faces) que concurren al nodo 
!    type(t_san),  pointer :: ar_sn(:)=>NULL() !.Segmentos  que concurren al nodo 
!    type(t_nan),  pointer :: ar_nn(:)=>NULL() !.Nodos que concurren al nodo 
!    type(t_nd_d), pointer :: nd_dm(:)=>NULL() !.Dominos a los que pertenece el nodo 
  end type t_obnd
!  -----------------------------------------------------------------------------
contains
! ------------------------------------------------------------------------------
subroutine lec_node (lu_m, arch_d, strout, node)
! ------------------------------------------------------------------------------
! Esta rutina lee las coordenadas de los nodos de la unidad lu_m
! ------------------------------------------------------------------------------
  implicit none
  integer(ip), intent(in)      :: lu_m
  character (len=*), intent(in):: arch_d,strout
  type(t_obnd)                 :: node
  !.
  integer(ip)                  :: io_err, i_err
  integer(ip)                  :: nnod, n_tn, ndim, i, j, k, lu_lec
  character (len=256)          :: nomb

  lu_lec = lu_m; call unitnumber (arch_d, strout, lu_lec, io_err)
  if (io_err > 0) call w_error (2,'subroutine lec_node',1)
  !.
  read (lu_lec, *,iostat=io_err) node%n_nod, n_tn, ndim
  if (io_err > 0) call w_error (3,'n_nod, n_tn, ndim',1)
  !
  allocate(node%ar_nd(node%n_nod), stat=i_err)
  if (i_err > 0 ) call w_error (4,'node%ar_nd(node%n_nod)',1)
  !.
  k = 0
  do i = 1, n_tn
    read ( lu_lec, *,iostat=io_err) nnod,nomb
    if (io_err > 0) call w_error (3,'nnod, nomb',1)
    !.call id_par_nod(lu_log,nomb,tipo=tipo)
    do j= 1, nnod
      k = k + 1
      allocate(node%ar_nd(k)%cxyz(ndim), stat=i_err)
      !.
      read (lu_lec, *,iostat=io_err) node%ar_nd(k)%n_nd, node%ar_nd(k)%p_pnd, &
                                     node%ar_nd(k)%cxyz(1:ndim)
      !.
      if (io_err > 0) call w_error (5,node%ar_nd(k-1)%n_nd,1)
    end do
  end do
  !.
  if (lu_lec /= lu_m) call close_unit (lu_lec)
  !. 
  write (std_o ,10) node%n_nod; write (lu_log,10) node%n_nod
  write (std_o, 20) node%ar_nd(1)%n_nd, node%ar_nd(1)%p_pnd, &
                    (node%ar_nd(1)%cxyz(i),i=1,ndim)
  write (std_o, 20) node%ar_nd(k)%n_nd, node%ar_nd(k)%p_pnd, &
                    (node%ar_nd(k)%cxyz(i),i=1,ndim)
  return
10 format ('###.Nodes,     read                               : ',I8)
20 format (2(I7,1X),3(E13.4))
end subroutine lec_node
! ------------------------------------------------------------------------------
function get_nnod(node) result(n_nod)
! ------------------------------------------------------------------------------
  implicit none
  type(t_obnd)                 :: node
  !.
  integer(ip)                  :: n_nod

  n_nod=node%n_nod
  return
end function get_nnod
! ------------------------------------------------------------------------------
subroutine allocate_ar_en (node)
! ------------------------------------------------------------------------------
  implicit none
  type(t_obnd)                 :: node
  !.
  integer(ip)                  :: i_err

  allocate(node%ar_en(node%n_nod), stat=i_err)
  if (i_err > 0 ) call w_error (4,'node%ar_en(node%n_nod)',1)
  return
end subroutine allocate_ar_en
! ------------------------------------------------------------------------------
subroutine allocate_ar_nn (node)
! ------------------------------------------------------------------------------
  implicit none
  type(t_obnd)                 :: node
 !.
  integer(ip)                  :: i_err


 allocate(node%ar_nn(node%n_nod), stat=i_err)
 if (i_err > 0 ) call w_error (4,'node%ar_nn(node%n_nod)',1)
 return
end subroutine allocate_ar_nn
! ------------------------------------------------------------------------------
subroutine allocate_ar_nn_je_nn (inod, n_nn, node)
! ------------------------------------------------------------------------------
! Esta rutina dimensiona los vectores  je_nn (nodos que concurren a nodos) de la
! estructura node.
! Donde:
!       inod: nodo en consideracion
!       n_nn: numero de nodos que concurren al nodo inod
! ------------------------------------------------------------------------------
  implicit none
  integer(ip), intent(in)      :: inod, n_nn
  type(t_obnd)                 :: node
  !.
  integer(ip)                  :: i_err


  allocate(node%ar_nn(inod)%je_nn(n_nn), stat=i_err)
  if (i_err > 0) call w_error (45, inod, 1)
  return
end subroutine allocate_ar_nn_je_nn
! ------------------------------------------------------------------------------
subroutine set_ar_nn_je_nn (inod, n_nn, n_st, node)
! ------------------------------------------------------------------------------
! Esta rutina asigna los valores de je_nn (nodos que concurren a nodos) de la
! estructura node.
! Donde:
!       inod: nodo en consideracion
!       n_nn: numero de nodos que concurren al nodo inod
!       n_st: vector que contiene los nodos que concurren a inod (segun fem)
! ------------------------------------------------------------------------------
  implicit none
  integer(ip), intent(in)   :: inod, n_nn, n_st(:)
  type(t_obnd)              :: node


  node%ar_nn(inod)%je_nn(1:n_nn)=n_st(1:n_nn)
  return
end subroutine set_ar_nn_je_nn
! ------------------------------------------------------------------------------
subroutine get_ar_nn_je_nn (node,inod, n_nn, n_st)
! ------------------------------------------------------------------------------
! Esta rutina asigna los valores de je_nn (nodos que concurren a nodos) de la
! estructura node.
! Donde:
!       inod: nodo en consideracion
!       n_nn: numero de nodos que concurren al nodo inod
!       n_st: vector que contiene los nodos que concurren a inod (segun fem)
! ------------------------------------------------------------------------------
  implicit none
  integer(ip), intent(in)   :: inod
  type(t_obnd)              :: node
  integer(ip), intent(out)  :: n_nn, n_st(:)

  n_nn         = size(node%ar_nn(inod)%je_nn) 
  n_st(1:n_nn) = node%ar_nn(inod)%je_nn
  return
end subroutine get_ar_nn_je_nn
! ------------------------------------------------------------------------------
function get_dim_ar_nn_je_nn (node) result (ne_je)
! ------------------------------------------------------------------------------
!
! ------------------------------------------------------------------------------
  implicit none
  type(t_obnd)               :: node
  integer(ip)                :: ne_je
  integer(ip)                :: i

  ne_je = 0
  do i=1,node%n_nod
    ne_je = ne_je + size(node%ar_nn(i)%je_nn)
  end do
  return
end function get_dim_ar_nn_je_nn
! ------------------------------------------------------------------------------
subroutine getdim_arnnjenn_mrow (node, n_nod, ne_je, mx_cl)
! ------------------------------------------------------------------------------
! Esta rutina me devuelve en numero de nodos, en numero de elementos distintos 
! de cero que tiene la matriz de rigidez local y el numero maximo de componente
! distintos de cero que tiene una fila.
! n_nod: numero de nodos o grados de libertad 
! ne_je: numero de elementos distintos que tiene la matriz de rigidez local
! mx_cl: maximo numero de componentes distintos de cero que tiene una fila
! ------------------------------------------------------------------------------
  implicit none
  type(t_obnd)               :: node
  integer(ip),intent(out)    :: n_nod, ne_je, mx_cl
  integer(ip)                :: i, nc


  n_nod = node%n_nod
  ne_je = 0; mx_cl = 0
  do i=1,n_nod
    nc = size(node%ar_nn(i)%je_nn) + 1
    ne_je = ne_je + nc
    if (nc > mx_cl ) mx_cl = nc
  end do
  return
end subroutine getdim_arnnjenn_mrow
! ------------------------------------------------------------------------------
subroutine get_nnozeros_rowi (inod, node, idom, nnz, dof_g, col_l, col_g)
! ------------------------------------------------------------------------------
!
! ------------------------------------------------------------------------------
  implicit none
  integer(ip), intent(in)    :: inod
  type(t_obnd)               :: node
  integer(ip), intent(out)   :: idom, nnz, dof_g, col_l(:), col_g(:)
  !.
  integer(ip)                :: i
  
  dof_g = node%ar_nd(inod)%n_nd                !.Nro. global del nodo
  idom  = node%nd_dm(inod)%nd_d(1)             !.Dom. al que pertenece la ecuacion 
  call get_ar_nn_je_nn (node,inod, nnz, col_l) !.
  nnz = nnz + 1; col_l(nnz) = inod
  call sort (nnz,col_l(1:nnz))
  !.
  do i=1,nnz
    col_g(i) = node%ar_nd(col_l(i))%n_nd
  end do
  return
end subroutine get_nnozeros_rowi
! ------------------------------------------------------------------------------
subroutine allocate_ar_en_je_en(inod,nel_ndi,node) 
! ------------------------------------------------------------------------------
! Esta rutina dimensiona los vectores  je_en (elementos que concurren a nodos)
! de la estructura node.
! ------------------------------------------------------------------------------
  implicit none
  integer(ip), intent(in)      :: inod, nel_ndi
  type(t_obnd)                 :: node
  !.
  integer(ip)                  :: i_err

  allocate(node%ar_en(inod)%je_en(nel_ndi), stat=i_err)
  if (i_err > 0) call w_error (44, inod, 1)
  return
end subroutine allocate_ar_en_je_en
! ------------------------------------------------------------------------------
subroutine  set_elm_in_jeen (iel,inod,ipos,node)
! ------------------------------------------------------------------------------
! 
! ------------------------------------------------------------------------------
  implicit none
  integer(ip), intent(in)      :: iel, inod, ipos
  type(t_obnd)                 :: node
  !.

  node%ar_en(inod)%je_en(ipos)=iel
  return
end subroutine set_elm_in_jeen

! ------------------------------------------------------------------------------
subroutine get_jeen(node, inod, n_en, conc)
! ------------------------------------------------------------------------------
! 
! ------------------------------------------------------------------------------
  implicit none
  integer(ip), intent(in)      :: inod
  type(t_obnd)                 :: node
  integer(ip), intent(out)     :: n_en, conc(:)
  !.
  
  n_en         = size(node%ar_en(inod)%je_en)
  conc(1:n_en) = node%ar_en(inod)%je_en
  return
end subroutine get_jeen
! ------------------------------------------------------------------------------
function get_dimje(node) result (n_je)
! ------------------------------------------------------------------------------
! 
! ------------------------------------------------------------------------------
  implicit none
  type(t_obnd)          :: node
  integer(ip)           :: n_je
  integer(ip)           :: i

  n_je = 0
  do i = 1, node%n_nod
    n_je = n_je + size(node%ar_en(i)%je_en) 
  end do
  return
end function get_dimje
! ------------------------------------------------------------------------------
subroutine get_nodecoord (inod,idim,coord,node)
! ------------------------------------------------------------------------------
! 
! ------------------------------------------------------------------------------
  use mpi_mod;
  implicit none
  integer(ip), intent(in)  :: inod
  integer(ip), intent(out) :: idim
  real(rp),    intent(out) :: coord(:)
  type(t_obnd)             :: node
  

  idim          = size(node%ar_nd(inod)%cxyz)
  coord(1:idim) = node%ar_nd(inod)%cxyz
  return
end subroutine get_nodecoord
! ------------------------------------------------------------------------------
subroutine get_nodecoord_abaqus (inod, n_nd, idim, coord, node)
! ------------------------------------------------------------------------------
! 
! ------------------------------------------------------------------------------
  implicit none
  integer(ip), intent(in)  :: inod
  integer(ip), intent(out) :: n_nd,idim
  real(rp),    intent(out) :: coord(:)
  type(t_obnd)             :: node
  
  n_nd          = node%ar_nd(inod)%n_nd
  idim          = size(node%ar_nd(inod)%cxyz)
  coord(1:idim) = node%ar_nd(inod)%cxyz
  return
end subroutine get_nodecoord_abaqus
! ------------------------------------------------------------------------------
subroutine allocate_nod_d ( n, node)
! ------------------------------------------------------------------------------
!.Dimensionado de Dominos a los que pertenece el nodo 
  implicit none
  integer(ip), intent(in)  :: n
  type(t_obnd)             :: node
  !.
  integer(ip)              :: i_err

  allocate(node%nd_dm(n), stat=i_err)
  if (i_err > 0) call w_error (4,'node%nd_dm(node%nnod)', 1)
  return
end subroutine allocate_nod_d
! ------------------------------------------------------------------------------
subroutine set_node_subdomain (inod, c_d, stg_d, node)
! ------------------------------------------------------------------------------
  implicit none
  integer(ip), intent(in)    :: inod
  integer(ip), intent(in)    :: c_d, stg_d(:)
  type(t_obnd)               :: node
  !.
  integer(ip)                :: i_err

  
  allocate(node%nd_dm(inod)%nd_d(c_d), stat=i_err)
  if (i_err > 0) call w_error (46,inod,1)
  node%nd_dm(inod)%nd_d = stg_d(1:c_d)
  return
end subroutine set_node_subdomain
! ------------------------------------------------------------------------------
subroutine get_node_subdomain(inod, n_d, dom, node)
! ------------------------------------------------------------------------------
  implicit none
  integer(ip), intent(in)   :: inod
  type(t_obnd)              :: node
  integer(ip), intent(out)  :: n_d, dom(:)

  n_d        = size(node%nd_dm(inod)%nd_d)
  dom(1:n_d) = node%nd_dm(inod)%nd_d
  return
end subroutine get_node_subdomain
! ------------------------------------------------------------------------------
subroutine fixing_node_dom(n, dm_e, n_pe, conc, node)
! ------------------------------------------------------------------------------
  implicit none
  integer(ip), intent(in)   :: n, dm_e, n_pe, conc(:)
  type(t_obnd)              :: node
  !.
  integer(ip), allocatable  :: dom(:)
  integer(ip)               :: i, j, inod, n_d, i_err

  allocate(dom(n), stat=i_err)
  if (i_err > 0) call w_error (69,n,1)
  do i=1,n_pe
    inod       = conc(i)
    n_d        = size(node%nd_dm(inod)%nd_d)
    dom(1:n_d) = node%nd_dm(inod)%nd_d(1:n_d)
    if (any(dom(1:n_d) == dm_e) ) then
      write (lu_log,10) inod, n_d, node%nd_dm(inod)%nd_d(1:n_d)
      !.
      do j = 1,n_d
        if (dom(j) == dm_e) then
          call i_swap (node%nd_dm(inod)%nd_d(1), node%nd_dm(inod)%nd_d(j))
          exit
        end if
      end do
      write (lu_log,10) inod, n_d, node%nd_dm(inod)%nd_d(1:n_d)
    end if
  end do
  return
10 format ('###.Node: ',I8,2X,I8,' domains: ',10(2X,I8))
end subroutine fixing_node_dom
! ------------------------------------------------------------------------------
function get_node_prop( inod, node) result (p_prop)
! ------------------------------------------------------------------------------
  implicit none
  integer(ip), intent(in) :: inod
  type(t_obnd)            :: node
  !.
  integer(ip)             :: p_prop

  p_prop = node%ar_nd(inod)%p_pnd
  return
end function get_node_prop
! ------------------------------------------------------------------------------
function get_nd_num_g (i,node) result (g_nn)
! ------------------------------------------------------------------------------
  implicit none
  integer(ip), intent(in) :: i
  type(t_obnd)            :: node
  integer(ip)             :: g_nn
 

  g_nn  = node%ar_nd(i)%n_nd
  return
end function get_nd_num_g

! ------------------------------------------------------------------------------
subroutine get_ar_nd (j, node, n_nd, p_pnd, cxyz)
! ------------------------------------------------------------------------------
  implicit none
  integer(ip), intent(in) :: j
  type(t_obnd)            :: node
  !.
  integer(ip), intent(out):: n_nd, p_pnd
  real(rp)                :: cxyz(:)
  integer(ip)             :: idim

  n_nd  = node%ar_nd(j)%n_nd
  p_pnd = node%ar_nd(j)%p_pnd
  idim  = size(node%ar_nd(j)%cxyz)
  cxyz(1:idim) = node%ar_nd(j)%cxyz
  return
end subroutine get_ar_nd
! ------------------------------------------------------------------------------
function get_node_dim(node) result(idim)
! ------------------------------------------------------------------------------
  implicit none
  type(t_obnd)            :: node
  !.
  integer(ip)             :: idim

  idim  = size(node%ar_nd(1)%cxyz)
  return
end function get_node_dim
! ------------------------------------------------------------------------------
function get_max_ar_nn (node) result( m_nnn )
! ------------------------------------------------------------------------------
  implicit none
  type(t_obnd)            :: node
  !.
  integer(ip)             :: m_nnn 
  integer(ip)             :: i, m_aux

  m_nnn =0
  do i=1,node%n_nod
    m_aux = size(node%ar_nn(i)%je_nn)
    if (m_aux > m_nnn) m_nnn= m_aux
  end do
  return
end function get_max_ar_nn
! ------------------------------------------------------------------------------
function get_nd_prop (idom, node) result(n_prop)
! ------------------------------------------------------------------------------
!. Esta funcion determina la cantidad de nodos propios que tiene el dominio. 
!  Esto es cuantos nodos tiene que calcular el procesador correspondiente
! ------------------------------------------------------------------------------
  implicit none
  integer(ip), intent(in)   :: idom
  type(t_obnd), intent(in)  :: node
  !.
  integer(ip)               :: i, n_prop

  n_prop = 0
  do i = 1, node%n_nod
    if (node%nd_dm(i)%nd_d(1) == idom) n_prop  = n_prop + 1
  end do
  return
end function get_nd_prop
! ------------------------------------------------------------------------------
subroutine get_nodes_dom_prop (idom, node, n_nd, n_pd, ln_d, lp_d)
! ------------------------------------------------------------------------------
! idom : numero del subdominio
! node : objeto nodo
! n_nd : numero de nodos en el subdominio
! n_pd : numero de propiedades en el dominio
! ln_d : lista de nodos en el subdominio
! lp_d : lista de numeros de propiedades nodales en el dominio  
! ------------------------------------------------------------------------------
  implicit none
  integer(ip), intent(in)   :: idom
  type(t_obnd), intent(in)  :: node
  integer(ip), intent(out)  :: n_nd, n_pd, ln_d(:), lp_d(:)
  !.
  integer(ip)               :: i, i_pn

  n_nd = 0; ln_d = 0; n_pd = 0; lp_d = 0
  !.
  do i = 1, node%n_nod
    if ( any( node%nd_dm(i)%nd_d == idom ) ) then
      n_nd  = n_nd+1; ln_d(i) = n_nd; 
      !.
      i_pn  = node%ar_nd(i)%p_pnd          !.Propiedad del nodo
      if (lp_d(i_pn) == 0) then
        n_pd = n_pd +1; lp_d (i_pn) = n_pd
      end if
    end if
  end do
  return
end subroutine get_nodes_dom_prop
! ------------------------------------------------------------------------------
subroutine bin_write_nodes (lu_m, nn_d, ln_d, lp_nd, node)
! ------------------------------------------------------------------------------
! lu_m : numero de unidad
! nn_d : numero de nodos en el subdominio
! ln_d : lista de nodos en el subdominio
! lp_nd: lista de numeros de propiedades nodales en el dominio  
! node : objeto nodo
! ------------------------------------------------------------------------------
  implicit none
  integer(ip), parameter    :: max_nn = 100
  integer(ip), intent(in)   :: lu_m, nn_d, ln_d(:), lp_nd(:)
  type(t_obnd), intent(in)  :: node
  !.
  integer(ip)               :: laux(max_nn),l_nn(max_nn) !.lista de nodos que concurren a nodo
  integer(ip)               :: ndim, n_nod, i, j, p_pnd, inod, c_nn, n_nn, n_d

  ndim = size(node%ar_nd(1)%cxyz)
  write (lu_m) '#NODES',nn_d, ndim
  n_nod = get_nnod (node) !.Numero de nodos
  !.Escritura de las coordenadas nodales y propiedades nodales de los nodos
  do j = 1,n_nod
    if (ln_d(j) /= 0) then
      p_pnd = node%ar_nd(j)%p_pnd
      !.
      write (lu_m) node%ar_nd(j)%n_nd, lp_nd(p_pnd), node%ar_nd(j)%cxyz(1:ndim)
    end if
  end do
  !.
  !.Escritura de la estructura de los nodos que concurren a nodos
  do j = 1,n_nod
    if (ln_d(j) /= 0) then
      call get_ar_nn_je_nn (node,j, n_nn, laux)
      !.
      c_nn = 0 !.contador
      do i=1,n_nn
        inod= laux(i)
        if (ln_d(inod) /=0) then
          c_nn = c_nn + 1
          l_nn(c_nn) = ln_d(inod)
        end if
      end do
      write(lu_m) c_nn
      write(lu_m) l_nn(1:c_nn)
    end if
  end do
  !.Escritura de la estructura de los subdominios que comparte el nodo
!  write(lu_m) n_nod
!  write(*,*) 'bin_write_nodes: ', n_nod
  do j = 1,n_nod
    if (ln_d(j) /= 0) then
      call get_node_subdomain(j, n_d, laux, node)
      write (lu_m) n_d, laux(1:n_d)
    end if
  end do
  return
end subroutine bin_write_nodes
! ------------------------------------------------------------------------------
subroutine ascii_write_nodes (lu_m, nn_d, ln_d, lp_nd, node)
! ------------------------------------------------------------------------------
! lu_m : numero de unidad
! nn_d : numero de nodos en el subdominio
! ln_d : lista de nodos en el subdominio
! lp_nd: lista de numeros de propiedades nodales en el dominio  
! node : objeto nodo
! ------------------------------------------------------------------------------
  implicit none
  integer(ip), parameter    :: max_nn = 100
  integer(ip), intent(in)   :: lu_m, nn_d, ln_d(:), lp_nd(:)
  type(t_obnd), intent(in)  :: node
  !.
  integer(ip)               :: laux(max_nn),l_nn(max_nn) !.lista de nodos que concurren a nodo
  integer(ip)               :: ndim, n_nod, i, j, p_pnd, inod, c_nn, n_nn, n_d

  ndim = size(node%ar_nd(1)%cxyz)
  write (lu_m,10) '#NODES',nn_d, ndim
  n_nod = get_nnod (node) !.Numero de nodos
  !.Escritura de las coordenadas nodales y propiedades nodales de los nodos
  do j = 1,n_nod
    if (ln_d(j) /= 0) then
      p_pnd = node%ar_nd(j)%p_pnd
      !.
      write (lu_m,20) node%ar_nd(j)%n_nd, lp_nd(p_pnd), node%ar_nd(j)%cxyz(1:ndim)
    end if
  end do
  !.
  !.Escritura de la estructura de los nodos que concurren a nodos
  write (lu_m,'(A)') '****NODOS QUE CONCURREN A NODOS'
  do j = 1,n_nod
    if (ln_d(j) /= 0) then
      call get_ar_nn_je_nn (node,j, n_nn, laux)
      !.
      c_nn = 0 !.contador
      do i=1,n_nn
        inod= laux(i)
        if (ln_d(inod) /=0) then
          c_nn = c_nn + 1
          l_nn(c_nn) = ln_d(inod)
        end if
      end do
      write(lu_m,30) c_nn
      write(lu_m,30) l_nn(1:c_nn)
    end if
  end do
  write (lu_m,'(A)') '****SUBDOMINIOS QUE COMPARTE EL NODO'
  !.Escritura de la estructura de los subdominios que comparte el nodo
  do j = 1,n_nod
    if (ln_d(j) /= 0) then
      call get_node_subdomain(j, n_d, laux, node)
      write (lu_m, 30) n_d, laux(1:n_d)
    end if
  end do
  return
10 format (A,2X,I8,2X,I3)
20 format (I8,2X,I3,3(2X,F12.6))
30 format (100(:,2X,I8)) 
end subroutine ascii_write_nodes
! ------------------------------------------------------------------------------
subroutine bin_read_nodes (lu_m, node)
! ------------------------------------------------------------------------------
! lu_m : numero de unidad
! nn_d : numero de nodos en el subdominio
! ln_d : lista de nodos en el subdominio
! lp_nd: lista de numeros de propiedades nodales en el dominio  
! node : objeto nodo
! ------------------------------------------------------------------------------
  implicit none
  integer(ip), parameter    :: max = 100
  !.
  integer(ip), intent(in)   :: lu_m
  type(t_obnd)              :: node
  !.
  integer(ip)               :: laux(max)

  character (len=6)         :: str
  integer(ip)               :: ndim, j, dim_nn, n_d
  integer(ip)               :: io_err, i_err


  read (lu_m,iostat=io_err) str, node%n_nod, ndim
 ! write (*,*)'str, node%n_nod, ndim', str, node%n_nod, ndim
  if (io_err > 0) call w_error (3,'str, node%n_nod, ndim',1)
  !
  allocate(node%ar_nd(node%n_nod), stat=i_err)
  if (i_err > 0 ) call w_error (4,'node%ar_nd(node%n_nod)',1)
  !.
  do j= 1, node%n_nod
    allocate(node%ar_nd(j)%cxyz(ndim), stat=i_err)
    if (i_err > 0 ) call w_error (4,'node%ar_nd(j)%cxyz(ndim)',1)
  end do
  !.
  do j= 1, node%n_nod
    !.
    read (lu_m, iostat=io_err) node%ar_nd(j)%n_nd, node%ar_nd(j)%p_pnd, &
                               node%ar_nd(j)%cxyz(1:ndim)
    !.
    if (io_err > 0) call w_error (5,node%ar_nd(j-1)%n_nd,1)
  end do
  !.
  !.Lectura de la estructura de los nodos que concurren a nodos
  call allocate_ar_nn (node)
  do j = 1,node%n_nod
    read (lu_m, iostat=io_err) dim_nn
    read (lu_m, iostat=io_err) laux(1:dim_nn)
    if (io_err > 0) call w_error (3,'dim_nn, laux(1:dim_nn)',1)
    !.
    call allocate_ar_nn_je_nn (j, dim_nn, node)
    call set_ar_nn_je_nn (j, dim_nn, laux(1:dim_nn), node)
  end do
  !.
  !.Lectura de los subdominios del nodo
  call allocate_nod_d (node%n_nod, node)
  do j = 1,node%n_nod
    read (lu_m, iostat=io_err) n_d, laux(1:n_d) !.Numero de dominios al que pertenece el nodo
    if (io_err > 0) call w_error (3,'n_d, laux(1:n_d)',1)
    call set_node_subdomain (j, n_d, laux(1:n_d), node)
  end do
  return
end subroutine bin_read_nodes
! ------------------------------------------------------------------------------
function deallocate_nodes (node) result(flg) 
! ------------------------------------------------------------------------------
  implicit none
  type(t_obnd)              :: node
  integer(ip)               :: flg
  !.
  integer(ip)               :: flg_n(6)
  
  
  flg_n = 0
  flg = 0
  !.Numero coordenadas y propiedades
  if (allocated(node%ar_nd)) flg_n(1) = deallocate_ar_nd(node)
!  if (associated(node%ar_nd)) flg_n(1) = deallocate_ar_nd(node)
  !.Elementos que concurren al nodo
  if (allocated(node%ar_en)) flg_n(2) = deallocate_ar_en(node)
!  if (associated(node%ar_en)) flg_n(2) = deallocate_ar_en(node)
  !.Caras (faces) que concurren al nodo 
  if (allocated(node%ar_fn)) flg_n(3) = deallocate_ar_fn(node)
!  if (associated(node%ar_fn)) flg_n(3) = deallocate_ar_fn(node)
  !.Segmentos  que concurren al nodo 
  if (allocated(node%ar_sn)) flg_n(4) = deallocate_ar_sn(node)
!  if (associated(node%ar_sn)) flg_n(4) = deallocate_ar_sn(node)
  !.Nodos que concurren al nodo 
  if (allocated(node%ar_nn)) flg_n(5) = deallocate_ar_nn(node)
!  if (associated(node%ar_nn)) flg_n(5) = deallocate_ar_nn(node)
  !.Dominos a los que pertenece el nodo 
  if (allocated(node%nd_dm)) flg_n(6) = deallocate_ar_dm(node)
!  if (associated(node%nd_dm)) flg_n(6) = deallocate_ar_dm(node)
  !.
  if (any(flg_n == 1)) flg = 1
  !.
  return
end function deallocate_nodes
! ------------------------------------------------------------------------------
function deallocate_ar_nd(node) result(flg)
! ------------------------------------------------------------------------------
  implicit none
  type(t_obnd)              :: node
  integer(ip)               :: flg
  !.
  integer(ip)               ::  i, istat 
  
  flg = 0
  do i=1,node%n_nod
    deallocate (node%ar_nd(i)%cxyz, stat=istat);
!    deallocate (node%ar_nd(i)%cxyz, stat=istat); nullify(node%ar_nd(i)%cxyz)
    if (istat /= 0) flg = 1
  end do
  deallocate (node%ar_nd, stat=istat);
!  deallocate (node%ar_nd, stat=istat); nullify(node%ar_nd)
  if (istat /= 0) flg = 1
  if (flg == 1) write (*,10) 
  return
10 format ('###.Error freeing memory in deallocate_ar_nd (mod_node.f90)')
end function deallocate_ar_nd
! ------------------------------------------------------------------------------
function deallocate_ar_en(node) result(flg)
! ------------------------------------------------------------------------------
  implicit none
  type(t_obnd)              :: node
  integer(ip)               :: flg
  !.
  integer(ip)               :: i, istat 
  
  flg = 0
  do i=1,node%n_nod
    deallocate (node%ar_en(i)%je_en, stat=istat);
!    deallocate (node%ar_en(i)%je_en, stat=istat); nullify(node%ar_en(i)%je_en)
    if (istat /= 0) flg = 1
  end do
  deallocate (node%ar_en, stat=istat);
!  deallocate (node%ar_en, stat=istat); nullify(node%ar_en)
  if (istat /= 0) flg = 1
  if (flg == 1) write (*,10) 
  return
10 format ('###.Error freeing memory in deallocate_ar_en (mod_node.f90)')
end function deallocate_ar_en
! ------------------------------------------------------------------------------
function deallocate_ar_fn(node) result(flg)
! ------------------------------------------------------------------------------
  implicit none
  type(t_obnd)              :: node
  integer(ip)               :: flg
  !.
  integer(ip)               :: istat 
  
  flg = 0
  deallocate (node%ar_fn, stat=istat);
!  deallocate (node%ar_fn, stat=istat); nullify(node%ar_fn)
  if (istat /= 0) flg = 1
  if (flg == 1) write (*,10) 
  return
10 format ('###.Error freeing memory in deallocate_ar_fn (mod_node.f90)')
end function deallocate_ar_fn
! ------------------------------------------------------------------------------
function deallocate_ar_sn(node) result(flg)
! ------------------------------------------------------------------------------
  implicit none
  type(t_obnd)              :: node
  integer(ip)               :: flg
  !.
  integer(ip)               :: istat 
  
  flg = 0
  deallocate (node%ar_sn, stat=istat);
!  deallocate (node%ar_sn, stat=istat); nullify(node%ar_sn)
  if (istat /= 0) flg = 1
  if (flg == 1) write (*,10) 
  return
10 format ('###.Error freeing memory in deallocate_ar_sn (mod_node.f90)')
end function deallocate_ar_sn
! ------------------------------------------------------------------------------
function deallocate_ar_nn(node) result(flg)
! ------------------------------------------------------------------------------
  implicit none
  type(t_obnd)              :: node
  integer(ip)               :: flg
  !.
  integer(ip)               ::  i, istat 
  
  flg = 0
  do i=1,node%n_nod
    deallocate (node%ar_nn(i)%je_nn, stat=istat);
!    deallocate (node%ar_nn(i)%je_nn, stat=istat); nullify(node%ar_nn(i)%je_nn)
    if (istat /= 0) flg = 1
  end do
  deallocate (node%ar_nn, stat=istat);
!  deallocate (node%ar_nn, stat=istat); nullify(node%ar_nn)
  if (istat /= 0) flg = 1
  if (flg == 1) write (*,10) 
  return
10 format ('###.Error freeing memory in deallocate_ar_nn (mod_node.f90)')
end function deallocate_ar_nn
! ------------------------------------------------------------------------------
function deallocate_ar_dm(node) result(flg)
! ------------------------------------------------------------------------------
  implicit none
  type(t_obnd)              :: node
  integer(ip)               :: flg
  !.
  integer(ip)               ::  i, istat 
  
  flg = 0
  do i=1,node%n_nod
    deallocate (node%nd_dm(i)%nd_d, stat=istat);
!    deallocate (node%nd_dm(i)%nd_d, stat=istat); nullify(node%nd_dm(i)%nd_d)
    if (istat /= 0) flg = 1
  end do
  deallocate (node%nd_dm, stat=istat);
!  deallocate (node%nd_dm, stat=istat); nullify(node%nd_dm)
  if (istat /= 0) flg = 1
  if (flg == 1) write (*,10) 
  return
10 format ('###.Error freeing memory in deallocate_ar_dm (mod_node.f90)')
end function deallocate_ar_dm
! ------------------------------------------------------------------------------
function get_global_dof (inod, node) result (g_dof)
! ------------------------------------------------------------------------------
  implicit none
  integer(ip), intent(in)   :: inod
  type(t_obnd)              :: node
  integer(ip)               :: g_dof

  g_dof  = node%ar_nd(inod)%n_nd
  return
end function get_global_dof

!!$! ------------------------------------------------------------------------------
!!$subroutine get_dofelem (npe, conc, node, gl)
!!$! ------------------------------------------------------------------------------
!!$  implicit none
!!$  integer(ip),  intent(in)  :: npe, conc(:)
!!$  type(t_obnd), intent(in)  :: node
!!$  integer(ip)               :: gl(:)
!!$
!!$  do i = 1, npe
!!$    inod  = conc(i)
!!$    gl(i) = node%ar_nd(inod)%n_nd
!!$  end do
!!$
!!$  return
!!$end subroutine get_dofelem

! ------------------------------------------------------------------------------
end module mod_node
! ------------------------------------------------------------------------------
