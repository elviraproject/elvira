!  -----------------------------------------------------------------------------
module mod_outputset
!  -----------------------------------------------------------------------------
  use mod_precision
  use mod_filename
  use mod_error 
!  -----------------------------------------------------------------------------
  implicit none
  !.
  type, public :: t_output
    private
    integer(ip)              :: n_var = 0   !.Numero variables a postprocesar
    integer(ip), allocatable :: var_o(:)
    integer(ip), allocatable :: frc_o(:)
!    integer(ip), pointer     :: var_o(:)
!    integer(ip), pointer     :: frc_o(:)

  end type t_output

  contains
!  -----------------------------------------------------------------------------
subroutine lec_outputset (lu_m, output)
!  -----------------------------------------------------------------------------
  implicit none
   integer(ip), intent(in)  :: lu_m
  type(t_output)            :: output
  !.
  integer(ip)               :: n_out, io_err, i_err, i

  read (lu_m, *, iostat=io_err) n_out
  if (io_err > 0) call w_error (3,'lec_outputset',1)
  if (n_out > 0) then
    output%n_var = n_out; 
    allocate(output%var_o(n_out), output%frc_o(n_out), stat=i_err)
    if (i_err > 0 ) call w_error (4,'(output%lv_out(n_out)',1)
    !.
    do i=1,n_out
      read (lu_m, *, iostat=io_err)  output%var_o(i), output%frc_o(i)
    end do
    !.
  end if
  write (std_o,10) n_out
  if (n_out > 0) then
    write(std_o,20) output%frc_o(1)
  else
    write(std_o,*) 'No global output will be generated'
  end if
 10 format('### Global output read                            :',I6)
 20 format('  Global output being generated. Freq: ',I6)
  !.
  return
end subroutine lec_outputset
!  -----------------------------------------------------------------------------
subroutine get_output_var ( ipst, output, flg_v, frc_v)
!  -----------------------------------------------------------------------------
  implicit none
  integer(ip),          intent(in)  :: ipst !.Variable a postprocesar
  type(t_output),       intent(in)  :: output
  integer(ip),optional, intent(out) :: flg_v, frc_v
  integer(ip)                       :: i
  
  !.
  
  flg_v = 0; frc_v = 0
  if ( output%n_var > 0 ) then
    do i=1,output%n_var
      if (output%var_o(i) == ipst) then
        flg_v = 1
        frc_v = output%frc_o(i)
        exit
      end if
    end do
  end if
  !.
  return
end subroutine get_output_var
!  -----------------------------------------------------------------------------
subroutine bin_write_outputset(lu_m, output)
!  -----------------------------------------------------------------------------
  implicit none
  integer(ip),    intent(in)  :: lu_m
  type(t_output), intent(in)  :: output
  !.
  integer(ip)                 :: i

  write (lu_m) '*G_OUTPUT'
  !.
  write (lu_m) output%n_var
  !.
  if (output%n_var > 0) then
    do i=1,output%n_var
      write (lu_m) output%var_o(i), output%frc_o(i)
    end do
  end if
  !.
  return
end subroutine bin_write_outputset
!  -----------------------------------------------------------------------------
subroutine ascii_write_outputset(lu_m, output)
!  -----------------------------------------------------------------------------
  implicit none
  integer(ip),    intent(in)  :: lu_m
  type(t_output), intent(in)  :: output
  !.
  integer(ip)                 :: i

  write (lu_m,'(A)') '*G_OUTPUT'
  !.
  write (lu_m,'(I8)') output%n_var
  !.
  if ( output%n_var > 0) then
    do i=1,output%n_var
      write (lu_m, 10) output%var_o(i), output%frc_o(i)
    end do
  end if
  !.
  return
10 format(100(:,1X,I8)) 
end subroutine ascii_write_outputset
!  -----------------------------------------------------------------------------
subroutine bin_read_outputset(lu_m, output)
!  -----------------------------------------------------------------------------
  implicit none
  integer(ip), intent(in)  :: lu_m
  type(t_output)           :: output
  !.
  character (len=9)        :: str
  integer(ip)              :: n_out, io_err, i_err, i

  read (lu_m,iostat=io_err) str
  if (io_err > 0) call w_error (3,'bin_read_outputset',1)
  read (lu_m,iostat=io_err)  n_out
  if (io_err > 0) call w_error (3,'bin_read_outputset',1)
  !.
  if (n_out > 0) then
    output%n_var = n_out; 
    allocate(output%var_o(n_out), output%frc_o(n_out), stat=i_err)
    if (i_err > 0 ) call w_error (4,'(output%lv_out(n_out)',1)
    !.
    do i=1,n_out
      read (lu_m, iostat=io_err)  output%var_o(i), output%frc_o(i)
    end do
    !.
  end if
  !.
  return
end subroutine bin_read_outputset
!  -----------------------------------------------------------------------------
function deallocate_outputset(output) result(flg)
!  -----------------------------------------------------------------------------
  implicit none
  type(t_output)           :: output
  integer(ip)              :: flg
  !.
  integer(ip)              :: istat
  
  flg = 0

  if (output%n_var > 0) then
    deallocate (output%var_o, output%frc_o, stat=istat)
    if (istat /= 0) flg = 1
  else
    flg = -1
  end if
  !.
  return
end function deallocate_outputset
!  -----------------------------------------------------------------------------
end module mod_outputset

