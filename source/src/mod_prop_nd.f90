!  -----------------------------------------------------------------------------
module mod_prop_nd
!  -----------------------------------------------------------------------------
  use mod_precision
  use mod_filename
  use mod_error
  use mod_busqueda
!  -----------------------------------------------------------------------------
  implicit none
!  ---------------------------- NODAL PROPERTIES -------------------------------
!  -----------------------------------------------------------------------------
  type t_prnd
     private
     integer(ip)              :: n_pnd    !  Property number
     integer(ip)              :: p_mat    !  Pointer to material
     integer(ip), allocatable :: dat_i(:) !  Integer properties assoc. to prop
     real(rp), allocatable    :: dat_r(:) !  Real properties assoc. to prop
  end type t_prnd
!  -----------------------------------------------------------------------------
  type, public:: t_obpnd
    private
    integer(ip)               :: n_prp = 0 ! Number of defined nodal properties 
    type(t_prnd), allocatable :: pr_nd(:)
  end type t_obpnd
!  -----------------------------------------------------------------------------    
contains
! ------------------------------------------------------------------------------
! ------------------------------------------------------------------------------
subroutine lec_pnod (lu_m, arch_d, strout, propnod)
! ------------------------------------------------------------------------------
! This routine reads nodal properties
! ------------------------------------------------------------------------------
  implicit none
  integer(ip), parameter       :: max=100
  integer(ip), intent(in)      :: lu_m
  character (len=*), intent(in):: arch_d,strout
  type (t_obpnd)               :: propnod
  !.
  integer(ip)                  :: lu_lec, io_err, i_err
  integer(ip)                  :: i, nprp, n_int, n_rea ,dat_i(max)
  real(rp)                     :: dat_r(max)

 
  lu_lec = lu_m; call unitnumber (arch_d, strout, lu_lec, io_err)
  if (io_err > 0) call w_error (32,'subroutine lec_pnod',1)
  !.
  read (lu_lec,*,iostat=io_err) propnod%n_prp
  if (io_err > 0) call w_error (33,'propnod%n_prp',1)
  !.
  allocate (propnod%pr_nd(propnod%n_prp), stat=i_err)
  if (i_err > 0 ) call w_error (34,'propnod%pr_nd(propnod%n_prp)',1)
  !.
  do i = 1, propnod%n_prp
    read (lu_lec, *, iostat = io_err)  propnod%pr_nd(i)%n_pnd,                  &
                                       propnod%pr_nd(i)%p_mat, n_int, n_rea,    &
                                       dat_i(1:n_int), dat_r(1:n_rea)
    if (io_err > 0) call w_error (35,'n_pnd, p_mat, n_int, n_rea',1)
    !.
    allocate (propnod%pr_nd(i)%dat_i(n_int),stat=i_err) 
    if (i_err > 0 ) call w_error (34,'propnod%pr_nd(i)%dat_i(n_int)',1)
    if (n_int > 0) then
      propnod%pr_nd(i)%dat_i(1:n_int) = dat_i(1:n_int)
    end if
    !.
    allocate (propnod%pr_nd(i)%dat_r(n_rea),stat=i_err) 
    if (i_err > 0 ) call w_error (34,'propnod%pr_nd(i)%dat_r(n_rea)',1)
    if (n_rea > 0) then
      propnod%pr_nd(i)%dat_r(1:n_rea) = dat_r(1:n_rea)
    end if
    if (io_err > 0) call w_error (35,'propnod%pr_nd(i)%()',1)
  end do
  !.
  if (lu_lec /= lu_m) call close_unit (lu_lec)
  !.
  write(std_o, 10) propnod%n_prp; write(lu_log,10) propnod%n_prp
  nprp=propnod%n_prp
  n_int=size(propnod%pr_nd(1)%dat_i)
  n_rea=size(propnod%pr_nd(1)%dat_r)
  if(n_rea > 0) then
     write (std_o, 20)  propnod%pr_nd(1)%n_pnd,          &
                propnod%pr_nd(1)%p_mat, n_int, n_rea,    &
                (propnod%pr_nd(1)%dat_i(i),i=1,n_int)
     write (std_o, 30) (propnod%pr_nd(1)%dat_r(i),i=1,n_rea) 
  else
     write (std_o, 21)  propnod%pr_nd(1)%n_pnd,          &
                propnod%pr_nd(1)%p_mat, n_int, n_rea,    &
                (propnod%pr_nd(1)%dat_i(i),i=1,n_int)
  end if
  n_int=size(propnod%pr_nd(nprp)%dat_i)
  n_rea=size(propnod%pr_nd(nprp)%dat_r)
  if(n_rea > 0) then
    write (std_o, 20)  propnod%pr_nd(nprp)%n_pnd,           &
                propnod%pr_nd(nprp)%p_mat, n_int, n_rea,    &
                (propnod%pr_nd(nprp)%dat_i(i),i=1,n_int)
    write (std_o, 30) (propnod%pr_nd(nprp)%dat_r(i),i=1,n_rea) 
  else
    write (std_o, 21)  propnod%pr_nd(nprp)%n_pnd,           &
                propnod%pr_nd(nprp)%p_mat, n_int, n_rea,    &
                (propnod%pr_nd(nprp)%dat_i(i),i=1,n_int)
  end if
  !.
  return
10 format ('###.Number of Nodal properties                    : ',I8)
20 format (4(I3,1X),10(I4,1X))
21 format (4(I3,1X),10(I4,1X))
30 format (10(E12.3))
end subroutine lec_pnod
!  -----------------------------------------------------------------------------
! ------------------------------------------------------------------------------
subroutine get_prop_nd (p_prop, propnod, dat_i, pmat)
!  -----------------------------------------------------------------------------
  implicit none
  integer(ip), intent(in)      :: p_prop
  type (t_obpnd)               :: propnod
  integer(ip), intent(out)     :: dat_i, pmat

  dat_i=0
  if(size(propnod%pr_nd(p_prop)%dat_i)>0) then
    dat_i = propnod%pr_nd(p_prop)%dat_i(1)
  endif
  pmat  = propnod%pr_nd(p_prop)%p_mat
  return
end subroutine get_prop_nd
! ------------------------------------------------------------------------------
! ------------------------------------------------------------------------------
function  get_celltype(p_prop, propnod) result(t_cel)
! ------------------------------------------------------------------------------
  implicit none
  integer(ip), intent(in)      :: p_prop
  type (t_obpnd)               :: propnod
  !.
  integer(ip)                  :: t_cel

   t_cel =0
   if(size(propnod%pr_nd(p_prop)%dat_i)>0) then
     t_cel = propnod%pr_nd(p_prop)%dat_i(1)
   endif
  return
end function get_celltype
! ------------------------------------------------------------------------------
! ------------------------------------------------------------------------------
function get_numpropnd(prnd) result(n_prp) 
!  -----------------------------------------------------------------------------
  implicit none
  type (t_obpnd)       :: prnd
  integer(ip)          :: n_prp

  n_prp = prnd%n_prp
  return
end function get_numpropnd
! ------------------------------------------------------------------------------
! ------------------------------------------------------------------------------
subroutine bin_write_pnod (lu_m, np_nd, lst_pnd, propnod)
! ------------------------------------------------------------------------------
! This subroutine writes nodal properties of a domain to file lu_m (bin)
! ------------------------------------------------------------------------------
! lu_m    : logic unit
! np_nd   : number of domain nodal properties
! lst_pnd : list of properties
! propnod : nodal properties
! ------------------------------------------------------------------------------
  implicit none
  integer(ip), intent(in)      :: lu_m, np_nd, lst_pnd(:)
  type (t_obpnd)               :: propnod
  !.
  integer(ip), allocatable     :: indx(:)
  integer(ip)                  :: n_int, n_rea, i,j,k, i_err

  allocate (indx(propnod%n_prp),stat=i_err) 
  if (i_err > 0 ) call w_error (99,'indx',1)
  write (lu_m) '#PROP_NOD', np_nd  !.Number of nodal properties in domain
  !.
  if (np_nd > 0) then
    call quick_sort (propnod%n_prp,lst_pnd,indx)
    !.
    do i=1,propnod%n_prp
      k=indx(i); j = lst_pnd(k)
      if (j > 0) then
        n_int = size(propnod%pr_nd(k)%dat_i)
        n_rea = size(propnod%pr_nd(k)%dat_r)
        !.
        write (lu_m) j, propnod%pr_nd(k)%p_mat, n_int, n_rea
        if (n_int > 0) write (lu_m) propnod%pr_nd(k)%dat_i
        if (n_rea > 0) write (lu_m) propnod%pr_nd(k)%dat_r
      end if
    end do
  end if
  return
end subroutine bin_write_pnod
! ------------------------------------------------------------------------------
! ------------------------------------------------------------------------------
subroutine ascii_write_pnod (lu_m, np_nd, lst_pnd, propnod)
! ------------------------------------------------------------------------------
! This subroutine writes nodal properties of a domain to file lu_m (ascii)
! ------------------------------------------------------------------------------
! lu_m    : logic unit
! np_nd   : number of domain nodal properties
! lst_pnd : list of properties
! propnod : nodal properties
! ------------------------------------------------------------------------------
  implicit none
  integer(ip), intent(in)      :: lu_m, np_nd, lst_pnd(:)
  type (t_obpnd)               :: propnod
  !.
  integer(ip)                  :: indx(propnod%n_prp)
  integer(ip)                  :: n_int, n_rea, i,j,k

 
  write (lu_m,10) '#PROP_NOD', np_nd   !.Number of nodal properties in domain
  !.
  if (np_nd > 0) then
    call quick_sort (propnod%n_prp,lst_pnd,indx)
    !.
    do i=1,propnod%n_prp
      k=indx(i); j = lst_pnd(k)
      if (j > 0) then
        n_int=0; n_rea = 0
        n_int = size(propnod%pr_nd(k)%dat_i)
        n_rea = size(propnod%pr_nd(k)%dat_r)
        !.
        write (lu_m,20) j, propnod%pr_nd(k)%p_mat, n_int, n_rea
        if (n_int > 0) write (lu_m,20) propnod%pr_nd(k)%dat_i
        if (n_rea > 0) write (lu_m,30) propnod%pr_nd(k)%dat_r
      end if
    end do
  end if
  return
10 format (A,2X,I3)
20 format (10(:,2X,I4))
30 format (10(:,2X,F12.6))
end subroutine ascii_write_pnod
! ------------------------------------------------------------------------------
! ------------------------------------------------------------------------------
subroutine bin_read_pnod (lu_m, propnod)
! ------------------------------------------------------------------------------
! This subroutine reads nodal properties from binary files
! ------------------------------------------------------------------------------
  implicit none
  integer(ip), intent(in)      :: lu_m
  type (t_obpnd)               :: propnod
  !.
  character (len=9)            :: str
  integer(ip)                  :: n_int, n_rea, i
  integer(ip)                  :: io_err, i_err

 
  read (lu_m, iostat=io_err) str, propnod%n_prp
  if (io_err > 0) call w_error (33,'propnod%n_prp',1)
  !.
  if (propnod%n_prp > 0) then
    allocate (propnod%pr_nd(propnod%n_prp), stat=i_err)
    if (i_err > 0 ) call w_error (34,'propnod%pr_nd(propnod%n_prp)',1)
    !.
    do i = 1, propnod%n_prp
      read (lu_m, iostat = io_err) propnod%pr_nd(i)%n_pnd,                      &
                                   propnod%pr_nd(i)%p_mat, n_int, n_rea
      if (io_err > 0) call w_error (35,'propnod%pr_nd(i) %n_pnd, %p_mat, n_int, n_rea',1)
      !.
      allocate (propnod%pr_nd(i)%dat_i(n_int),stat=i_err) 
      if (i_err > 0 ) call w_error (34,'propnod%pr_nd(i)%dat_i(n_int)',1)
      if ( n_int > 0) then
        read (lu_m, iostat = io_err) propnod%pr_nd(i)%dat_i(1:n_int)
        if (io_err > 0) call w_error (35,'propnod%pr_nd(i)%dat_i',1)
      end if
      !.
      allocate (propnod%pr_nd(i)%dat_r(n_rea),stat=i_err) 
      if (i_err > 0 ) call w_error (34,'propnod%pr_nd(i)%dat_r(n_rea)',1)
      if (n_rea > 0) then
        read (lu_m, iostat = io_err)  propnod%pr_nd(i)%dat_r(1:n_rea)
        if (io_err > 0) call w_error (35,'propnod%pr_nd(i)%dat_r',1)
      end if
    end do
  end if
  return
end subroutine bin_read_pnod
! ------------------------------------------------------------------------------
! ------------------------------------------------------------------------------
function deallocate_pnod (propnod) result(flg)
! ------------------------------------------------------------------------------
  implicit none
  type (t_obpnd)               :: propnod
  integer(ip)                  :: flg
  !.
  integer(ip)                  :: i_err, i, istat

  if (allocated(propnod%pr_nd)) then
    i_err = 0
    do i=1,propnod%n_prp
      deallocate (propnod%pr_nd(i)%dat_i, stat=istat)
      if (istat /= 0) then
            i_err = 1
            write(*,*) 'prop_nod: error with dat_i'
      endif
      deallocate (propnod%pr_nd(i)%dat_r, stat=istat)
      if (istat /= 0) then
            i_err = 1
            write(*,*) 'prop_nod: error with dat_r'
      end if
    end do
    deallocate (propnod%pr_nd, stat=istat)
    if (istat == 0) flg = 0 
    if ((istat /= 0).or.(i_err == 1)) then
            flg = 1
            write(*,*) 'Error with pr_nd'
    endif
  else
    flg = -1
  end if
  return
end function deallocate_pnod
! ------------------------------------------------------------------------------
end module mod_prop_nd
! ------------------------------------------------------------------------------
