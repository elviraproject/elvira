! -------------------------------------------------------------------------------
module mod_quadrature
! -------------------------------------------------------------------------------
  use mod_precision 
! -------------------------------------------------------------------------------
  implicit none
  public  :: quadrature
  private :: gauss_1d, gauss_2d, gauss_3d, gauss_coeff, GaussQuad
  private :: dunavanttrianglerule, keasttetrarule

  contains
! -------------------------------------------------------------------------------
subroutine quadrature ( ndim, m_qp, nip, pt, wt)
! -------------------------------------------------------------------------------
  implicit none
  integer,  intent(in)  :: ndim , m_qp, nip
  real(rp), intent(out) :: pt(:,:), wt(:)


  if (m_qp == 0) then
    select case (ndim)
    case(2)
      call dunavanttrianglerule (nip, pt, wt)
    case(3)
      call keasttetrarule (nip, pt, wt)
    case default
      stop 'Error in quadrature, whit ndim'
    end select
  else
    call GaussQuad(ndim, m_qp, nip, pt, wt)
  end if
  return
end subroutine quadrature
! -------------------------------------------------------------------------------
subroutine GaussQuad (ndim, m_qp, nip, pt, wt)
! -------------------------------------------------------------------------------
! ndim = dimension (1, 2 or 3)
! m_qp = number of tabulated 1-d points
! nip  = m_qp => 1D; m_qp**2 => 2D; m_qp**3 => 3D point
! gpt  = tabulated 1-d quadrature points
! gwt  = tabulated 1-d quadrature weights
! pt   = calculated coords in a line, square or cube
! wt   = calculated weights in a line, square or cube
! -------------------------------------------------------------------------------
  implicit none
  integer,  intent(in)  :: ndim , m_qp, nip
  real(rp), intent(out) :: pt(:,:), wt(:)

  select case (ndim)
  case(1)
    call gauss_1d (m_qp, nip, pt, wt)
  case(2)
    call gauss_2d (m_qp, nip, pt, wt) 
  case(3)
    call gauss_3d (m_qp, nip, pt, wt)
  case default
    stop 'Error in GaussQuad, whit ndim'
  end select
  return
end subroutine GaussQuad
! -------------------------------------------------------------------------------
subroutine gauss_1d (m_qp, nip, pt, wt)
! -------------------------------------------------------------------------------
! Extract m_qp 1-d gauss data from tables
! -------------------------------------------------------------------------------
  implicit none
  integer,  intent(in)  :: m_qp, nip
  real(rp), intent(out) :: pt (1,nip), wt(nip)
  !.---- Local variable ---------------------------------------------------------
  integer(ip)           :: n_gp, i
  !.---- Automatic arrays -------------------------------------------------------
  real(rp)     :: gpt (m_qp), gwt (m_qp)

  n_gp = m_qp
  if ( n_gp /= nip ) then
    n_gp = nip
    write (6,'(A,3(1X,I3))') '###.WARNING: data changed, gauss_1d',m_qp,nip,n_gp
  end if
  !.
  call gauss_coeff (n_gp, gpt, gwt) !.get data from table
  !.
  do i = 1, n_gp
    pt (1, i) = gpt (i)
    wt (i)    = gwt (i)
  end do
end subroutine gauss_1d
! -------------------------------------------------------------------------------
subroutine gauss_2d (m_qp, nip, pt, wt)
! -------------------------------------------------------------------------------
! Use m_qp 1-d gaussian data to generate nip quadrature data for a square
! -------------------------------------------------------------------------------
! m_qp = number of tabulated 1-d points
! nip  = m_qp**2 = number of 2-d points
! gpt  = tabulated 1-d quadrature points
! gwt  = tabulated 1-d quadrature weights
! pt   = calculated coords in a square
! wt   = calculated weights in a square
! -------------------------------------------------------------------------------
  implicit none
  integer,  intent(in)  :: m_qp, nip
  real(rp), intent(out) :: pt (2, nip), wt (nip)
  !.---- Local variable ---------------------------------------------------------
  integer(ip)           :: n_gp,k,i,j
  !.---- Automatic arrays -------------------------------------------------------
  real(rp)              :: gpt (m_qp), gwt (m_qp)

  n_gp = m_qp
  if ( (n_gp * n_gp) /= nip ) then
    n_gp = sqrt (float (nip) ) + 0.1
    write (6,'(A,3(1X,I3))') '###.WARNING: data corrected, gauss_2d',m_qp,nip,n_gp
  end if
  !.
  call gauss_coeff (n_gp, gpt, gwt) !.get data from table
  !.
  k = 0
  do i = 1, n_gp !.loop over generated points
    do j = 1, n_gp
      k = k + 1
      wt (k) = gwt (i) * gwt (j)
      pt (1, k) = gpt (j)
      pt (2, k) = gpt (i)
    end do
  end do
end subroutine gauss_2d
! -------------------------------------------------------------------------------
subroutine gauss_3d (m_qp, nip, pt, wt)
! -------------------------------------------------------------------------------
! Use 1-d gaussian data to generate quadrature data for a cube
! -------------------------------------------------------------------------------
! m_qp = number of tabulated 1-d points
! nip = m_qp**3 = number of 3-d points
! gpt = tabulated 1-d quadrature points
! gwt = tabulated 1-d quadrature weights
! pt  = calculated coords in a cube
! wt  = calculated weights in a cube
! -------------------------------------------------------------------------------
  implicit none
  integer,  intent(in)  :: m_qp, nip
  real(rp), intent(out) :: pt (3, nip), wt (nip)
!.---- Local variable -----------------------------------------------------------
  integer (ip)          :: n_gp,n_gp3,i,j,k,l
!.---- Automatic arrays ---------------------------------------------------------
  real(rp) :: gpt (m_qp), gwt (m_qp)

  n_gp = m_qp
  n_gp3 = n_gp * n_gp * n_gp
  if ( n_gp3 /= nip ) then
    n_gp = (float (nip) ) ** (1. / 3.)
    write (6,'(A)')          '###.WARNING: data changed in gauss_3d where'
    write (6,'(A,3(1X,I3))') '1D rule, 3D requested, 3D used = ', m_qp, nip, n_gp
  end if
  !.
  call gauss_coeff (n_gp, gpt, gwt) !.get table data
  !.
  k = 0
  do l = 1, n_gp  !.loop over generated points
    do i = 1, n_gp
      do j = 1, n_gp
        k = k + 1
        wt (k) = gwt (i) * gwt (j) * gwt (l)
        pt (1, k) = gpt (j)
        pt (2, k) = gpt (i)
        pt (3, k) = gpt (l)
      end do
    end do
  end do
end subroutine gauss_3d
! -------------------------------------------------------------------------------
subroutine gauss_coeff (m_qp, pt, wt)
! -------------------------------------------------------------------------------
! Gaussian quadrature abscissae and weight coeffs
! -------------------------------------------------------------------------------
! m_qp  = no. of gauss points in one dimension
! pt    = abscissae of gauss points
! wt    = weights  of gauss points
! nmax  = max. no. of points tabulated herein
! -------------------------------------------------------------------------------
  implicit none
  include 'gausscoef.inc'
  integer,  parameter   :: nmax = 12
  integer,  intent(in)  :: m_qp
  real(rp), intent(out) :: pt (m_qp), wt (m_qp)
!.---- Local variable -----------------------------------------------------------
  integer (ip)          :: n_gp

  n_gp = m_qp
  if ( n_gp > nmax ) then
    n_gp = nmax
    write (6, * ) 'warning, gauss_coeff used gauss = ', nmax
  end if
  !.
  if ( n_gp < 1 ) stop 'gauss = 0, no points in gauss_coeff'
  !.
  select case (n_gp)
  case ( 1); pt = pt_1;   wt = wt_1;  !.n_gp =  1, precision =  1
  case ( 2); pt = pt_2;   wt = wt_2;  !.n_gp =  2, precision =  3
  case ( 3); pt = pt_3;   wt = wt_3;  !.n_gp =  3, precision =  5
  case ( 4); pt = pt_4;   wt = wt_4;  !.n_gp =  4, precision =  7
  case ( 5); pt = pt_5;   wt = wt_5;  !.n_gp =  5, precision =  9
  case ( 6); pt = pt_6;   wt = wt_6;  !.n_gp =  6, precision = 11
  case ( 7); pt = pt_7;   wt = wt_7;  !.n_gp =  7, precision = 13
  case ( 8); pt = pt_8;   wt = wt_8;  !.n_gp =  8, precision = 15
  case ( 9); pt = pt_9;   wt = wt_9;  !.n_gp =  9, precision = 17
  case (10); pt = pt_10;  wt = wt_10; !.n_gp = 10, precision = 19
  case (11); pt = pt_11;  wt = wt_11; !.n_gp = 11, precision = 21
  case (12); pt = pt_12;  wt = wt_12; !.n_gp = 12, precision = 23
  case default
    !.
  end select
  return
end subroutine gauss_coeff
! -------------------------------------------------------------------------------
subroutine dunavanttrianglerule (nip, pt, wt)
! ------------------------------------------------------------------------------
! Dunavant quadrature rule for triangles, to degree = 17, in unit coordinates,
! i.j.n.m.e. vol. 21, pp.1129-1148, 1985
! ------------------------------------------------------------------------------
! for  m_qp = 1,3,4,6,7,12,13,16,19,25,27,33,37,42,48,52,61
!    degree = 1,2,3,4,5, 6, 7, 8, 9,10,11,12,13,14,15,16,17
! nip      = number of quadrature points
! pt       = returned quadrature coordinates
! wt       = returned quadrature weights
! ------------------------------------------------------------------------------
  implicit none
  include 'dunavanttrianglerule.inc'
  integer,  intent(in)  :: nip
  real(rp), intent(out) :: pt (2, nip), wt (nip)

  select case (nip)
  case ( 1) ; wt = wt_1  ; pt = pt_1  !.degree =>  1
  case ( 3) ; wt = wt_3  ; pt = pt_3  !.degree =>  2
  case ( 4) ; wt = wt_4  ; pt = pt_4  !.degree =>  3
  case ( 6) ; wt = wt_6  ; pt = pt_6  !.degree =>  4
  case ( 7) ; wt = wt_7  ; pt = pt_7  !.degree =>  5
  case (12) ; wt = wt_12 ; pt = pt_12 !.degree =>  6
  case (13) ; wt = wt_13 ; pt = pt_13 !.degree =>  7
  case (16) ; wt = wt_16 ; pt = pt_16 !.degree =>  8
  case (19) ; wt = wt_19 ; pt = pt_19 !.degree =>  9
  case (25) ; wt = wt_25 ; pt = pt_25 !.degree => 10
  case (27) ; wt = wt_27 ; pt = pt_27 !.degree => 11
  case (33) ; wt = wt_33 ; pt = pt_33 !.degree => 12
  case (37) ; wt = wt_37 ; pt = pt_37 !.degree => 13
  case (42) ; wt = wt_42 ; pt = pt_42 !.degree => 14
  case (48) ; wt = wt_48 ; pt = pt_48 !.degree => 15
  case (52) ; wt = wt_52 ; pt = pt_52 !.degree => 16
  case (61) ; wt = wt_61 ; pt = pt_61 !.degree => 17
  case default
    stop 'Invalid rule dunavanttrianglerule'
  end select
  return
end subroutine dunavanttrianglerule
! -----------------------------------------------------------------------------
subroutine keasttetrarule (nip, pt, wt)
! -----------------------------------------------------------------------------
! Keast unit coordinate quadrature rule for tetrahedra, degree 1 to 8, c.m.a.m.e.
! v. 55, pp. 339-348, 1986 [with sign & scale fix] copyright 1998, j. e. akin
! given nqp        = 1, 4, 5, 10, 11, 15, 24, 31, 45
! exact for degree = 0, 1, 2,  3,  4,  5,  6,  7,  8
! -----------------------------------------------------------------------------
! nip = number of quadrature points
! pt  = returned quadrature coordinates
! wt  = returned quadrature weights
! -----------------------------------------------------------------------------
  implicit none
  include 'keasttetrarule.inc'
  integer(ip),  intent (in)  :: nip
  real(rp),     intent (out) :: pt (3, nip), wt (nip)

  select case (nip)
  case ( 1) ; wt = wt_1  ; pt = pt_1    !.degree => 0
  case ( 4) ; wt = wt_4  ; pt = pt_4    !.degree => 1
  case ( 5) ; wt = wt_5  ; pt = pt_5    !.degree => 2
  case (10) ; wt = wt_10 ; pt = pt_10   !.degree => 3
  case (11) ; wt = wt_11 ; pt = pt_11   !.degree => 4
  case (15) ; wt = wt_15 ; pt = pt_15   !.degree => 5
  case (24) ; wt = wt_24 ; pt = pt_24   !.degree => 6
  case (31) ; wt = wt_31 ; pt = pt_31   !.degree => 7
  case (45) ; wt = wt_45 ; pt = pt_45   !.degree => 8
  case default
    stop 'Invalid rule keasttetrarule'
  end select
  return
end subroutine KeastTetraRule


end module mod_quadrature
