! -------------------------------------------------------------------------------
module mod_preprocesor
! -------------------------------------------------------------------------------
!  Record of Revisions:
!    Date            Programmer              Description of change
!    ====            ==========              =====================
!  30/07/08        Elvio A. Heidenreich      Codigo Original
! -------------------------------------------------------------------------------
!   Este modulo es para preprocesar la malla, esto es verificar la malla y cons-
! truir todas las estructuras auxiliares necesarias para el calculo.
!   Las funciones principales son:
!  
! 1.Verificar la conectividad y si hay o no nodos repetidos. Las rutinas encar-
!   gadas de ello son:
!   ESTAS RUTINAS HAY QUE PROGRAMARLAS
! 
! 2.Construir los grafos, de elementos que concurren a nodos, elementos que con-
!   curren a elementos y nodos que concurren a nodos. Las rutinas encargadas 
!   son:
!       #.grafo_elmnode(n_nod, mshdata)
!       #.grafo_ElmElm (n_elm, mshdata)
!       #.grafo_NodNod (n_nod, mshdata)
!
! 3.Division de la malla, llamadas a las librerias METIS y rutinas que marcan
!   los elementos y nodos de cada dominio y el grafo de nodo-dominio. Estas
!   rutinas son:
!       #.       
!       #.  
!       #.  
!
! 4.Tambien llama a las rutinas que escriben los archivos binario de la malla y 
!   liberan memoria del proceso master. Estas son:


! -------------------------------------------------------------------------------
  use mod_precision
  use mod_busqueda
  use mod_filename
  use mod_error
  use mod_meshdata
  use mod_stepdata
  use mod_cmckee
!  -----------------------------------------------------------------------------
implicit none

contains
!  -----------------------------------------------------------------------------
subroutine grafo_elmnode(n_nod, mshdata)
! ------------------------------------------------------------------------------ 
!  Record of Revisions:
!    Date            Programmer              Description of change
!    ====            ==========              =====================
!  30/07/08        Elvio A. Heidenreich      Codigo Original
! -------------------------------------------------------------------------------
! Esta rutina dimensiona y llena las estructuras de los elementos que concurren
! a un nodo.
! |in   |.n_nod  : numero de nodos de la malla
! |inout|.mshdata: estructura de datos que contiene la informacion de la malla
! -------------------------------------------------------------------------------
  implicit none
  !.Numero maximo de nodos que puede tener un elemento
  integer(ip), parameter  :: max_n=100 
  !.
  integer(ip), intent(in) :: n_nod
  type(t_mesh)            :: mshdata
  !--- Variables locales --------------------------------------------------------
  !.Vectores auxiliares
  integer(ip)             :: ien(n_nod) 
  integer(ip)             :: conc(max_n)
  !.
  integer(ip)             :: n_elm, i, j, k, n_pe

  !.Dimensionado del array de estructura de elementos que concurre a un nodo.
  call allocate_ar_en(mshdata%node)
  !.Llenado de la base de datos de elementos ----------------------------
  n_elm = get_nelm(mshdata%elem) !.numero 
  !.
  ien=0 !.Contador del nro. de elementos que concurren a nodos
  do i=1,n_elm
    call get_elmconc (i,n_pe,conc,mshdata%elem)
    do j=1,n_pe
      k = conc(j)           !.nodo
      ien(k) = ien(k)  + 1  !.Cont. nro de elem que concurren al nodo.
    end do
  end do
  !.
  !.Dimensionado de la estructura de los elementos que concurren a nodo
  do i=1,n_nod
    call allocate_ar_en_je_en(i,ien(i),mshdata%node)
  end do
  !.
  !.Llenado de la estructura de los elementos que concurren a nodo
  ien = 0
  do i=1,n_elm
    call get_elmconc (i,n_pe,conc,mshdata%elem)
    do j=1,n_pe
      k      = conc(j)                    !.nodo
      ien(k) = ien(k)  + 1 
      call set_elm_in_jeen (i, k, ien(k), mshdata%node)
    end do
  end do 
  !.
  write(*, 10)
  return
10 format('###.Node-elements graph built .......')
end subroutine grafo_elmnode
!  -----------------------------------------------------------------------------
subroutine grafo_ElmElm (n_elm, mshdata)
! ------------------------------------------------------------------------------
!  Record of Revisions:
!    Date            Programmer              Description of change
!    ====            ==========              =====================
!  30/07/08        Elvio A. Heidenreich      Codigo Original
! -------------------------------------------------------------------------------
! Esta rutina dimensiona y llena las estructuras de los elementos que concurren
! a un elemento.
! |in   |.n_elm  : numero de nodos de elementos de la
! |inout|.mshdata: estructura de datos que contiene la informacion de la malla
! ------------------------------------------------------------------------------- 
  implicit none
  integer(ip), parameter  :: max = 100
  integer(ip), intent(in) :: n_elm  
  type(t_mesh)            :: mshdata
  !--- Variables locales --------------------------------------------------------
  !.Vectores auxiliares (VA)
  integer(ip)             :: i_aux(n_elm) !.VA
  integer(ip)             :: conc(max)    !.VA, conectividad nodal
  integer(ip)             :: el_n(2*max)  !.VA, elem. que concurre a un nodo
  integer(ip)             :: e_st(10*max) !.VA, elem. que concurre a un elemento
  !.
  integer(ip)             :: i, j, k, n_ee, inod, n_en, ielm, n_pe

  !.Dimensionado del array de est. de elemento que concurren a elementos
  call allocate_eae (mshdata%elem) 
  !.
  i_aux = 0
  do i=1,n_elm
    !.Recupero la conectividad del elemento  i
    call get_elmconc (i, n_pe, conc, mshdata%elem) 
    n_ee=0 !.contador del nro. de elemento que concurren al elemento i
    do j=1,n_pe
      inod = conc(j)    !.Conectividad, nodo
      !.Recupera en el_n(n_en) los elm. que concurren al nodo inod
      call get_jeen (mshdata%node, inod, n_en, el_n) 
      do k=1,n_en
        ielm=el_n(k)
        if ( (ielm /= i) .and. (i_aux(ielm) /= i)) then
          n_ee       = n_ee + 1; 
          e_st(n_ee) = ielm;    !.Almacenamiento temporal
          i_aux(ielm)= i
        end if
      end do
    end do
    !.Dimensionado de la estructura de elemento que concurre al elemento i
    call allocate_eae_je_ee (i, n_ee, mshdata%elem)
    !.Ordeno los elementos
    call sort (n_ee,e_st(1:n_ee))
    !.Alamcenamiento de e_st(1:n_ee)
    call set_eae_je_ee_vec (i, n_ee, e_st(1:n_ee), mshdata%elem)
    !.
  end do
  !.
  write(*, 10)
  return
10 format('###.Element-Element graph built ....')
end subroutine grafo_ElmElm
!  -----------------------------------------------------------------------------
subroutine grafo_NodNod (n_nod, mshdata)
! ------------------------------------------------------------------------------
!  Record of Revisions:
!    Date            Programmer              Description of change
!    ====            ==========              =====================
!  30/07/08        Elvio A. Heidenreich      Codigo Original
! -------------------------------------------------------------------------------
! Esta rutina dimensiona y llena las estructuras de los nodos que concurren a un
! nodo.
! |in   |.n_nod  : numero de nodos de la malla
! |inout|.mshdata: estructura de datos que contiene la informacion de la malla
! -------------------------------------------------------------------------------
 
  implicit none
  integer(ip), intent(in) :: n_nod   !.numero de nodos
  type(t_mesh)            :: mshdata !.malla
  !.
  integer(ip), parameter  :: max = 100
  !--- Variables locales --------------------------------------------------------
  !.Vectores auxiliares (VA)
  integer(ip)             :: i_aux(n_nod) !.VA
  integer(ip)             :: conc(max)    !.VA, conectividad nodal
  integer(ip)             :: el_n(2*max)  !.VA, elem. que concurren a un nodo
  integer(ip)             :: n_st(10*max) !.VA, elem. que concurren a un elem.
  !.
  integer(ip)             :: i, j, k, n_nn, inod, n_en, ielm, n_pe

  !.Dimensionado del array de est. de nodos que concurren a un nodo
  call allocate_ar_nn (mshdata%node) 
  !.
  i_aux = 0;
  do i=1,n_nod
    !.Recupero los elementos que concurren al nodo i
    call get_jeen (mshdata%node, i, n_en, el_n)
    n_nn = 0
    do j=1,n_en
      ielm=el_n(j) !. elemento
      !.Recupero la conectivida del elemento  ielm
      call get_elmconc (ielm, n_pe, conc, mshdata%elem) 
      do k = 1, n_pe
        inod= conc(k) !.nodo del elemento ielm
        if ( (inod /= i) .and. (i_aux(inod) /= i)) then
          n_nn       = n_nn + 1; 
          n_st(n_nn) = inod;    !.Almacenamiento temporal del nodo
          i_aux(inod)= i        !.Marco el array auxiliar
        end if
      end do
    end do
    !.Dimensionado de la estructura de nodos que concurren al nodo i
    call allocate_ar_nn_je_nn (i, n_nn, mshdata%node)
    !.
    call sort (n_nn,n_st(1:n_nn))
    !.Alamcenamiento de n_st(1:n_nn)
    call set_ar_nn_je_nn (i, n_nn, n_st(1:n_nn), mshdata%node)
    !.
  end do
  !.
  write(*, 10)
  !.
  return
10 format('###.Node-node graph built ...........')
end subroutine grafo_NodNod
!  -----------------------------------------------------------------------------
subroutine callmetis_partgraph(n_dom,mshdata)
!  -----------------------------------------------------------------------------
! n_dom : numero de dominios
! Esta rutina me divide el dominio en n_dom subdominios.
!  -----------------------------------------------------------------------------
  implicit none
  integer(ip), parameter  :: max_n=100
  integer(ip), parameter  :: options(1:5) = (/ 0, 0, 0, 0, 0/); 
  integer(ip), parameter  :: wgt(1:3)     = (/ 0, 0, 0/)
  !.
  integer(ip), intent(in) :: n_dom
  type(t_mesh)            :: mshdata
  !.
  integer(ip), allocatable:: xadj(:), adjncy(:),part(:)
  !.
  integer(ip)             :: elm_d(n_dom)
  integer(ip)             :: n_elm, i, n_en 
  integer(ip)              :: ki,kf,n_jel
!  integer(8)              :: ki,kf,n_jel
  integer(ip)             :: i_err,edgecut
  integer(ip)             :: conc(max_n)


  n_elm = get_nelm (mshdata%elem)           !.numero de elementos
  n_jel = get_dimeae_je_ee(mshdata%elem)    !.dimension de adjncy
  write(*,*) 'nelm: ', n_elm, 'n_jel: ',n_jel
  !.
  allocate (xadj(n_elm+1), adjncy(n_jel),part(n_elm), stat = i_err)
  if (i_err > 0) call w_error (44,'xadj(n_elm+1), adjncy(n_jel),part(n_elm)', 1) 
  !.
  xadj=0; xadj(1) = 1; adjncy = 0; part = 0
  do i=1,n_elm
    call get_eae_je_ee(i, n_en, conc, mshdata%elem)
    ki=xadj(i); xadj(i+1) = ki + n_en; kf = xadj(i+1)- 1
    !.
!    write(*,*) i,ki,kf,n_en
    adjncy(ki:kf)=conc(1:n_en)
  end do
  write (*,10); write (lu_log,10)
  !.
  if (n_dom > 1) then
    if( n_dom <= 8) then
      call Metis_PartGraphRecursive(n_elm,xadj,adjncy,wgt(1),wgt(2),wgt(3),1, &
           n_dom,options,edgecut,part)
    else
      call Metis_PartGraphKway     (n_elm,xadj,adjncy,wgt(1),wgt(2),wgt(3),1, &
           n_dom,options,edgecut,part)
    end if
    write (*,20); write (lu_log,20)
    !.
    do i=1,n_dom
      elm_d(i)= count (part == i)
    end do
    !.
    write (*,40) (i,elm_d(i),i=1,n_dom)
  else
    part(1:n_elm) = 1
  end if
  !.
  !.Un elemento solo pertenece a un dominio
  call set_element_subdomain(mshdata%elem,part)
  !.Los nodos de frontera pueden pertenecer a mas de un dominio. La siguiente
  !.rutina calcula a que dominios pertenece cada nodo. 
  call findNodeSubdomains (n_dom, part, mshdata)
  !.
  write (*,30); write (lu_log,30)
  !.Libero memoria
  deallocate (xadj, adjncy,part, stat = i_err)
  !.
  return
10 format('###.Element neighboring array built .')
20 format('###.Element graph partitioned .......')
30 format('###.Nodes have been distributed .....')
40 format('###.Number of Elements per domain:',/,4(:' Domain(',I3,')= ',I8))
end subroutine callmetis_partgraph
!!$!  -----------------------------------------------------------------------------
!!$subroutine ver_ndelm_dom(mshdata) 
!!$!  -----------------------------------------------------------------------------
!!$  implicit none
!!$  integer(ip), parameter  :: max_n=100
!!$  type(t_mesh)            :: mshdata
!!$  !.
!!$  integer(ip)             :: dom (max_n)
!!$  integer(ip)             :: conc(max_n) 
!!$  integer(ip)             :: i, j, k, n_elm, dm_e, n_pe, inod, n_d, n
!!$ 
!!$  n_elm = get_nelm(mshdata%elem)
!!$  !.
!!$  do i=1,n_elm
!!$    dm_e = get_elm_subdomain(i, mshdata%elem)  !.dominio del elemento
!!$    call get_elmconc (i, n_pe, conc, mshdata%elem)
!!$    !.
!!$    k = 0; n = 0
!!$    do j = 1, n_pe
!!$      inod = conc(j)
!!$      call get_node_subdomain(inod, n_d, dom, mshdata%node)
!!$      if (dom(1) == dm_e) then
!!$        exit
!!$      else
!!$        k = k + 1
!!$        if ( n < n_d) n = n_d
!!$      end if
!!$    end do
!!$    !.
!!$    if (k == n_pe) then
!!$      write (lu_log,'(A,I8,A)') '###.Elemento: ',i,' c/nodos en otro dominio'
!!$      call fixing_node_dom( n, dm_e, n_pe, conc, mshdata%node)
!!$    end if
!!$  end do
!!$  return
!!$end subroutine ver_ndelm_dom

!  -----------------------------------------------------------------------------
subroutine ver_ndelm_dom(mshdata) 
!  -----------------------------------------------------------------------------
  implicit none
  integer(ip), parameter  :: max_n=100
  type(t_mesh)            :: mshdata
  !.
  integer(ip)             :: dom (max_n)
  integer(ip)             :: conc(max_n) 
  integer(ip)             :: i, j, k, n_elm, dm_e, n_pe, inod, n_d, n
 
  n_elm = get_nelm(mshdata%elem)
  !.
  do i=1,n_elm
    dm_e = get_elm_subdomain(i, mshdata%elem)  !.dominio del elemento
    call get_elmconc (i, n_pe, conc, mshdata%elem)
    !.
    k = 0; n = 0
    do j = 1, n_pe
      inod = conc(j)
      call get_node_subdomain(inod, n_d, dom, mshdata%node)
      if (dom(1) == dm_e) then
        exit
      else
        k = k + 1
        if ( n < n_d) n = n_d
      end if
    end do
    !.
    if (k == n_pe) then
      write (lu_log,'(A,I8,A)') '###.Element: ',i,' w/nodes in other domain'
      write (*,'(A,I8,A)') '###.Element: ',i,' w/nodes in other domain'
      call fixing_node_dom( n, dm_e, n_pe, conc, mshdata%node)
    end if
  end do
  return
end subroutine ver_ndelm_dom
!  -----------------------------------------------------------------------------
subroutine findNodeSubdomains (n_dom, part, mshdata)
!  -----------------------------------------------------------------------------
!. n_dom       : numero de dominios
!. part(n_elm) : vector que contiene el numero de dominio de cada elemento.
!. mshdata     : estructura de datos de la malla
!  -----------------------------------------------------------------------------
  implicit none
  integer(ip), parameter  :: max_n=100
  integer(ip), intent(in) :: n_dom, part(:)
  type(t_mesh)            :: mshdata
  !.
  integer(ip)             :: el_n(max_n)
  !.
  integer(ip)             :: flg_d(n_dom), stg_d(n_dom)
  integer(ip)             :: n_nod, i, j, c_d, n_en, ielm, idom
  !
  integer(ip)             :: nm_el, k


  n_nod = get_nnod(mshdata%node)
  !.
  call allocate_nod_d (n_nod, mshdata%node) !.Dimensionado 
  !.
  do i=1,n_nod
    !.Recupero los elementos que concurren al nodo i
    call get_jeen (mshdata%node, i, n_en, el_n)
    !.
    flg_d= 0      !.flag
    do j= 1, n_en
      ielm = el_n(j)    !.Elemento que concurre al nodo i
      idom = part(ielm) !.El elemento ielm pertenece al dominio idom
      !.
      flg_d(idom) = flg_d(idom) + 1
    end do
    !.
    c_d = 0; nm_el=0
    do j=1,n_dom
      if (flg_d(j) > 0) then
        c_d = c_d + 1
        stg_d(c_d)  = j !.almaceno el dominio al que pertenece
        if (nm_el < flg_d(j)) then
          nm_el = flg_d(j)
          idom  = j; k = c_d
        end if
      end if
    end do
    !.
    if (c_d > 1) call i_swap (stg_d(1),stg_d(k))
    !.
    call set_node_subdomain (i, c_d, stg_d, mshdata%node)
    !.
  end do
  !.
  return
end subroutine findNodeSubdomains
!!$!  -----------------------------------------------------------------------------
!!$subroutine findNodeSubdomains (n_dom, part, mshdata)
!!$!  -----------------------------------------------------------------------------
!!$!. n_dom       : numero de dominios
!!$!. part(n_elm) : vector que contiene el numero de dominio de cada elemento.
!!$!. mshdata     : estructura de datos de la malla
!!$!  -----------------------------------------------------------------------------
!!$  implicit none
!!$  integer(ip), parameter  :: max_n=100
!!$  integer(ip), intent(in) :: n_dom, part(:)
!!$  type(t_mesh)            :: mshdata
!!$  !.
!!$  integer(ip)             :: el_n(max_n)
!!$  !.
!!$  integer(ip)             :: flg_d(n_dom), stg_d(n_dom)
!!$  integer(ip)             :: n_nod, i, j, c_d, n_en, ielm, idom
!!$  !.
!!$
!!$
!!$  n_nod = get_nnod(mshdata%node)
!!$  !.
!!$  call allocate_nod_d (n_nod, mshdata%node) !.Dimensionado 
!!$  !.
!!$  do i=1,n_nod
!!$    !.Recupero los elementos que concurren al nodo i
!!$    call get_jeen (mshdata%node, i, n_en, el_n)
!!$    c_d = 0       !.Contador de cuantos dominios comparten el nodo
!!$    flg_d= 0      !.flag
!!$    do j= 1, n_en
!!$      ielm  = el_n(j)   !.Elemento que concurre al nodo i
!!$      !.
!!$      idom = part(ielm) !.El nodo i pertenece al dominio idom
!!$      !.
!!$      if (flg_d(idom) == 0) then
!!$        flg_d(idom) = 1; c_d = c_d + 1
!!$        stg_d(c_d)  = idom !.almaceno el dominio al que pertenece
!!$      end if
!!$    end do
!!$    !.
!!$    call set_node_subdomain (i, c_d, stg_d, mshdata%node)
!!$    !.
!!$  end do
!!$  !.
!!$  return
!!$end subroutine findNodeSubdomains




!  -----------------------------------------------------------------------------
subroutine callmetis_fillreducing(mshdata)
!  -----------------------------------------------------------------------------
! n_dom : numero de dominios
! 
!  -----------------------------------------------------------------------------
  implicit none
  integer(ip), parameter  :: max_n=1000
  integer(ip), parameter  :: options(1:8) = (/ 0, 0, 0, 0, 0, 0, 0, 0/); 
  !.
  type(t_mesh)            :: mshdata
  !.
  integer(ip), allocatable:: xadj(:), adjncy(:), perm(:), iperm(:)
  !.
  integer(ip)             :: n_nod, i, ki, kf, n_jnd, n_nn
  integer(ip)             :: i_err
  integer(ip)             :: c_nn (max_n)


  n_nod =  get_nnod(mshdata%node)               !.Numero de nodos
  n_jnd =  get_dim_ar_nn_je_nn (mshdata%node)   !.dimension de adjncy
  !.
  allocate (xadj(n_nod+1), adjncy(n_jnd),perm(n_nod),iperm(n_nod), stat = i_err)
  if (i_err > 0) call w_error (44,'xadj(n_elm+1), adjncy(n_jel),part(n_elm)', 1) 
  !.
  xadj=0; xadj(1) = 1; adjncy = 0; perm = 0; iperm = 0
  do i=1,n_nod
    call get_ar_nn_je_nn ( mshdata%node,i, n_nn, c_nn)
    ki=xadj(i); xadj(i+1) = ki + n_nn; kf = xadj(i+1)- 1
    !.
    adjncy(ki:kf) = c_nn(1:n_nn)
  end do
  !.
  call call_cmckee(n_nod,n_jnd,xadj,adjncy,iperm)
  !.
  !call Metis_NodeNd (n_nod,xadj,adjncy,1,options,perm,iperm)
  !.
  return
end subroutine callmetis_fillreducing
!  -----------------------------------------------------------------------------
subroutine abaqus_file(n_dom, n_elm, n_nod, mshdata,fname)
!  -----------------------------------------------------------------------------
  implicit none
  integer(ip), parameter  :: max = 100
  integer(ip), intent(in) :: n_dom, n_elm, n_nod
  type(t_mesh)            :: mshdata
  character (len=*)       :: fname
  !.
  integer(ip)             :: conc(max),dom(max)
  !.
  integer(ip)             :: iaux(n_elm), jaux(n_nod)

  integer(ip)             :: i, j, k, idim, n_pe, lu_ab, io_err, idom, n_d
  real(rp)                :: coord(3)

  !.
 
  call a_unit(lu_ab) !.Asignacion de numero de unidad
  open (unit=lu_ab,file=fname,status='unknown',iostat=io_err)
  if (io_err > 0) call w_error(8 ,'abaqus.inp',1) 
  !.
  write (lu_ab,'(A)') '*NODE, NSET=GLOBAL'
  !.
  do i=1,n_nod
    call get_nodecoord (i,idim,coord,mshdata%node)
    write (lu_ab, 10) i,coord(1:idim)
  end do
  !.
  write (lu_ab,'(A)') '*ELEMENT, TYPE=C3D8, ELSET=G1'
  !.
  do i=1,n_elm
    call get_elmconc  (i,n_pe,conc,mshdata%elem)
    write (lu_ab, 20) i,conc(1:n_pe)
  end do
  !.
  write (lu_ab,'(A)') '*SOLID SECTION, ELSET=G1, MATERIAL=M1, ORIENTATION=S0'
  !.
  write (lu_ab,'(A)') '*MATERIAL, NAME=M1'
  write (lu_ab,'(A)') '*ELASTIC, TYPE=ISOTROPIC'
  write (lu_ab,'(A)') '   210000.,       0.3,       20.'
  write (lu_ab,'(A)') '*EXPANSION, TYPE=ISO'
  write (lu_ab,'(A)') '  0.000012,       20.'
  write (lu_ab,'(A)') '*DENSITY'
  write (lu_ab,'(A)') '    7.8E-6'
  !.Escritura de los grupos de elementos
  if (associated_eae(mshdata%elem)) then
    do i=1,n_dom
      write (lu_ab,30)'P'//achar(i+48)
      k=0
      do j=1,n_elm
        idom = get_elm_subdomain(j, mshdata%elem)
        if (idom == i) then
          k=k+1
          iaux(k) = j
        end if
      end do
      !.
      write (lu_ab,40) iaux(1:k)
    end do
  end if
  !.Escritura de los grupos de nodos
  do i=1,n_dom
    write (lu_ab,50)'N'//achar(i+48)
    k=0 
    do j=1,n_nod
      call get_node_subdomain(j,n_d,dom, mshdata%node)
      if (n_d == 1 .and. dom(1) == i) then 
        k=k+1; jaux(k) = j
      elseif (any(dom(1:n_d) == i)) then
        k=k+1; jaux(k) = j
      end if
    end do
   ! print *,'dominio: ',i,' nodos ',k
    write (lu_ab,40) jaux(1:k)
  end do
  !.
  write (lu_ab,50)'NNDM'
  k=0
  do j=1,n_nod
    call get_node_subdomain(j,n_d,dom, mshdata%node)
    if (n_d > 1) then 
      k=k+1; jaux(k) = j
    end if
  end do
  !.
  write (lu_ab,40) jaux(1:k)
  !.
  write (lu_ab,'(A)') '*STEP, INC=100'
  write (lu_ab,'(A)') 'METIS'
  write (lu_ab,'(A)') '*STATIC'
  write (lu_ab,'(A)') '*END STEP'
  !.
  call  close_unit  (lu_ab)
  return
10 format (I8,',',F12.6,',',F12.6,',',F12.6)
20 format ( I7, 8(',',I7))
30 format ('*ELSET, ELSET= ',A3)  
40 format (I8,',',I8,',',I8,',',I8,',',I8,',',I8,',',I8,',',I8,',',I8,',',I8)
50 format ('*NSET, NSET=',A4)  
end subroutine abaqus_file
!  -----------------------------------------------------------------------------
subroutine distribucion_NodInterface (n_dom, n_nod, mshdata)
!  -----------------------------------------------------------------------------
!  Esta rutina distribuye los nodos de la interface al procesador que menos nodos
!  tiene.
! ------------------------------------------------------------------------------ 
  implicit none
  integer(ip), intent(in) :: n_dom, n_nod
  type(t_mesh)            :: mshdata !.malla
  !.
  integer(ip)             :: f_dn(n_nod)
  integer(ip)             :: dom (n_dom)   !.V.A. dominios a los que pertenece el nodo
  integer(ip)             :: cn_d(n_dom)   !.V.A. contador de nodos en cada dominios
  !.
  integer(ip)             :: i, j, k, n_d, id, jd, kd, n_max

  f_dn = 0; cn_d=0
  do i=1,n_nod
    call get_node_subdomain (i, n_d, dom, mshdata%node) !.Recupero dominio del nodo i
    if (n_d == 1) then !.Senialo y cuento los nodos de cada dominio
      id =dom(n_d); f_dn(i) = id; cn_d(id) = cn_d(id) + 1 
    end if
  end do
  !.
  write (*,10) sum(cn_d)
  write (*,20) (i,cn_d(i),i=1,n_dom)
  write (*,30) n_nod - sum(cn_d)
  !.
!!$  do i=1,n_nod
!!$    if (f_dn(i) == 0) then
!!$      call get_node_subdomain (i, n_d, dom, mshdata%node)
!!$      n_max = n_nod + 1
!!$      do j = 1, n_d
!!$        jd=dom(j)
!!$        if (cn_d(jd) < n_max) then
!!$          n_max=cn_d(jd);
!!$          kd= jd !.dominio
!!$          k = j  !.posicion en el vector dom 
!!$        end if
!!$      end do
!!$      cn_d(kd)= cn_d(kd) + 1
!!$      f_dn(i) = kd;
!!$      call i_swap (dom(1),dom(k))
!!$      call set_node_subdomain (i, n_d, dom, mshdata%node)
!!$    end if
!!$  end do
!!$  write (*,20) (i,cn_d(i),i=1,n_dom)
  !.
  !.Rutina que verifica que al menos un nodos de cada elemento pertenezcan al 
  !.mismo dominio.
  call ver_ndelm_dom(mshdata)
  !.
  return
10 format('>>>>>.Interior nodes :',I8,'. Per domain: ') 
20 format (4(:,' Domain(',I3,') =',2X,I8,', '))
30 format('>>>>>.Boundary nodes:',I8,', distributing.')
end subroutine distribucion_NodInterface

!!$subroutine distribucion_NodInterface (n_dom, n_nod, mshdata)
!!$!  -----------------------------------------------------------------------------
!!$!  Esta rutina distribuye los nodos de la interface al procesador que menos nodos
!!$!  tiene.
!!$! ------------------------------------------------------------------------------ 
!!$  implicit none
!!$  integer(ip), intent(in) :: n_dom, n_nod
!!$  type(t_mesh)            :: mshdata !.malla
!!$  !.
!!$  integer(ip)             :: f_dn(n_nod)
!!$  integer(ip)             :: dom (n_dom)   !.V.A. dominios a los que pertenece el nodo
!!$  integer(ip)             :: cn_d(n_dom)   !.V.A. contador de nodos en cada dominios
!!$  !.
!!$  integer(ip)             :: i, j, k, n_d, id, jd, kd, n_max
!!$
!!$  f_dn = 0; cn_d=0
!!$  do i=1,n_nod
!!$    call get_node_subdomain (i, n_d, dom, mshdata%node) !.Recupero dominio
!!$    if (n_d == 1) then !.Senialo y cuento los nodos de cada dominio
!!$      id =dom(n_d); f_dn(i) = id; cn_d(id) = cn_d(id) + 1 
!!$    end if
!!$  end do
!!$  !.
!!$  write (*,10) sum(cn_d)
!!$  write (*,20) (i,cn_d(i),i=1,n_dom)
!!$  write (*,30) n_nod - sum(cn_d)
!!$  !.
!!$  do i=1,n_nod
!!$    if (f_dn(i) == 0) then
!!$      call get_node_subdomain (i, n_d, dom, mshdata%node)
!!$      n_max = n_nod + 1
!!$      do j = 1, n_d
!!$        jd=dom(j)
!!$        if (cn_d(jd) < n_max) then
!!$          n_max=cn_d(jd);
!!$          kd= jd !.dominio
!!$          k = j  !.posicion en el vector dom 
!!$        end if
!!$      end do
!!$      cn_d(kd)= cn_d(kd) + 1
!!$      f_dn(i) = kd;
!!$      call i_swap (dom(1),dom(k))
!!$      call set_node_subdomain (i, n_d, dom, mshdata%node)
!!$    end if
!!$  end do
!!$  write (*,20) (i,cn_d(i),i=1,n_dom)
!!$  !.
!!$  !.Rutina que verifica que al menos un nodos de cada elemento pertenezcan al 
!!$  !.mismo dominio.
!!$  call ver_ndelm_dom(mshdata)
!!$  !.
!!$  return
!!$10 format('>>>>>.Nodos interiores :',I8,', por dominio: ') 
!!$20 format (4(:,' Dominio(',I3,') =',2X,I8,', '))
!!$30 format('>>>>>.Nodos de frontera:',I8,', repartiendo: ')
!!$end subroutine distribucion_NodInterface

!  -----------------------------------------------------------------------------
subroutine write_binfile (fname, n_dom, meshdata, stepdata)
!  -----------------------------------------------------------------------------
!. n_dom
! ln_d : lista  de nodos del dominio
! le_d : lista  de elementos del dominio
! lp_nd: lista  de numero de propiedades nodales
! lp_ed: lista  de numero de propiedades elementales
! n_d  : numero de dominios al que pertenece el nodo
! dom  : arreglo de dominio
!  -----------------------------------------------------------------------------
  implicit none
  character (len=*), intent(in) :: fname
  integer(ip),       intent(in) :: n_dom
  type(t_mesh)                  :: meshdata
  type(t_step)                  :: stepdata
  !.
  integer(ip)                   :: lu_m, lu_a
  integer(ip)                   :: n_nod,n_elm,n_pnd,n_pel,nn_d,np_nd,ne_d,np_ed
  integer(ip)                   :: i, m, i_err, io_err
  character (len=256)           :: f_name
  !.
  integer(ip), allocatable      :: ln_d(:), le_d(:),lp_nd(:), lp_ed(:)

  n_nod = get_nnod      (meshdata%node) !.Numero de nodos
  n_elm = get_nelm      (meshdata%elem) !.Numero de elementos
  n_pnd = get_numpropnd (meshdata%prnd) !.Numero de propiedades nodo
  n_pel = get_numpropelm(meshdata%prel) !.Numero de propiedades elemento
  !.
  allocate (ln_d(n_nod), le_d(n_elm),lp_nd(n_pnd), lp_ed(n_pel), stat=i_err)
  if (i_err > 0) call w_error (4,'ln_d(:), le_d(:),lp_nd(:), lp_el(:)',1)
  !.
  do i=1,n_dom
    call a_unit(lu_m)   !.Asignacion de numero de unidad 
    call file_name (fname(1:len_trim(fname)),'.bin',i,f_name,m)
    open (unit=lu_m,file = f_name(1:m),status='unknown',iostat=io_err,form='unformatted')
    !.
!    call a_unit(lu_a)   !.Asignacion de numero de unidad 
!    call file_name (fname(1:len_trim(fname)),'.dat',i,f_name,m)
!    open (unit=lu_a,file = f_name(1:m), status='unknown',iostat=io_err)
    !. 
    !nd_prop = get_nd_prop    (i, meshdata%node)
    !print *,'nd_prop ',nd_prop
    call get_nodes_dom_prop (i, meshdata%node, nn_d, np_nd, ln_d, lp_nd)
    !.
    call get_eleme_dom_prop (i, ne_d, np_ed, le_d, lp_ed, meshdata%elem)
    !.
    call bin_write_mshdata (lu_m, nn_d, ne_d, np_nd, np_ed, ln_d, le_d, lp_nd,  &
                            lp_ed, meshdata)
    !.
    call bin_write_stepdata (lu_m, ln_d, le_d, stepdata)
    !.
!    call ascii_write_mshdata (lu_a, nn_d, ne_d, np_nd, np_ed, ln_d, le_d, lp_nd,   &
!                              lp_ed, meshdata)
!    call ascii_write_stepdata(lu_a, ln_d, le_d, stepdata)
    !.
    call close_unit (lu_m);! write ( *, 10) i
!    call close_unit (lu_a);! write ( *, 20) i
  end do
  !.
  write (*,30) n_dom
  !.
  deallocate (ln_d, le_d,lp_nd, lp_ed)
  !.
  return
10 format ('>>>.Mesh and step data binary file has been written for domain:',I4) 
20 format ('>>>.Mesh and step data ascii file has been written for domain:',I4) 
30 format ('>>>.',I3,' binary files have been written')
end subroutine write_binfile
!  -----------------------------------------------------------------------------
subroutine bin_write_mshdata (lu_m, nn_d, ne_d, np_nd, np_ed, ln_d, le_d, lp_nd,   &
                          lp_ed, meshdata)
!  -----------------------------------------------------------------------------
! lu_m : unidad de escritura
! nn_d : numero de nodos del dominio
! ne_d : numero de elementos del dominio
! ln_d : lista  de nodos del dominio
! le_d : lista  de elementos del dominio
! np_nd: numero de propiedades nodales del dominio
! np_ed: numero de propiedades elementales del dominio
! lp_nd: lista  de numero de propiedades nodales
! lp_ed: lista  de numero de propiedades elementales
!  -----------------------------------------------------------------------------
  implicit none
  integer(ip), intent(in)   :: lu_m,nn_d, ne_d, np_nd, np_ed 
  integer(ip), intent(in)   :: ln_d(:), le_d(:), lp_nd(:),lp_ed(:) 
  type(t_mesh)              :: meshdata
  !.

  write (lu_m) '#TITLE'
  write (lu_m) meshdata%title
  !.
  write (lu_m) '#ANALISYSTYPE'
  write (lu_m) meshdata%atype 
  !.
  call bin_write_rsys (lu_m, meshdata%rsys)   !.Sistema de referencia
  !.
  call bin_write_table(lu_m, meshdata%table)  !.Tablas
  !.
  call bin_write_hist (lu_m, meshdata%hist)   !.Historias
  !.
  call bin_write_mat  (lu_m, meshdata%mate)   !.Materiales
  !.
  call bin_write_pnod (lu_m, np_nd, lp_nd, meshdata%prnd) !.Propiedades de nodos
  !.
  call bin_write_prelm (lu_m, np_ed, lp_ed, meshdata%prel)!.Propiedades de elementos
  !.
  call bin_write_nodes (lu_m, nn_d, ln_d, lp_nd, meshdata%node) !.Nodos
  !.
  call bin_write_elm   (lu_m, ne_d, ln_d, le_d, lp_ed, meshdata%elem) !.Elementos
  !.
  call bin_write_grpnod (lu_m, ln_d, meshdata%grpnd) !.Grupo de nodos
  !.
  call bin_write_grpel  (lu_m, le_d, meshdata%grpel) !.Grupo de elementos
  !.
  return
end subroutine bin_write_mshdata
!  -----------------------------------------------------------------------------
subroutine ascii_write_mshdata (lu_m, nn_d, ne_d, np_nd, np_ed, ln_d, le_d, lp_nd,   &
                          lp_ed, meshdata)
!  -----------------------------------------------------------------------------
! lu_m : unidad de escritura
! nn_d : numero de nodos del dominio
! ne_d : numero de elementos del dominio
! ln_d : lista  de nodos del dominio
! le_d : lista  de elementos del dominio
! np_nd: numero de propiedades nodales del dominio
! np_ed: numero de propiedades elementales del dominio
! lp_nd: lista  de numero de propiedades nodales
! lp_ed: lista  de numero de propiedades elementales
!  -----------------------------------------------------------------------------
  implicit none
  integer(ip), intent(in)   :: lu_m,nn_d, ne_d, np_nd, np_ed 
  integer(ip), intent(in)   :: ln_d(:), le_d(:), lp_nd(:),lp_ed(:) 
  type(t_mesh)              :: meshdata
  !.

  write (lu_m,10) '#TITLE'
  write (lu_m,10) meshdata%title
  !.
  write (lu_m,10) '#ANALISYSTYPE'
  write (lu_m,20) meshdata%atype 
  !.
  call ascii_write_rsys (lu_m, meshdata%rsys)   !.Sistema de referencia
  !.
  call ascii_write_table(lu_m, meshdata%table)  !.Tablas
  !.
  call ascii_write_hist (lu_m, meshdata%hist)   !.Historias
  !.
  call ascii_write_mat  (lu_m, meshdata%mate)   !.Materiales
  !.
  call ascii_write_pnod (lu_m, np_nd, lp_nd, meshdata%prnd) !.Propiedades de nodos
  !.
  call ascii_write_prelm (lu_m, np_ed, lp_ed, meshdata%prel)!.Propiedades de elementos
  !.
  call ascii_write_nodes (lu_m, nn_d, ln_d, lp_nd, meshdata%node) !.Nodos
  !.
  call ascii_write_elm   (lu_m, ne_d, ln_d, le_d, lp_ed, meshdata%elem) !.Elementos
  !.
  call ascii_write_grpnod (lu_m, ln_d, meshdata%grpnd) !.Grupo de nodos
  !.
  call ascii_write_grpel  (lu_m, le_d, meshdata%grpel) !.Grupo de elementos
  !.
  return
10 format (A)
20 format (I8)
end subroutine ascii_write_mshdata
!  -----------------------------------------------------------------------------
subroutine bin_write_stepdata (lu_m, ln_d, le_d, stepdata)
!  -----------------------------------------------------------------------------
  implicit none
  integer(ip),intent(in)      :: lu_m
  integer(ip), intent(in)     :: ln_d(:), le_d(:)
  type(t_step)                :: stepdata
  !

  write (lu_m) '#STEP'
  !.
  call bin_write_rstart(lu_m, stepdata%rstrt) !*RESTART
  !.
  call bin_write_prmnd  (lu_m, ln_d, stepdata%prmnd)  !.'*PARAM_NODE'
  !. 
  call bin_write_prmelm (lu_m, le_d, stepdata%prmelm) !.'*PARAM_ELEM'
  !.
  write (lu_m) '*INTEG_SCH', stepdata%i_sch
  !.
  write (lu_m) '*SOLVER',    stepdata%i_sol
  !.
  call bin_write_timeinc(lu_m, stepdata%timeinc)
  !.
  write (lu_m) '*INITIALCOND'
  !.
  call bin_write_stm (lu_m, ln_d, stepdata%stmls) !. '*STIMULUS'
  !.
  write (lu_m) '*BOUNDARY'
  !.
  write (lu_m) '*NODELOAD'
  !.
  write (lu_m) '*ELELOAD'
  !.
  call bin_write_node_out (lu_m,ln_d, stepdata%nd_out)   !.'*NODEOUTPUT'
  !.
  call bin_write_elem_out (lu_m,le_d, stepdata%elm_out)  !.'*ELEMOUTPUT'
  !.
  call bin_write_outputset(lu_m, stepdata%g_out)         !.'*G_OUTPUT'
  !.
  return
end subroutine bin_write_stepdata
!  -----------------------------------------------------------------------------
subroutine ascii_write_stepdata (lu_m, ln_d, le_d, stepdata)
!  -----------------------------------------------------------------------------
  implicit none
  integer(ip),intent(in)      :: lu_m
  integer(ip), intent(in)     :: ln_d(:), le_d(:)
  type(t_step)                :: stepdata
  !

  write (lu_m,10) '#STEP'
  !.
  call ascii_write_rstart (lu_m, stepdata%rstrt) !*RESTART
  !.
  call ascii_write_prmnd  (lu_m, ln_d, stepdata%prmnd)  !.'*PARAM_NODE'
  !. 
  call ascii_write_prmelm (lu_m, le_d, stepdata%prmelm) !.'*PARAM_ELEM'
  !.
  write (lu_m,20) '*INTEG_SCH', stepdata%i_sch
  !.
  write (lu_m,20) '*SOLVER',    stepdata%i_sol
  !.
  call ascii_write_timeinc(lu_m, stepdata%timeinc)
  !.
  write (lu_m,10) '*INITIALCOND'
  !.
  call ascii_write_stm (lu_m, ln_d, stepdata%stmls) !. '*STIMULUS'
  !.
  write (lu_m,10) '*BOUNDARY'
  !.
  write (lu_m,10) '*NODELOAD'
  !.
  write (lu_m,10) '*ELELOAD'
  !.
  call ascii_write_node_out (lu_m,ln_d, stepdata%nd_out)   !.'*NODEOUTPUT'
  !.
  call ascii_write_elem_out (lu_m,le_d, stepdata%elm_out)  !.'*ELEMOUTPUT'
  !.
  call ascii_write_outputset(lu_m, stepdata%g_out)         !.'*G_OUTPUT'
  !.
  return
10 format (A)
20 format (A,2X,I8)
end subroutine ascii_write_stepdata
!  -----------------------------------------------------------------------------
subroutine deallocate_mshdata (meshdata)
!  -----------------------------------------------------------------------------
! lu_m : unidad de escritura
! nn_d : numero de nodos del dominio
! ne_d : numero de elementos del dominio
! ln_d : lista  de nodos del dominio
! le_d : lista  de elementos del dominio
! np_nd: numero de propiedades nodales del dominio
! np_ed: numero de propiedades elementales del dominio
! lp_nd: lista  de numero de propiedades nodales
! lp_ed: lista  de numero de propiedades elementales
!  -----------------------------------------------------------------------------
  implicit none
  type(t_mesh)              :: meshdata
  !.
  integer(ip)               :: i,flg(10), lu_o

  lu_o=6; flg = 0
  !.Liberando memoria del sistema de referencia
  flg( 1) = deallocate_rysrf(meshdata%rsys)
  call mensajes_deallocate (lu_o,'###.Sistema de referencia: ',flg(1))
  !.Liberando memoria de Tablas
  flg( 2) = deallocate_tabla (meshdata%table)
  call mensajes_deallocate (lu_o,'###.Tablas: ',flg( 2))
  !.Liberando memoria Historias
  flg( 3) = deallocate_hist (meshdata%hist)
  call mensajes_deallocate (lu_o,'###.Historias: ',flg( 3))
  !.Liberando memoria Materiales
  flg( 4) = deallocate_mat  (meshdata%mate)
  call mensajes_deallocate (lu_o,'###.Materiales: ',flg( 4))
  !.Liberando memoria Propiedades de nodos
  flg( 5) = deallocate_pnod (meshdata%prnd)
  call mensajes_deallocate (lu_o,'###.Propiedades de nodos: ',flg( 5))
  !.Liberando memoria Propiedades de elementos
  flg( 6) = deallocate_prelm (meshdata%prel)
  call mensajes_deallocate (lu_o,'###.Propiedades de elementos: ',flg( 6))
  !.Liberando memoria Nodos
  flg( 7) = deallocate_nodes (meshdata%node) 
  call mensajes_deallocate (lu_o,'###.Nodos: ',flg( 7))
  !.Liberando memoria Elementos
  flg( 8) = deallocate_elm   (meshdata%elem) 
  call mensajes_deallocate (lu_o,'###.Elementos: ',flg( 8))
  !.Liberando memoria Grupo de nodos
  flg( 9) = deallocate_grpnod (meshdata%grpnd)
  call mensajes_deallocate (lu_o,'###.Grupos de nodos: ',flg( 9))
  !.Liberando memoria Grupo de elementos
  flg(10) = deallocate_grpel  (meshdata%grpel)
  call mensajes_deallocate (lu_o,'###.Grupos de elementos: ',flg(10))
  !.

  if (any(flg == 1)) call w_error (65,'mensajes_deallocate', 1) 
  !.
  return
end subroutine deallocate_mshdata
! ------------------------------------------------------------------------------
subroutine mensajes_deallocate (lu_o,str, flg)
! ------------------------------------------------------------------------------
! flg = -1 nunca fue dimensionado
! flg =  0 memoria liberada
! flg =  1 problemas al liberar memoria  
  implicit none
  integer(ip),       intent(in)  :: lu_o, flg
  character (len=*), intent(in)  :: str
  !.

  select case(flg)
  case (-1); write (lu_o,10) str
  case ( 0); write (lu_o,20) str
  case ( 1); write (lu_o,30) str
  case default
  end select
  return
10 format ('###.Memory not used : ', A)
20 format ('###.Memory freed    : ', A)
30 format ('###.Error  freeing  : ', A)
end subroutine mensajes_deallocate
!  -----------------------------------------------------------------------------
subroutine deallocate_stepdata (stepdata)
!  -----------------------------------------------------------------------------
  implicit none
  type(t_step)                :: stepdata
  !
  integer(ip)                 :: lu_o, flg(7)

  lu_o=6; flg = 0
 ! print *,' !.Liberando memoria de parametro de nodos'

  flg( 1) = deallocate_prmnd (stepdata%prmnd)
  call mensajes_deallocate (lu_o,'###.Nodal mask: ',flg(1))
  !print *,' !.Liberando memoria de parametro de elementos'
  flg( 2) = deallocate_prmelm (stepdata%prmelm)
  call mensajes_deallocate (lu_o,'###.Element mask: ',flg(2))
  !.
  write (lu_o,'(A)') '>>Not Dimensioned '//'*INITIALCOND'
  !.
  flg( 3) = deallocate_rstrt (stepdata%rstrt)
  call mensajes_deallocate (lu_o,'###.Restart : ',flg(3))
  !.
  flg( 4) = deallocate_stmls (stepdata%stmls)
  call mensajes_deallocate (lu_o,'###.Stimulus : ',flg(4))
  !.
  write (lu_o,'(A)') '>>Not Dimensioned '//'*BOUNDARY'
  !.
  write (lu_o,'(A)') '>>Not Dimensioned '//'*NODELOAD'
  !.
  write (lu_o,'(A)') '>>Not Dimensioned '//'*ELELOAD'
  !.
  flg( 5) = deallocate_node_out (stepdata%nd_out)
  call mensajes_deallocate (lu_o, '*NODEOUTPUT', flg(5))
  !.
  flg( 6) = deallocate_elem_out (stepdata%elm_out)
  call mensajes_deallocate (lu_o, '*ELEMOUTPUT', flg(6))
  !.
  flg( 7) =  deallocate_outputset(stepdata%g_out)
  call mensajes_deallocate (lu_o,' *G_OUTPUT ',flg(7))
  !.
  return
end subroutine deallocate_stepdata
! ------------------------------------------------------------------------------
! $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
! ------------------------------------------------------------------------------
end module mod_preprocesor
! ------------------------------------------------------------------------------
! -------------------------------- F I N ---------------------------------------
! ------------------------------------------------------------------------------
