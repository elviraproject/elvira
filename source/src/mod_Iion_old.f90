! Test-EM
module mod_Iion
! -------------------------------------------------------------------------------
  use mod_precision
!  use mod_error
  use mod_rm
  use mod_tentusscher
  use mod_lr2k
  use mod_FK
  use mod_IZ
  use mod_nygren
  use mod_PKFstewart
  use mod_grandi
  use mod_carro
  use mod_grandiHF
  use mod_gpv
 
  implicit none

contains
! -------------------------------------------------------------------------------
subroutine get_Iion (t_cel, dt, Istm, Iax, Vo, Vp, Iion, p_rm, v_ve,v_cr)
! -------------------------------------------------------------------------------
! t_cell: cell type. Not all models required this parameter
! dt    : time increment
! Istm  : Stimulation current
! Iax   : Axial current
! Vo    : Potential
! Vp    : Potential partial time derivative
! Iion  : Ionic current
! p_rm  : vector with modifiable model parameters
! v_ve  : vector with electric model state variables
! v_cr  : vector with model currents
! -------------------------------------------------------------------------------
  implicit none
  integer(ip), intent(in)    :: t_cel
  real(rp),    intent(in)    :: dt, Istm, Iax, Vo, Vp
  real(rp),    intent(out)   :: Iion
  real(rp),    intent(in)    :: p_rm(:)
  real(rp),    intent(inout) :: v_ve(:)
  real(rp),    intent(out)   :: v_cr(:)


  select case(t_cel)
  case(-1)    !.Roger and McCulloc Model
    call IZ_A_P01 (Vo, dt, Iion, p_rm)
  case(0)    !.Roger and McCulloc Model
    call RMC_A_P01 (Vo, dt, v_ve(1), Iion, p_rm)
  case(1:3)   !.Ten Tusscher Model
    call TenTusscher_A_P01 (t_cel,dt,Istm,Iax,Vo,Iion,p_rm, v_ve, v_cr)
  case(4:6)   !.Luo Rudy Model
    call LR2K_A_P01    (dt, Vp, Vo, Iion, p_rm, v_ve, v_cr)
  case(7:9)   !.Fenton Karma Model
    call FK_A_P01     (dt, Vo, Iion, p_rm, v_ve, v_cr)
  case(10)    !.Nygren Model
    call Nygren_A_P01 (dt, Vo, Iion, p_rm, v_ve, v_cr)
  case(11)    !.Steward_PKF Model
    call PKFstewart_A_P01 (t_cel, dt, Istm, Vo, Iion, p_rm, v_ve, v_cr)
  case(12:13) !.Grandi Model
    call Grandi_A_P01 (t_cel, dt, Vo, Iion, p_rm, v_ve, v_cr)
  case(14:15) !.Carro Model
    call Carro_A_P01 (t_cel, dt, Vo, Iion, p_rm, v_ve, v_cr)
  case(16:17) !.Heart Failure Grandi Model
    call GrandiHF_A_P01 (t_cel, dt, Vo, Iion, p_rm, v_ve, v_cr)
  case(18)    !.GPV Model
    call Gpv_A_P01 (t_cel, dt, Vo, Iion, p_rm, v_ve, v_cr)
  case default
  end select
  !.
  return
end subroutine Get_Iion
! -------------------------------------------------------------------------------
function get_numstatevar (t_cel)  result(nvar)
! -------------------------------------------------------------------------------
  implicit none
  integer(ip), intent(in)    :: t_cel
  integer(ip)                :: nvar

  select case(t_cel)
  case(-1)    !.Ischemic Zone Model
    nvar = nvar_iz  !1
  case(0)     !.Roger and McCulloc Model
    nvar = nvar_rmc !1
  case(1:3)   !.Ten Tusscher Model
    nvar = nvar_tt  !19
  case(4:6)   !.Luo Rudy Model
    nvar = nvar_lr  !27
  case(7:9)   !.Fenton Karma Model
    nvar = nvar_fk  !3
  case(10)    !.Nygren Model
    nvar = nvar_ny  !35
  case(11)    !.Purkinje Stewart Model
    nvar = nvar_pkf !20
  case(12:13) !.Grandi Model
    nvar = nvar_gr
  case(14:15) !.Carro Model
    nvar = nvar_crr
  case(16:17) !.Grandi Model Heart Failure
    nvar = nvar_grHF
  case(18)    !.GPV Model
    nvar = nvar_gpv
  case default
    nvar =  0 
  end select
  !.
  return
end function get_numstatevar
! -------------------------------------------------------------------------------
function get_numcur (t_cel)  result(ncur)
! -------------------------------------------------------------------------------
  implicit none
  integer(ip), intent(in)    :: t_cel
  integer(ip)                :: ncur

  select case(t_cel)
  case(-1)    !.Ischemic Zone Model
    ncur = ncur_iz  ! 0
  case(0)     !.Roger and McCulloc Model
    ncur = ncur_rmc ! 0
  case(1:3)   !.Ten Tusscher Model
    ncur = ncur_tt  !14
  case(4:6)   !.Luo Rudy Model
    ncur = ncur_lr  !21
  case(7:9)   !.Fenton Karma Model
    ncur =  ncur_fk !4
  case(10)    !.Nygren Model
    ncur = ncur_ny  !19
  case(11)    !.Purkinje Stewart Model
    ncur = ncur_pkf !16
  case(12:13) !.Grandi Model
    ncur = ncur_gr
  case(14:15) !.Carro Model
    ncur = ncur_crr
  case(16:17) !.Grandi Model Heart Failure
    ncur = ncur_grHF
  case(18)    !.GPV Model
    ncur = ncur_gpv
  case default
    ncur =  0 
  end select
  !.
  return
end function get_numcur
! ------------------------------------------------------------------------------
function get_ic_em(t_cel,n_var) result (v_me)
! ------------------------------------------------------------------------------ 
  implicit none
  integer(ip), intent(in)  :: t_cel, n_var
  real(rp)                 :: v_me(n_var)

  select case(t_cel)
  case(-1)    !.Ischemic Zone Model
    v_me(1:n_var) = 0.0
  case(0)    !.Roger and McCulloc Model
    v_me(1:n_var) = 0.0
  case(1:3)   !.Ten Tusscher Model
    v_me(1:n_var) = ic_TenTusscher(t_cel) 
  case(4:6)   !.Luo Rudy Model
    v_me(1:n_var) = ic_lr2k(t_cel) 
  case(7:9)   !.Fenton Karma Model
    v_me(1:n_var) = ic_FK(t_cel) 
  case(10)    !.Nygren Model
    v_me(1:n_var) = ic_Nygren(t_cel)
  case(11)    !.Purkinje Stewart  Model
    v_me(1:n_var) = ic_PKFstewart(t_cel)
  case(12:13) !.Grandi Model
    v_me(1:n_var) = ic_Grandi(t_cel)
  case(14:15) !.Carro Model
    v_me(1:n_var) = ic_Carro(t_cel)
  case(16:17) !.Grandi Model Hear Failure
    v_me(1:n_var) = ic_GrandiHF(t_cel)
  case(18)    !.GPV Model
    v_me(1:n_var) = ic_Gpv(t_cel)
  case default
    write (*,10) t_cel ; stop
  end select
  !.
  return
10 format('###.Error en get_ic_em, t_cel = ',I6,' no implementado')
end function get_ic_em
! ------------------------------------------------------------------------------
subroutine get_parameter_nd (t_cel, p_rm, np)
! ------------------------------------------------------------------------------
  implicit none
  integer(ip), intent(in)  :: t_cel
  real(rp),    intent(out) :: p_rm(:)
  integer(ip), intent(out) :: np


  select case(t_cel)
  case(-1)    !.Roger and McCulloc Model
    np= np_iz;   p_rm(1:np) = get_parameter_IZ   (t_cel)
    !np= 3;   p_rm(1:np) = get_parameter_IZ   (t_cel)
  case(0)    !.Roger and McCulloc Model
    np= np_rmc;   p_rm(1:np) = get_parameter_RM   (t_cel)
    !np= 6;   p_rm(1:np) = get_parameter_RM   (t_cel)
  case(1:3)   !.Ten Tusscher Model
    np= np_tt;  p_rm(1:np) = get_parameter_TT   (t_cel)
    !np= 21;  p_rm(1:np) = get_parameter_TT   (t_cel)
  case(4:6)   !.Luo Rudy Model
    np= np_lr;  p_rm(1:np) = get_parameter_LR2K (t_cel) 
    !np= 22;  p_rm(1:np) = get_parameter_LR2K (t_cel) 
  case(7:9)   !.Fenton Karma Model
    np= np_fk;  p_rm(1:np) = get_parameter_FK   (t_cel) 
    !np= 30;  p_rm(1:np) = get_parameter_FK   (t_cel) 
  case(10)    !.Nygren Model
    np= np_ny;  p_rm(1:np) = get_parameter_NYG  (t_cel)
    !np= 13;  p_rm(1:np) = get_parameter_NYG  (t_cel)
  case(11)    !.Purkinje Stewart Model
    np= np_pkf;  p_rm(1:np) = get_parameter_PKF  (t_cel)
    !np= 19;  p_rm(1:np) = get_parameter_PKF  (t_cel)
  case(12:13)    !. Grandi Model
    np= np_gr;  p_rm(1:np) = get_parameter_GR   (t_cel)
  case(14:15)    !. Carro Model
    np= np_crr;  p_rm(1:np) = get_parameter_CRR (t_cel)
  case(16:17)    !. Grandi Model Heart Failure
    np= np_grHF;  p_rm(1:np) = get_parameter_GRHF  (t_cel)
  case(18)       !. GPV Model 
    np= np_gpv;  p_rm(1:np) = get_parameter_Gpv  (t_cel)
  case default
    write (*,10) t_cel ; stop
  end select
  return
10 format('###.Error en get_parameter_nd, t_cel = ',I6,' no implementado')
end subroutine get_parameter_nd
! ------------------------------------------------------------------------------
function initcond_elecmodel (t_cel) result(Vi)
! ------------------------------------------------------------------------------
  implicit none
  integer(ip), intent(in) :: t_cel
  real(rp)                :: Vi

  select case (t_cel)
  case(-1);    Vi = Vi_IZ       !.Ischemic Zone;
  case(0);     Vi = Vi_RMC      !.Roger y McCulloc;
  case(1:3);   Vi = Vi_TT       !.Ten Tusscher
  case(4:6);   Vi = Vi_LR       !.Luo Rudy
  case(7:9);   Vi = Vi_FK       !.Fenton Karma
  case(10);    Vi = Vi_NY       !.Nygren
  case(11);    Vi = Vi_PKF      !.Purkinje Stewart
  case(12);    Vi = Vi_GR_end   !.Grandi ENDO
  case(13);    Vi = Vi_GR_epi   !.Grandi EPI
  case(14);    Vi = Vi_CRR_end  !.Carro ENDO
  case(15);    Vi = Vi_CRR_epi  !.Carro EPI
  case(16);    Vi = Vi_GRHF_end !.Grandi Heart Failure ENDO
  case(17);    Vi = Vi_GRHF_epi !.Grandi Heart Failure EPI
  case(18);    Vi = Vi_GPV   !.GPV
  case default
    write (*,10) t_cel ; stop
  end select
  return
10 format('###.Error en initcond_elecmodel, t_cel = ',I6,' no implementado')
end function initcond_elecmodel
! -------------------------------------------------------------------------------
subroutine flag_opttemp (t_cel, ite,  Jion, Irel, dvdt, dt, stp, flg_c, dt_c)
! -------------------------------------------------------------------------------
! i    : numero del nodo
! ite  : numero de la iteracion
! Jion : valor de la corriente de estimulo
! dvdt : valor de la derivada temporal en el paso anterios
! dt   : paso temporal
! stp  : numero de pasos que llevo sin calcular el Iion
! flg_c: bandera de calculo, se calcula o no el Jion 
! dt_c : con que paso de tiempo se calcula el Jion
! -------------------------------------------------------------------------------
  implicit none
  !.
  integer(ip), intent(in)     :: t_cel, ite
  real(rp),    intent(in)     :: Jion, Irel, dvdt, dt
  integer(ip), intent(inout)  :: stp
  logical                :: flg_c
  real(rp), intent(out)  :: dt_c

  !.
  select case (t_cel)
  case(-1) !.Ischemic Zone;
    dt_c = dt; stp  = 1
  case(0) !.Roger y McCulloc;
    dt_c = dt; stp  = 1
  case(1:3) !.Ten Tusscher
    flg_c = (abs(dvdt) >= m_dvdtTT).or.(stp == m_stpTT).or.(ite < m_iteTT).or.(Jion > 0);
  case(4:6) !.Luo Rudy
    flg_c = (Irel > m_IrelLR).or.(abs(dvdt) >= m_dvdtLR).or.(stp == m_stpLR).or.(ite < m_iteLR)
  case(7:9) !.Fenton Karma
    flg_c = (abs(dvdt) >= m_dvdtFK).or.(stp == m_stpFK).or.(ite < m_iteFK).or.(Jion > 0);
  case(10)  !.Nygren
    flg_c = .true.
    dt_c = dt; stp  = 1
   ! flg_c = (abs(dvdt) >= m_dvdtNY).or.(stp == m_stpNY).or.(ite < 5).or.(Jion > 0);
  case(11)  !.Stewart
    flg_c = .true.
    dt_c = dt; stp  = 1
   ! flg_c = (abs(dvdt) >= m_dvdtPKF).or.(stp == m_stpPKF).or.(ite < 5).or.(Jion > 0);
  case(12:13)  !.Grandi
    flg_c = .true.
    dt_c = dt; stp  = 1
   ! flg_c = (abs(dvdt) >= m_dvdtGR).or.(stp == m_stpGR).or.(ite < 5).or.(Jion > 0);
  case(14:15)  !.Carro
    flg_c = .true.
    dt_c = dt; stp  = 1
   ! flg_c = (abs(dvdt) >= m_dvdtCRR).or.(stp == m_stpCRR).or.(ite < 5).or.(Jion > 0);
  case(16:17)  !.Grandi Heart Failure
    flg_c = .true.
    dt_c = dt; stp  = 1
   ! flg_c = (abs(dvdt) >= m_dvdtGRHF).or.(stp == m_stpGRHF).or.(ite < 5).or.(Jion > 0);
  case(18)    !.GPV
    flg_c = .true.
    dt_c = dt; stp  = 1
   ! flg_c = (abs(dvdt) >= m_dvdtGRHF).or.(stp == m_stpGRHF).or.(ite < 5).or.(Jion > 0);
  case default
    write (*,10) t_cel ; stop
  end select
  !.
  if (flg_c) then
    dt_c = float(stp)*dt; stp  = 1
  else
    stp = stp + 1  !.Incremento de contador 
  end if
  return
10 format('###.Error en flag_opttemp, t_cel = ',I6,' no implementado')
end subroutine flag_opttemp
! ------------------------------------------------------------------------------
subroutine PrintNumCellEM (str,t_cel,nc)
! ------------------------------------------------------------------------------
  implicit none
  character(len=*), intent(in) :: str
  integer(ip), intent(in)      :: t_cel, nc

  select case(t_cel)
  case(-1)    !.Roger and McCulloc Model
    write(*,10)                  str,'---Ischemic Zone Model:',nc
  case(0)    !.Roger and McCulloc Model
    write(*,10)                  str,'---Roger and McCulloc Model:',nc
  case(1:3)   !.Ten Tusscher Model
    if (t_cel == 1)  write(*,10) str,'---ENDO, Ten Tusscher Model:',nc
    if (t_cel == 2)  write(*,10) str,'---MID,  Ten Tusscher Model:',nc
    if (t_cel == 3)  write(*,10) str,'---EPI,  Ten Tusscher Model:',nc
  case(4:6)   !.Luo Rudy Model
    if (t_cel == 4)  write(*,10) str,'---ENDO, Luo Rudy Model:    ',nc
    if (t_cel == 5)  write(*,10) str,'---MID,  Luo Rudy Model:    ',nc
    if (t_cel == 6)  write(*,10) str,'---EPI,  Luo Rudy Model:    ',nc
  case(7:9)   !.Fenton Karma Model
    if (t_cel == 7)  write(*,10) str,'---ENDO, Fenton Karma Model:',nc
    if (t_cel == 8)  write(*,10) str,'---MID,  Fenton Karma Model:',nc
    if (t_cel == 9)  write(*,10) str,'---EPI,  Fenton Karma Model:',nc
  case(10)    !.Nygren Model
    write(*,10)                  str,'---Nygren Model:            ',nc
  case(11)    !.Purkinje Stewart Model
    write(*,10)                  str,'---Purkinje Stewart Model:  ',nc
  case(12:13) !.Grandi Model
    if (t_cel == 12)  write(*,10) str,'---ENDO, Grandi Model:',nc
    if (t_cel == 13)  write(*,10) str,'---EPI,  Grandi Model:',nc
  case(14:15) !.Grandi Model
    if (t_cel == 14)  write(*,10) str,'---ENDO, Carro Model (GrandiMdf):',nc
    if (t_cel == 15)  write(*,10) str,'---EPI,  Carro Model (GrandiMdf):',nc
  case(16:17) !.Grandi Model Heart Failure
    if (t_cel == 16)  write(*,10) str,'---ENDO, Grandi Model Heart Failure:',nc
    if (t_cel == 17)  write(*,10) str,'---EPI,  Grandi Model Heart Failure:',nc
  case(18)    !.GPV Model
    write(*,10)                  str,'---Gpv Model:            ',nc
  case default
    write(*,20)                  str,'---Unknow cell type: ', t_cel, nc
  end select
  return
10 format (A,A,I8)
20 format (A,A,I3,I8)
end subroutine PrintNumCellEM
  
end module mod_Iion
