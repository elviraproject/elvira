module mod_material
!  ----------------------------------------------------------------------------
  use mod_precision
  use mod_filename
  use mod_error
!  --------------------------------  MATERIALS --------------------------------
!  ----------------------------------------------------------------------------
  implicit none
  type t_mat
    private
    integer(ip)           :: n_mat   ! Material Number
    integer(ip)           :: p_srf   ! pointer to reference system (not used)
    integer(ip)           :: p_his   ! pointer to material history (not used)
    integer(ip)           :: tipo    ! Type of material (not used)
    real(rp), allocatable :: data(:) ! Array of material parameters
 !   real(rp), pointer     :: data(:) ! Array of material parameters
  end type t_mat
!  -----------------------------------------------------------------------------
  type, public :: t_obmat 
    private
    integer(ip)             :: n_mat=0 ! Total number fo materials defined
    type(t_mat),allocatable :: mat(:)
!    type(t_mat),pointer     :: mat(:)
  end type t_obmat
! ------------------------------------------------------------------------------
contains
! ------------------------------------------------------------------------------
! ------------------------------------------------------------------------------
subroutine read_material (lu_m, arch_d, strout, mate)
! ------------------------------------------------------------------------------
! This subroutine reads material information from unit lu_m
! ------------------------------------------------------------------------------
  implicit none
  integer(ip), intent(in)      :: lu_m
  character (len=*), intent(in):: arch_d, strout
  type(t_obmat)                :: mate
  !
  integer(ip)                  :: lu_lec, i, io_err, i_err, idum


  lu_lec = lu_m; call unitnumber (arch_d, strout, lu_lec, io_err)
  if (io_err > 0) call w_error (32,'subroutine lec_mat',1)
  !.
  read (lu_lec, *,iostat = io_err) mate%n_mat
  if (io_err > 0) call w_error (16,'mate%n_mat',1)
  !.
  allocate (mate%mat(mate%n_mat), stat=i_err)
  if (i_err > 0 ) call w_error (17,'mate%mat(mate%n_mat)',1)
  !.
  do i=1,mate%n_mat
    read (lu_lec, *, iostat = io_err) idum,idum,idum,idum,idum
    if (io_err > 0) call w_error (18,'idum,idum,idum',1)
    !.
    allocate (mate%mat(i)%data(idum),stat=i_err)
    if (i_err > 0 ) call w_error (17,'mate%mat(i)%data(:)',1)
    !.
    backspace(lu_lec)
    read ( lu_lec, *, iostat = io_err) mate%mat(i)%n_mat, mate%mat(i)%p_srf,  &
                                       mate%mat(i)%p_his, mate%mat(i)%tipo,   &
                                       idum,mate%mat(i)%data
    if (io_err > 0) call w_error (18,'mate%mat(i)',1)
  end do
  if (lu_lec /= lu_m) call close_unit(lu_lec)
  !.
  write(std_o, 10) mate%n_mat;  write(lu_log, 10) mate%n_mat; 
  idum=size(mate%mat(1)%data)
  write (std_o, 20) mate%mat(1)%n_mat, mate%mat(1)%p_srf,  &
              mate%mat(1)%p_his, mate%mat(1)%tipo,   &
              (mate%mat(1)%data(i),i=1,idum)
  write (std_o, 20) mate%mat(mate%n_mat)%n_mat, mate%mat(mate%n_mat)%p_srf,  &
              mate%mat(mate%n_mat)%p_his, mate%mat(mate%n_mat)%tipo,   &
              (mate%mat(mate%n_mat)%data(i),i=1,idum)
  !.
  return
10 format ('###.Materials read                                : ',I8)
20 format (4(I4,1X),10(E12.3));
end subroutine read_material
!  -----------------------------------------------------------------------------
!  -----------------------------------------------------------------------------
function get_I_Cm(p_mat,mate) result(I_Cm)
!  -----------------------------------------------------------------------------
  implicit none
  integer(ip), intent(in)    :: p_mat
  type(t_obmat),intent(in)   :: mate
  !.
  real(rp)                   :: I_Cm

  I_Cm = 1.0/mate%mat(p_mat)%data(3)
  return
end function get_I_Cm
!  -----------------------------------------------------------------------------
!  -----------------------------------------------------------------------------
subroutine get_materialprop(i,mate,data, n_mat, p_srf, p_his, tipo) 
!  -----------------------------------------------------------------------------
  implicit none
  integer(ip),   intent(in)    :: i
  type(t_obmat), intent(in)    :: mate
  !.
  integer(ip),  intent(out), optional :: n_mat, p_srf, p_his, tipo
  real(rp),     intent(out), optional :: data(:)
  !.
  integer(ip)                         :: n

  if (present(data)) then
    n = size(mate%mat(i)%data); 
    data(1:n)= mate%mat(i)%data(1:n)
  end if
  !.
  if (present(n_mat)) n_mat = mate%mat(i)%n_mat
  if (present(p_srf)) p_srf = mate%mat(i)%p_srf
  if (present(p_his)) p_his = mate%mat(i)%p_his 
  if (present(tipo )) tipo  = mate%mat(i)%tipo
  return
end subroutine get_materialprop
!  -----------------------------------------------------------------------------
!  -----------------------------------------------------------------------------
subroutine bin_write_mat (lu_m, mate)
! ------------------------------------------------------------------------------
! This subroutine writes material information to unit lu_m (binary)
! ------------------------------------------------------------------------------
  implicit none
  integer(ip), intent(in)    :: lu_m
  type(t_obmat)              :: mate
  !
  integer(ip)                :: i


  write (lu_m) '#MATERIAL', mate%n_mat
  !.
  do i=1,mate%n_mat
    write (lu_m) mate%mat(i)%n_mat, mate%mat(i)%p_srf,                          &
                 mate%mat(i)%p_his, mate%mat(i)%tipo, size(mate%mat(i)%data)
    write (lu_m) mate%mat(i)%data
  end do
  return
end subroutine bin_write_mat
!  -----------------------------------------------------------------------------
!  -----------------------------------------------------------------------------
subroutine ascii_write_mat (lu_m, mate)
! ------------------------------------------------------------------------------
! This subroutine writes material information to unit lu_m (ascii)
! ------------------------------------------------------------------------------
  implicit none
  integer(ip), intent(in)    :: lu_m
  type(t_obmat)              :: mate
  !
  integer(ip)                :: i


  write (lu_m,10) '#MATERIAL', mate%n_mat
  !.
  do i=1,mate%n_mat
    write (lu_m,20) mate%mat(i)%n_mat, mate%mat(i)%p_srf,                          &
                    mate%mat(i)%p_his, mate%mat(i)%tipo, size(mate%mat(i)%data)
    write (lu_m,30) mate%mat(i)%data
  end do
  return
10 format (A,2X,I3)
20 format (5(2X,I3))
30 format (10(:,2X,F12.6))
end subroutine ascii_write_mat
! ------------------------------------------------------------------------------
! ------------------------------------------------------------------------------
subroutine bin_read_mat (lu_m, mate)
! ------------------------------------------------------------------------------
! This subroutine reads material information from binary files
! ------------------------------------------------------------------------------
  implicit none
  integer(ip), intent(in)    :: lu_m
  type(t_obmat)              :: mate
  !
  character (len=9)          :: str
  integer(ip)                :: i, io_err, i_err, n_dat

  read (lu_m, iostat=io_err) str, mate%n_mat
  if (io_err > 0) call w_error (16,'mate%n_mat',1)
  !.
  allocate (mate%mat(mate%n_mat), stat=i_err)
  if (i_err > 0 ) call w_error (17,'mate%mat(mate%n_mat)',1)
  !.
  do i=1,mate%n_mat
    read (lu_m, iostat = io_err) mate%mat(i)%n_mat, mate%mat(i)%p_srf,          &
                                 mate%mat(i)%p_his, mate%mat(i)%tipo, n_dat
    if (io_err > 0) call w_error (18,' mate%mat(i)%n_mat, p_srf , ...',1)
    !.
    allocate (mate%mat(i)%data(n_dat),stat=i_err)
    if (i_err > 0 ) call w_error (17,'mate%mat(i)%data(n_dat)',1)
    !.
    read (lu_m, iostat = io_err) mate%mat(i)%data
    !.
    if (io_err > 0) call w_error (18,'mate%mat(i)%data',1)
  end do
  !.
  return
end subroutine bin_read_mat
!  -----------------------------------------------------------------------------
function deallocate_mat (mate) result(flg)
!  -----------------------------------------------------------------------------
  implicit none
  type(t_obmat)              :: mate
  integer(ip)                :: flg
  !.
  integer(ip)                :: i_err, i, istat


  if (allocated(mate%mat)) then
!  if (associated(mate%mat)) then
    i_err = 0
    do i=1,mate%n_mat
      deallocate (mate%mat(i)%data, stat=istat)
      if (istat /= 0) i_err = 1
    end do
    deallocate (mate%mat, stat=istat)
    if (istat == 0) flg = 0 
    if ((istat /= 0).or.(i_err == 1)) flg = 1
  else
    flg = -1
  end if
  return
end function deallocate_mat
!  -----------------------------------------------------------------------------
end module mod_material
!  -----------------------------------------------------------------------------
