!  -----------------------------------------------------------------------------
module mod_meshdata
!  -----------------------------------------------------------------------------
  use mod_precision
  use mod_filename
  use mod_error
  use mod_readcir
  use mod_sysref
  use mod_history
  use mod_table
  use mod_material
  use mod_prop_nd
  use mod_prop_elm
  use mod_node
  use mod_elements
  use mod_group_nd
  use mod_group_elm
!  -----------------------------------------------------------------------------
  implicit none
  type, public:: t_mesh
    character(len=256)     :: title    !.titulo
    character(len=256)     :: fn_msh   !.nombre del archivo de la malla
    character(len=256)     :: fn_pst   !.nombre del archivo de postroceso
    integer(ip)            :: atype    !.tipo de analisis
    type(t_obsrf)          :: rsys     !.sistema de referencia
    type(t_obtab)          :: table    !.tablas
    type(t_obhis)          :: hist     !.historias
    type(t_obmat)          :: mate     !.materiales
    type(t_obpnd)          :: prnd     !.propiedad en los nodos
    type(t_obpel)          :: prel     !.propiedad en los elementos
    type(t_obnd)           :: node     !.nodos
    type(t_obelm)          :: elem     !.elementos
    type(t_obgnd)          :: grpnd    !.grupo de nodos
    type(t_obgel)          :: grpel    !.grupo de elementos
  end type t_mesh
!  -----------------------------------------------------------------------------
contains
!  -----------------------------------------------------------------------------
subroutine file_open (lu_msh, fn_msh)
!  -----------------------------------------------------------------------------
  implicit none
  integer(ip), intent(out)     :: lu_msh
  character(len=*),intent(in)  :: fn_msh
  !.
  integer(ip)                  :: m, io_err
  !

  ! --------- Apertura del archivo de la malla ----------------------------------
  m=len_trim(fn_msh)
  call a_unit(lu_msh) !.Asignacion de numero de unidad
  open (unit=lu_msh,file=fn_msh(1:m),status='old',iostat=io_err)
  if (io_err > 0) call w_error(8 ,fn_msh(1:m),1)
  return
end subroutine file_open
!  -----------------------------------------------------------------------------  
subroutine get_meshdata(lu_msh, fn_msh, mshdata)
!  -----------------------------------------------------------------------------
  implicit none
  integer(ip),intent(in)      :: lu_msh
  character(len=*),intent(in) :: fn_msh
  type(t_mesh)                :: mshdata
  !.
  character(len=256)          :: strout
  integer(ip)                 :: io_err
  logical                     :: flag
  !.

  write (*,10); call write_n_str(6, 80,'='); 
  write (*,20); call write_n_str(6, 80,'=');
  !.
  do
    call srch_lec (lu_msh,'#',strout, flag, io_err)
    if (flag) then
      if     (index(strout,'#TITLE')        == 1) then !.Lectura del titulo
        !.
        read (lu_msh,'(A)',iostat=io_err) mshdata%title
        if (io_err > 0) call w_error (1 ,'mshdata%title',0)
        write(     *,30) mshdata%title(1:len_trim(mshdata%title))
        write(lu_log,30) mshdata%title(1:len_trim(mshdata%title))
        !.
      elseif (index(strout,'#ANALISYSTYPE') == 1) then !.Lectura del tipo de analisis
        !.
        read (lu_msh,*,iostat=io_err) mshdata%atype 
        if (io_err > 0) call w_error (1 ,'n_nod, n_tn, ndim',0)
        write(*,40) mshdata%atype; write(lu_log,40) mshdata%atype;
        !.
      elseif (index(strout,'#REFSYSTEM')    == 1) then !.Lectura de sist. referencia
        !.
        call lec_sysref(lu_msh, mshdata%rsys)
        !.
      elseif (index(strout,'#TABLE')        == 1) then !.Lectura de tablas
        !.
        call lec_tabla (lu_msh, mshdata%table)
        !.
      elseif (index(strout,'#HISTORY')      == 1) then !.Lectura de historias
        !.
        call read_hist(lu_msh, mshdata%hist)
        !.
      elseif (index(strout,'#MATERIAL')     == 1) then !.Lectura de material
        !.
        call read_material (lu_msh, fn_msh, strout, mshdata%mate)
        !.
      elseif (index(strout,'#PROP_NOD')     == 1) then !.Lectura de prop. en los nodos
        !.
        call lec_pnod (lu_msh, fn_msh, strout, mshdata%prnd)
        !.
      elseif (index(strout,'#PROP_ELEM')    == 1) then !.Lectura de prop. en los elementos
        !.
        call lec_prelm (lu_msh, fn_msh, strout, mshdata%prel)
        !.
      elseif (index(strout,'#NODES')        == 1) then !.Lectura de nodos
        !.
        call lec_node (lu_msh, fn_msh, strout, mshdata%node)
        !.
      elseif (index(strout,'#ELEMENTS')     == 1) then !.Lectura de  elementos
        !.
        call lec_elem (lu_msh, fn_msh, strout, mshdata%elem)
        !.
      elseif (index(strout,'#GRPNODE')      == 1) then !.Lectura de grupo de nodos
        !.
        call lec_grpnod (lu_msh, fn_msh, strout, mshdata%grpnd)
        !.
      elseif (index(strout,'#GRPELEM')      == 1) then !.Lectura de grupo de elementos
        !.
        call lec_grpelm (lu_msh, fn_msh, strout, mshdata%grpel)
        !.
      elseif (index(strout,'#STEP')         == 1) then !.Lectura de step. Sale
        backspace(lu_msh); 
        write(*,'(A)')
        exit
      end if
    else
      if (index(strout,'#END') == 1 ) exit
    end if
  end do
  return
10 format (/)
20 format (T30,'M E S H  -  D A T A')
30 format ('###.Title : ', A)
40 format ('###.Analysis Type                                 : ',I8)
end subroutine get_meshdata
!  -----------------------------------------------------------------------------
subroutine bin_read_mshdata (lu_m, meshdata)
!  -----------------------------------------------------------------------------
! lu_m : unidad de lectura
! nproc: numero de proceso 
! fn_msh : nombre del archivo de lectura
!  -----------------------------------------------------------------------------
  implicit none
  integer(ip),       intent(in) :: lu_m
  type(t_mesh)                  :: meshdata
  !.
  character(len= 80)            :: str
  integer(ip)                   :: io_err 


  read (lu_m, iostat=io_err) str(1:6)
  read (lu_m, iostat=io_err) meshdata%title 
  if (io_err > 0) call w_error (1 ,'mshdata%title',0)
  !.
  read (lu_m, iostat=io_err) str(1:13)
  read (lu_m, iostat=io_err) meshdata%atype 
  if (io_err > 0) call w_error (1 ,'meshdata%atype',0)
  !.
  call bin_read_rsys  (lu_m, meshdata%rsys);  !print *,'###.Lectura del Sistema de referencia'
  !.
  call bin_read_table (lu_m, meshdata%table); !print *,'###.Lectura de Tablas'
  !.
  call bin_read_hist  (lu_m, meshdata%hist);  !print *,'###.Lectura de Historias'
  !.
  call bin_read_mat   (lu_m, meshdata%mate);  !print *,'###.Lectura de Materiales'
  !.
  call bin_read_pnod  (lu_m, meshdata%prnd);  !print *,'###.Lectura de Propiedades de nodos'
  !.
  call bin_read_prelm (lu_m, meshdata%prel);  !print *,'###.Lectura de Propiedades de elementos'
  !.
  call bin_read_nodes (lu_m, meshdata%node);  !print *,'###.Lectura de Nodos'
  !.
  call bin_read_elm   (lu_m, meshdata%elem);  !print *,'###.Lectura de Elementos'
  !.
  call bin_read_grpnod(lu_m, meshdata%grpnd); !print *,'###.Lectura de Grupo de nodos'
  !.
  call bin_read_grpel (lu_m, meshdata%grpel); !print *,'###.Lectura de Grupo de elementos'
  !.
  return
end subroutine bin_read_mshdata
! ------------------------------------------------------------------------------
subroutine get_TcelMat_nd (meshdata, i, t_cel, I_Cm)
! ------------------------------------------------------------------------------
  implicit none              
  type(t_mesh), intent(in)    :: meshdata
  integer(ip),  intent(in)    :: i
  integer(ip),  intent(out)   :: t_cel
  real(rp),     intent(out)   :: I_Cm
  !.
  integer(ip)                 :: p_prop, p_mat

  p_prop = get_node_prop( i, meshdata%node)  !.Propiedad del nodo i
  !.
  call get_prop_nd (p_prop, meshdata%prnd, t_cel, p_mat)
  !.
  I_Cm = get_I_Cm (p_mat, meshdata%mate)
  return
end subroutine get_TcelMat_nd
! ------------------------------------------------------------------------------
subroutine get_nnod_me (str, meshdata, n_nm, I_Cm, iaux, itip); 
! ------------------------------------------------------------------------------
! t_cel = 1,2,3 ten tusscher
! t_cel = 4,5,6 luo rudy
! t_cel = 7,8,9 fenton karma
! t_cel = 10    nygren
! ------------------------------------------------------------------------------
  implicit none              
  character (len=*), intent(in) :: str
  type(t_mesh),   intent(in)    :: meshdata
  integer(ip), intent(out)      :: n_nm, iaux(:), itip(:)
  real(rp), intent(out)         :: I_Cm
  !.
  integer(ip)                   :: n_nod, i, p_prop, p_mat, pmat, t_cel
  !.

  p_mat = 0
  n_nod = get_nnod(meshdata%node)
  select case(str)
  case ('tt')
    n_nm=0
    do i=1,n_nod
      p_prop = get_node_prop( i, meshdata%node)
      call get_prop_nd (p_prop, meshdata%prnd, t_cel, pmat)
      if ((t_cel == 1).or.(t_cel == 2).or.(t_cel == 3)) then
        n_nm = n_nm + 1
        iaux(n_nm) = i
        itip(n_nm) = t_cel
        p_mat      = pmat
      end if
    end do
  case ('lr')
    n_nm=0
    do i=1,n_nod
      p_prop = get_node_prop( i, meshdata%node)
      call get_prop_nd (p_prop, meshdata%prnd, t_cel, pmat)
      if ((t_cel == 4).or.(t_cel == 5).or.(t_cel == 6)) then
        n_nm = n_nm + 1
        iaux(n_nm) = i
        itip(n_nm) = t_cel
        p_mat      = pmat
      end if
    end do
  case ('FK')
    n_nm=0
    do i=1,n_nod
      p_prop = get_node_prop( i, meshdata%node)
      call get_prop_nd (p_prop, meshdata%prnd, t_cel, pmat)
      if ((t_cel == 7).or.(t_cel == 8).or.(t_cel == 9)) then
        n_nm = n_nm + 1
        iaux(n_nm) = i
        itip(n_nm) = t_cel
        p_mat      = pmat
      end if
    end do
  case ('nygren')
    n_nm=0
    do i=1,n_nod
      p_prop = get_node_prop( i, meshdata%node)
      call get_prop_nd (p_prop, meshdata%prnd, t_cel, pmat)
      if (t_cel == 10) then
        n_nm = n_nm + 1
        iaux(n_nm) = i
        itip(n_nm) = t_cel
        p_mat      = pmat
      end if
    end do
  case default
    write (*,*) 'no implementado'
  end select
  !.
  if (p_mat > 0) I_Cm = get_I_Cm (p_mat, meshdata%mate)
  !.
  return
end subroutine get_nnod_me
! ------------------------------------------------------------------------------
subroutine get_TcelMat_elm (meshdata, i, t_cel, ICm)
! ------------------------------------------------------------------------------
  implicit none              
  type(t_mesh),intent(in)    :: meshdata
  integer(ip), intent(in)    :: i
  integer(ip), intent(out)   :: t_cel
  real(rp),    intent(out)   :: ICm
  !.
  integer(ip)                :: p_prop, p_mat
  !.

  p_prop = get_elemprop(i, meshdata%elem) !.Puntero a la propiedad
  call get_propelm_mat_tcel (p_prop, meshdata%prel, t_cel, p_mat)
  ICm = get_I_Cm (p_mat, meshdata%mate)
  return
end subroutine get_TcelMat_elm

! ------------------------------------------------------------------------------
subroutine get_elem_me (str, meshdata, n_em, I_Cm, iaux, itip); 
! ------------------------------------------------------------------------------
! t_cel = 1,2,3 ten tusscher
! t_cel = 4,5,6 luo rudy  
! t_cel = 7,8,9 fenton karma  
! ------------------------------------------------------------------------------
  implicit none              
  character (len=*), intent(in) :: str
  type(t_mesh),   intent(in)    :: meshdata
  integer(ip), intent(out)      :: n_em, iaux(:), itip(:)
  real(rp), intent(out)         :: I_Cm
  !.
  integer(ip)                   :: n_elm, i, p_prop, p_mat, pmat, t_cel
  !.

  pmat = 0; iaux = 0; itip = 0; I_Cm = 0.0
  n_elm = get_nelm(meshdata%elem)
  select case(str)
  case ('tt')
    n_em=0
    do i=1,n_elm
      p_prop = get_elemprop(i, meshdata%elem)
      call get_propelm_mat_tcel (p_prop, meshdata%prel, t_cel, p_mat)
      if ((t_cel == 1).or.(t_cel == 2).or.(t_cel == 3)) then
        n_em = n_em + 1
        iaux(n_em) = i
        itip(n_em) = t_cel
        pmat       = p_mat
      end if
    end do
  case ('lr')
    write (*,*) 'no implementado'
  case ('FK')
    n_em=0
    do i=1,n_elm
      p_prop = get_elemprop( i, meshdata%elem)
      call get_propelm_mat_tcel (p_prop, meshdata%prel, t_cel, pmat)
      if ((t_cel == 7).or.(t_cel == 8).or.(t_cel == 9)) then
        n_em = n_em + 1
        iaux(n_em) = i
        itip(n_em) = t_cel
        p_mat      = pmat
      end if
    end do
  case default
    write (*,*) 'no implementado'
  end select
  !.
  if (pmat > 0) I_Cm = get_I_Cm (pmat, meshdata%mate)
  !.
  return
end subroutine get_elem_me


! ------------------------------------------------------------------------------
! $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
! ------------------------------------------------------------------------------
end module mod_meshdata
! ------------------------------------------------------------------------------
! -------------------------------- F I N ---------------------------------------
! ------------------------------------------------------------------------------
