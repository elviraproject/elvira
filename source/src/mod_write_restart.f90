!  -----------------------------------------------------------------------------
module mod_write_restart
!  -----------------------------------------------------------------------------
  use mod_precision
  use mod_filename
  use mod_error 
  use mod_filename
  use mod_USER_monodomain
  use mod_com_EM_nd
  use mod_com_EM_elm
!  -----------------------------------------------------------------------------
  implicit none
  integer(ip), parameter, private :: mx_me=100
!
contains  
! ------------------------------------------------------------------------------
! ##############################################################################
!  -----------------------------------------------------------------------------
subroutine ReadReStartVar (t_str, iam, nin, vnd, velm, prblm)
!  -----------------------------------------------------------------------------
  implicit none
  integer(ip),  intent(in)     :: t_str, iam, nin
  type(t_vnd),  intent(inout)  :: vnd
  type(t_velm), intent(inout)  :: velm
  type(t_prblm),intent(in)     :: prblm
  !.
  integer(ip)                  :: i, j, lu_b, n_nod, n_elm, n_v
  integer(ip)                  :: io_err
  real(rp)                     :: val(mx_me)

  select case (t_str)
    !.
  case(0,2,4);
    lu_b = openbin_file_r(prblm%stpdata%rstrt,iam)
    !.
    read (lu_b, iostat=io_err) n_nod
    if (io_err > 0) call w_error ( 84,'n_nod (nro. de nodos) ', 1)
    if (n_nod /= get_nnod(prblm%mshdata%node)) call w_error (85,'n_nod/=get_nnod(prblm%mshdata%node)',0)
    !.
    do i=1,n_nod
      read (lu_b, iostat=io_err) j, vnd%Vo_n(j), vnd%Vn_n(j)
      if (io_err > 0) call w_error ( 86, i, 0)
    end do
    !.
    !call w_error ( 92, n_nod, 0)
    do i=1,n_nod
      read(lu_b)  n_v, val(1:n_v)
      call set_EM_nd (i, n_v ,val) 
    end do
    !call w_error ( 93, n_nod, 0)
    !.
    if (nin >  0) then
      read (lu_b, iostat=io_err) n_elm
      if (io_err > 0) call w_error ( 84,'n_elm (numb.of  elements) ', 0)
      if (n_elm /= get_nelm (prblm%mshdata%elem) ) call w_error ( 85,'n_elm/=get_nelm(prblm%mshdata%elem)',0)
      !.
      do i=1,n_elm
        read (lu_b) j, velm%Vo_e(1:nin,j), velm%Vn_e(1:nin,j)
        if (io_err > 0) call w_error ( 87, i, 0)
      end do
      !.
      !call w_error ( 94, n_elm, 0)
      do i=1,n_elm
        do j=1,nin
          read(lu_b) n_v, val(1:n_v)
          call set_EM_elm (i, j, n_v, val) 
        end do
      end do
      !call w_error ( 95, n_elm, 0)
      !.
    end if
    call close_unit (lu_b)
    write (*,10) iam, lu_b
  case(-1,1,3) 
    !.
  case default
    call w_error ( 96, t_str, 1)
  end select
  return
10 format ('###.PROCESS:',I4,' UNIT: ',I4, ' CLOSED')
end subroutine ReadReStartVar
! ------------------------------------------------------------------------------
! ##############################################################################
! ------------------------------------------------------------------------------
subroutine WriteReStartVar (t_str, n_str, iam, ite, nin, vnd, velm, prblm)
! ------------------------------------------------------------------------------
  implicit none
  integer(ip),  intent(in)  :: t_str, n_str, iam, ite, nin
  type(t_vnd),  intent(in)  :: vnd
  type(t_velm), intent(in)  :: velm
  type(t_prblm),intent(in)  :: prblm
  !.
  integer(ip)               :: i, dt_i, lu_b

  select case (t_str)
    !.
  case(1:2);
    !.
    if (ite <= get_rstrt_data(prblm%stpdata%rstrt, n_str) ) then
      do i = 1,n_str
        dt_i = get_rstrt_data(prblm%stpdata%rstrt, i)
        if (ite == dt_i) then  
          lu_b = openbin_file_w (prblm%stpdata%rstrt, ite, iam)
          !.
          call Write_Velec_Vnd (lu_b, vnd, prblm)
          if (nin >  0) call Write_Velec_Velm (lu_b, nin, velm, prblm)
          call close_unit (lu_b)
        end if
      end do
    end if
    !.
  case(3:4);
    !.
    dt_i = get_rstrt_data(prblm%stpdata%rstrt, 1) 
    if ( mod(ite,dt_i) == 0 ) then
      lu_b = openbin_file_w (prblm%stpdata%rstrt, ite, iam)
      !.
      call Write_Velec_Vnd (lu_b, vnd, prblm)
      if (nin >  0) call Write_Velec_Velm (lu_b, nin, velm, prblm)
      call close_unit (lu_b)
    end if
    !.
  case default
    write(*,10) t_str; write(lu_log,10)  t_str 
  end select

  return
10 format ('###.WARNING "WriteReStartVar", type:',I6,' not defined')
end subroutine WriteReStartVar
! ------------------------------------------------------------------------------
function openbin_file_w (rstrt, ite, iam) result(lu_b)
! ------------------------------------------------------------------------------
  implicit none

  type(t_rsrt), intent(in)  :: rstrt

  integer(ip),  intent(in)  :: ite, iam
  integer(ip)               :: lu_b
  !.
  character (len=256)       :: fn_w 
  integer(ip)               :: m

  call a_unit(lu_b) 
  call get_rstrt_fn_w (rstrt, fn_w, m) !.Nombre de la raiz del archivo
  !.
  fn_w=fn_w(1:m)//'_'; m = m + 1
  call join_str_num (fn_w, ite, m) !.Raiz_ + iteracion
  !.
  fn_w=fn_w(1:m)//'_prc_'; m = m + 5
  call join_str_num (fn_w, iam, m) !.Raiz_ + iteracion_ + proceso
  !.
  fn_w = fn_w(1:m)//'.bin'; m = m + 4  !.Raiz_ + iteracion_ + proceso + .bin
  write (*,10) iam, fn_w(1:m)
  !.
  open (unit=lu_b, file=fn_w(1:m), status='unknown', form='unformatted')
  !.
  return
10 format ('###.PROCESS:',I4,' SAVING FILE:', A)
end function openbin_file_w
! ------------------------------------------------------------------------------
function openbin_file_r (rstrt, iam) result(lu_b)
! ------------------------------------------------------------------------------
  implicit none

  type(t_rsrt), intent(in)  :: rstrt

  integer(ip),  intent(in)  :: iam
  integer(ip)               :: lu_b
  !.
  character (len=256)       :: fn_r 
  integer(ip)               :: m

  call a_unit(lu_b) 
  call get_rstrt_fn_r (rstrt, fn_r, m) !.Nombre de la raiz del archivo
  call join_str_num (fn_r, iam, m) !.Raiz_iteracion_ + proceso
  !.
  fn_r = fn_r(1:m)//'.bin'; m = m + 4
  write (*,10) iam, fn_r(1:m),lu_b
  !.
  open (unit=lu_b, file=fn_r(1:m), status='old', form='unformatted')
  !.
  return
10 format ('###.PROCESS:',I4,' OPENING FILE:', A,' UNIT:',I4)
end function openbin_file_r
! ------------------------------------------------------------------------------
subroutine Write_Velec_Vnd (lu_b, vnd, prblm )
! ------------------------------------------------------------------------------
  implicit none
  integer(ip),   intent(in) :: lu_b
  type(t_vnd),   intent(in) :: vnd
  type(t_prblm), intent(in) :: prblm
  !.
  integer(ip)               :: n_nod, i, n_v
  real(rp)                  :: val(mx_me)

  n_nod = get_nnod (prblm%mshdata%node)  !.number of nodes 
  !.
  write(lu_b) n_nod
  do i=1,n_nod
    write(lu_b) i, vnd%Vo_n(i), vnd%Vn_n(i)
  end do
  !.
  do i=1,n_nod
    call get_EM_nd (i, n_v ,val) 
    write(lu_b)  n_v, val(1:n_v)
  end do
  !.
  return
end subroutine Write_Velec_Vnd
! ------------------------------------------------------------------------------
subroutine Write_Velec_Velm ( lu_b, nin, velm, prblm)
! ------------------------------------------------------------------------------
  implicit none
  integer(ip),   intent(in) :: lu_b, nin
  type(t_velm),  intent(in) :: velm
  type(t_prblm), intent(in) :: prblm
  !.
  integer(ip)               :: n_elm, i, j, n_v
  real(rp)                  :: val(mx_me)

  n_elm = get_nelm (prblm%mshdata%elem)  !.number of elements
  !.
  write(lu_b) n_elm
  do i=1,n_elm
    write(lu_b) i, velm%Vo_e(1:nin,i), velm%Vn_e(1:nin,i)
  end do
  !.
  do i=1,n_elm
    do j=1,nin
      call get_EM_elm (i, j, n_v, val) 
      write(lu_b) n_v, val(1:n_v)
    end do
  end do
  !. 
  return
end subroutine Write_Velec_Velm
! ------------------------------------------------------------------------------
! ------------------------------------------------------------------------------
end module mod_write_restart
