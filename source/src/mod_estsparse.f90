!  -----------------------------------------------------------------------------
module mod_estsparse
!  -----------------------------------------------------------------------------
  use mod_precision
  use mod_error
  use mod_meshdata
  use mpi_mod
  implicit none
!  -----------------------------------------------------------------------------
!  ----------------------- ALMACENAMIENTO EN CRS -------------------------------
!                          COMPRESSED ROW STORAGE
!. dof_l    : numero de grados de libertad locales
!. row(:)   : numero de valores distintos de cero en cada fila 
!. row_g(:) : puntero al numero de fila global 
!. col_l(:) : puntero a la columna, a nivel local
!. col_g(:) : puntero a la columna, a nivel global
!  -----------------------------------------------------------------------------
  type, public:: t_crs
    private
    integer(ip)              :: ndof_l
    !integer(ip), allocatable :: row_d(:)
    integer(ip), allocatable :: row(:)
    integer(ip), allocatable :: halo(:)
    integer(ip), allocatable :: row_g(:)
    integer(ip), allocatable :: col_l(:)
    integer(ip), allocatable :: col_g(:)
    real(rp),    allocatable :: vl_kr(:)
    real(rp),    allocatable :: vl_mm(:)
  end type t_crs
!  type, public:: t_crs
!    private
!    integer(ip)              :: ndof_l
!    !integer(ip), pointer :: row_d(:)=>NULL()
!    integer(ip), pointer :: row(:)=>NULL()
!    integer(ip), pointer :: halo(:)=>NULL()
!    integer(ip), pointer :: row_g(:)=>NULL()
!    integer(ip), pointer :: col_l(:)=>NULL()
!    integer(ip), pointer :: col_g(:)=>NULL()
!    real(rp),    pointer :: vl_kr(:)=>NULL()
!    real(rp),    pointer :: vl_mm(:)=>NULL()
!  end type t_crs
!  -----------------------------------------------------------------------------
! ndof_g: numero de grados de libertad globales
!  vg   : vector que me indica a que proceso corresponde cada grado de libertad
! sprs_m: matriz sparse, que tiene los indices locales y globales.
  type, public :: t_slvr
    integer(ip)              :: ndof_g
    integer(ip), allocatable :: vg(:)
    type(t_crs)              :: sprs_m
  end type t_slvr
!  type, public :: t_slvr
!    integer(ip)          :: ndof_g
!    integer(ip), pointer :: vg(:)=>NULL()
!    type(t_crs)          :: sprs_m
!  end type t_slvr
!  -----------------------------------------------------------------------------
contains 
!  -----------------------------------------------------------------------------
subroutine allocate_slvr_sprs_m(ndof_l, nn_cer, sprs_m)
!  -----------------------------------------------------------------------------
  implicit none
  integer(ip), intent(in)      :: ndof_l, nn_cer 
  type(t_crs)                  :: sprs_m
  !.
  integer(ip)                  :: i_err   


  sprs_m%ndof_l=ndof_l
  allocate(sprs_m%row(ndof_l+1), sprs_m%row_g(ndof_l), sprs_m%halo(ndof_l),      &
           sprs_m%col_l(nn_cer), sprs_m%col_g(nn_cer), sprs_m%vl_kr(nn_cer), stat=i_err)
  if (i_err > 0 ) call w_error (4,'(sub "allocate_sprs_m") : sprs_m',1)

  sprs_m%row  =0;  sprs_m%row_g=0; sprs_m%col_l=0;  sprs_m%col_g=0
  sprs_m%vl_kr=0.0
  return
end subroutine allocate_slvr_sprs_m
!  -----------------------------------------------------------------------------
subroutine allocate_slvr_sprs_m_vl_mm (flg, sprs_m)
!  -----------------------------------------------------------------------------
  implicit none
  integer(ip), intent(in):: flg !. = 0 matriz de masa diagonal, /= 0 consistente 
  type(t_crs)            :: sprs_m
  !.
  integer(ip)            :: n, i_err   

  if ( flg == 0) then   !. = 0 matriz de masa diagonal
    n = sprs_m%ndof_l
  else                  !. /= 0 matriz de masa consistente
    n = size(sprs_m%col_l)
  end if
  !.
  allocate(sprs_m%vl_mm(n), stat=i_err)
  if (i_err > 0 ) call w_error (4,'(sub "allocate_slvr_sprs_m_vl_mm") : sprs_m',1)
  sprs_m%vl_mm = 0.0
  return
end subroutine allocate_slvr_sprs_m_vl_mm
!  -----------------------------------------------------------------------------
subroutine deallocate_slvr_sprs_m(sprs_m)
!  -----------------------------------------------------------------------------
  implicit none
  type(t_crs)                  :: sprs_m
  !.
  integer(ip)                  :: i_err
    
  deallocate(sprs_m%row,sprs_m%row_g,sprs_m%col_l,sprs_m%col_g,sprs_m%vl_kr,stat=i_err)
  if (i_err > 0 ) call w_error (65,'(sub "deallocate_sprs_m") : sprs_m',1)
  return
end subroutine deallocate_slvr_sprs_m
!  -----------------------------------------------------------------------------
subroutine allocate_slvr_vg (ndof_g, slvr)
!  -----------------------------------------------------------------------------
  implicit none
  integer(ip), intent(in)      :: ndof_g
  type(t_slvr)                 :: slvr
  !.
  integer(ip)                  :: i_err


  slvr%ndof_g=ndof_g
  allocate(slvr%vg(ndof_g), stat=i_err)
  if (i_err > 0 ) call w_error (4,'(sub "allocate_slvr_vg") : vg',1)
  slvr%vg = 0
  return
end subroutine allocate_slvr_vg
!  -----------------------------------------------------------------------------
subroutine deallocate_slvr_vg ( slvr)
!  -----------------------------------------------------------------------------
  implicit none
  type(t_slvr)                 :: slvr
  !.
  integer(ip)                  :: i_err


  slvr%ndof_g = 0
  deallocate(slvr%vg, stat=i_err)
  if (i_err > 0 ) call w_error (4,'(sub "deallocate_slvr_vg") : vg',1)
  return
end subroutine deallocate_slvr_vg
!  -----------------------------------------------------------------------------
subroutine assign_dof_domain (iam, mshdata,sprs_m)
!  -----------------------------------------------------------------------------
!  Esta rutina asigna los grados de libertad globales y locales a los nodos
!  -----------------------------------------------------------------------------
  implicit none
  integer(ip)                :: iam
  type(t_mesh)               :: mshdata
  type(t_crs)                :: sprs_m
  !.
  integer(ip)                :: ndof_l, nn_cer, mx_cl
  integer(ip), allocatable   :: col_l(:), col_g(:), halo(:) 
  
  integer(ip)                :: i, ki, kf, nnz, dof_g, i_err, c_eq, c_ha, idom


  call getdim_arnnjenn_mrow (mshdata%node, ndof_l, nn_cer, mx_cl)
  !.
  call allocate_slvr_sprs_m( ndof_l, nn_cer, sprs_m)
  !.
  allocate(col_l(mx_cl), col_g(mx_cl), halo(ndof_l), stat=i_err) 
  !.
  sprs_m%row(1) = 1;
  c_eq = 0; c_ha = 0
  do i=1,sprs_m%ndof_l
    call get_nnozeros_rowi(i,mshdata%node, idom, nnz, dof_g, col_l, col_g)
    !print *, i, idom, nnz, dof_g, col_l(1:nnz), col_g(1:nnz)
    ki = sprs_m%row(i); kf = ki + nnz -1
    !.
    sprs_m%row(i+1)     = ki + nnz
    sprs_m%row_g(i)     = dof_g  
    if (idom == iam + 1) then
      c_eq = c_eq + 1
      sprs_m%halo(c_eq)=i
    else
      c_ha = c_ha + 1
      halo(c_ha) = i
    end if
    sprs_m%col_l(ki:kf) = col_l(1:nnz)
    sprs_m%col_g(ki:kf) = col_g(1:nnz)
  end do
  !.
  c_eq = c_eq + 1
  sprs_m%halo(c_eq:sprs_m%ndof_l) =  halo(1:c_ha)
  !.
  deallocate(col_l, col_g, halo, stat=i_err) 

  return
end subroutine assign_dof_domain
! ------------------------------------------------------------------------------
subroutine get_halo(sprs_m,halo)
!  -----------------------------------------------------------------------------
  implicit none
  type(t_crs)                :: sprs_m
  integer(ip), intent(out)   :: halo(:)
  !.
  integer(ip)                :: ndof_l
  
  ndof_l = sprs_m%ndof_l
  halo(1:ndof_l) = sprs_m%halo(1:ndof_l)
  return
end subroutine get_halo
! ------------------------------------------------------------------------------
subroutine print_dof_domain(lu_m, myrank, sprs_m)
! ------------------------------------------------------------------------------
  implicit none
  integer(ip), intent(in)   :: lu_m, myrank
  type(t_crs)              :: sprs_m
  !.
  integer(ip)               :: i , ki, kf, nnz

  write(lu_m, 10) '###.DOMINIO: ',myrank
  write(lu_m, 10) 'Grados de libertad: ', sprs_m%ndof_l
  !.
  do i=1,sprs_m%ndof_l
    ki  = sprs_m%row(i); kf = sprs_m%row(i+1) - 1
    nnz = kf - ki + 1
    !.
    write (lu_m, 20) i, sprs_m%row_g(i), nnz
    write (lu_m, 30) '###.LOCAL : ', sprs_m%col_l(ki:kf)
    write (lu_m, 30) '###.GLOBAL: ', sprs_m%col_g(ki:kf)
  end do
  return
10 format (A,2X,I8)
20 format ('#.Fila Local: ',I5, ' #.Fila Global: ', I5,' #.Nro dif. cero: ',I5)
30 format (A,20(:,1X,I5))
end subroutine print_dof_domain
! ------------------------------------------------------------------------------
subroutine psbcdall_setting (mshdata, nproc, ndof, vg)
! ------------------------------------------------------------------------------
!.Esta rutina construye dos vectores que son necesarios en todos los procesos 
! para saber que fila calcula cada proceso. Estos arregros son necesario para
! las libreria PSBLAS.
! ------------------------------------------------------------------------------
  implicit none
  type(t_mesh)              :: mshdata
  integer(ip), intent(in)   :: ndof, nproc
  integer(ip), intent(out)  :: vg(:)
  !.
  integer(ip)               :: dom(nproc)
  integer(ip)               :: i, n_d
  
  do i= 1,ndof
    call get_node_subdomain(i, n_d, dom, mshdata%node)
    vg(i)= dom(1) - 1
  end do
  return
end subroutine psbcdall_setting
! ------------------------------------------------------------------------------
subroutine bin_write_psbcdall_vg (lu_m, ndof, nproc, vg)
! ------------------------------------------------------------------------------
!
! ------------------------------------------------------------------------------
  implicit none
  integer(ip), intent(in)   :: lu_m, ndof, nproc
  integer(ip), intent(in)   :: vg(:)
  !.
  
  write (lu_m) '*PSBCDALL_VG',ndof,nproc
  write (lu_m) vg(1:ndof)
  return
end subroutine bin_write_psbcdall_vg
! ------------------------------------------------------------------------------
subroutine ascii_write_psbcdall_vg (lu_m, ndof, nproc, vg)
! ------------------------------------------------------------------------------
!
! ------------------------------------------------------------------------------
  implicit none
  integer(ip), intent(in)   :: lu_m, ndof, nproc
  integer(ip), intent(in)   :: vg(:)
  !.
  
  write (lu_m,10) '*PSBCDALL_VG',ndof,nproc
  write (lu_m,20) vg(1:ndof)
  return
10 format (A,2(2X,I8))
20 format (20(:,1X,I3))
end subroutine ascii_write_psbcdall_vg
! ------------------------------------------------------------------------------
subroutine assembly_ke(nlib, gl_e, ke, sprs_m) 
! ------------------------------------------------------------------------------
  implicit none
  integer(ip), intent(in)   :: nlib, gl_e(:)
  real(rp)                  :: ke(:,:)
  type(t_crs)               :: sprs_m
  !.
  integer(ip)               :: i, ir, j, jc, k

  
  do i = 1, nlib
    ir = gl_e(i)         !.Fila de la matriz de rigidez global
    if ( ir > 0 ) then
      do j = 1, nlib
        jc =  gl_e(j)    !.Columna de la matriz de rigidez global
        if( jc > 0) then
          !.
          k = f_crst (ir, jc, sprs_m%row, sprs_m%col_l) !.Funcion de Ubicacion 
          sprs_m%vl_kr(k) = sprs_m%vl_kr(k) + ke(i,j)
         ! print *,k,sprs_m%vl_kr(k)
        end if
      end do
    end if
  end do
  return
end subroutine assembly_ke
! ------------------------------------------------------------------------------
subroutine assembly_me (flg, nlib, gl_e, me, sprs_m) 
! ------------------------------------------------------------------------------
  implicit none
  integer(ip), intent(in)   :: flg, nlib, gl_e(:)
  real(rp)                  :: me(:,:)
  type(t_crs)               :: sprs_m
  !.
  integer(ip)               :: i, ir, j, jc, k


  if ( flg == 0) then   !. = 0 matriz de masa diagonal
    do i = 1, nlib
      ir = gl_e(i)         !.Fila de la matriz de rigidez global
      sprs_m%vl_mm(ir) = sprs_m%vl_mm(ir) + me(i,i)
    !  print *,' assembly_me', sprs_m%vl_mm(ir)
    end do
  else                  !. /= 0 matriz de masa consistente
    do i = 1, nlib
      ir = gl_e(i)         !.Fila de la matriz de rigidez global
      if ( ir > 0 ) then
        do j = 1, nlib
          jc =  gl_e(j)    !.Columna de la matriz de rigidez global
          if( jc > 0) then
            k = f_crst (ir, jc, sprs_m%row, sprs_m%col_l) !.Funcion de Ubicacion 
            sprs_m%vl_mm(k) = sprs_m%vl_mm(k) + me(i,j)
          end if
        end do
      end if
    end do
  end if
  return
end subroutine assembly_me
! ------------------------------------------------------------------------------
subroutine writefile_slvrsetsprs_m(lu_m, sprs_m)
! ------------------------------------------------------------------------------
  implicit none
  integer(ip), intent(in)   :: lu_m
  type(t_crs), intent(in)   :: sprs_m
  !.
  integer(ip)               :: i, ki, kf

  do i=1,sprs_m%ndof_l
    !.
    write (lu_m, 10) i,sprs_m%row_g(i)
    !.
    ki=sprs_m%row(i); kf = sprs_m%row(i+1)-1
    write (lu_m, 20) 'Columnas locales:  ',sprs_m%col_l(ki:kf)
    write (lu_m, 20) 'Columnas globales: ',sprs_m%col_g(ki:kf)
    write (lu_m, 30) 'Valores rigidez:   ',sprs_m%vl_kr(ki:kf)
    if (allocated(sprs_m%vl_mm)) write (lu_m,30) 'Valores masa: ',sprs_m%vl_mm(i)
!    if (associated(sprs_m%vl_mm)) write (lu_m,30) 'Valores masa: ',sprs_m%vl_mm(i)
  end do
  return
10 format ('##.Fila, local: ', I8,' global: ',I8)
20 format (A,20(:,1X,I6))
30 format (A,20(:,1X,G14.6))

end subroutine writefile_slvrsetsprs_m
! -------------------------------------------------------------------------------
! &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
! -------------------------------------------------------------------------------
subroutine calculo_sprs_kr_exp (dt, sprs_m) 
! ------------------------------------------------------------------------------
  implicit none
  real(rp),    intent(in)   :: dt
  type(t_crs), intent(inout):: sprs_m
  !.
  integer(ip)               :: i, j, ki, kf


  do i=1,sprs_m%ndof_l
    ki = sprs_m%row(i); kf = sprs_m%row(i+1) - 1
    do j=ki,kf
      sprs_m%vl_kr(j) = -(dt/sprs_m%vl_mm(i))*sprs_m%vl_kr(j)
    end do
  end do
  return
end subroutine calculo_sprs_kr_exp
! ------------------------------------------------------------------------------
function calculo_vn_exp ( sprs_m, Vo, Va) result(U)
! ------------------------------------------------------------------------------
  implicit none
  type(t_crs), intent(in)   :: sprs_m
  real(rp),    intent(in)   :: Vo(:), Va(:)
  real(rp)                  :: U(sprs_m%ndof_l)
  !.
  integer(ip)               :: i, ki, kf

  do i=1,sprs_m%ndof_l
    ki = sprs_m%row(i); kf = sprs_m%row(i+1) - 1
    U(i)= dot_product(sprs_m%vl_kr(ki:kf),Va(sprs_m%col_l(ki:kf))) + Vo(i)
  end do
  return
end function calculo_vn_exp

! ------------------------------------------------------------------------------
subroutine mm_lump_plus_dt_x_kr (dt, sprs_m)
! ------------------------------------------------------------------------------
  implicit none
  real(rp),    intent(in)   :: dt
  type(t_crs), intent(inout):: sprs_m
  !.
  integer(ip)               :: i, j, ki, kf

  do i=1,sprs_m%ndof_l
    ki = sprs_m%row(i); kf = sprs_m%row(i+1) - 1
    do j=ki,kf
      if (sprs_m%col_l(j) == i) then
        sprs_m%vl_kr(j) = dt*sprs_m%vl_kr(j) + sprs_m%vl_mm(i) 
      else
        sprs_m%vl_kr(j) = dt*sprs_m%vl_kr(j)
      end if
    end do
  end do
  return
end subroutine mm_lump_plus_dt_x_kr
! ------------------------------------------------------------------------------
subroutine  mm_cons_plus_dt_x_kr (dt, sprs_m)
! ------------------------------------------------------------------------------
  implicit none
  real(rp),    intent(in)   :: dt
  type(t_crs), intent(inout):: sprs_m
  !.
  integer(ip)               :: i
  
  do i=1,size(sprs_m%col_l)
    sprs_m%vl_kr(i) = dt*sprs_m%vl_kr(i) + sprs_m%vl_mm(i) 
  end do
  return
end subroutine mm_cons_plus_dt_x_kr
! ------------------------------------------------------------------------------
subroutine mm_plus_dt_x_kr (dt, sprs_m)
! ------------------------------------------------------------------------------
  implicit none
  real(rp),    intent(in)   :: dt
  type(t_crs), intent(inout):: sprs_m


  if (size(sprs_m%vl_kr) == size(sprs_m%vl_mm)) then
    call mm_cons_plus_dt_x_kr (dt, sprs_m)
  else
    call mm_lump_plus_dt_x_kr (dt, sprs_m)
  end if
  return
end subroutine mm_plus_dt_x_kr

! ------------------------------------------------------------------------------
function imp_rhs_mm_cons ( sprs_m, Vo) result(Rhs)
! ------------------------------------------------------------------------------
  implicit none
  type(t_crs), intent(in)   :: sprs_m
  real(rp),    intent(in)   :: Vo(:)
  real(rp)                  :: Rhs(sprs_m%ndof_l)
  !.
  integer(ip)               :: i, ki, kf

  do i=1,sprs_m%ndof_l
    ki = sprs_m%row(i); kf = sprs_m%row(i+1) - 1
    Rhs(i)= dot_product(sprs_m%vl_mm(ki:kf),Vo(sprs_m%col_l(ki:kf)))
  end do
  return
end function imp_rhs_mm_cons
! ------------------------------------------------------------------------------
function imp_rhs_mm_lump ( sprs_m, Vo) result(Rhs)
! ------------------------------------------------------------------------------
  implicit none
  type(t_crs), intent(in)   :: sprs_m
  real(rp),    intent(in)   :: Vo(:)
  real(rp)                  :: Rhs(sprs_m%ndof_l)
  !.
  integer(ip)               :: i

  do i=1,sprs_m%ndof_l
    Rhs(i)= sprs_m%vl_mm(i)*Vo(i)
  end do
  return
end function imp_rhs_mm_lump
! ------------------------------------------------------------------------------
function implicit_rhs ( sprs_m, Vo) result(Rhs)
! ------------------------------------------------------------------------------
  implicit none
  type(t_crs), intent(in)   :: sprs_m
  real(rp),    intent(in)   :: Vo(:)
  real(rp)                  :: Rhs(sprs_m%ndof_l)
  !.

  if (sprs_m%ndof_l == size(sprs_m%vl_mm)) then
    Rhs = imp_rhs_mm_lump ( sprs_m, Vo)
  else
    Rhs = imp_rhs_mm_cons ( sprs_m, Vo)
  end if
  return
end function implicit_rhs
!  -----------------------------------------------------------------------------
subroutine get_global_rowcol_sprs_m(i_dof, sprs_m, n_vl, row_g, col_g, val_g)
!  -----------------------------------------------------------------------------
  implicit none
  integer(ip), intent(in)   :: i_dof 
  type(t_crs), intent(in)   :: sprs_m
  !.
  integer(ip), intent(out)  :: n_vl, row_g(:), col_g(:)
  real(rp),    intent(out)  :: val_g(:)
  integer(ip)               :: ki, kf

  ki = sprs_m%row(i_dof); kf = sprs_m%row(i_dof+1) - 1; n_vl= kf-ki + 1
  !.
  row_g(1:n_vl) = sprs_m%row_g(i_dof)
  col_g(1:n_vl) = sprs_m%col_g(ki:kf)
  val_g(1:n_vl) = sprs_m%vl_kr(ki:kf)
  return
end subroutine get_global_rowcol_sprs_m
!  -----------------------------------------------------------------------------
function get_nnz_sprs_m(sprs_m) result(nnz)
!  -----------------------------------------------------------------------------
 implicit none
  type(t_crs), intent(in)   :: sprs_m
  integer(ip)               :: nnz

  nnz = size(sprs_m%vl_kr)
  return
end function get_nnz_sprs_m
!  -----------------------------------------------------------------------------
function get_ndof_l_sprs_m(sprs_m) result(ndof_l)
!  -----------------------------------------------------------------------------
  implicit none
  type(t_crs), intent(in)   :: sprs_m
  integer(ip)               :: ndof_l

  ndof_l = sprs_m%ndof_l
  return
end function get_ndof_l_sprs_m
!  -----------------------------------------------------------------------------
subroutine get_mmatrix(sprs_m, m_mtx)
!  -----------------------------------------------------------------------------
  implicit none
  type(t_crs),  intent(inout) :: sprs_m
  real(rp), intent(out)       :: m_mtx(:)
  !.
  integer(ip)                 :: i


  do i=1,sprs_m%ndof_l
    m_mtx(i)= sprs_m%vl_mm(i) 
  end do
  return
end subroutine get_mmatrix
!  -----------------------------------------------------------------------------
subroutine set_mmatrix(iam, sprs_m, m_mtx)
!  -----------------------------------------------------------------------------
  implicit none
  integer(ip),  intent(in)    :: iam
  type(t_crs),  intent(inout) :: sprs_m
  real(rp), intent(in)        :: m_mtx(:)
  !.
  integer(ip)                 :: i

  do i=1,sprs_m%ndof_l
    sprs_m%vl_mm(i) = m_mtx(i)
  end do
  !.
!  write (iam+400,'(10(:,1X,F10.5))') sprs_m%vl_mm(1:sprs_m%ndof_l)
  return
end subroutine set_mmatrix
!  -----------------------------------------------------------------------------
subroutine mm_completing1  ( iam, ndof_l, sprs_m, node)
!  -----------------------------------------------------------------------------
  implicit none
  integer(ip),  intent(in)    :: ndof_l
  type(t_crs),  intent(inout) :: sprs_m
  type(t_obnd), intent(in)    :: node
  !
  integer(ip)                 :: i, j, nd_v, idom, dom(30)
  integer(ip)                 :: comm, iam, np, ierr, dst, status(MPI_STATUS_SIZE), tag
  real(rp)                    :: s_dat, r_dat
  real(rp)                    :: Aux(ndof_l)
  integer(ip)                 :: sr_t = mpi_double_precision
 
 
  Aux = 0.0
  comm = mpi_comm_world      
  call mpi_comm_rank ( comm, iam, ierr)       !.get current proc ID
  call mpi_comm_size ( comm,  np, ierr)       !.get number of procs
  !.
  if (size(sprs_m%vl_kr) /= size(sprs_m%vl_mm)) then
!.
    do i = 1,sprs_m%ndof_l
      !.
      call get_node_subdomain(i, nd_v, dom, node)
      !.
      if (nd_v > 1) then
        !.
        tag = get_global_dof ( i, node) 
        !.
        s_dat = sprs_m%vl_mm(i) !.Dato a enviar
        !.
        do j = 1, nd_v
          idom = dom(j) - 1;  dst = idom; 
          !.
          if (idom /= iam) then 
            call mpi_sendrecv(s_dat, 1, sr_t, dst, tag, &
                              r_dat, 1, sr_t, dst, tag, comm, status, ierr)
            Aux(i) = Aux(i) + r_dat
          end if
        end do
      end if
    end do
  else
    !.
  end if
  !.
  sprs_m%vl_mm(1:ndof_l) = sprs_m%vl_mm(1:ndof_l) + Aux(1:ndof_l)
  !.

  write (iam+300,'(10(:,1X,F10.5))') sprs_m%vl_mm(1:ndof_l)
  return
end subroutine mm_completing1
!!$!  -----------------------------------------------------------------------------
!!$subroutine mm_completing  (ictxt, iam, np, sprs_m, node)
!!$!  -----------------------------------------------------------------------------
!!$  
!!$
!!$  implicit none
!!$  integer(ip), intent(in)     :: ictxt, iam, np
!!$  type(t_crs),  intent(inout) :: sprs_m
!!$  type(t_obnd), intent(in)    :: node
!!$  !
!!$  integer(ip)                 :: i, j, k, nd_v, idom, dom(30)
!!$  integer(ip)                 :: comm, iam, np, ierr, dst, src, status
!!$  real(rp)                    :: s_dat, r_dat
!!$ 
!!$
!!$
!!$
!!$  do i= 1, ndof_l
!!$
!!$    call get_node_subdomain(row_g(1,1), nd_v, dom, node)
!!$
!!$      if (nd_v > 1) then
!!$        do j=1, nd_v
!!$          idom = dom(j) -1
!!$          if (idom == iam) then 
!!$            do k=1, nd_v
!!$              dst = dom(k) -1
!!$              if (dst /= iam) then
!!$                d_dat=n_vl
!!$                call psb_snd (ictxt, d_dat,  dst, 1)
!!$                call psb_snd (ictxt, row_g, dst, n_vl)
!!$                call psb_snd (ictxt, col_g, dst, n_vl) 
!!$                call psb_snd (ictxt, val_g, dst, n_vl)
!!$              end if
!!$            end do
!!$          else
!!$            src = idom;
!!$            call psb_rcv (ictxt, r_dat, src, 1); n_dat=r_dat(1,1)
!!$            call psb_rcv (ictxt, row_i, src, n_dat)
!!$            call psb_rcv (ictxt, col_i, src, n_dat)
!!$            call psb_rcv (ictxt, val_i, src, n_dat)
!!$            !.
!!$            call psb_spins(n_dat, row_i(1:n_dat,1), col_i(1:n_dat,1), val_i(1:n_dat,1), A, desc_a, info)
!!$          end if
!!$        end do
!!$      end if
!!$    end do





end module mod_estsparse


     
      
    
