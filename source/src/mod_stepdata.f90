!  -----------------------------------------------------------------------------
module mod_stepdata
!  -----------------------------------------------------------------------------
  use mod_precision
  use mod_filename
  use mod_error 
  use mod_readcir
  use mod_rstart
  use mod_time_inc 
  use mod_param_nd
  use mod_param_elm
  use mod_ndout
  use mod_elmout
  use mod_stimulus
  use mod_outputset
!  -----------------------------------------------------------------------------
  implicit none
  !.
!  -----------------------------------------------------------------------------
  type, public:: t_step
    character(len=256)     :: title    !.titulo
    type(t_rsrt)           :: rstrt    !.re start
    type(t_obprmnd)        :: prmnd    !.Nodal parameters
    type(t_obprmelm)       :: prmelm   !.Elemental parameters
    integer(ip)            :: i_sch    !.Integration scheme
    integer(ip)            :: i_sol    !.Type of solver
    type(t_tinc)           :: timeinc  !.Time increment
    !.......
    type(t_obstm)          :: stmls    !.
    type(t_ndout)          :: nd_out   !.Nodes for postprocessing
    type(t_elmout)         :: elm_out  !.Elements for postprocessing
    type(t_output)         :: g_out    !.Binnary output for postprocessing
  end type t_step
!  -----------------------------------------------------------------------------
contains
!  -----------------------------------------------------------------------------  
subroutine get_stepdata (lu_msh, fn_msh, stepdata,flg_fin)
!  -----------------------------------------------------------------------------
  implicit none
  integer(ip),intent(in)      :: lu_msh
  character(len=*),intent(in) :: fn_msh
  type(t_step)                :: stepdata
  logical                     :: flg_fin
  !
  character(len=256)          :: strout
  integer(ip)                 :: lu_lec, io_err
  logical                     :: flag
  !.


  write (*,10); call write_n_str(6, 80,'='); 
  write (*,20); call write_n_str(6, 80,'=');

  call srch_lec (lu_msh,'#STEP', strout, flag, io_err)
  lu_lec = lu_msh;
  !.
  if (flag) then
    call unitnumber (fn_msh, strout, lu_lec, io_err)
    if (io_err > 0)  call w_error (2,'subroutine get_stepdata',1)
    do
      call srch_lec02 (lu_lec,'#','*',strout, flag, io_err)
      !.
      if (flag) then
        if     (index(strout,'#END') == 1) then
          !.
          write(*,'(2A)') strout
          exit
          !.
        elseif (index(strout,'*RESTART')    == 1) then !.Reading parameters for restart
          !.
          call lecc_rstart (lu_lec, fn_msh, strout, stepdata%rstrt)
          !.
        elseif (index(strout,'*PARAM_NODE') == 1) then !.Reading nodal mask
          !.
          call lec_prmnd (lu_lec, fn_msh, strout, stepdata%prmnd)
          !.
        elseif (index(strout,'*PARAM_ELEM') == 1) then !.Reading element mask
          !.
          call  lec_prmelm (lu_lec, fn_msh, strout, stepdata%prmelm)
          !.
        elseif (index(strout,'*INTEG_SCH')  == 1) then !.Reading time integration scheme
          read (lu_lec,*,iostat=io_err)  stepdata%i_sch
          if (io_err > 0) call w_error (3,'stepdata%i_sch',1)
          !.
          elseif (index(strout,'*SOLVER')    == 1) then !.Reading type of solver
          read (lu_lec,*,iostat=io_err)  stepdata%i_sol
          if (io_err > 0) call w_error (3,'stepdata%i_sol',1)
          !.
        elseif (index(strout,'*TIME_INC')   == 1) then !.Reading time incrementation
          call lec_time_inc(lu_lec, stepdata%timeinc)
          !.
        elseif (index(strout,'*INITIALCOND')== 1) then !.Reading initial condition (not used)
          !.
          write(*,'(A)') strout(1:len_trim(strout))
          !.
        elseif (index(strout,'*STIMULUS')   == 1) then !.Reading stimulus
          !.
          call lecc_stm (lu_lec, fn_msh, strout, stepdata%stmls)
          !.
        elseif (index(strout,'*BOUNDARY')   == 1) then !.Reading boundary conditions (not used)
          !.
          write(*,'(A)') strout(1:len_trim(strout))
          !.
        elseif (index(strout,'*NODELOAD')   == 1) then !.Reading nodal loading (not used)
          !.
          write(*,'(A)') strout(1:len_trim(strout))
          !.
        elseif (index(strout,'*ELELOAD')    == 1) then !.Reading element loading (not used)
          !.
          write(*,'(A)') strout(1:len_trim(strout))
          !.
        elseif (index(strout,'*NODEOUTPUT') == 1) then !.Reading nodes for output
          !.
          call lec_node_out(lu_lec, fn_msh, strout, stepdata%nd_out)
          !.
        elseif (index(strout,'*ELEMOUTPUT') == 1) then !.Reading elements for output 
          !.
          call lec_elem_out(lu_lec, fn_msh, strout, stepdata%elm_out)
          !.
        elseif (index(strout,'*G_OUTPUT') == 1) then !.Reading parameters for binary output
          !.
          call lec_outputset (lu_lec, stepdata%g_out)
          !.
        end if
      else
        if (io_err == -1) exit
      end if
    end do
  else
    if   (index(strout,'#END') == 1)  flg_fin = .true.
  end if
  !.
  if (lu_lec /= lu_msh) call close_unit (lu_lec)
  !.
  return
10 format (/)
20 format (T30,'S T E P  -  D A T A')
end subroutine get_stepdata
!  -----------------------------------------------------------------------------
function get_int_temp(stepdata) result (i_sch)
!  -----------------------------------------------------------------------------
  implicit none
  type(t_step)                :: stepdata
  integer(ip)                 :: i_sch

  i_sch = stepdata%i_sch
  return
end function get_int_temp
!  -----------------------------------------------------------------------------
function get_solvertype(stepdata) result(i_sol)
!  -----------------------------------------------------------------------------
  implicit none
  type(t_step)                :: stepdata
  integer(ip)                 :: i_sol

  i_sol = stepdata%i_sol
  return
end function get_solvertype
!  -----------------------------------------------------------------------------
subroutine bin_read_stepdata (lu_m, stepdata)
!  -----------------------------------------------------------------------------
  implicit none
  integer(ip),intent(in)      :: lu_m
  type(t_step)                :: stepdata
  !
  character(len= 80)          :: str
  integer(ip)                 :: io_err 


  read (lu_m, iostat=io_err) str(1:5);                 ! print *, '#STEP'
  if (io_err > 0) call w_error (1 ,'#STEP',0)
  !.
  call bin_read_rstart(lu_m, stepdata%rstrt);          ! print *, '*RESTART'
  !.
  call bin_read_prmnd  (lu_m, stepdata%prmnd);         ! print *, '*PARAM_NODE'
  !. 
  call bin_read_prmelm (lu_m, stepdata%prmelm);        ! print *, '*PARAM_ELEM'
  !.
  read (lu_m, iostat=io_err) str(1:10), stepdata%i_sch;! print *, '*INTEG_SCH'
  if (io_err > 0) call w_error (1 ,'*INTEG_SCH',0)
  !.
  read (lu_m, iostat=io_err) str(1:7), stepdata%i_sol; ! print *, '*SOLVER'
  if (io_err > 0) call w_error (1 ,'*SOLVER',0)
  !.
  call bin_read_timeinc(lu_m, stepdata%timeinc);       ! print *, '*TIMEINC' 
  !.
  read (lu_m, iostat=io_err) str(1:12);                ! print *, '*INITIALCOND'
  !.
  call bin_read_stm (lu_m, stepdata%stmls);            ! print *, '*STIMULUS'
  !.
  read (lu_m, iostat=io_err) str(1:9);                 ! print *, '*BOUNDARY'
  if (io_err > 0) call w_error (1 ,'*BOUNDARY',0)
  !.
  read (lu_m, iostat=io_err) str(1:9);                 ! print *, '*NODELOAD'
  if (io_err > 0) call w_error (1 ,'*NODELOAD',0)
  !.
  read (lu_m, iostat=io_err) str(1:8);                 ! print *, '*ELELOAD'
  if (io_err > 0) call w_error (1 ,'*ELELOAD',0)
  !.
  call bin_read_node_out(lu_m, stepdata%nd_out)        ! print *, '*NODEOUTPUT'
  !.
  call bin_read_elem_out(lu_m, stepdata%elm_out)       ! print *, '*ELEMOUTPUT'
  !.
  call bin_read_outputset(lu_m,stepdata%g_out)         ! print *, '*G_OUTPUT'
  !.
  return
end subroutine bin_read_stepdata
! ------------------------------------------------------------------------------
! $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
! ------------------------------------------------------------------------------
end module mod_stepdata
! ------------------------------------------------------------------------------
! -------------------------------- F I N ---------------------------------------
! ------------------------------------------------------------------------------
