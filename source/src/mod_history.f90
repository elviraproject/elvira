!  ----------------------------------------------------------------------------
module mod_history
!  ----------------------------------------------------------------------------
  use mod_precision
  use mod_error
!  ----------------------------------------------------------------------------
  implicit none
!  ----------------------------------   HISTORIAS ------------------------------
!  Elvio Heidenreich => 25/05/2005
!  Definicion de los campos del objeto HISTORIAS
!  Donde:
!       data = Vector de datos de la historia, que permanecen constantes a lo
!              largo del programa
!       ampl = Amplitud de la carga
!       n_hst = Numero de usuario de la historia
!       tipo = Tipo de la historia (vs temp, vs tiempo, vs desp, etc)
!  -----------------------------------------------------------------------------
  type t_hist
    private 
    integer(ip)          :: n_hst
    integer(ip)          :: tipo
    real(rp)             :: ampl
    real(rp),allocatable :: data(:)
  end type t_hist
!  -----------------------------------------------------------------------------
  type, public:: t_obhis
    private
    integer(ip)                :: nhst = 0
    type(t_hist), allocatable  :: hstr(:)
  end type t_obhis
!  -----------------------------------------------------------------------------
contains
!  -----------------------------------------------------------------------------
subroutine read_hist(lu_m, hist)
! ------------------------------------------------------------------------------
! Esta rutina lee las historias (funciones) de la unidad lu_m
! ------------------------------------------------------------------------------
  implicit none
  integer(ip)                   :: lu_m
  type(t_obhis)                 :: hist
  !.
  integer(ip)                   :: io_err, i_err, n_dat, i


  read (lu_m, *, iostat = io_err) hist%nhst
  if (io_err > 0) call w_error (22,'hist%nhst',1)
  !.
  allocate (hist%hstr(hist%nhst), stat=i_err)
  if (i_err > 0 ) call w_error (23,'hist%hstr(hist%nhst)',1)
  !.
  do i = 1,hist%nhst
    read (lu_m, *, iostat = io_err) hist%hstr(i)%n_hst,hist%hstr(i)%tipo, &
                                    hist%hstr(i)%ampl, n_dat
    if (io_err > 0) call w_error (24,'n_dat',1)
    !.
    allocate (hist%hstr(i)%data(n_dat),stat=i_err)
    if (i_err > 0 ) call w_error (23,'hist%hstr(i)%dato(idum)',1)
    !.
    read (lu_m, *, iostat = io_err) hist%hstr(i)%data(1:n_dat)
    if (io_err > 0) call w_error (24,' hist%hstr(i)%()',1)
  end do
!
  write(*,10) hist%nhst; write(lu_log,10) hist%nhst 
! 
  return
10 format ('###.Historias,  leidos                            : ',I8)
end subroutine read_hist
! ------------------------------------------------------------------------------ 
subroutine bin_write_hist(lu_m, hist)
! ------------------------------------------------------------------------------ 
  implicit none
  integer(ip)                   :: lu_m
  type(t_obhis)                 :: hist
  !.
  integer(ip)                   :: i


  write (lu_m) '#HISTORY', hist%nhst
  !.
  if (hist%nhst > 0) then
    do i = 1,hist%nhst
      write (lu_m) hist%hstr(i)%n_hst, hist%hstr(i)%tipo, &
                   hist%hstr(i)%ampl,  size(hist%hstr(i)%data)
      write (lu_m) hist%hstr(i)%data
    end do
  end if
  return
end subroutine bin_write_hist
! ------------------------------------------------------------------------------ 
subroutine ascii_write_hist(lu_m, hist)
! ------------------------------------------------------------------------------ 
  implicit none
  integer(ip)                   :: lu_m
  type(t_obhis)                 :: hist
  !.
  integer(ip)                   :: i


  write (lu_m,10) '#HISTORY', hist%nhst
  !.
  if (hist%nhst > 0) then
    do i = 1,hist%nhst
      write (lu_m,20) hist%hstr(i)%n_hst, hist%hstr(i)%tipo, &
                   hist%hstr(i)%ampl,  size(hist%hstr(i)%data)
      write (lu_m,30) hist%hstr(i)%data
    end do
  end if
  return
10 format (A,2X,I3)
20 format (2X,I3,2X,I3,2X,F12.6,2X,I3)
30 format (10(:,2X,F12.6))
end subroutine ascii_write_hist
! ------------------------------------------------------------------------------ 
subroutine bin_read_hist (lu_m, hist)
! ------------------------------------------------------------------------------
! Esta rutina lee las historias (funciones) de la unidad lu_m
! ------------------------------------------------------------------------------
  implicit none
  integer(ip)                   :: lu_m
  type(t_obhis)                 :: hist
  !.
  character (len=8)             :: str
  integer(ip)                   :: io_err, i_err, n_dat, i

  read (lu_m, iostat = io_err) str, hist%nhst
  if (io_err > 0) call w_error (22,'hist%nhst',1)
  !.
  if (hist%nhst > 0) then
    allocate (hist%hstr(hist%nhst), stat=i_err)
    if (i_err > 0 ) call w_error (23,'hist%hstr(hist%nhst)',1)
    !.
    do i = 1,hist%nhst
      read (lu_m, iostat = io_err) hist%hstr(i)%n_hst, hist%hstr(i)%tipo, &
                                   hist%hstr(i)%ampl,  n_dat
      if (io_err > 0) call w_error (24,'hist%hstr(i)%n_hst, %tipo, %ampl',1)
      !.
      allocate (hist%hstr(i)%data(n_dat),stat=i_err)
      if (i_err > 0 ) call w_error (23,'hist%hstr(i)%data(n_dat)',1)
      !.
      read (lu_m, iostat = io_err) hist%hstr(i)%data
      if (io_err > 0) call w_error (24,' hist%hstr(i)%data ',1)
    end do
  end if
  !.
  return
end subroutine bin_read_hist
! ------------------------------------------------------------------------------
function deallocate_hist (hist) result(flg)
! ------------------------------------------------------------------------------
  implicit none
  type(t_obhis)                 :: hist
  !.
  integer(ip)                   :: flg
  integer(ip)                   :: i_err, i, istat

  if (allocated(hist%hstr)) then
    i_err = 0
    do i=1,hist%nhst
      deallocate (hist%hstr(i)%data, stat=istat)
      if (istat /= 0) i_err = 1
    end do
    deallocate (hist%hstr, stat=istat)
    if  (istat == 0) flg = 0 
    if ((istat /= 0).or.(i_err == 1)) flg = 1
  else
    flg = -1
  end if
  return 
end function deallocate_hist

! ------------------------------------------------------------------------------
end module mod_history
