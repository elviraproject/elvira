!  ------------------------------------------------------------------------------
module mod_USER_monodomain
!  ------------------------------------------------------------------------------
  use mod_precision
  use mod_typematelm
  use mod_problemsetting
  use mod_quadrature
  use mod_fforma
  use mpi_mod
  use mod_jacobiano
  use mod_genmesh
!  -----------------------------------------------------------------------------
!  ------------------------ Private Module variables  --------------------------
!  --------------------------- saved between calls -----------------------------
!  -----------------------------------------------------------------------------
!  i_tipo   : element type, previously processed
!  ntpg     : total number of Gauss points
!  ndim     : element dimension 
!  n_te     : number of stress components at Gauss point (not used) 
!  n_pg     : number of Gauss points on each direction
!  n_pe     : number of nodes per element
!  nlib     : number of degrees of freedom per element    
!  ff(:,:)  : array of shape functions evaluated all integration points
!  df(:,:,:): array of shape function derivative evaluated at all
!             integration points
!  gp(:,:)  : coordinates of Gauss points
!  gw(:)    : weights of Gauss points
!  -----------------------------------------------------------------------------
  implicit none

  type, public :: t_e
    private
    integer(ip) :: ndim
    integer(ip) :: n_pe
    integer(ip) :: nlib
    integer(ip) :: ntqp
    logical     :: flg_b
  end type t_e
  !.
  type, public :: t_me
    private
    integer(ip) :: nnod
    integer(ip) :: nelm
    integer(ip) :: n_pe
  end type t_me
  !.
  type, public:: t_velm    !.Elemental variables
    real(rp), allocatable   :: Vo_e(:,:)  !.Potentials
    real(rp), allocatable   :: Vn_e(:,:)  !.Potentials
    real(rp), allocatable   :: Va_e(:,:)  !.Potentials
    real(rp), allocatable   :: Vp_e(:,:)  !.Temporal derivative
    real(rp), allocatable   :: Qi_e(:,:)  !.Total ionic current
  end type t_velm
  !type, public:: t_velm    !.Elemental variables
  !  real(rp), pointer   :: Vo_e(:,:)=>NULL()  !.Potentials
  !  real(rp), pointer   :: Vn_e(:,:)=>NULL()  !.Potentials
  !  real(rp), pointer   :: Va_e(:,:)=>NULL()  !.Potentials
  !  real(rp), pointer   :: Vp_e(:,:)=>NULL()  !.Temporal derivative
  !  real(rp), pointer   :: Qi_e(:,:)=>NULL()  !.Total ionic current
  !end type t_velm
  !.
  type, public:: t_vnd     !.Nodal variables
    real(rp),  allocatable  :: Vo_n(:)  !.Potentials
    real(rp),  allocatable  :: Vn_n(:)  !.Potentials
    real(rp),  allocatable  :: Va_n(:)  !.Potentials
    real(rp),  allocatable  :: Vp_n(:)  !.Temporal derivative
    real(rp),  allocatable  :: Qi_n(:)  !.Total ionic current 
    real(rp),  allocatable  :: Rhs (:)  !.Right term
  end type t_vnd
  !type, public:: t_vnd     !.Nodal variables
  !  real(rp),  pointer  :: Vo_n(:)=>NULL()  !.Potentials
  !  real(rp),  pointer  :: Vn_n(:)=>NULL()  !.Potentials
  !  real(rp),  pointer  :: Va_n(:)=>NULL()  !.Potentials
  !  real(rp),  pointer  :: Vp_n(:)=>NULL()  !.Temporal derivative
  !  real(rp),  pointer  :: Qi_n(:)=>NULL()  !.Total ionic current 
  !  real(rp),  pointer  :: Rhs (:)=>NULL()  !.Right term
  !end type t_vnd

  type, public :: t_est
    real(rp), allocatable :: Me (:)     !.Elemental mass matrix
    real(rp), allocatable :: Fi (:)     !.Internal force vector
    real(rp), allocatable :: Ke21(:,:)  !.
    real(rp), allocatable :: Ke22i(:,:) !.
    real(rp), allocatable :: Ke(:,:)    !.
    real(rp), allocatable :: Me22i(:)
  end type t_est
  !type, public :: t_est
  !  real(rp), pointer :: Me (:)=>NULL()     !.Elemental mass matrix
  !  real(rp), pointer :: Fi (:)=>NULL()     !.Internal force vector
  !  real(rp), pointer :: Ke21(:,:)=>NULL()  !.
  !  real(rp), pointer :: Ke22i(:,:)=>NULL() !.
  !  real(rp), pointer :: Ke(:,:)=>NULL()    !.
  !  real(rp), pointer :: Me22i(:)=>NULL()
  !end type t_est
  !.
  !.Elemental variables
  type(t_est), allocatable :: edat(:)
  type(t_vnd)              :: vnd
  type(t_velm)             :: velm
  !.
  real(rp), allocatable, private  :: ff(:,:)
  real(rp), allocatable, private  :: df(:,:,:)
  real(rp), allocatable, private  :: q_pt(:,:)
  real(rp), allocatable, private  :: q_wt(:)
  !real(rp), pointer, private  :: ff(:,:)
  !real(rp), pointer, private  :: df(:,:,:)
  !real(rp), pointer, private  :: q_pt(:,:)
  !real(rp), pointer, private  :: q_wt(:)
  !.
  save ff, df, q_pt, q_wt

contains 
!  -----------------------------------------------------------------------------
subroutine userelem_monodomain (iel, prblm, Ke)
!  -----------------------------------------------------------------------------
!   iel      : element number
!   mshdata  : mesh data
!   Ke       : elemental matrices
! -------------------------------------------------------------------------------
  implicit none
  integer(ip),   intent(in)            :: iel
  type(t_prblm), intent(in)            :: prblm
  type(t_mtr),   intent(inout)         :: Ke  
! -------------------------------------------------------------------------------
  character (len=12)                  :: nomb
  integer(ip)                         :: ndim, n_pe, nlib, n_qp, ntqp, prme, m
  !.
  type (t_e),  save                   :: elm
  type (t_me), save                   :: m_e
  integer(ip), save                   :: i_tipo = 0
  logical,     save                   :: flg_me, flg_ei
  !.
  integer(ip)                         :: tipo,esq_it 
  integer(ip), save                   :: ndimXD


  tipo = get_elemtipo( iel, prblm%mshdata%elem)  !.Element type
  !.
  if (i_tipo /= tipo) then  
    !.
    flg_me =.false.
    !.
    call id_elem_tip (tipo, nomb=nomb, ndim=ndim, n_pe=n_pe, nlib = nlib,       &
                      n_qp=n_qp, ntqp=ntqp, prme = prme)
    !.
    i_tipo=tipo
    !.
    m=len_trim(nomb)
    if (nomb(1:1) == 'M') then !.Is a macro element
      flg_me   = .true.
      !.
      m_e%nnod = nlib !.number of dof in the macro element
      m_e%nelm = prme !.number of elements in the macro element
      m_e%n_pe = n_pe !.number of nodes in the macro element
      !.
      call id_par_ele(nomb(5:m), n_pe=n_pe, nlib=nlib, n_qp=n_qp, ntqp=ntqp)
      !.
      flg_ei = .false. !.Integration scheme
      esq_it = get_int_temp  (prblm%stpdata)   
      if ((esq_it == 0).or.(esq_it == 1)) flg_ei = .true.
    end if
    !.
    ndimXD=ndim !.Dimension for element coordinates
    if (ndim>0) then
      if ((tipo >= 61).and.(tipo <=66)) ndim = 1
      elm%ndim  = ndim
      elm%n_pe  = n_pe
      elm%nlib  = nlib
      elm%ntqp  = ntqp
      elm%flg_b = (nomb(m:m) == 'B')
    !.
    !.Allocation for Gauss points and weights
      call dim_qpqw (iel,ndim,ntqp) 
    !.Allocation for shape functioins and derivatives 
      call dim_ffdf (iel,ndim, nlib, ntqp)
    !.Coordinates of Gauss points
      call quadrature ( ndim, n_qp, ntqp, q_pt, q_wt)
    !.Evaluated shape functions and derivatives in natural element coordinates
      call fforma (nomb, nlib, q_pt, df, ff)
    !.
    end if
  end if
  !.
  if (flg_me) then    ! Macroelement
    if (flg_ei) then
      call m_elm_pot_exp (ndimXD, iel, prblm, m_e, elm, ke%me)
    else
      call m_elm_pot     (ndimXD, iel, prblm, m_e, elm, ke%ke)
    end if
  else                ! Standard Element
    if (ndimXD<1) then
      call elemConect(iel, prblm, elm, ke%me, ke%ke)
    else
      call elemPot  (ndimXD, iel, prblm, elm, ke%me, ke%ke)
    end if
  end if
  !.
  return
end subroutine userelem_monodomain
!  -----------------------------------------------------------------------------
subroutine dim_qpqw (i_el, n_dim, n_tqp) 
!  -----------------------------------------------------------------------------
!  This subroutine allocates memory for Gauss points and weights
!  -----------------------------------------------------------------------------
  implicit none
  integer(ip), intent(in)    :: i_el, n_dim, n_tqp
  integer(ip)                :: i_err

  if (.not. allocated(q_pt)) then
!  if (.not. associated(q_pt)) then
    allocate(q_pt(n_dim, n_tqp), q_wt(n_tqp), stat=i_err)
    if (i_err > 0 ) call w_error (318, 'q_pt(), q_wt()', i_el)
  else 
    deallocate(q_pt, q_wt)
    allocate(q_pt(n_dim, n_tqp), q_wt(n_tqp), stat=i_err)
    if (i_err > 0 ) call w_error (318, 'q_pt(), q_wt()', i_el)
  end if
  q_pt = 0.0; q_wt = 0.0
  return
end subroutine dim_qpqw
!  -----------------------------------------------------------------------------
subroutine dim_ffdf (i_el,n_dim, n_pe, n_tqp)
!  -----------------------------------------------------------------------------
!  This subroutine allocates memory for shape functions and its derivatives.
!  -----------------------------------------------------------------------------
  implicit none
  integer(ip), intent(in)    :: i_el, n_dim, n_pe, n_tqp
  integer(ip)                :: i_err

  if (.not. allocated(ff)) then
!  if (.not. associated(ff)) then
    allocate(ff(n_pe, n_tqp), df(n_dim, n_pe, n_tqp), stat=i_err)
    if (i_err > 0 ) call w_error (318,'ff(),df() ',i_el)
  else 
    deallocate(ff, df )
    allocate(ff(n_pe, n_tqp), df(n_dim, n_pe, n_tqp), stat=i_err)
    if (i_err > 0 ) call w_error (318,'ff(),df() ',i_el)
  end if
  ff=0.0; df=0.0
  return
end subroutine dim_ffdf
! -------------------------------------------------------------------------------
subroutine elemConect(iel, prblm, elm, Me, Ke)
! -------------------------------------------------------------------------------
! Element matrices for conector element
! -------------------------------------------------------------------------------
! iel : element number
! elm : structure with the fundamental element data
!       elm%n_pe= n_pe   number of element nodes
!       elm%nlib= nlib   number of degrees of freedom
!       elm%ntqp= ntqp   total number of Gauss points
!       elm%fl_b= (nomb(m:m) == 'B')  flag that indicates if is an enhanced
!       element with bubble
! Me(n_pe)       : mass matrix
! Ke(n_pe, n_pe) : stiffness matrix
! -------------------------------------------------------------------------------
 implicit none
  integer(ip),  intent(in)      :: iel
  type(t_prblm), intent(in)     :: prblm
  type(t_e),    intent(in)      :: elm
  real(rp),     intent(out)     :: Me(:,:),  Ke(:,:)
! -------------------------------------------------------------------------------
  real(rp)                      :: Dcon, f_dr(3)
! ------------
  integer(ip)                   :: pp_e, ierr, p_mat  
  integer(ip)                   :: myrank

  
  call mpi_comm_rank (mpi_comm_world, myrank, ierr)
  Me = 0.0_rp; Ke = 0.0_rp
  !.--- number of element property --------------------------------------------
  pp_e = get_elemprop (iel, prblm%mshdata%elem)
  !.--- pointer to material ---------------------------------------------------
  p_mat = get_nummatelm(pp_e, prblm%mshdata%prel)
  !.Connector resistivity -----------------------------------------------------
  call get_materialprop(p_mat, prblm%mshdata%mate,data=f_dr) 
  Dcon = f_dr(1)
  Ke(1,1) = Dcon; Ke(1,2) =-Dcon;
  Ke(2,1) =-Dcon; Ke(2,2) = Dcon;  
  
end subroutine elemConect
! -------------------------------------------------------------------------------
subroutine elemPot(ndimXD, iel, prblm, elm, Me, Ke)
! -------------------------------------------------------------------------------
! Element matrices for standard element
! -------------------------------------------------------------------------------
! iel : element number
! elm : structure with the fundamental element data
!       elm%n_pe= n_pe   number of element nodes
!       elm%nlib= nlib   number of degrees of freedom
!       elm%ntqp= ntqp   total number of Gauss points
!       elm%fl_b= (nomb(m:m) == 'B')  flag that indicates if is an enhanced
!       element with bubble
! Me(n_pe)       : mass matrix
! Ke(n_pe, n_pe) : stiffness matrix
! -------------------------------------------------------------------------------
 implicit none
  integer(ip),  intent(in)            :: ndimXD, iel
  type(t_prblm), intent(in)           :: prblm
  type(t_e),    intent(in)            :: elm
  real(rp),     intent(out), optional :: Me(:,:),  Ke(:,:)
! -------------------------------------------------------------------------------
! ------------- Element variables -----------------------------------------------
  real(rp), allocatable             :: xn_e(:,:)
  integer(ip), allocatable          :: cn_e(:)
!  real(rp),                        :: xn_e(ndimXD,elm%n_pe)
!  integer(ip),                     :: cn_e(elm%n_pe)
!--------- Variables of element to be integrated --------------------------------
  real(rp), allocatable              :: xn(:,:)
  real(rp), allocatable              :: D(:,:)
  real(rp), allocatable              :: KeB(:,:), MeB(:)
  real(rp)                           :: Cm
!  real(rp)                          :: xn(elm%ndim,elm%nlib)
!  real(rp)                          :: D(elm%ndim,elm%ndim), Cm
!  real(rp)                          :: KeB(elm%nlib, elm%nlib), MeB(elm%nlib)
! ------------
  real(rp)                          :: data(3),f_dr(3), dt
  integer(ip)                       :: i, pp_e, ierr, m_c, n_rea, p_mat
  
  integer(ip)                       :: myrank

  
  call mpi_comm_rank (mpi_comm_world, myrank, ierr)

  allocate(xn_e(ndimXD,elm%n_pe),cn_e(elm%n_pe),xn(elm%ndim,elm%nlib))
  allocate(D(elm%ndim,elm%ndim),KeB(elm%nlib,elm%nlib),MeB(elm%nlib))
  !.
  Me = 0.0_rp; Ke = 0.0_rp
  !.  
  !.---- conectivity, coordinates and number of element properties ------------
  call get_melmdata (iel, prblm%mshdata, ndimXD, elm%n_pe, cn_e, xn_e, pp_e)
  !.--- Element properties (fiber directio and material data) -----------------
  call get_propelm(pp_e, prblm%mshdata%prel, p_mat, n_rea, f_dr(1:elm%ndim))
  if ((n_rea /= elm%ndim).and.(elm%ndim>1) ) write (*,'(A)') 'Wrong fiber definition'
  call get_materialprop(p_mat, prblm%mshdata%mate,data=data) 
  !.Computation of conduction tensor and capacitance --------------------------------------
  call get_diften_cap (elm%ndim, f_dr(1:elm%ndim), data, D, Cm)
  
  if (elm%ndim /= ndimXD) then ! accounting for 1D elements in 3D space
    xn(1,1)=0.0_rp
    do i=2,elm%n_pe
      xn(1,i)=sqrt( (xn_e(1,1) - xn_e(1,i))**2 + (xn_e(2,1) - xn_e(2,i))**2 + &
              (xn_e(3,1) - xn_e(3,i))**2)
    end do
    xn_e(:,:)=0.0_rp; xn_e(1,:)=xn(1,:)
  end if
  !
  !Macro-elements
  !
  if (elm%flg_b) then
    call mesh_bubble(elm%ndim, elm%n_pe, elm%nlib, xn_e(1:elm%ndim,:), xn(1:elm%ndim,:))
  else
    xn(1:elm%ndim,:) = xn_e(1:elm%ndim,:) 
  end if
  ! -------- matrix assembly for the macro element -------------
  KeB = 0.0_rp; MeB = 0.0_rp
  !.
  m_c = 2
  if (m_c == 1) then !.Consistent mass matrix
    call matriz_me_y_ke_2 (elm%ntqp,elm%ndim,elm%nlib,xn,Cm,D,Ke,Me,ierr)
  else  !.Condensed mass matrix
    call matriz_me_y_ke (elm%ntqp,elm%ndim,elm%nlib,elm%flg_b,xn,Cm,D,KeB,MeB,ierr)
    !.
    if (ierr > 0) then
      write (*,10) iel; stop
    end if
    !.
    if (elm%flg_b) then
      call get_stpdata_time_inc (prblm%stpdata%timeinc, dtini = dt)
      call Cond_alm_me(iel,elm%n_pe,elm%nlib,dt,MeB,KeB,Ke)
    else
      Ke = KeB
      do i=1,elm%n_pe; 
        Me(i,i) = MeB(i)
      enddo
    end if
  end if
!!$  !.
  return
10 format ('###.Jacobian less than zero in element: ',I8)
end subroutine elemPot
!--------------------------------------------------------------------------------
subroutine get_melmdata(iel, mshdata, ndim, npe, cn_me, xn_me, e_pp)
!--------------------------------------------------------------------------------
  implicit none
  integer(ip),  intent(in)     :: iel, ndim, npe
  type(t_mesh), intent(in)     :: mshdata
  integer(ip),  intent(out)    :: cn_me(npe), e_pp
  real(rp),     intent(out)    :: xn_me(ndim,npe)
  !.
  integer(ip)                  :: i, idim, n_pe
  integer(ip)                  :: ierr, nproc, myrank


  call mpi_comm_size (mpi_comm_world, nproc, ierr)
  call mpi_comm_rank (mpi_comm_world, myrank,ierr)

  call get_elmconc (iel, n_pe, cn_me, mshdata%elem)
  !.
  do i=1,npe
    call get_nodecoord (cn_me(i), idim, xn_me(1:ndim, i), mshdata%node)
  enddo
  !.
  e_pp = get_elemprop (iel, mshdata%elem)
  !.
  return
end subroutine get_melmdata
!  -----------------------------------------------------------------------------
subroutine get_diften_cap (ndim, f_dr, data, D, Cm)
!  -----------------------------------------------------------------------------
  implicit none
  integer(ip), intent(in)      :: ndim
  real(rp), intent(in)         :: f_dr(:),data(:)
  real(rp), intent(out)        :: D(ndim,ndim), Cm
  !.
  integer(ip)                  :: i
  real(rp)                     :: d0, r, S_l, S_t


  d0  = data(1)  !.Conductivity along the fiber 
  r   = data(2)  !.Transversal/Longitudinal conductivity ratio
  Cm  = data(3)  !.Membrane capacity
!
  S_l  = d0*(1.0 - r)      !.Aux Variable
  S_t  = d0*r              !.Aux Variable
  do i=1,ndim
    D(i,:) = S_l*f_dr(i)*f_dr(1:ndim); D(i,i)= D(i,i) + S_t
  end do  
  return
end subroutine get_diften_cap
!  -----------------------------------------------------------------------------
subroutine  mesh_bubble(ndim, n_pe, nlib, xn_e, xn)
!  -----------------------------------------------------------------------------
  implicit none
  integer(ip), intent(in)    :: ndim, n_pe, nlib
  real(rp),    intent(in)    :: xn_e(ndim,n_pe)
  real(rp),    intent(out)   :: xn  (ndim,nlib)
  !.
  integer(ip)                :: i
  real(rp)                   :: fct
  
  fct = 1.0/float(n_pe)
  xn(1:ndim,1:n_pe) = xn_e(1:ndim,1:n_pe)
  !.
  do i=1,ndim
    xn(i,nlib) = fct*sum(xn_e(i,1:n_pe))
  end do
  return
end subroutine mesh_bubble
!--------------------------------------------------------------------------------
subroutine matriz_me_y_ke(ntqp, ndim, npe, flg_b, xn, Cm, D, Ke, Me, ierr)
!--------------------------------------------------------------------------------
  implicit none
  integer(ip), intent(in)     :: ntqp, ndim, npe
  logical                     :: flg_b 
  real(rp),    intent(in)     :: xn(ndim,npe), Cm, D(ndim,ndim) 
  !.
  real(rp),    intent(out)    :: Ke(npe,npe), Me(npe)
  integer(ip), intent(out)    :: ierr
  !.
  real(rp)                    :: dcf(ndim,npe), MeC(npe,npe)
  integer(ip)                 :: i,j 
  real(rp)                    :: djb, dv


  Ke = 0.0; Me=0.0; MeC = 0
  do i=1,ntqp
    call jacobiano (ndim, npe, xn, df(1:ndim,:,i), djb, dcf, ierr)
    if (ierr == 1) then
      print *,'ptg', i, 'djb',djb,'determinant of jacobian'
      stop
    end if
    dv  = q_wt(i)*djb
    Ke  = Ke + mat_Ke (npe, ndim, npe, dcf, D, dv)
    !.
    !Me  = Me + Cm*dv
    !.
    Mec = mat_Me (npe, ff(:,i), Cm, dv)
    do j=1,npe
      Me(j)  = Me(j) + sum(Mec(j,:))
    end do
  end do
  !.
!  call scale_Me (ndim, npe, flg_b, Me)
  return
end subroutine matriz_me_y_ke
!--------------------------------------------------------------------------------
subroutine matriz_me_y_ke_2 (ntqp, ndim, npe, xn, Cm, D, Ke, Me, ierr)
!--------------------------------------------------------------------------------
  implicit none
  integer(ip), intent(in)     :: ntqp, ndim, npe
  real(rp),    intent(in)     :: xn(ndim,npe), Cm, D(ndim,ndim) 
  !.
  real(rp),    intent(out)    :: Ke(npe,npe), Me(npe,npe)
  integer(ip), intent(out)    :: ierr
  !.
  real(rp)                    :: dcf(ndim,npe)!, MeC(npe,npe)
  integer(ip)                 :: i         
  real(rp)                    :: djb, dv

  Ke = 0.0; Me=0.0
  !.
  do i=1,ntqp
    call jacobiano (ndim, npe, xn, df(1:ndim,:,i), djb, dcf, ierr)
    if (ierr == 1) then
      print *,'ptg', i, 'djb',djb
      stop
    end if
    dv  = q_wt(i)*djb
    !.
    Ke  = Ke + mat_Ke (npe, ndim, npe, dcf, D, dv)
    Me  = Me + mat_Me (npe, ff(:,i), Cm, dv)
  end do
  !.
  return
end subroutine matriz_me_y_ke_2
!  -----------------------------------------------------------------------------
function mat_Me(n,ff,Cm,dv) result(Me)
!  -----------------------------------------------------------------------------
!  matriz elemental Me para problemas de potencial
!  -----------------------------------------------------------------------------
  implicit none
  integer, intent(in)    :: n
  real(rp), intent(in)   :: ff(n),Cm,dv
  real(rp)               :: CmDv,Me(n,n)
  integer                :: i
  
  CmDv=Cm*dv
  do i=1,n
    Me(i,:)= CmDv*ff(i)*ff
  end do
  return
end function mat_Me
!  -----------------------------------------------------------------------------
function mat_Ke (nd,nt,nl,dcf,D,dv) result(Ke)
!  -----------------------------------------------------------------------------
!  matriz elemental Ke para problemas de potencial
!  -----------------------------------------------------------------------------
!  nd : numero de nodos por elemento
!  nt : numero de tensiones
!  nl : numero de libertades del elemento = (nd*n_gdlpornodo)
!  dcf: derivada cartesiana de las funciones de forma
!  D  : matriz constitutiva
!  dv : volumen del elemento
!  -----------------------------------------------------------------------------
  implicit none
  integer(ip),intent(in)  :: nd,nt,nl
  real(rp),   intent(in)  :: dcf(nt,nd),D(nt,nt)
  real(rp),   intent(in)  :: dv
  real(rp)                :: DB(nt,nl)
  real(rp)                :: Ke(nl,nl)

  DB  = matmul(D,dcf)     !.B = dcf
  Ke  = matmul(transpose(dcf),DB)*dv
  return
end function mat_Ke
!--------------------------------------------------------------------------------
subroutine scale_Me (ndim, npe, flg_b, Me)
!--------------------------------------------------------------------------------
  implicit none
  real(rp), parameter         :: u_t=1.0/3.0, u_s=1.0/6.0
  integer(ip), intent(in)     :: ndim, npe
  logical                     :: flg_b
  real(rp),    intent(inout)  :: Me(npe)
  !.
  select case (ndim)
  case (1)
    select case (npe)
    case (2); Me(1:2) = 0.5*Me(1:2)
    case (3); Me(1:2) = 0.25*Me(1:2); Me(3) = 0.50*Me(3)  
    case default
    end select
  case(2)
    select case(npe)
    case (3); Me(1:3) =  u_t*Me(1:3)
    case (4)
      if (flg_b) then
        Me(1:3) = u_s*Me(1:3); Me(4)=0.5*Me(4);
      else
        Me(1:4) = 0.25*Me(1:4);
      end if
    case (5); Me(1:4) = 0.25*Me(1:4); Me(5) = 0.0; 
      ! Me(1:5) = 0.2*Me(1:5)  Me(1:4) = 0.1875*Me(1:4); Me(5) = 0.25*Me(5); ! Me(1:5) = 0.2*Me(1:5)
    case default                                       
    end select
  case(3)
    select case(npe)
    case(4); Me(1:4) = 0.2500*Me(1:4);
    case(5); Me(1:4) = 0.1250*Me(1:4);  Me(5) = 0.50*Me(5);
    case(8); Me(1:8) = 0.1250*Me(1:8);
    case(9); Me(1:8) = 0.0625*Me(1:8); Me(9)=0.5*Me(9);
     ! Me(1:8) = 0.09375*Me(1:8); Me(9)=0.25*Me(9);
    case default
    end select
  case default
    stop 'Error en M_masa_c_nodal, con ndim'
  end select
  return
end subroutine scale_Me
!--------------------------------------------------------------------------------
subroutine Cond_alm_me(iel, nr, n, dt, Me, Ke, Kr)
!--------------------------------------------------------------------------------
!. This subroutine performs the static condensation of the stiffness matrix and 
!. saves
!. Macroelement mass matrix and submatrices K_21 y K22^-1.
!. iel : macro element number
!. nr  : reduced number of degrees of freedom
!. n   : number of nodes in macro element, internal + external nodes
!. Ke  : macro element stiffness matrix,
!. Me  : macro elemento mass matrix
!. Kr  : Reduced stiffness matrix. This the matrix to be assembled
!--------------------------------------------------------------------------------
  use mod_invcond, only: InvCondMat
  implicit none
  !.
  integer(ip), intent(in)     :: iel, nr, n
  real(rp),    intent(in)     :: dt, Me(:)
  real(rp),    intent(inout)  :: Ke(:,:)
  real(rp),    intent(out)    :: Kr(:,:)
  !.
  integer(ip)                 :: i, m, l
 

  Ke=dt*Ke;
  do i=1, n 
    Ke(i,i) = Me(i) + Ke(i,i); 
  enddo
  !.  
  call InvCondMat ( n, nr, Ke) !.Condensacion de la matriz Ke
  !.
  m= n - nr; l= nr+1
  !.
  Kr(1:nr,1:nr) = Ke(1:nr,1:nr);
  !.
  edat(iel)%Me   (1:n)     = Me(1:n)
  !edat(iel)%Ke21 (1:m,1:nr)= Ke(l:n,1:nr)
  edat(iel)%Ke21 (1:m,1:nr)= matmul(Ke(l:n,l:n), Ke(l:n,1:nr))
  !
  edat(iel)%Ke22i(1:m,1:m) = Ke(l:n,l:n)
  !.
  return
end subroutine Cond_alm_me
!--------------------------------------------------------------------------------
subroutine Cond_alm_me_exp (iel, nr, n, dt, Me, Ke, Kr)
!--------------------------------------------------------------------------------
!. Esta rutina hace la condensacion estatica de la matriz de rigidez y guarda 
!. la matriz de masa del macro elemento y las submatrices K_21 y K22^-1.
!. iel : numero del macro elemento
!. nr  : numero de grados de libertad reducido 
!. n   : numero de nodos en el macro elemento, nodos internos mas externos
!. Ke  : matriz de rigidez del macro elemento,
!. Me  : matriz de masa del macro elemento
!. Kr  : matriz de rigidez reducida. Esta es la matriz que se ensambla
!--------------------------------------------------------------------------------
  use mod_invcond, only: InvCondMat
  implicit none
  integer(ip), intent(in)     :: iel, nr, n
  real(rp),    intent(in)     :: dt, Me(:)
  real(rp),    intent(inout)  :: Ke(:,:)
  real(rp),    intent(out)    :: Kr(:,:)
  !.
  integer(ip)                 :: i, m, l
 

  Ke= - dt*Ke;
  !.
  m= n - nr; l= nr+1
  !.
  Kr=0.0
  do i=1,nr
    Kr(i,i) =1.0/Me(i);
  end do
  !.
  edat(iel)%Me(1:n)    = Me(1:n)
  edat(iel)%Ke(1:n,1:n)= Ke(1:n,1:n)
  !.
  do i=1,m
    edat(iel)%Me22i(i)    = 1.0/Me(nr+i)
  end do
  !.
  return
end subroutine Cond_alm_me_exp
! -------------------------------------------------------------------------------
subroutine allocatememory (flg_ei,n_nod, n_elm, nlib, n_pe)
! -------------------------------------------------------------------------------
  implicit none
  logical,     intent(in)      :: flg_ei  !.me indica si el macro es implicito/explicito
  integer(ip), intent(in)      :: n_nod, n_elm, nlib, n_pe
  !.
  integer(ip)                  :: n_int, i
  integer(ip)                  :: i_err

  
  if (n_nod > 0 ) then
    allocate(vnd%Vo_n(n_nod),vnd%Vn_n(n_nod),vnd%Vp_n(n_nod),vnd%Va_n(n_nod),   &
             vnd%Qi_n(n_nod),vnd%Rhs(n_nod), stat=i_err)
    if (i_err /= 0) call w_error (4,'allocatememory, vnd', 1)
    vnd%Vo_n = 0.0; vnd%Vn_n = 0.0; vnd%Vp_n = 0.0 
    vnd%Va_n = 0.0; vnd%Qi_n = 0.0; vnd%Rhs  = 0.0
  else
    call w_error ( 66, n_nod, 1)
  end if
  !.
  n_int = nlib - n_pe
  !.
  if ( (n_int > 0).and.(n_elm > 0) ) then
    allocate(velm%Vo_e(n_int,n_elm),velm%Vn_e(n_int,n_elm),velm%Vp_e(n_int,n_elm), &
             velm%Va_e(n_int,n_elm),velm%Qi_e(n_int,n_elm),edat(n_elm),stat=i_err)
    if (i_err /= 0) call w_error (4,'allocatememory, velm, edat', 1)
    !.
    velm%Vo_e = 0.0; velm%Vn_e = 0.0; velm%Vp_e = 0.0
    velm%Va_e = 0.0; velm%Qi_e = 0.0;
    !.
    do i=1,n_elm
      if (flg_ei) then
        allocate (edat(i)%Me(nlib),edat(i)%Ke(nlib,nlib),edat(i)%Me22i(n_int),stat=i_err)
        if (i_err /= 0) call w_error (4,'(sub: allocate_edat) => edat(i)%...',1)
        edat(i)%Me   = 0.0; edat(i)%Ke = 0.0; edat(i)%Me22i= 0.0
      else
        allocate (edat(i)%Me(nlib),edat(i)%Ke21(n_int,n_pe),edat(i)%Ke22i(n_int,n_int),  &
                  edat(i)%Fi(n_int),stat=i_err)
        if (i_err /= 0) call w_error (4,'(sub: allocate_edat) => edat(i)%...',1)
        edat(i)%Me   = 0.0; edat(i)%Ke21 = 0.0
        edat(i)%Ke22i= 0.0; edat(i)%Fi   = 0.0
      end if
    end do
  else
    if (n_elm <= 0) call w_error ( 67, n_elm, 1)
  end if
  ! 
  return
end subroutine allocatememory
!  ------------------------------------------------------------------------------
subroutine  m_elm_pot (ndimXD, iel, prblm, m_e, elm, Ke)
!  ------------------------------------------------------------------------------
! ---------------------- Variables locales del modulo ---------------------------
! xn(:,:)     : coordenadas de los nodos del macro elemento
! xyz_me(ndim,    nnod_me): coord. de todos los elem. componentes del macro elemento
! elm_me(n_pe_me, nelm_me): conectividad de todos los elementos del macro elemento


!  D(:,:)   : matriz que contiene las propiedades del material del elemento
!  B(:,:)   : array de las derivadas cartesianas de la funcion de forma
!  DB(:,:)  : D * B
!  BDB(:,:) : B^T * D * B 
!  dcf(:,:) : derivada cartesiana de las funciones de forma
!  -----------------------------------------------------------------------------
  implicit none
  real(rp), parameter                 :: u_36=1.0/36.0, u_60 = 1.0/60.0
  integer(ip),   intent(in)           :: ndimXD, iel
  type(t_prblm), intent(in)           :: prblm
  type (t_me),   intent(in)           :: m_e
  type(t_e),     intent(in)           :: elm
  real(rp),      intent(out),optional :: Ke(:,:)
!--------- Variables del elemento a integrar ------------------------------------
  real(rp)                          :: xn(elm%ndim,elm%nlib) 
  integer(ip)                       :: cn(elm%nlib)
  real(rp)                          :: KeB(elm%nlib, elm%nlib), MeB(elm%nlib)
! ------- Variables del macro elemento ------------------------------------------
  real(rp)                          :: xn_me(ndimXD,m_e%n_pe)
  integer(ip)                       :: cn_me(m_e%n_pe)
  !-- Malla en el macro elemento
  real(rp)                          :: xn_mme(elm%ndim, m_e%nnod)
  integer(ip)                       :: cn_mme(elm%nlib, m_e%nelm)
  real(rp)                          :: Ke_mme(m_e%nnod, m_e%nnod), Me_mme(m_e%nnod)
!  real(rp)                          :: Me_mme_c(m_e%nnod,m_e%nnod)
! -------------------------------------------------------------------------------
  real(rp)                          :: D(elm%ndim,elm%ndim), Cm
  real(rp)                          :: data(3),f_dr(3), dt , At, h!, i_sm, h
  integer(ip)                       :: i, pp_e, ierr, p_mat, n_rea !, j, k,
  integer(ip)                       :: iam 

  MeB = 0.0; KeB = 0.0

!!$  call get_melmdata (iel,elm%ndim, m_e%n_pe, cn_me, xn_me, pp_e)
!!$  !.--- obtencion de la direccion de la fibra y los datos del material -------
!!$  call get_elmpropmat (pp_e, elm%ndim, f_dr(1:elm%ndim), data) 
!!$  !.Tensor de difusividad y capacitancia -------------------------------------
!!$  call get_diften_cap (elm%ndim, f_dr(1:elm%ndim), data, D, Cm)
  !.--- Conectividad, coordenadas y propiedad del elemento macro elemento--------
  call get_melmdata (iel, prblm%mshdata, ndimXD, m_e%n_pe, cn_me, xn_me, pp_e)
  !.--- Obtencion de la direccion de la fibra y los datos del material ----------
  call get_propelm(pp_e, prblm%mshdata%prel, p_mat, n_rea, f_dr(1:elm%ndim))
  if (n_rea /= elm%ndim) write (*,'(A)') 'La cagaste con la direccion de fibras'
  call get_materialprop(p_mat, prblm%mshdata%mate,data=data) 
  !.--- Tensor de difusividad y capacitancia ------------------------------------
  call get_diften_cap (elm%ndim, f_dr(1:elm%ndim), data, D, Cm)
  !.--- Mallado del macro elemento ----------------------------------------------
  if (elm%ndim /= ndimXD) then
    xn(1,1)=0.0
    do i=2,elm%n_pe
      xn(1,i)=sqrt( (xn_me(1,1) - xn_me(1,i))**2 + (xn_me(2,1) - xn_me(2,i))**2 + &
              (xn_me(3,1) - xn_me(3,i))**2)
    end do
    xn_me(:,:)=0.0; xn_me(1,:)=xn(1,:)
  end if
  !.
  call malla_me (elm%ndim,m_e%n_pe,xn_me,elm%flg_b, elm%nlib,  m_e%nnod,        &
                 m_e%nelm,xn_mme,cn_mme)
  !.
  ! -------- ensamble del sistema de ecuaciones en el macro elemento ------------
  Ke_mme = 0.0; Me_mme=0.0
  !.
  do i = 1, m_e%nelm !.Numero de elementos en el macro elemento
    cn(1:elm%nlib)            = cn_mme(1:elm%nlib, i) !.Conectividad
    xn(1:elm%ndim, 1:elm%nlib)= xn_mme(:,cn)          !.Coordenadas del elemento 
    !.
    call matriz_me_y_ke(elm%ntqp,elm%ndim,elm%nlib,elm%flg_b,xn,Cm,D, KeB,MeB,ierr)
  !  call matriz_me_y_ke_2 (elm%ntqp, elm%ndim, elm%npe, xn, Cm, D, Ke, Me, ierr)
 
    !.
    call ensambla_me (elm%nlib, cn, KeB, MeB, Ke_mme, Me_mme)
  end do
  !.
  !.Condensacion estatica.
  call get_stpdata_time_inc (prblm%stpdata%timeinc, dtini = dt)
  !.  
!  call mpi_comm_rank (mpi_comm_world, iam,ierr)

!  if (iel == 1) then
!    do i=1, m_e%nnod
 !      write (iam+100,'(50(2X,F12.9))') Ke_mme(i,:)
!    end do
!   write (iam+100,  '(50(2X,F12.9))') Me_mme
!  end if
  
  !.
  select case (elm%ndim)
  case(2)
    h=sqrt( (xn_mme(1,1)-xn_mme(1,2))**2+(xn_mme(2,1)-xn_mme(2,2))**2 )
    At=h*h
    if (m_e%nnod == 8) then !.opcion para macro elemento lineal MQ05HT2DQ04
      Me_mme(1:4) = 0.155330085889911*At
      Me_mme(5:8) = 0.094669914110089*At
    end if
    !.
    if ( m_e%nnod == 13) then !.opcion para macro elemento con burbuja MQ05HT2DQ04B
      Me_mme(1:4) = 0.116497564417433*At
      Me_mme(5:8) = 0.071002435582567*At
      Me_mme(9:12)= 0.051776695296637*At
      Me_mme(13)  = 0.042893218813452*At
    end if
  case(3)
!!$    h=sqrt((xn_mme(1,1)-xn_mme(1,2))**2+(xn_mme(2,1)-xn_mme(2,2))**2+ &
!!$           (xn_mme(3,1)-xn_mme(3,2))**2)
!!$    At=h*h*h
!!$    if (m_e%nnod == 16) then
!!$      !.opcion para macro elemento lineal MQ05HT2DQ04
!!$      Me_mme(1: 8) = 0.09375*At
!!$      Me_mme(9:16) = 0.03125*At
!!$    end if
    if (m_e%nnod == 23) then
      !.opcion para macro elemento lineal MQ05HT2DQ04
      At=0.5*sum(Me_mme(9:23))
      Me_mme( 1: 8) =  Me_mme(1:8) + 0.125*At
      Me_mme( 9:23) =  0.5*Me_mme(9:23)
!!$      Me_mme(17:23) = u_60*At
!!$      !.
!!$      Me_mme=0.85*Me_mme
!!$
!!$     ! Me_mme( 1: 8) = 3.0/40.0*At
!!$     ! Me_mme( 9:16) = 1.0/40.0*At
!!$     ! Me_mme(17:22) = 1.0/28.0*At
!!$     ! Me_mme( 23)    = 2.0/28.0*At
!!$     
!!$     ! Me_mme( 1: 8) = (22.0/320.0)*At
!!$     ! Me_mme( 9:16) = (6.0/320.0)*At
!!$     ! Me_mme((/17,18,20,22/)) = (1.0/40.0)*At
!!$     ! Me_mme((/19,21/))       = (3.0/40.0)*At
!!$     ! Me_mme( 23) = (1.0/20.0)*At
    end if
  case default
  end select
  !
  call Cond_alm_me(iel, m_e%n_pe, m_e%nnod, dt, Me_mme, Ke_mme, Ke)
  ! write (*,'(A)')'###.Matriz de rigidez del macro elemento condensado'
  ! do i=1,m_e%nnod
  !   write (*,'(100(2X,F12.6))') (Ke_mme(i, j), j=1,m_e%nnod)
  ! end do

  ! write (*,'(A)')'###.-------------Matriz elemental a ensamblar------------------'
  ! do j=1,4
  !   write (*,'(100(2X,F12.6))') (Ke(j, k), k=1,4)
  ! end do
  ! pause
  !------------------------------------------------------------------------------
  return
end subroutine m_elm_pot
!  ------------------------------------------------------------------------------
subroutine  m_elm_pot_exp (ndimXD, iel, prblm, m_e, elm, Me)
!  ------------------------------------------------------------------------------
! ---------------------- Variables locales del modulo ---------------------------
! xn(:,:)     : coordenadas de los nodos del macro elemento
! xyz_me(ndim,    nnod_me): coord. de todos los elem. componentes del macro elemento
! elm_me(n_pe_me, nelm_me): conectividad de todos los elementos del macro elemento


!  D(:,:)   : matriz que contiene las propiedades del material del elemento
!  B(:,:)   : array de las derivadas cartesianas de la funcion de forma
!  DB(:,:)  : D * B
!  BDB(:,:) : B^T * D * B 
!  dcf(:,:) : derivada cartesiana de las funciones de forma
!  -----------------------------------------------------------------------------
  implicit none
  integer(ip),   intent(in)           :: ndimXD, iel
  type(t_prblm), intent(in)           :: prblm
  type (t_me),   intent(in)           :: m_e
  type(t_e),     intent(in)           :: elm
  real(rp),      intent(out),optional :: Me(:,:)
!--------- Variables del elemento a integrar ------------------------------------
  real(rp)                          :: xn(elm%ndim,elm%nlib) 
  integer(ip)                       :: cn(elm%nlib)
  real(rp)                          :: KeB(elm%nlib, elm%nlib), MeB(elm%nlib)
! ------- Variables del macro elemento ------------------------------------------
  real(rp)                          :: xn_me(elm%ndim,m_e%n_pe)
  integer(ip)                       :: cn_me(m_e%n_pe)
  !-- Malla en el macro elemento
  real(rp)                          :: xn_mme(elm%ndim, m_e%nnod)
  integer(ip)                       :: cn_mme(elm%nlib, m_e%nelm)
  real(rp)                          :: Ke_mme(m_e%nnod, m_e%nnod), Me_mme(m_e%nnod)
!  real(rp)                          :: Me_mme_c(m_e%nnod,m_e%nnod)

! ------------
  real(rp)                          :: D(elm%ndim,elm%ndim), Cm
  real(rp)                          :: data(3),f_dr(3), dt , At!, i_sm, h
  integer(ip)                       :: i, pp_e, ierr, p_mat, n_rea! , j, k
  integer(ip)                       :: iam 

  MeB = 0.0; KeB = 0.0
  !.--- Conectividad, coordenadas y propiedad del elemento macro elemento--------
  call get_melmdata (iel, prblm%mshdata, elm%ndim, m_e%n_pe, cn_me, xn_me, pp_e)
  !.--- Obtencion de la direccion de la fibra y los datos del material ----------
  call get_propelm(pp_e, prblm%mshdata%prel, p_mat, n_rea, f_dr(1:elm%ndim))
  if (n_rea /= elm%ndim) write (*,'(A)') 'La cagaste con la direccion de fibras'
  call get_materialprop(p_mat, prblm%mshdata%mate,data=data) 
  !.--- Tensor de difusividad y capacitancia ------------------------------------
  call get_diften_cap (elm%ndim, f_dr(1:elm%ndim), data, D, Cm)
  !.--- Mallado del macro elemento
  call malla_me (elm%ndim,m_e%n_pe,xn_me,elm%flg_b, elm%nlib,  m_e%nnod,        &
                 m_e%nelm,xn_mme,cn_mme)
  !.
  ! -------- ensamble del sistema de ecuaciones en el macro elemento ------------
  Ke_mme = 0.0; Me_mme=0.0
  !.
  do i = 1, m_e%nelm !.Numero de elementos en el macro elemento
    cn(1:elm%nlib)            = cn_mme(1:elm%nlib, i) !.Conectividad
    xn(1:elm%ndim, 1:elm%nlib)= xn_mme(:,cn)          !.Coordenadas del elemento 
    !.
    call matriz_me_y_ke(elm%ntqp,elm%ndim,elm%nlib,elm%flg_b,xn,Cm,D, KeB,MeB,ierr)
  !  call matriz_me_y_ke_2 (elm%ntqp, elm%ndim, elm%npe, xn, Cm, D, Ke, Me, ierr)
 
    !.
    call ensambla_me (elm%nlib, cn, KeB, MeB, Ke_mme, Me_mme)
  end do
  !.
  !.Condensacion estatica.
  call get_stpdata_time_inc (prblm%stpdata%timeinc, dtini = dt)
  !.  
  call mpi_comm_rank (mpi_comm_world, iam,ierr)
!!$  if (iel == 1) then
!!$    do i=1, m_e%nnod
!!$      write (iam+100,'(50(2X,F12.9))') Ke_mme(i,:)
!!$    end do
!!$   write (iam+100,  '(50(2X,F12.9))') Me_mme
!!$  end if
  !.
  select case (elm%ndim)
  case(3)
    if (m_e%nnod == 23) then
      !.opcion para macro elemento lineal MQ05HT2DQ04
      At=0.5*sum(Me_mme(9:23))
      Me_mme( 1: 8) =  Me_mme(1:8) + 0.125*At
      Me_mme( 9:23) =  0.5*Me_mme(9:23)
    end if
  case default
  end select
  !
  call Cond_alm_me_exp (iel, m_e%n_pe, m_e%nnod, dt, Me_mme, Ke_mme, Me)

  
!  write (*,'(A)') '###.Matriz de rigidez del macro elemento condensado'
!  do i=1,m_e%nnod
!    write (*,'(100(2X,F12.6))') (Ke_mme(i,j), j=1, m_e%nnod)
!  end do

!  write (*,'(A)')'###.-------------Matriz elemental a ensamblar------------------'
!  do j=1,4
!    write (*,'(100(2X,F12.6))') (Me(j, k), k=1,4)
!  end do
!  pause
  !------------------------------------------------------------------------------
  return
end subroutine m_elm_pot_exp
!------------------------------------------------------------------------------

!!$!  ------------------------------------------------------------------------------
!!$subroutine  m_elm_pot_exp (iel, mshdata, m_e, elm, Me)
!!$!  ------------------------------------------------------------------------------
!!$! ---------------------- Variables locales del modulo ---------------------------
!!$! xn(:,:)     : coordenadas de los nodos del macro elemento
!!$! xyz_me(ndim,    nnod_me): coord. de todos los elem. componentes del macro elemento
!!$! elm_me(n_pe_me, nelm_me): conectividad de todos los elementos del macro elemento
!!$
!!$
!!$!  D(:,:)   : matriz que contiene las propiedades del material del elemento
!!$!  B(:,:)   : array de las derivadas cartesianas de la funcion de forma
!!$!  DB(:,:)  : D * B
!!$!  BDB(:,:) : B^T * D * B 
!!$!  dcf(:,:) : derivada cartesiana de las funciones de forma
!!$!  -----------------------------------------------------------------------------
!!$  implicit none
!!$  integer(ip), intent(in)           :: iel
!!$  type(t_mesh), intent(in)          :: mshdata
!!$  type (t_me), intent(in)           :: m_e
!!$  type(t_e),   intent(in)           :: elm
!!$  real(rp),    intent(out),optional :: Me(:)
!!$!--------- Variables del elemento a integrar ------------------------------------
!!$  real(rp)                          :: xn(elm%ndim,elm%nlib) 
!!$  integer(ip)                       :: cn(elm%nlib)
!!$  real(rp)                          :: KeB(elm%nlib, elm%nlib), MeB(elm%nlib)
!!$! ------- Variables del macro elemento ------------------------------------------
!!$  real(rp)                          :: xn_me(elm%ndim,m_e%n_pe)
!!$  integer(ip)                       :: cn_me(m_e%n_pe)
!!$  !-- Malla en el macro elemento
!!$  real(rp)                          :: xn_mme(elm%ndim, m_e%nnod)
!!$  integer(ip)                       :: cn_mme(elm%nlib, m_e%nelm)
!!$  real(rp)                          :: Ke_mme(m_e%nnod, m_e%nnod), Me_mme(m_e%nnod)
!!$! ------------
!!$  real(rp)                          :: D(elm%ndim,elm%ndim), Cm
!!$  real(rp)                          :: data(3),f_dr(3), dt, h, At
!!$  integer(ip)                       :: i, pp_e, ierr,j,k
!!$
!!$
!!$  MeB = 0.0; KeB = 0.0
!!$  !.  
!!$  !.--- conectividad, coordenadas y propiedad del elemento macro elemento------
!!$
!!$  call get_melmdata (iel,elm%ndim, m_e%n_pe, cn_me, xn_me, pp_e)
!!$  !.--- obtencion de la direccion de la fibra y los datos del material --------
!!$  call get_elmpropmat (pp_e, elm%ndim, f_dr(1:elm%ndim), data) 
!!$  !.Tensor de difusividad y capacitancia --------------------------------------
!!$  call get_diften_cap (elm%ndim, f_dr(1:elm%ndim), data, D, Cm)
!!$  !.Mallado del macro elemento
!!$  call malla_me (elm%ndim,m_e%n_pe,xn_me,elm%flg_b, elm%nlib,  m_e%nnod,        &
!!$                 m_e%nelm,xn_mme,cn_mme)
!!$  ! -------- ensamble del sistema de ecuaciones en el macro elemento ------------
!!$  Ke_mme = 0.0; Me_mme=0.0
!!$  !.
!!$  do i = 1, m_e%nelm !.Numero de elementos en el macro elemento
!!$    cn(1:elm%nlib)            = cn_mme(1:elm%nlib, i) !.Conectividad
!!$    xn(1:elm%ndim, 1:elm%nlib)= xn_mme(:,cn)          !.Coordenadas del elemento 
!!$    !.
!!$    call matriz_me_y_ke(elm%ntqp,elm%ndim,elm%nlib,elm%flg_b,xn,Cm,D, KeB,MeB,ierr) 
!!$    !.
!!$    call ensambla_me (elm%nlib, cn, KeB, MeB, Ke_mme, Me_mme)
!!$  end do
!!$  !------------------------------------------------------------------------------
!!$  !.Condensacion estatica. En este caso no hace falta hacer condensacion estatica
!!$  ! solo hay que almacenar las matrices correspondientes
!!$  call get_dt (dt)
!!$  !.
!!$  call Cond_alm_me_exp (iel, m_e%n_pe, m_e%nnod, dt, Me_mme, Ke_mme, Me)
!!$  !------------------------------------------------------------------------------
!!$  return
!!$end subroutine m_elm_pot_exp
!--------------------------------------------------------------------------------
subroutine ensambla_me (npe, cn_e, KeB, MeB, Ke_me, Me_me)
!--------------------------------------------------------------------------------
  implicit none
  integer(ip), intent(in)     :: npe, cn_e(:)
  real(rp),    intent(in)     :: KeB(:,:), MeB(:)
  real(rp),    intent(inout)  :: Ke_me(:,:), Me_me(:)
  !.
  integer(ip)                 :: i, j, i_fl, j_cl

  do i=1,npe
    i_fl = cn_e(i)
    do j=1,npe
      j_cl = cn_e(j)
      Ke_me(i_fl,j_cl) = Ke_me(i_fl,j_cl) + KeB(i,j)
    end do
    Me_me(i_fl) =  Me_me(i_fl) + MeB(i)
  end do
  return
end subroutine ensambla_me
! ------------------------------------------------------------------------------
subroutine Rhs_Bubble (n_elm, elem, Vnqi_n, Veqi_n, Rhs)
! ------------------------------------------------------------------------------
! n_elm  : numero de elementos
! Vnqi_n : potencial en los vertices de los macro elementos. 
!          Calculado a partir de la ec. unicelular
! Veqi_n : potencial en el interior de los macro elementos.
!          Calculado a partir de la ec. unicelular
! Rhs    : termino derecho ensamblado.
! ------------------------------------------------------------------------------
!.n_pe   : numero de vertices en el macro elemento
!.n_in = nlib - n_pe : numero de nodos dentro de cada macro elemento
! ------------------------------------------------------------------------------
  implicit none
  integer(ip), intent(in)    :: n_elm
  type(t_obelm)              :: elem
  real(rp),intent(in)        :: Vnqi_n(:), Veqi_n(:,:)
  real(rp),intent(out)       :: Rhs(:)
  !.
  integer(ip)                :: tipo, i, npe
  integer(ip), save          :: i_tipo=0, n_pe=0, nlib=0, n_in=0
  integer(ip)                :: conc(8)
  real(rp)                   :: Vv_me(8)

  !.--- Suponemos que todos los macroelementos son iguales ----------------------
  tipo  = get_elemtipo (1,elem)      !.tipo de elemento
  if (i_tipo /= tipo) then
    call id_elem_tip (tipo, n_pe=n_pe, nlib = nlib)  !.numero de nodos y libertades
    n_in = nlib - n_pe                               !.numero de nodos internos
    i_tipo = tipo
  end if
  !.
  Rhs=0.0
  do i=1,n_elm              !.Loop sobre los elementos
    call get_elmconc ( i, npe, conc(1:n_pe), elem) !.Conectividad nodal
    !.Recupero el potencial de los vertices del elemento. (macro elemento)
    Vv_me (1:n_pe) = Vnqi_n( conc(1:n_pe) )
    !.Veqi_n(1:n_in,i): potencial en los nodos interiores de cada elemento. (macro elemento)
    !.Calculo el aporte al termino derecho del macro elemento.
    Rhs(conc(1:n_pe)) = Rhs(conc(1:n_pe)) + Rhs_me(i, n_pe, n_in, nlib, Vv_me, Veqi_n(1:n_in,i))
    !.
  end do
  !.
  return
end subroutine Rhs_Bubble
! ------------------------------------------------------------------------------
subroutine Rhs_Bubble02 (n_elm, elem, Vnqi_n, Veqi_n, Rhs)
! ------------------------------------------------------------------------------
! n_elm  : numero de elementos
! Vnqi_n : potencial en los vertices de los macro elementos. 
!          Calculado a partir de la ec. unicelular
! Veqi_n : potencial en el interior de los macro elementos.
!          Calculado a partir de la ec. unicelular
! Rhs    : termino derecho ensamblado.
! ------------------------------------------------------------------------------
!.n_pe   : numero de vertices en el macro elemento
!.n_in = nlib - n_pe : numero de nodos dentro de cada macro elemento
! ------------------------------------------------------------------------------
  implicit none
  integer(ip), intent(in)    :: n_elm
  type(t_obelm)              :: elem
  real(rp),intent(in)        :: Vnqi_n(:), Veqi_n(:,:)
  real(rp),intent(out)       :: Rhs(:)
  !.
  integer(ip)                :: tipo, i, npe
  integer(ip), save          :: i_tipo=0, n_pe=0, nlib=0, n_in=0
  integer(ip)                :: conc(8)
  real(rp)                   :: Vv_me(8)

  !.--- Suponemos que todos los macroelementos son iguales ----------------------
  tipo  = get_elemtipo (1,elem)      !.tipo de elemento
  if (i_tipo /= tipo) then
    call id_elem_tip (tipo, n_pe=n_pe, nlib = nlib)  !.numero de nodos y libertades
    n_in = nlib - n_pe                               !.numero de nodos internos
    i_tipo = tipo
  end if
  !.
  Rhs=0.0
  do i=1,n_elm              !.Loop sobre los elementos
    call get_elmconc ( i, npe, conc(1:n_pe), elem) !.Conectividad nodal
    !.print *,i,n_pe, conc(1:n_pe)
    !.Recupero el potencial de los vertices del elemento. (macro elemento)
    Vv_me (1:n_pe) = Vnqi_n( conc(1:n_pe) )
    !.Veqi_n(1:n_in,i): potencial en los nodos interiores de cada elemento. (macro elemento)
    !.Calculo el aporte al termino derecho del macro elemento.
    Rhs(conc(1:n_pe)) = Rhs(conc(1:n_pe)) + Rhs_me02(i, n_pe, n_in, nlib, Vv_me, Veqi_n(1:n_in,i))
    !.
  end do
  !.
  return
end subroutine Rhs_Bubble02
! ------------------------------------------------------------------------------
subroutine Rhs_Bubble_exp (n_elm, elem, Vo_n, Vn_n, Vo_e, Vn_e, Rhs)
! ------------------------------------------------------------------------------
! n_elm  : numero de elementos
! Vo_n   : potencial en los vertices de los macro elementos. 
!          Calculado a partir de la ec. unicelular
! Vn_n   : potencial en los vertices de los macro elementos, en el paso n 
! Vo_e   : potencial en el interior de los macro elementos.
!          Calculado a partir de la ec. unicelular
! Vn_e   : potencial en el interior de los macro elementos.
! Rhs    : termino derecho ensamblado.
! ------------------------------------------------------------------------------
!.n_pe   : numero de vertices en el macro elemento
!.n_in = nlib - n_pe : numero de nodos dentro de cada macro elemento
! ------------------------------------------------------------------------------
  implicit none
  integer(ip), intent(in)    :: n_elm
  type(t_obelm)              :: elem
  real(rp),intent(in)        :: Vo_n(:), Vn_n(:), Vo_e(:,:)
  real(rp),intent(inout)     :: Vn_e(:,:)
  real(rp),intent(out)       :: Rhs(:)
  !.
  integer(ip)                :: tipo, i, npe
  integer(ip), save          :: i_tipo=0, n_pe=0, nlib=0, n_in=0, m=0
  integer(ip)                :: conc(8)
  real(rp)                   :: Vo(30),Vn(30),Rhs_me(30)


  !.--- Suponemos que todos los macroelementos son iguales ----------------------
  tipo  = get_elemtipo (1,elem)      !.tipo de elemento
  if (i_tipo /= tipo) then
    call id_elem_tip (tipo, n_pe=n_pe, nlib = nlib)  !.numero de nodos y libertades
    n_in = nlib - n_pe !.numero de nodos internos
    m=n_pe+1
    i_tipo = tipo
  end if
  !.
  Rhs=0.0
  do i=1,n_elm              !.Loop sobre los elementos
    call get_elmconc ( i, npe, conc(1:n_pe), elem) !.Conectividad nodal
    !.Recupero el potencial de los vertices del elemento. (macro elemento)
    Vo(1:n_pe) = Vo_n(conc(1:n_pe))  !.Calculado a partir del qion
    Vn(1:n_pe) = Vn_n(conc(1:n_pe))  !.Calculado en el tiempo n
    !.Recupero el potencial en el interior de los elementos
    Vo(m:nlib) = Vo_e(1:n_in,i)  !.Calculado a partir del Iion
    Vn(m:nlib) = Vn_e(1:n_in,i)  !.Calculado en el tiempo n
    !.Calculo el aporte al termino derecho del macro elemento y el potencial 
    ! en el tiempo n+1 en el interior del elemento
    call RhsVne_me_exp (i, n_pe, n_in, nlib, Vo, Vn, Rhs_me, Vn_e(1:n_in,i))
    Rhs(conc(1:n_pe)) = Rhs(conc(1:n_pe)) + Rhs_me(1:n_pe)
    !.
  end do
  !.
  return
end subroutine Rhs_Bubble_exp
! ------------------------------------------------------------------------------
function Rhs_me02 (i, npe, nin, nlib, Vv, Vi) 
! ------------------------------------------------------------------------------
  implicit none
  integer(ip), intent(in)    :: i, npe, nin, nlib
  real(rp),    intent(in)    :: Vv(npe), Vi(nin)
  !.
  real(rp)                   :: Rhs_me02(npe)
  !.
  real(rp)                   :: Fv(npe), Fi(nin)
  
  
  Fv = edat(i)%Me(1:npe)     *Vv(1:npe);!  write (*,'(A,30(1X,F9.3))') 'Fv:',Fv
  Fi = edat(i)%Me(npe+1:nlib)*Vi(1:nin);!  write (*,'(A,30(1X,F9.3))') 'Fi:',Fi
  !.Se guarda para recuperar luego el potencial.
  edat(i)%Fi(1:nin) =  Fi(1:nin)
  !.
  Rhs_me02(1:npe) = Fv(1:npe) - matmul( transpose(edat(i)%Ke21),Fi(1:nin))  
  !.
  return
end function Rhs_me02
! ------------------------------------------------------------------------------
subroutine RhsVne_me_exp (i, npe, nin, nlib, Vo, Vn, Rhs, Ven1 )
! ------------------------------------------------------------------------------
  implicit none
  integer(ip), intent(in)    :: i, npe, nin, nlib
  real(rp),    intent(in)    :: Vo(nlib), Vn(nlib)
  !.
  real(rp),    intent(out)   :: Rhs(:),Ven1(:)
  !.
  real(rp)                   :: F(nlib)
  integer(ip)                :: m
  
  F  = edat(i)%Me*Vo(1:nlib) + matmul(edat(i)%Ke,Vn(1:nlib))
  !.
  Rhs(1:npe)= F(1:npe)
  !.
  m=npe+1
  Ven1(1:nin)= edat(i)%Me22i(1:nin)*F(m:nlib)
  !.
  return
end subroutine RhsVne_me_exp
! ------------------------------------------------------------------------------
function Rhs_me(i, npe, nin, nlib, Vv, Vi) 
! ------------------------------------------------------------------------------
  implicit none
  integer(ip), intent(in)    :: i, npe, nin, nlib
  real(rp),    intent(in)    :: Vv(npe), Vi(nin)
  !.
  real(rp)                   :: Rhs_me(npe)
  !.
  real(rp)                   :: Fv(npe), Fi(nin), K12K22i(npe, nin)
  
  
  Fv = edat(i)%Me(1:npe)     *Vv(1:npe);!  write (*,'(A,30(1X,F9.3))') 'Fv:',Fv
  Fi = edat(i)%Me(npe+1:nlib)*Vi(1:nin);!  write (*,'(A,30(1X,F9.3))') 'Fi:',Fi

  !.Se guarda para recuperar luego el potencial.
  edat(i)%Fi(1:nin) =  Fi(1:nin) 
  K12K22i = matmul(transpose(edat(i)%Ke21), edat(i)%Ke22i)  
  !.
  Rhs_me(1:npe) = Fv(1:npe) - matmul(K12K22i, Fi(1:nin))
  !.
  return
end function Rhs_me

! ------------------------------------------------------------------------------
subroutine Vn_Elem_Bubble (n_elm, elem, Vn_n, Vn_e)
! ------------------------------------------------------------------------------
! Vn_n : potencial en los nodos
! Ve_e : potencial en los elementos
! ------------------------------------------------------------------------------
  implicit none
  integer(ip), intent(in)    :: n_elm
  type(t_obelm),intent(in)   :: elem
  real(rp),intent(in)        :: Vn_n(:)
  real(rp),intent(out)       :: Vn_e(:,:)
  !.
  integer(ip)                :: tipo, i, npe
  integer(ip), save          :: i_tipo=0, n_pe=0, nlib=0, n_in=0
  integer(ip)                :: conc(8)
  real(rp)                   :: Vv(8)

  !.Suponemos que todos los macroelementos son iguales -------------------------
  tipo  = get_elemtipo (1,elem)      !.tipo de elemento
  if (i_tipo /= tipo) then
    call id_elem_tip (tipo, n_pe=n_pe, nlib = nlib)  !.numero de nodos y libertades
    n_in = nlib - n_pe                               !.numero de nodos internos
    i_tipo = tipo
  end if
  !.
  do i=1,n_elm
    call get_elmconc ( i, npe, conc(1:n_pe), elem) !.Conectividad nodal
    !.Recupero el potencial de los vertices del elemento. (macro elemento)
    Vv (1:n_pe)    = Vn_n(conc(1:n_pe))
    !.
    Vn_e(1:n_in,i) = Vn_intelm(i, n_pe, n_in, Vv)
  end do
  return
end subroutine Vn_Elem_Bubble
! ------------------------------------------------------------------------------
subroutine Vn_Elem_Bubble02 (n_elm, elem, Vn_n, Vn_e)
! ------------------------------------------------------------------------------
! Vn_n : potencial en los nodos
! Ve_e : potencial en los elementos
! ------------------------------------------------------------------------------
  implicit none
  integer(ip), intent(in)    :: n_elm
  type(t_obelm),intent(in)   :: elem
  real(rp),intent(in)        :: Vn_n(:)
  real(rp),intent(out)       :: Vn_e(:,:)
  !.
  integer(ip)                :: tipo, i, npe
  integer(ip), save          :: i_tipo=0, n_pe=0, nlib=0, n_in=0
  integer(ip)                :: conc(8)
  real(rp)                   :: Vv(8)

  !.Suponemos que todos los macroelementos son iguales -------------------------
  tipo  = get_elemtipo (1,elem)      !.tipo de elemento
  if (i_tipo /= tipo) then
    call id_elem_tip (tipo, n_pe=n_pe, nlib = nlib)  !.numero de nodos y libertades
    n_in = nlib - n_pe                               !.numero de nodos internos
    i_tipo = tipo
  end if
  !.
  do i=1,n_elm
    call get_elmconc ( i, npe, conc(1:n_pe), elem) !.Conectividad nodal
    !.Recupero el potencial de los vertices del elemento. (macro elemento)
    Vv (1:n_pe)    = Vn_n(conc(1:n_pe))
    !.
    Vn_e(1:n_in,i) = Vn_intelm02 (i, n_pe, n_in, Vv)
  end do
  return
end subroutine Vn_Elem_Bubble02
!  -----------------------------------------------------------------------------
function Vn_intelm(i, npe, nin, Vv) result(Vn_ie)
!  -----------------------------------------------------------------------------
  implicit none
  integer(ip), intent(in)    :: i, npe, nin
  real(rp),    intent(in)    :: Vv(:)
  !.
  real(rp)                   :: Vn_ie(nin)
  !.
  real(rp)                   :: Vaux(nin)

  !.
  Vaux(1:nin)  = edat(i)%Fi(1:nin) - matmul(edat(i)%Ke21, Vv(1:npe))  
  !.
  Vn_ie(1:nin) =  matmul(edat(i)%Ke22i, Vaux(1:nin))  
  return
end function Vn_intelm
!  -----------------------------------------------------------------------------
function Vn_intelm02 (i, npe, nin, Vv) result(Vn_ie)
!  -----------------------------------------------------------------------------
  implicit none
  integer(ip), intent(in)    :: i, npe, nin
  real(rp),    intent(in)    :: Vv(:)
  !.
  real(rp)                   :: Vn_ie(nin)
  !.
  real(rp)                   :: Vaux(nin)

  !.
  Vaux(1:nin)  =  matmul(edat(i)%Ke21, Vv(1:npe))  
  !.
  Vn_ie(1:nin) =  matmul(edat(i)%Ke22i, edat(i)%Fi(1:nin)) - Vaux(1:nin)   
  return
end function Vn_intelm02

!  -----------------------------------------------------------------------------
! -------------------------------------------------------------------------------
! &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
! -------------------------------------------------------------------------------
! ------------------------------------------------------------------------------
! ----------- DERIVADA TEMPORAL  Y REASIGNACION EN NODOS Y ELEMENTOS -----------
!  -----------------------------------------------------------------------------
subroutine DerTempReasig_Node (n, id_t, Vo_n, Vn_n, Va_n, Vp_n)
!  -----------------------------------------------------------------------------
  implicit none
  real(rp), parameter        :: Vtm = -3.0 
  integer(ip), intent(in)    :: n      !.numero de nodos
  real(rp),    intent(in)    :: id_t
  real(rp),    intent(inout) :: Vo_n(:), Vn_n(:), Va_n(:), Vp_n(:)


  Vp_n(1:n) = id_t*(Vn_n(1:n) - Va_n(1:n)); !.Devivada temporal
  !.
  !where (Vp_n(:) < Vtm)
  !  Vo_n(:) = 0.3*Vn_n(:) + 0.70*Vo_n(:)  !.Inyeccion de corriente axial
  !elsewhere
    Vo_n(:) = Vn_n(:)  
  !end where
  !.
  Va_n(1:n) = Vo_n(1:n)  !.Reasignacion para el prox. dt
  return
end subroutine DerTempReasig_Node
!  -----------------------------------------------------------------------------
subroutine DerTempReasig_Elm  (m, n, id_t, Vo_e, Vn_e, Va_e, Vp_e)
!  -----------------------------------------------------------------------------
  implicit none
  real(rp), parameter        :: Vtm = -3.0 !.Modelo de Ten Tusscher
  integer(ip), intent(in)    :: m, n !.nodos interiores, elementos
  real(rp),    intent(in)    :: id_t
  real(rp),    intent(inout) :: Vo_e(:,:), Vn_e(:,:), Va_e(:,:), Vp_e(:,:)


  Vp_e(1:m,1:n) = id_t*(Vn_e(1:m,1:n) - Va_e(1:m,1:n)); !.Devivada temporal
  !.
  !where (Vp_e(:,:) < Vtm)
  !  Vo_e(:,:) = 0.3*Vn_e(:,:) + 0.7*Vo_e(:,:)  !.Inyeccion de corriente axial
  !elsewhere
    Vo_e(:,:) = Vn_e(:,:)  
  !end where
  !.
  Va_e(1:m,1:n) = Vo_e(1:m,1:n)                         !.Reasig. para el prox. dt
  return
end subroutine DerTempReasig_Elm
!  -----------------------------------------------------------------------------



end module mod_USER_monodomain
!  -----------------------------------------------------------------------------
