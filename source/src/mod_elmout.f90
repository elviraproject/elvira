!  -----------------------------------------------------------------------------
module mod_elmout
!  -----------------------------------------------------------------------------
!   Esta modulo lee una lista de elementos en los que se quiere ver la evolucion  
! de las variables del problema, y cada cuantos pasos de tiempo los valores en
! los elementos van a ser guardados.
!  -----------------------------------------------------------------------------
  use mod_precision
  use mod_filename
  use mod_error
!  -----------------------------------------------------------------------------
  implicit none
!  -----------------------------------------------------------------------------

!  -----------------------------------------------------------------------------
!  ne_p  : numero de elementos para postproceso
!. n_int : cada cuantos int. se guardan las variables
!. l_elm : lista de elementos
!  -----------------------------------------------------------------------------
  type, public :: t_elmout
    private
    integer(ip)             :: ne_p = 0
    integer(ip)             :: n_int = 0
    integer(ip)             :: flag = 0
    integer(ip),allocatable :: l_elm(:)
  end type t_elmout
  !.
contains
! ------------------------------------------------------------------------------
subroutine lec_elem_out(lu_m, arch_d, strout, elm_out)
! ------------------------------------------------------------------------------
! Esta rutina lee los nodos para postproceso
! ------------------------------------------------------------------------------
  implicit none
  integer(ip), intent(in)      :: lu_m
  character (len=*), intent(in):: arch_d, strout
  type(t_elmout)               :: elm_out
  !.
  integer(ip)                  :: lu_lec, io_err, i_err


  lu_lec = lu_m; call unitnumber (arch_d, strout, lu_lec, io_err)
  if (io_err > 0) call w_error (47,'subroutine lec_elem_out',1)
  !.
  read (lu_lec,*,iostat=io_err) elm_out%ne_p, elm_out%n_int, elm_out%flag
  if (io_err > 0) call w_error (3,'elm_out%nn_p, elm_out%n_int, elm_out%flag',1)
  !
  allocate (elm_out%l_elm(elm_out%ne_p), stat=i_err)
  if (i_err > 0 ) call w_error (4,'elm_out%l_nd(elm_out%nn_p)',1)
  !. 
  read (lu_lec,*,iostat=io_err) elm_out%l_elm(1:elm_out%ne_p) 
  if (io_err > 0) call w_error (3,'elm_out%l_nd(1:elm_out%nn_p)',1)
  !.
  if (lu_lec /= lu_m) call close_unit (lu_lec)
  write(*,10) elm_out%ne_p; write(lu_log,10) elm_out%ne_p 
  if(elm_out%flag.eq.1) then
    write(*,*) '### writing all state variables' 
    write(lu_log,*) '### writing all state variables' 
  else
    write(lu_log,*) '### writing only the action potential' 
  endif
  !.
  return
10 format ('###.Elements for postprocessing, leidos                 : ',I8)
end subroutine lec_elem_out
! ------------------------------------------------------------------------------
subroutine bin_read_elem_out (lu_m, elm_out)
! ------------------------------------------------------------------------------
  implicit none
  integer(ip), intent(in)      :: lu_m
  type(t_elmout)               :: elm_out
  !.
  character (len=11)           :: str
  integer(ip)                  :: io_err, i_err

  !.
  read (lu_m, iostat=io_err) str,elm_out%ne_p, elm_out%n_int, elm_out%flag
  if (io_err > 0) call w_error (3,'elm_out%nn_p, elm_out%n_int, elm_out%flag',1)
  !.
  if (elm_out%ne_p > 0) then
    allocate (elm_out%l_elm(elm_out%ne_p), stat=i_err)
    if (i_err > 0 ) call w_error (4,'elm_out%l_nd(elm_out%nn_p)',1)
    !. 
    read (lu_m, iostat=io_err) elm_out%l_elm(1:elm_out%ne_p) 
    if (io_err > 0) call w_error (3,'elm_out%l_nd(1:elm_out%nn_p)',1)
  end if
  return
end subroutine bin_read_elem_out
! ------------------------------------------------------------------------------
subroutine bin_write_elem_out (lu_m,le_d, elm_out)
! ------------------------------------------------------------------------------
  implicit none
  integer(ip), intent(in)      :: lu_m, le_d(:)
  type(t_elmout)               :: elm_out
  !.
  integer(ip)                  :: ce_p, i, iel
  integer(ip)                  :: lst(elm_out%ne_p)

  ce_p=0
  do i=1,elm_out%ne_p
    iel = elm_out%l_elm(i)
    if (le_d(iel) > 0) then
      ce_p = ce_p+1
      lst(ce_p)=le_d(iel)
    end if
  end do
  !.
  write(lu_m) '*ELEMOUTPUT', ce_p, elm_out%n_int, elm_out%flag
  !.
  if (ce_p > 0) write (lu_m) lst(1:ce_p)
  return
end subroutine bin_write_elem_out
! ------------------------------------------------------------------------------
subroutine ascii_write_elem_out (lu_m,le_d, elm_out)
! ------------------------------------------------------------------------------
  implicit none
  integer(ip), intent(in)      :: lu_m, le_d(:)
  type(t_elmout)               :: elm_out
  !.
  integer(ip)                  :: ce_p, i, iel
  integer(ip)                  :: lst(elm_out%ne_p)

  ce_p=0
  do i=1,elm_out%ne_p
    iel = elm_out%l_elm(i)
    if (le_d(iel) > 0) then
      ce_p = ce_p+1
      lst(ce_p)=le_d(iel)
    end if
  end do
  !.
  write(lu_m,10) '*ELEMOUTPUT', ce_p, elm_out%n_int, elm_out%flag
  !.
  if (ce_p > 0) write (lu_m,20) lst(1:ce_p)
  return
10 format(A,3(2X,I6))
20 format(10(:,2X,I8))
end subroutine ascii_write_elem_out
!  -----------------------------------------------------------------------------
function deallocate_elem_out (elm_out) result(flg)
!  -----------------------------------------------------------------------------
  implicit none 
  type(t_elmout)           :: elm_out
  integer(ip)              :: flg
  !.
  integer(ip)              :: istat
  
  flg = 0
  if (allocated(elm_out%l_elm)) then
    deallocate (elm_out%l_elm, stat=istat)
    if (istat /= 0) flg = 1
  else
    flg = -1
  end if
  return
end function deallocate_elem_out
! ------------------------------------------------------------------------------
function get_elmout_nep(elm_out) result(ne_p)
! ------------------------------------------------------------------------------
  implicit none
  type(t_elmout)           :: elm_out
  integer(ip)              :: ne_p


  ne_p = elm_out%ne_p
  return
end function get_elmout_nep
! ------------------------------------------------------------------------------
function get_elmout_nint (elm_out) result(n_int)
! ------------------------------------------------------------------------------
  implicit none
  type(t_elmout)           :: elm_out
  integer(ip)              :: n_int

  n_int = elm_out%n_int
  return
end function get_elmout_nint
! ------------------------------------------------------------------------------
function get_elmout_flag (elm_out) result(flag)
! ------------------------------------------------------------------------------
  implicit none
  type(t_elmout)           :: elm_out
  integer(ip)              :: flag

  flag = elm_out%flag
  return
end function get_elmout_flag
! ------------------------------------------------------------------------------
function get_elm_post( elm_out) result(lst_ep)
! ------------------------------------------------------------------------------
  implicit none
  type(t_elmout)           :: elm_out
  integer(ip)              :: lst_ep(size(elm_out%l_elm))
  integer(ip)              :: n

  n = 0
  if (allocated(elm_out%l_elm)) then
    n = size(elm_out%l_elm)
  end if
  !.
  if (n > 0) then
    lst_ep(1:n) = elm_out%l_elm(1:n)
  end if
  return
end function get_elm_post
! ------------------------------------------------------------------------------

end module mod_elmout
! ------------------------------------------------------------------------------
