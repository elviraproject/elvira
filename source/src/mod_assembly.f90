!  ------------------------------------------------------------------------------
module mod_typematelm
!  ------------------------------------------------------------------------------
  use mod_precision
  implicit none
  type  t_mtr 
    integer(ip)              :: ndof
    integer(ip), allocatable :: gl(:)      !.list of element d.o.f
    real(rp),    allocatable :: ke(:,:)    !.elemental stiffness matrix
    real(rp),    allocatable :: me(:,:)    !.elemental mass matrix
    real(rp),    allocatable :: vf(:)      !.vector of elemental forces
!    integer(ip), pointer :: gl(:)=>NULL()  !.list of element d.o.f   
!    real(rp),    pointer :: ke(:,:)=>NULL()!.elemental stiffness matrix  
!    real(rp),    pointer :: me(:,:)=>NULL()!.elemental mass matrix 
!    real(rp),    pointer :: vf(:)=>NULL()  !.vector of elemental forces
  end type t_mtr

end module mod_typematelm
!  ------------------------------------------------------------------------------
!  ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
!  ------------------------------------------------------------------------------

!  ------------------------------------------------------------------------------
module mod_assembly
!  ------------------------------------------------------------------------------
  use mod_precision 
  use mod_error
  use mod_typematelm
  use mod_problemsetting
  use mpi_mod
!  ------------------------------------------------------------------------------
  implicit none
!  ------------------------------------------------------------------------------
contains
!  ------------------------------------------------------------------------------
subroutine assemblymatrix (user_elem, prblm)
!  ------------------------------------------------------------------------------
!  Ensambla la matriz de rigidez y de masa
!  nin        [in ] : numero de grados de libertad internos
!  user_elem  [in ] : elemento de usuario
!  prblm      [in ] : datos del problema 
!  KgCRS      [out] : matriz de rigidez global
!  ------------------------------------------------------------------------------
  implicit none
  !.
  interface        
    subroutine user_elem (iel, prblm, ke)
      use mod_precision
      use mod_typematelm
      use mod_problemsetting
      implicit none
      integer(ip),   intent(in)   :: iel
      type(t_prblm), intent(in)   :: prblm
      type(t_mtr),  intent(inout) :: ke  
    end subroutine user_elem
  end interface
  !.
  type(t_prblm), intent(inout)  :: prblm
  !.
  type(t_mtr)                   :: ke 
  integer(ip)                   :: i, n_elm, tipo, nlib_e, n_pe, nin, nlib
  logical                       :: flg_ei
  integer(ip)                   :: esq_it
!  integer(ip)                       :: myrank, ierr


  !.call mpi_comm_rank (mpi_comm_world, myrank, ierr)
  tipo  = get_elemtipo (1,prblm%mshdata%elem)      !.tipo de elemento
  call id_elem_tip (tipo, n_pe=n_pe, nlib = nlib)  !.numero de nodos y libertades
  nin = nlib - n_pe                                !.numero de nodos internos
  !.nin = 0 => elemento lineal
  !.nin = 1 => elemento con burbuja
  !.nin > 1 => macro elemento
  !.Esquema de integracion temporal, para ser usado con el macro elemento -------
  flg_ei = .false.; esq_it = get_int_temp  (prblm%stpdata)   
  if ((esq_it == 0).or.(esq_it == 1)) flg_ei = .true.
  !.----------- Dimensiona la matriz de masa ------------------------------------
  if ((nin == 0).or.(flg_ei)) then
    call allocate_slvr_sprs_m_vl_mm (0, prblm%slvrset%sprs_m)
    write (*,'(A)') '###.Global mass matrix dimensioned'
  end if
  !------------------------------------------------------------------------------
  n_elm = get_nelm (prblm%mshdata%elem)  !.numero de elementos
  !  print *,'###.assemblymatrix:',n_elm
  !.
  do i=1, n_elm
    tipo = get_elemtipo(i, prblm%mshdata%elem)         !.Tipo de elemento
    call assembly_idelem (tipo, nlib_e = nlib_e, npe=n_pe)
    !.
    call allocinit_ke(i, nlib_e, n_pe, prblm%mshdata, ke)
    !.
    call user_elem (i, prblm, ke) !-- Llamada al elemento de usuario ----
    !.userelem_monodomain (iel, prblm, Ke)
    !write (myrank+33,10) 'ke%gl',i,ke%gl
    !write (myrank+33,20) 'ke%ke',ke%ke
    if (nin == 0) then
      call assembly_ke(nlib_e, ke%gl, ke%ke, prblm%slvrset%sprs_m)
      call assembly_me(0, nlib_e, ke%gl, ke%me, prblm%slvrset%sprs_m)
    else
      if (flg_ei)  then
        call assembly_me(0, nlib_e, ke%gl, ke%me, prblm%slvrset%sprs_m)
      else
        call assembly_ke(nlib_e, ke%gl, ke%ke, prblm%slvrset%sprs_m)
      end if
    end if
    !.
    !call assembly_fe(nlib_e, ke%gl, ke%vf, prblm%slvrset%sprs_m)
  end do
  !.
  !call writefile_slvrsetsprs_m(myrank+100, prblm%slvrset%sprs_m)
  !.
10 format(A,I6,/,4(:,1X,I6))
20 format(A,/,4(:,1X,G14.6))
  return
end subroutine assemblymatrix
! -------------------------------------------------------------------------------
subroutine allocinit_ke(i, nlib, n_pe, mshdata, ke)
! -------------------------------------------------------------------------------
! Reserva de memoria para la estructura ke e inicializacion de la misma.
! -------------------------------------------------------------------------------
  implicit none
  integer(ip), intent(in)    :: i, nlib, n_pe
  type(t_mesh), intent(in)   :: mshdata
  type(t_mtr)                :: ke  
  !.
  integer(ip), save          :: ndof = 0
  integer(ip)                :: conc(n_pe)
  
  integer(ip)                :: npe, i_err
  
  if (ndof /= nlib) then 
    if (allocated(ke%gl))  deallocate (ke%gl, ke%ke, ke%me, ke%vf, stat=i_err)
    !.
    allocate (ke%gl(nlib), ke%ke(nlib,nlib), ke%me(nlib,nlib),      &
              ke%vf(nlib), stat=i_err)
    if (i_err > 0 ) call w_error (4,'(sub "allocinit_ke") : ke%...',1)
    ke%ndof = nlib
    ke%gl = 0; ke%ke = 0.0; ke%me = 0.0; ke%vf = 0.0
  end if
  !.
  call get_elmconc (i, npe, conc, mshdata%elem)
  !.
  ke%gl = conc(1:npe)

  !call get_dofelem (npe, conc, mshdata%node, ke%gl)
  return
end subroutine allocinit_ke


!!$!  ------------------------------------------------------------------------------
!!$subroutine subassembly_ke (ke, slvrset)
!!$! -------------------------------------------------------------------------------
!!$  implicit none
!!$  type(t_me), intent(in)     :: ke
!!$  type(t_slvr)               :: slvrset
!!$  !.
!!$  
!!$
!!$  integer(ip), intent(in)    :: i, nlib, n_pe
!!$  type(t_mesh), intent(in)   :: mshdata
!!$ 
!!$  !.




!  ------------------------------------------------------------------------------
end module mod_assembly
!  ------------------------------------------------------------------------------

