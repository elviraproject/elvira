  integer(ip),  parameter, private  :: n_e=99  ! Number of error message
  integer(ip)                       :: ind_e(n_e)
  
  !.
    type(t_cerr), private     :: c_err(1:n_e)= (/ &
    t_cerr( 001, ' ###.ERROR, READING IN '), & 
    t_cerr( 002, ' ###.ERROR, OPENING FILE OF NODES '), & 
    t_cerr( 003, ' ###.ERROR, READING, '), &
    t_cerr( 004, ' ###.ERROR, ASSIGNING MEMORY, VARIABLE, '), &
    t_cerr( 005, ' ###.ERROR, READING NODES, SUBROUTINE lec_node, last read node: '), &
    t_cerr( 006, ' ###.ERROR, READING ELEMENTS, SUBROUTINE lec_elem, last read element: '), &
    t_cerr( 007, ' ###.ERROR, OPENING FILE OF ELEMENTS, '), &
    t_cerr( 008, ' ###.ERROR, OPENING FILE '), &
    t_cerr( 009, ' ###.ERROR, READING FILE id_elem.conf, THE ELEMENT TYPE: '), &
    t_cerr( 010, ' ###.ERROR, IN FILE id_elem.conf, THE SPECIFIED ELEMENT TYPE DOES NOT EXISTS: '), &
    t_cerr( 011, ' ###.ERROR,("sub: id_elem_tip") ELEMENT TYPE NOT FOUND: '), &
    t_cerr( 012, ' ###.ERROR,("sub: max_npe") '), &
    t_cerr( 013, ' ###.ERROR,("sub: node_on_el_face") WRONG NUMBER OF NODES PER FACE: '), &
    t_cerr( 014, ' ###.ERROR,("sub: node_on_el_face") DIMENSION NOT DEFINED: '), &
    t_cerr( 015, ' ###.ERROR,("sub: elm_node"), ASSIGNING MEMORY TO node%ar_en(k), k = '), &
    t_cerr( 016, ' ###.ERROR,("sub: read_material"), READING THE NUMBER OF MATERIALS '), &
    t_cerr( 017, ' ###.ERROR,("sub: read_material"), ASSIGNING MEMORY, VARIABLE: '), &
    t_cerr( 018, ' ###.ERROR,("sub: read_material"), READING VARIABLE: '), &
    t_cerr( 019, ' ###.ERROR,("sub: lec_sysref"),READING THE NUMBER OF SYSTEMS OF REFERENCE '), &
    t_cerr( 020, ' ###.ERROR,("sub: lec_sysref"),ASSIGNING MEMORY, VARIABLE: '), &
    t_cerr( 021, ' ###.ERROR,("sub: lec_sysref"),READING VARIABLE: '), &
    t_cerr( 022, ' ###.ERROR,("sub: lec_hist"),  READING THE NUMBER OF HISTORIES:   '), &
    t_cerr( 023, ' ###.ERROR,("sub: lec_hist"),  ASSIGNING MEMORY, VARIABLE: '), &
    t_cerr( 024, ' ###.ERROR,("sub: lec_sysref"),READING VARIABLE: '), &
    t_cerr( 025, ' ###.ERROR,("sub: lec_tabla"), READING THE NUMBER OF TABLES: '), &
    t_cerr( 026, ' ###.ERROR,("sub: lec_tabla"), ASSIGNING MEMORY, VARIABLE: '), &
    t_cerr( 027, ' ###.ERROR,("sub: lec_tabla"), READING VARIABLE: '), &
    t_cerr( 028, ' ###.ERROR,("sub: lec_prelm"), OPENING FILE OF PROP. ELEM, '), &
    t_cerr( 029, ' ###.ERROR,("sub: lec_prelm"), READING THE NUMBER OF PROP. ELEM '), &
    t_cerr( 030, ' ###.ERROR,("sub: lec_prelm"), ASSIGNING MEMORY, VARIABLE: '), &
    t_cerr( 031, ' ###.ERROR,("sub: lec_prelm"), READING VARIABLE: '), &
    t_cerr( 032, ' ###.ERROR,("sub: lec_pnod"),  OPENING FILE PROP. NODES, '), &
    t_cerr( 033, ' ###.ERROR,("sub: lec_pnod"),  READING THE NUMBER OF PROP. NODAL '), &
    t_cerr( 034, ' ###.ERROR,("sub: lec_pnod"),  ASSIGNING MEMORY, VARIABLE: '), &
    t_cerr( 035, ' ###.ERROR,("sub: lec_pnod"),  READING VARIABLE: '), &
    t_cerr( 036, ' ###.ERROR,("sub: lec_grelm"), OPENING FILE GROUP OF ELEM, '), &
    t_cerr( 037, ' ###.ERROR,("sub: lec_grelm"), READING NUMBER OF GROUP OF ELEM, '), &
    t_cerr( 038, ' ###.ERROR,("sub: lec_grelm"), ASSIGNING MEMORY, VARIABLE: '), &
    t_cerr( 039, ' ###.ERROR,("sub: lec_grelm"), READING VARIABLE: '), &
    t_cerr( 040, ' ###.ERROR,("sub: lec_grnod"), OPENING FILE GROUP OF NODES, '), &
    t_cerr( 041, ' ###.ERROR,("sub: lec_grnod"), READING NUMBER OF GROUP OF NODES, '), &
    t_cerr( 042, ' ###.ERROR,("sub: lec_grnod"), ASSIGNING MEMORY, VARIABLE: '), &
    t_cerr( 043, ' ###.ERROR,("sub: lec_grnod"), READING VARIABLE: '), &
    t_cerr( 044, ' ###.ERROR,("sub: allocate_ar_en_je_en"), ASSIG. MEM. (node%ar_en(:)%je_en(:)), NODE:'), &
    t_cerr( 045, ' ###.ERROR,("sub: allocate_ar_nn_je_nn"), ASSIG. MEM. (node%ar_nn(:)%je_nn(:)), NODE:'), &
    t_cerr( 046, ' ###.ERROR,("sub: set_node_subdomain  "), ASSIG. MEM. (node%nd_dm(:)%nd_d(:)),  NODE:'), &
    t_cerr( 047, ' ###.ERROR, OPENING FILE OF NODAL PARAMETERS: '), &
    t_cerr( 048, ' ###.ERROR,("sub: allocate_ar_nn_id_en"), ASSIG. MEM. (node%ar_nn(:)%id_nn(:)), NODE:'), &
    t_cerr( 049, ' ###.ERROR,("sub: lec_prmnd"), ASSIG. MEM., VAR., prmnd%prm_nd(i)%dat_, i:'), &
    t_cerr( 050, ' ###.ERROR,("sub: lec_prmnd"), READING VARIABLE: nro_n,nro_p, Parameter:'), &
    t_cerr( 051, ' ###.ERROR, OPENING FILE OF ELEMENT PARAMETERS:'), &
    t_cerr( 052, ' ###.ERROR,("sub: lec_prmelm"), READING VARIABLE: nro_n,nro_p, Parameter:'), &
    t_cerr( 053, ' ###.ERROR,("sub: lec_prmelm"), ASSIG. MEM., VAR., prmelm%prm_elm(i)%dat_, i:'), &
    t_cerr( 054, ' ###.ERROR, OPENING FILE OF STIMULATED NODES:'), &
    t_cerr( 055, ' ###.ERROR,("sub: lec_stm"),    READING VARIABLE:  nro_n, n_stm, i:'), &
    t_cerr( 056, ' ###.ERROR,("sub: lec_stm"), ASSIG. MEM. VAR. stmls%hstm(i)%data(3*n_stm),history:'), &
    t_cerr( 057, ' ###.ERROR,("sub: lecc_rstart"),    READING VARIABLE:'), &
    t_cerr( 058, ' ###.ERROR,("sub: read_inc"), ASSIG. MEM. VAR. rstrt%data(n), n:'), &
    t_cerr( 059, ' ###.ERROR,("sub: lecc_stm"), READING:'), &
    t_cerr( 060, ' ###.ERROR,("sub: lecc_stm"), READING stmls%ndstm%node(i) position:'), &
    t_cerr( 061, ' ###.ERROR,("sub: lecc_stm"), READING stmls%grpstm%name(i) position:'), &
    t_cerr( 062, ' ###.ERROR,("sub: allocate_optemp")  ASSIG. MEM. VAR.:'), &
    t_cerr( 063, ' ###.ERROR,("sub: Inic_tt_nd")  ASSIG. MEM. VAR.:'), &
    t_cerr( 064, ' ###.ERROR,("sub: DimMod_TT_nd")  ASSIG. MEM. VAR.:'), &
    t_cerr( 065, ' ###.ERROR, FREEING MEMORY SUBROUTINE :'), &
    t_cerr( 066, ' ###.ERROR,("sub: allocatememory") n_nod:'), &
    t_cerr( 067, ' ###.ERROR,("sub: allocatememory") n_elm:'), &
    t_cerr( 068, ' ###.WARNING,("fun: get_ndout_nnp"): DIFFERENCE BETWEEN nn_l /= nn_p:               '), &
    t_cerr( 069, ' ###.ERROR,("sub: fixing_node_dom") ASSIG. MEM. VAR. dom:'), &
    t_cerr( 070, ' ###.ERROR,("sub: Inic_FK_nd")    ASSIG. MEM. VAR.:'), &
    t_cerr( 071, ' ###.ERROR,("sub: DimMod_FK_nd")  ASSIG. MEM. VAR.:'), &
    t_cerr( 072, ' ###.ERROR,("sub: Inic_FK_elm")   ASSIG. MEM. VAR.:'), &
    t_cerr( 073, ' ###.ERROR,("sub: DimMod_FK_elm") ASSIG. MEM. VAR.:'), &
    t_cerr( 074, ' ###.ERROR,("sub: BinRead_ModElec_Nd") READING VARIABLE:'), &
    t_cerr( 075, ' ###.ERROR,("sub: BinRead_ModElec_Nd") THERE IS A DIFFERENCE =>'), &
    t_cerr( 076, ' ###.ERROR,("sub: BinRead_ModElec_Nd") MODEL Ten Tusscher, NODE ='), &
    t_cerr( 077, ' ###.ERROR,("sub: BinRead_ModElec_Nd") MODEL Fenton Karma, NODE ='), &
    t_cerr( 078, ' ###.ERROR,("sub: BinRead_ModElec_Nd") MODEL Nygren,       NODE ='), &
    t_cerr( 079, ' ###.ERROR,("sub: BinRead_ModElec_Elm") READIND VARIABLE:'), &
    t_cerr( 080, ' ###.ERROR,("sub: BinRead_ModElec_Elm") THERE IS A DIFFERENCE =>'), &
    t_cerr( 081, ' ###.ERROR,("sub: BinRead_ModElec_Elm") MODEL Ten Tusscher, ELEMENT ='), &
    t_cerr( 082, ' ###.ERROR,("sub: BinRead_ModElec_Elm") MODEL Fenton Karma, ELEMENT ='), &
    t_cerr( 083, ' ###.ERROR,("sub: BinRead_ModElec_Elm") MODEL Nygren,       ELEMENT ='), &
    t_cerr( 084, ' ###.ERROR,("sub: ReadReStartVar") READING VARIABLE:'), &
    t_cerr( 085, ' ###.ERROR,("sub: ReadReStartVar") THERE IS A DIFFERENCE =>'), &
    t_cerr( 086, ' ###.ERROR,("sub: ReadReStartVar") READING j,vnd%Vo_n(j),vnd%Vn_n(j), NODE ='), &
    t_cerr( 087, ' ###.ERROR,("sub: ReadReStartVar") READING j,velm%Vo_e(:,j),velm%Vn_e(:,j), ELEMENT ='), &
    t_cerr( 088, ' ###.ERROR,("sub: BinRead_ModElec_Nd")  MODEL Luo Rudy 2000, NODE ='), &
    t_cerr( 089, ' ###.ERROR,("sub: BinRead_ModElec_Elm") MODEL Luo Rudy 2000, ELEMENT ='), &
    t_cerr( 090, ' ###.ERROR,("sub: BinRead_ModElec_Nd")  END OF FILE, PROCESS :'), &
    t_cerr( 091, ' ###.ERROR,("sub: BinRead_ModElec_Elm") END OF FILE, PROCESS :'), &
    t_cerr( 092, ' ###.sub:ReadReStartVar => NODAL POTENTIALS READ, NODES:'), &
    t_cerr( 093, ' ###.sub:ReadReStartVar => NODAL ELECTRIC VARIABLES READ, NODES:'), &
    t_cerr( 094, ' ###.sub:ReadReStartVar => ELEMENT POTENTIALS READ, ELEMENTS:'), &
    t_cerr( 095, ' ###.sub:ReadReStartVar => ELEMENT ELECTRIC VARIABLES, ELEMENTS:'), &
    t_cerr( 096, ' ###.ERROR,("sub: BinRead_ModElec_Elm"), INCORRECT RESTART PARAMETER:'), &
    t_cerr( 097, ' ###.ERROR,("sub: fill_psb_parts"), ASSIG. MEM VAR:'), &
    t_cerr( 098, ' ###.ERROR,("sub: psblas_solver"), ASSIG. MEM VAR:'), &
    t_cerr( 099, ' ###.ERROR,("sub: bin_write_pnod"), ASSIG. MEM VAR:') &
    /)

