#!/bin/bash
# $1 number of processes
# $2 input data file with full path
# $3 output identifier with full path of output directory
# $4 name of log file for output when running in interactive mode
# SOFT_PATH: path to main package folder

SOFT_PATH=<Elvira instalation directory>
COMP=<compiler for execution (intel, gcc, ...)>
#source /usr/mpi/intel/openmpi-*/bin/mpivars.sh
#
# RUNNING WITH CONDOR
#
# Uncomment next line if excecuting with openmpi
#mpirun --mca btl self,sm -np $1 $SOFT_PATH/bin/mainelv_openmpi -i $2 -o $3
#
# Uncomment next line if excecuting with mpich2
#mpirun -np $1 $SOFT_PATH/bin/mainelv_openmpi -i $2 -o $3
#
#
# RUNNING IN INTERACTIVE MODE
#
# Uncomment next line if excecuting openmpi
nohup mpirun --mca btl self,sm -np $1 $SOFT_PATH/bin/mainelv_$(COMP)_openmpi -i $2 -o $3 >& $4 &
#
# Uncomment next line if excecuting with mpich2
#nohup mpirun -np $1 $SOFT_PATH/bin/mainelv_openmpi -i $2 -o $3 >& $4 &
#
# RUNNING IN INTERACTIVE MODE OPENMPI ON INFINIBAND
#
# Update <openmpi directory> before using this script. For hermes
# it corresponds to /opt/openmpi-1.6.4/intel for intel
#                   /opt/openmpi-1.6.4/gcc48 for gcc
#
#nohup mpirun --prefix <openmpi directory> --mca btl openib,self,sm -hostfile machines -np $1 $SOFT_PATH/bin/mainelv_openmpi_intel -i $2 -o $3 >& $4 &
#
#
