#!/bin/bash
# $1 number of processes
# $2 input data file with full path
# $3 output identifier with full path of output directory
# $4 name of log file for output
# SOFT_PATH: path to main package folder

SOFT_PATH=<Elvira instalation directory>
COMP=<compiler for execution (intel, gcc, ...)>

#source /usr/mpi/intel/mvapich2-*/bin/mpivars.sh

nohup mpirun_rsh -hostfile machines -n $1 $SOFT_PATH/bin/mainelv_$(COMP)_infiniband -i $2 -o $3 >& $4 &
