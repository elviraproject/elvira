Before installing and running the software you must have installed in your system

- intel fortran compiler (version 12.0), or gnu 4.8 fortran compiler
- a version of mvapich2 or openmpi

a) The software has been tested for infiniband platform (mvapich2_intel-1.7) and 
   openmpi platforms (openmpi_intel-1.4.3)
   infiniband and openmpi.
b) The distribution comes with precompiled external (BLAS, METIS, PSBLAS) libraries for 
   both compilers and both mpi platforms. The folder elvira_libs/ contains these external 
   libraries already compiled with mvapich2_intel-1.7 and openmpi_intel-1.4.3. If you
   plan to use a different mpi fortran compiler you will need to recompile the 
   external libraries
c) You will find two execution scripts usefull for each platform. In case of 
   having mpich2 installed in your computer node (no infiniband), you may have
   to modified the excecution script runelv_openmpi.

1) Before compiling you need to check compiler you have loaded in the system. You
   can make this either by means of the mpi-selector-menu or by loading the specific
   module (module load <module_name>). To check which mpi compiler is using your system
   by default, e.g., intel or gfortran, just type mpif90 --version. In case of 
   compiling with gfortran make sure to have verion 4.8.
   
2) Once you have checked what compilers you are using execute:

   make clean

   - Compiling with Intel + Infiniband MPI (default)

      make COMP=intel MPI=infiniband (or just make)

   - Compiling with Intel + Open MPI

      make COMP=intel MPI=openmpi (or make MPI=openmpi)

   - Compiling with gfortran + Infiniband MPI

      make COMP=gfortran MPI=infiniband (or just make COMP=gcc)

   - Compiling with gfortran + Open MPI

      make COMP=gfortran MPI=openmpi

   For those working with MacOSx, there is a specific version compiled for
   gfortran 4.8 running on OSx system (10.7 or greater). You need to have
   installed the developer tools that incorporate an optimized version of
   BLAS libraries for MacOsx. You also need to have installed mpich2
   To compile this particular version, you need first to open the file 
   make.inc located in the source/ folder and uncomment the line 

   #BLAS= -lblas -llapack

   and comment the line

   BLAS  =  $(LIBM)/blas_LINUX.a

   Once you have done this the execute

   make clean
   make COMP=gccOsx MPI=openmpi

   Important Note for cluster Hermes users: 
   
   Compilation of the code can be carried out in a dedicated node. To access the
   node execute

   ssh mpi

   Once in the node, the command 'module available' give a list of available 
   compilers. Currently, the following compilers are available:

   - mvapich2_gcc48
   - mvapich2_intel
   - openmpi_gcc48
   - openmpi_intel

   Select the corresponding compiler and compile Elvira as described above.

